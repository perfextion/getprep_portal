﻿var counter = 0; //1000 will  run it every 1 second
var TotalTime = 0;
window.onload = setInterval(timer, 1000);
function timer() {
    counter = counter + 1;
    TotalTime = TotalTime + 1;
    var minutes = "";
    var seconds = "";
    if (counter > 59) {
        minutes = parseInt((parseInt(counter) / 60)).toString() + " mm ";
        seconds = parseInt((parseInt(counter) % 60)).toString() + " ss";
    }
    else {
        seconds = counter + " ss"
    }
    var totalminutes = "";
    var totalseconds = "";
    if (TotalTime > 59) {
        totalminutes = parseInt((parseInt(TotalTime) / 60)).toString() + " mm ";
        totalseconds = parseInt((parseInt(TotalTime) % 60)).toString() + " ss";
    }
    else {
        totalseconds = TotalTime + " ss"
    }

    $(".lblTime").text("Time : " + minutes + seconds);
    $(".lblTotalTime").text("Total Time : " + totalminutes + totalseconds);

    //Do code for showing the number of seconds here
}