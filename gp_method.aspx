﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="gp_method.aspx.cs" Inherits="gp_method" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">


 <div id="wrapper-content">
        <!-- PAGE WRAPPER-->
        <div id="page-wrapper">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <!-- CONTENT-->
                <div class="content">
                    <div class="section background-opacity page-title set-height-top">
                        <div class="container">
                            <div class="page-title-wrapper">
                                <!--.page-title-content-->
                                <h2 class="captions">GetPrep Methodology</h2>
                                
                            </div>
                        </div>
                    </div>            
                    <!--INTRO EDUGATE-->
                    <div class="group-title-index">
                        <h4 class="top-title">SomeText</h4>

                        <h2 class="center-title">Our Methodology</h2>

                        <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                    </div>
                    <div class="section intro-edu">
                        <div class="container">
                            <div class="intro-edu-wrapper">
                                <div class="row">
                                    <div class="top-courses-item">
                                        <div class="about-edugate-layout-2">
                                            <div class="about-edugate-layout-2-wrapper">
                                                <div class="about-edugate-content"> 
                                                    <div class="oval">
                                                        <img src="assets/images/what_is_GP.jpg" alt="" class="img-responsive" />
                                                    </div>
                                                    <p>
                                                         With GetPrep, you get the smartest approach to test prep and tutoring. GetPrep recognizes that each individual has a unique and specific learning style. It is 
                                therefore important to first and foremost assess these specifics within every student 
                                and gear lessons, practice sessions, and tutoring accordingly.
                                                    </p> 

                                                    <p>
                                <strong>Clear Goals</strong>: Our smart approach to tutoring relies on competent teachers who work with students 
                                  to identify  particular problem areas of each student, be they in content, study skills, test anxiety or motivational deficits.
                            </p>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="gp-about-intro-content">
                                         
                                           <p>
                                <strong>Personalized Plan for Each Student</strong>:
                                We create a personalized program unique to each student based on the initial assessment and 
                                by incorporating any input from the parents regarding their child's specific needs. This individualized plan 
                                maps out all the skills the student needs to learn to acheive his or her individual goals.
                                
                                <br />
                                
                                 
                                To deliver such personalized teaching to individual students, Getprep Methodology
                                leverages a flexible framework consisting of a combination of conceptual review sessions, 
                                study / practice sessions and 1-on-1 or small group tutoring.  As the student progresses through each of these sessions,
                                we adjust the learning plan as needed, so the student stays challenged, but not frustrated.
                               
                                </p> 
                        <p>
                                <strong>Best Tutors and Instructors</strong>:
                                We hire the best and make them better by providing them with right tools to assess, teach and monitor
                               student's performance.  Most importantly, we recognize that learning and teaching are two
                            different skills.  We require that all of our tutors have teaching experience prior to working 
                            for GetPrep.
                            
                             </p> 
                                
                           
                                
                             <p>    
                                 <strong>Best Curriculum material and Technology</strong>:
                                This is where GetPrep methodology's unique and core strength lies.  Getprep blends
                                 personal instruction with digital tools to allow quick review of core concepts,follow on practice to digest and
                                 apply concepts taught, capture any remaining skill gaps to reinforce the material again.

                                 Our online adaptive practice faciltates periodic and brief assessment quizs to ensure that previously learned concepts
                                 are refreshed and retained. This approach lets our tutors understand each student's 
                                 learning and retaining pattern in a clear manner.

                                
                               
                                </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <hr>
                    </div>
                  
                 
                   

                    </div>
                    </div>
                  
                </div>
            </div>
  

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

