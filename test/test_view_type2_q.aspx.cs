﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;

public partial class test_view_type2_q : System.Web.UI.Page
{
    string str_type2_id = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        string str_type2_id = Request.QueryString.GetValues("type2_id") == null ? "120434" : (Request.QueryString.GetValues("type2_id")[0].ToString());
       
        
        Page.Title = "GetPrep| Sample view of Q:" + str_type2_id ;


        lbl_center.Text = "<br><br>";

       

        o_type2_q item_q = new o_type2_q(str_type2_id);


        lbl_center.Text += item_q.view_type2_q_full();


        lbl_center.Text += " ***** Rendered *****  <br><br> ";

        lbl_center.Text += item_q.rendered_type2_q();


    }



    protected string get_modified_q(o_type2_q q_item_inp)
    {
        String data = "";
        if (q_item_inp.q_type != "MCM")
        {
            data = q_item_inp.q_text;
        }
        string returnvalue = "";
        string Test = "";

        if (q_item_inp.q_type == "MFIB")
        {
            int i = 0;
            while ((i = data.IndexOf('<', i)) != -1)
            {

                var startString = data.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf(">"));
                string inputtxt = "input type='text' style='width:" + s.Split(':')[1] + "%' class='mfib'";
                data = data.Replace(s, inputtxt);
                i++;
            }

        }
        else if (q_item_inp.q_type == "DD")
        {
            int i = 0;
            int k = 0;
            while ((i = data.IndexOf('{', i)) != -1)
            {

                var startString = data.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                string inputtxt = "<select id=" + k + " answer='' onchange='return funchangeddanswer(this);' class='ddl '><option>Select</option>";
                String[] ddldata = (s.Split(':')[1]).Split(',');
                for (int j = 0; j < ddldata.Length; j++)
                {
                    inputtxt = inputtxt + "<option>" + ddldata[j] + "</option>";

                }
                inputtxt = inputtxt + "</select>";
                data = data.Replace("{" + s + "}", inputtxt);
                i++;
                k++;
            }
        }
        else if (q_item_inp.q_type == "MCM")
        {
            string strAnswerType = q_item_inp.answer_choices;
            string chkoption = "";
            if (q_item_inp.ans_1 != "" && q_item_inp.ans_1 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[0].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[0].ToString() + "' >" + q_item_inp.ans_1 + "</label></div> ";

            }
            if (q_item_inp.ans_2 != "" && q_item_inp.ans_2 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[1].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[1].ToString() + "' >" + q_item_inp.ans_2 + "</label></div> ";


            }
            if (q_item_inp.ans_3 != "" && q_item_inp.ans_3 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[2].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[2].ToString() + "' >" + q_item_inp.ans_3 + "</label></div> ";
            }
            if (q_item_inp.ans_4 != "" && q_item_inp.ans_4 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[3].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[3].ToString() + "' >" + q_item_inp.ans_4 + "</label></div> ";
            }
            if (q_item_inp.ans_5 != "" && q_item_inp.ans_5 != "-")
            {

                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[4].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[4].ToString() + "' >" + q_item_inp.ans_5 + "</label></div> ";
            }
            data = chkoption;
        }

        return data;
    }

}

