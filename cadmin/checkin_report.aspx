﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cadmin/C_AdminMaster.master" AutoEventWireup="true" CodeFile="checkin_report.aspx.cs" Inherits="cadmin_checkin_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="Server">
    <div class="section section-padding courses-detail">
        <div class="container">
            <div class="courses-detail-wrapper col-12">
                <div class="row">
                    <div class="col-md-12 layout-left">
                        <div class="course-syllabus">
                            <div class="course-syllabus-title underline">Checkin Report</div>
                            <div class="course-table">
                                <div class="outer-container">
                                    <div class="inner-container">
                                        <div class="table-header">
                                            <table class="edu-table-responsive">
                                                <thead>
                                                    <tr class="heading-table">
                                                        <th class="col-4">Profile Pic</th>
                                                        <th class="col-2">Name</th>
                                                        <th class="col-3">Checkin time</th>

                                                    </tr>
                                                </thead>
                                                <asp:Label ID="lblCheckin" runat="server"></asp:Label>
                                            </table>
                                        </div>

                                        <br />
                                        <br />
                                        <div class="course-syllabus-title underline">CheckOut Report</div>

                                        <div class="table-header">
                                            <table class="edu-table-responsive">
                                                <thead>
                                                    <tr class="heading-table">
                                                        <th class="col-4">Profile Pic</th>
                                                        <th class="col-2">Name</th>
                                                        <th class="col-3">Checkin time</th>
                                                        <th class="col-4">Checkout Time </th>
                                                    </tr>
                                                </thead>
                                                <asp:Label ID="lblCheckout" runat="server"></asp:Label>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

