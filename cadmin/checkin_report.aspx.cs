﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cadmin_checkin_report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep Center| Checkin Report";
        if (!Page.IsPostBack)
        {
            GetDetails();
        }
    }

    private void GetDetails()
    {


        string str_sql = "select Convert(varchar(20),a.checkindate,109) checkindate,Convert(varchar(20),a.checkoutdate,109) checkoutdate,b.f_name,b.l_name,b.profilepic_path from student_attendance as a join students as b on a.st_id=b.st_id  where cast(checkindate as date) =cast(getdate() as date) "; 
      
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

       

        foreach (DataRow dtr1 in dt1.Rows)
        {
            string profilepic = "<img src='/assets/images/no_image_thumb.gif' alt='No Pic' width='200px' height='200px'/>";
            if (!string.IsNullOrEmpty(dtr1["profilepic_path"].ToString()))
            {
                profilepic = "<img src='" + dtr1["profilepic_path"].ToString() + "' alt='No Pic' width='200px' height='200px'/>";
            }
            if (!string.IsNullOrEmpty(dtr1["checkoutdate"].ToString()))
            {
                lblCheckin.Text += "<tr class='table-row'>";
                lblCheckin.Text += "<td> " + profilepic + "</td>";
                lblCheckin.Text += "<td>  " + dtr1["f_name"].ToString() + dtr1["l_name"].ToString() + "</td >";
                lblCheckin.Text += "<td> " + dtr1["checkindate"].ToString() + " </td >";
                lblCheckin.Text += "</tr >";
            }
            else
            {
                lblCheckout.Text += "<tr class='table-row'>";
                lblCheckout.Text += "<td> " + profilepic + " </td>";
                lblCheckout.Text += "<td>  " + dtr1["f_name"].ToString() + dtr1["l_name"].ToString() + "</td >";
                lblCheckout.Text += "<td>  " + dtr1["checkindate"].ToString() + "</td >";
                lblCheckout.Text += "<td> " + dtr1["checkoutdate"].ToString() + " </td >";
                lblCheckout.Text += "</tr >";
            }
        }
 
    }
}