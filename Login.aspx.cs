﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Security.Policy;

public partial class Login : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void login_Click(object sender, EventArgs e)
    {
        string login = "select st_id,plan_join_dt,username,password,isadmin,f_name,l_name,LastLoginDate,isnull(convert(varchar(11),signup_date,106),'')signup_date from students where username = '" + txtemail.Text + "' and password = '" + txtPassword.Text + "'";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(login);

            if (dt.Rows.Count > 0)
            {

                string id = dt.Rows[0]["st_id"].ToString();
                string plan_join_dt = dt.Rows[0]["plan_join_dt"].ToString();
                string username = dt.Rows[0]["username"].ToString();
                Session["userid"] = id;
                Session["username"] = username;
                Session["StudentName"] = dt.Rows[0]["f_name"].ToString() + " " + dt.Rows[0]["l_name"].ToString();
                Session["SignUp_date"] = dt.Rows[0]["signup_date"].ToString();
                if (dt.Rows[0]["LastLoginDate"] == null)
                {
                    sendMail();

                }
                gen_db_utils.gp_sql_execute("update students set LastLoginDate=getdate() where st_id=" + id);
                if (dt.Rows[0]["isadmin"].ToString() == "True")
                {
                    //admin
                    Session["roleid"] = 2;
                    Response.Redirect("/Portal/Dashboard.aspx");
                }

                if (plan_join_dt == "1/1/2000 12:00:00 AM")
                {

                    Session["roleid"] = 0;
                    // send mail

                    Response.Redirect("/Portal/Welcome.aspx");
                }
                else
                {

                    Session["roleid"] = 1;
                    Response.Redirect("/Portal/Dashboard.aspx");
                }
            }
            else
            {
                lblErrormsg.Text = "Invalid Username or Password";
                lblErrormsg.Style.Add("color", "red");
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Login.aspx/login_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, login, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    protected void Register_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Registration.aspx");
    }
    public void sendMail()
    {
        string str_login = "select f_name,l_name from students where username = '" + txtemail.Text + "'";
        DataTable data = gen_db_utils.gp_sql_get_datatable(str_login);
        string firstName = data.Rows[0]["f_name"].ToString();
        string lastName = data.Rows[0]["l_name"].ToString();
        EmailUtility.SendMail(txtemail.Text, "", GetMailSubject(), GetMailBody(firstName, lastName));

    }
    private string GetMailSubject()
    {
        string subject = "Welcome to GetPrep Academy";

        return subject;
    }
    private string GetMailBody(string firstName, string lastName)
    {
        string body = "Dear" + " " + firstName + " " + lastName + ", <br><br><br>"
                      + " Welcome to the GetPrep academy.<br><br> "
                      + "Please complete your profile by choosing courses, plans and so on."
                      + "<br><br>"

                     + " Thank you for being a valued GetPrep Member. <br><br>"

                       + " GetPrep Customer Care ";

        return body;
    }

    public void changePassword()
    {
        string Token = "";
        Token = (Guid.NewGuid()).ToString();
        string strupd = "update students set PasswordToken='" + Token + "',PasswordExpirationDate=dateadd(hh,24,getdate()) where username='" + txtUserName.Text + "';select e_mail from students where username='" + txtUserName.Text + "'";
        try
        {

            string GetBaseUrl = Request.Url.OriginalString.Replace(Request.Url.PathAndQuery, "") + Request.ApplicationPath;
            string Link = GetBaseUrl + "Changepassword.aspx?Token=" + Token;
            gen_db_utils.gp_sql_execute(strupd);
            EmailUtility.SendMail(txtUserName.Text, "", GetSignUpMailSubject(), GetSignUpMailBody(Link));

        }
        catch (Exception Ex)
        {
            LogError.Log("Login.aspx/changePassword", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strupd, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    protected void ChangePassword_Click(object sender, EventArgs e)
    {
        try
        {

            changePassword();
            divUsername.Visible = false;
            btnChangePassword.Visible = false;
            lblMessage.Visible = true;
            lblMessage.Text = "Your Password change request link has been sent to your registered email.";
            lblMessage.Style.Add("color", "green");


        }
        catch (Exception Ex)
        {
            LogError.Log("Login.aspx/ChangePassword_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }


    private string GetSignUpMailSubject()
    {
        string subject = "GetPrep Forgot password link";

        return subject;
    }
    private string GetSignUpMailBody(string Link)
    {
        string body = "Dear User, <br><br><br>"
                      + " Below is the link to change the password of your account on GetPrep.<br><br> "
                     + Link + "<br><br>"

                     + " Thank you for being a valued GetPrep Member. <br><br>"

                       + " GetPrep Customer Care ";

        return body;
    }


}



