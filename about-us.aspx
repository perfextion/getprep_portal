﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="about-us.aspx.cs" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

    <div id="wrapper-content">
        <!-- PAGE WRAPPER-->
        <div id="page-wrapper">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <!-- CONTENT-->
                <div class="content">

                    <div class="section background-opacity page-title set-height-top">
                        <div class="container">
                            <div class="page-title-wrapper">
                                <!--.page-title-content-->
                                <h2 class="captions">about us</h2>
                                <ol class="breadcrumb">
                                    <li><a href="index.html">Home</a></li>
                                    <li class="active"><a href="#">About</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>

                    <!--INTRO EDUGATE-->
                    <div class="group-title-index">
                        <h4 class="top-title">SomeText</h4>

                        <h2 class="center-title">WHAT IS <b>GetPrep</b> ?</h2>

                        <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                    </div>
                    <div class="section intro-edu">
                        <div class="container">
                            <div class="intro-edu-wrapper">
                                <div class="row">
                                    <div class="top-courses-item">
                                        <div class="about-edugate-layout-2">
                                            <div class="about-edugate-layout-2-wrapper">
                                                <div class="about-edugate-content"> 
                                                    <div class="oval">
                                                        <img src="assets/images/what_is_GP.jpg" alt="" class="img-responsive" />
                                                    </div>
                                                    <p>
                                                        GetPrep is a premier tutoring and test prep organization based in the SW Austin area. 
                                         Our mission is very simple — 
                                        we provide the best educational experience for our students by providing them with top-level teachers and mentors.
                                        The key component of our success is our exclusive screening, training, and qualification process in which 
                                        GetPrep is dedicated to recruiting and selecting only highly qualified and proven teachers to 
                                        meet our students’ academic and learning needs.
                                                    </p>



                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="gp-about-intro-content">
                                         
                                            <p>
                                                During their tutoring sessions, students learn to commit and own their learning experience 
                                       by utilizing creative study strategies and working with rigorous content. GetPrep
                                       provides the highest quality of individualized and small group classroom instruction 
                                        which promote intellectual and personal development in a positive and fun learning environment. 
                                        We encourage our students to adopt positive study habits, become independent learners and thinkers,
                                         and communicate openly and frequently with their teachers.
                                            </p>
                                            <p>
                                                For a FREE Consultation, please call (512) 501-3690. 
                                        Contact us and we can discuss how we can develop a plan to help your child achieve success.
                                            </p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                    </div>
                </div>


            </div>
        </div>
     
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

