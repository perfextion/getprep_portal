﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="Changepassword.aspx.cs" Inherits="Default2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <style>
        .center {
            text-align: center;
        }

        .margin {
            margin-left: 30%;
            min-height: 400px;
        }
    </style>
    <div class="container"> 

        <div class="page-login ">
            <div class="login-table rlp-table" style="min-height:500px">
                <%--<div class="login-title rlp-title center" runat="server" id="divtit">Change Password!</div>--%>
                 <div class="col-sm-12 center" style="margin-top:10%;font-size:18px">
            <asp:Label ID="lblMsg" runat="server" > </asp:Label>
             <b><asp:Label ID="lblErrormsg" runat="server" > </asp:Label></b>
        </div>
                <div class="login-form bg-w-form rlp-form  " runat="server" id="divBody">
                    <div class="row margin">
                        <div class="col-sm-7  ">
                            <div class="panel panel-default" style="background-color: #eee">


                                <div class="panel-body">

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <label>UserName</label>
                                            <asp:TextBox ID="txtUserName" placeholder="Enter Your UserName" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName" ErrorMessage=" UserName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>



                                        <div class="col-sm-12">
                                            <label for="Password">Password</label>
                                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Enter Password " CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password field is required."></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>

                                        </div>


                                        <div class="col-sm-12">

                                            <label>ConfirmPassword</label>
                                            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" placeholder="Enter Confirm Password " CssClass="form-control"></asp:TextBox>
                                            <asp:CompareValidator runat="server" ID="cmpConfirmPassword" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password and ConfirmPassword must be match !" />
                                            <div class="help-block"></div>
                                        </div>




                                        <div class="col-sm-12 ">

                                            <div class="login-submit">

                                                <asp:Button ID="btnChngPassword" runat="server" Text="Change Password" CssClass="btn btn-login btn-green pull-right" OnClick="ChngPassword_Click" />
                                            </div>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

