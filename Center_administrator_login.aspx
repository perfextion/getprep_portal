﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Center_administrator_login.aspx.cs" Inherits="Center_Checkin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Center Checkin</title>
    <link type="text/css" rel="stylesheet" href="assets/css/color-1.css" />
    <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-3.3.5/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900,300">
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome-4.4.0/css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-svg/css/Glyphter.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- HEADER-->
    <form id="form1" runat="server"> 
        <!-- WRAPPER-->
        <div id="wrapper-content">
            <!-- PAGE WRAPPER-->
            <div id="page-wrapper">
                <!-- MAIN CONTENT-->
                <div class="main-content">
                    <!-- CONTENT-->
                    <div class="content">
                        <div class="page-login ">
                            <div class="container">
                                <div class="login-wrapper rlp-wrapper">
                                    <div class="login-table rlp-table">
                                        <a href="index.html">
                                            <img src="assets/images/logo-color-1.png" alt="" class="login"></a>

                                        <div class="login-title rlp-title">Administrator Login!</div>
                                        <div class="login-form bg-w-form rlp-form" style="margin-left:30%">
                                             <asp:Label ID="lblLoginStatus" runat="server" style="color:red;"></asp:Label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="regemail" class="control-label form-label">administrator username <span class="highlight">*</span></label>
                                                    <asp:TextBox ID="txtLoginemail" placeholder="Enter Your EmailAddress" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqLoginemail" ValidationGroup="loginvalidation" runat="server"
                                                        ControlToValidate="txtLoginemail" ErrorMessage=" Email is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <div class="help-block"></div>
                                                </div> </div>
                                                 <div class="row">
                                                <div class="col-md-6">
                                                    <label for="regpassword" class="control-label form-label">password <span class="highlight">*</span></label>

                                                    <asp:TextBox ID="txtLoginPassword" runat="server" TextMode="Password" placeholder="Enter Password " CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfloginpassword" ValidationGroup="loginvalidation" runat="server" ControlToValidate="txtLoginPassword" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password field is required."></asp:RequiredFieldValidator>
                                                    <div class="help-block"></div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        <div class="login-submit">
                                            <asp:Button ID="login" runat="server" OnClick="login_Click" ValidationGroup="loginvalidation" CssClass="btn btn-login btn-green" Text="Log in" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="assets/libs/bootstrap-3.3.5/js/bootstrap.min.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/owl-carousel-2.0/owl.carousel.min.js"></script>
        <script src="assets/libs/appear/jquery.appear.js"></script>
        <script src="assets/libs/count-to/jquery.countTo.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING SCRIPTS FOR PAGE-->
    </form>
</body>
</html>
