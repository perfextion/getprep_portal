﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" runat="Server">
    <title>Austin's Best In-Person High School Subject Tutoring Help</title>
    <meta name="keywords" content="middle school, high school subjects, tutoring, ACT prep, SAT prep, college admissions, counseling" />
    <meta name="description" content="GetPrep Academy offers the best middle school, high school subjects, tutoring, ACT prep, SAT prep, college admissions, college counseling Help" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <!-- SLIDER BANNER-->
    <div class="section slider-banner set-height-top">
        <div class="slider-item">
            <div class="slider-1">
                <div class="slider-caption">
                    <div class="container">
                        <h5 class="text-info-2" style="color: #834750;">In-person High Quality Tutoring</h5>
                        <h1 class="text-info-1" style="color: #834750;">Math & Science Subjects</h1>
                        <p class="text-info-3" style="color: #834750;">for Middle and High School Students</p>
                        <button class="btn btn-green" onclick="return Register(this);" id="" ><span>Start learning now!</span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-item">
            <div class="slider-2">
                <div class="slider-caption">
                    <div class="container">
                        <h5 class="text-info-2" style="color: #FFFFFF;">Homework help, Review Classes to understand and practice difficult concepts </h5>

                        <h1 class="text-info-1" style="color: #FFFFFF;">Boost your GPA</h1>

                        <p class="text-info-3" style="color: #FFFFFF;">Affordabe and Flexible Monthly Plans</p>
                        <button class="btn btn-green" onclick="return Register(this);" id="StartLearning_1"  ><span>Start learning now!</span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-item">
            <div class="slider-3">
                <div class="slider-caption">
                    <div class="container">
                        <h5 class="text-info-2" style="color: #FFFFFF;">Ace the SAT Subject Tests and AP Exams </h5>

                        <h1 class="text-info-1" style="color: #FFFFFF;">With Continuous Preparation </h1>

                        <p class="text-info-3" style="color: #FFFFFF;">in Sync with School Work</p>
                        <button class="btn btn-green" onclick="return Register(this);" id="StartLearning_2" ><span>Start learning now!</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CHOOSE COURSES-->

    <!-- PROGRESS BARS
                <div class="section progress-bars section-padding">
                    <div class="container">
                        <div class="progress-bars-content">
                            <div class="progress-bar-wrapper"><h2 class="title-2">Some important facts about us</h2>

                                <div class="row">
                                    <div class="content">
                                        <div class="col-sm-3">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="45" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">teachers</p></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="56" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">courses</p></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="165" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">members</p></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="progress-bar-number">
                                                <div data-from="0" data-to="15" data-speed="1000" class="num">0</div>
                                                <p class="name-inner">Countries</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="group-button">
                                    <button onclick="window.location.href='categories.html'" class="btn btn-transition-3"><span>Purchase theme</span></button>
                                    <button onclick="window.location.href='about-us.html'" class="btn btn-green-3"><span>start Learn now</span></button>
                                </div>
                                <div class="group-btn-slider">
                                    <div class="btn-prev"><i class="fa fa-angle-left"></i></div>
                                    <div class="btn-next"><i class="fa fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    -->
    <!-- TOP COURSES-->
    <div class="section section-padding top-courses">
        <div class="container">
            <div class="group-title-index">
                <h4 class="top-title">CHOOSE FROM OUR LARGE SELCTION OF COURSES</h4>

                <h2 class="center-title">Top Subjects offered by GetPrep</h2>

                <div class="bottom-title"><i class="bottom-icon icon-a-1-01-01"></i></div>

            </div>

            <div class="clearfix"></div>
            <div class="top-courses-wrapper">
                <div class="top-courses-slider">
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">Algebra </a>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-1.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        Algebra is designed to give students a foundation for all future mathematics courses. Topics include: foundations of Algebra, solving
equations, solving inequalities, an introduction to functions, linear functions, systems of equations and
                                        <%--inequalities, exponents and exponential functions, polynomials and factoring, quadratic functions and
equations, radical expressions and equations, and data analysis and probability.--%>
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_1"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">AP Chemistry</a>


                                    <div class="oval">
                                        <img src="assets/images/courses/courses-2.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        Pre-AP Chemistry is a first year chemistry course designed to meet the needs of the student who plans on continuing on in AP Chemistry
     or eventually taking a college chemistry class. 
                                    </p>


                                    <button onclick="return Register(this);" id="LearnNow_2"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">AP Physics</a>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-3.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        AP Physics 1 is an algebra-based, introductory college-level physics course. During this course students explore various topics:
kinematics; dynamics; circular motion and gravitation; energy; momentum; simple harmonic motion; torque and rotational motion;
electric charge and electric force; DC circuits; and mechanical waves and sound.
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_3"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-courses-slider">
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">AP Calculus</a>

                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-4.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        While Calculus is offered as an advanced course, an increasing number of high school students are taking a year of Calculus 
    prior to joining colleges. This AP course covers topics related to limits, derivatives, definite integrals, 
    and the Fundamental Theorem of Calculus.
                                    </p>

                                    <button onclick="return Register(this);" id="LearnNow_4"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">AP Statistics</a>

                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-5.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        The AP Statistics course is an introductory, non-calculus-based college course in statistics. The
course introduces students to the major concepts and tools for
collecting, analyzing, and drawing conclusions from data. Topics covered include - exploring data, sampling
and experimentation, anticipating patterns, and statistical inference.
                                    </p>

                                    <button onclick="return Register(this);" id="LearnNow_5"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">AP Computer Science A</a>

                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-6.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        Computer Science A emphasizes object-oriented programming methodology with an emphasis on problem solving and algorithm development 
    and is meant to be the equivalent of a first-semester course in computer science.  Topics covered include design strategies 
    and methodologies, organization of data (data structures), approaches to processing data (algorithms), 
    analysis of potential solutions, and the ethical and social implications of computing.
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_6"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-courses-slider">
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">PSAT Preparation</a>

                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-7.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        PSAT can be the most important test a high school student will ever take.
The PSAT names the highest achieving students in the nation to qualify as National Merit Semifinalists (who can go on to become Finalists).
This title and honor can, at many colleges, result in larger amounts of scholarship than any amount of scholarship received from the SAT
     itself.
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_7"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">ACT Preparation</a>

                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-9.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        ACT is an entrance exam used by most colleges and universities to make admissions decisions. 
  The ACT has four sections: English, Reading, Math and Science, as well as an optional 40-minute writing test. 
    Some schools may require the writing test. 
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_8"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="top-courses-item">
                        <div class="edugate-layout-2">
                            <div class="edugate-layout-2-wrapper">
                                <div class="edugate-content">
                                    <a href="courses-detail.html" class="title">SAT Preparation</a>


                                    <div class="description"></div>
                                    <div class="clearfix"></div>
                                    <div class="oval">
                                        <img src="assets/images/courses/courses-8.jpg" alt="" class="img-responsive" />
                                    </div>
                                    <p>
                                        Every year, roughly two million US students take the SAT. Most of these students are college-bound, and virtually every college in the US accepts the SAT as a form of entrance exam.
    Many of those colleges actually require the SAT for admission. The College Board made content, format, and scoring changes to the SAT in 2016. 
    The redesigned SAT test prioritizes content that reflects the kind of reading and math students will encounter in college and 
    their future work lives. 
                                    </p>
                                    <button onclick="return Register(this);" id="LearnNow_9"  class="btn btn-green"><span>learn now</span></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="group-btn-top-courses-slider">
                    <div class="btn-prev">&lsaquo;</div>
                    <div class="btn-next">&rsaquo;</div>
                </div>
            </div>
        </div>
    </div>
    <!-- WHY CHOOSE US-->
    <div class="section why-choose-us">
        <div class="why-choose-us-wrapper-top">
            <div class="container">
                <div class="why-choose-us-wrapper">
                    <h2 class="title-2">Why GetPrep?</h2>

                    <p>
                        GetPrep is an academic tutoring center providing a comprehensive range of supplementary educational services for middle school 
                                    and high school students. We serve students who are struggling to succeed in their coursework as well as those who 
                                    are ranked at the very top of their class and enrolled in the most challenging courses. 
                                    Because our students have various tutoring goals, we offer flexible and affordable tutoring packages with a mix 
                                    of homework help sessions, subject review sessions and one-on-one tutoring.
                    </p>
                    <button onclick="return Register(this);" id="Enroll_1"  class="btn btn-green-2"><span>Enroll now</span></button>
                </div>
                <div data-wow-delay="0.2s" data-wow-duration="1.2s" class="background-girl-1 wow fadeInUp">
                    <img src="assets/images/why_school.jpg" alt="" class="img-responsive" />
                </div>
            </div>
        </div>
        <div class="why-choose-us-wrapper-bottom background-opacity">
            <div class="container">
                <div data-wow-delay="0.4s" data-wow-duration="1s" class="row why-choose-us-wrapper wow zoomIn">
                    <div class="customs-row">
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-pencil-square-o"></i>

                            <p>Over 15 courses in Math & Science</p>
                        </div>
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-compress"></i>

                            <p>Refund if not Satisfied</p>
                        </div>
                    </div>
                    <div class="customs-row">
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-paper-plane"></i>

                            <p>Flexible learning packages with concepts review and follow on practice</p>
                        </div>
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-folder-open"></i>

                            <p>Flexible and Affordable monthly plans</p>
                        </div>
                    </div>
                    <div class="customs-row">
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-external-link"></i>

                            <p>Subject Review in sync with school progress </p>
                        </div>
                        <div class="col-sm-4 col-xs-6 section-icon">
                            <i class="fa fa-stumbleupon-circle"></i>

                            <p>Flexible payment methods</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PRICING-->
    <div class="section section-padding pricing">
        <div class="container">
            <div class="group-title-index">
                <h4 class="top-title">Choose Your Pricing Plan</h4>

                <h2 class="center-title">Flexible Monthly Payment Plans</h2>

                <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
            </div>
            <div class="row">
                <div class="wrapper">
                    <div class="col-md-12">
                        <div class="well">
                            <div id="myCarousel" class="carousel slide">

                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default" href="#myCarousel" data-slide="prev">
                                                        <i class="fa fa-chevron-left" aria-hidden="true"></i> Flex Plans</button></span>
                                                <span class="col-lg-6 col-md-6 col-sm-6 text-center plan col-xs-12">
                                                    <h3>Value Plans</h3>
                                                </span>
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default pull-right" href="#myCarousel" data-slide="next">
                                                        Lite Plans <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="text-new col-lg-12">
                                                    <p>
                                                        Value plans are perfectly suited for students taking advanced classes and require help in understading concepts and practice solving problems regularly.  
								Depending on the need students can choose a plan that offers help for one, two or three subjects. 			
                                                    </p>
                                                    <p>
                                                        Each plan offers well designed sequence of Concept Review sessions,	Study Sessions to practice with guidance from tutors / instructirs and one-on-tutoring time with expert tutors to 
								assess and coach the students to do well in that subject. Prices and included tutoring time are listed below in detail.					 
                                                    </p>
                                                    <br />
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-lg-4 col-md-4 col-xs-12">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <p data-from="0" data-to="300" data-speed="1000" class="inner-number">1</p>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">Silver</h3>

                                                        <p class="pricing-subtitle">$300 per month </p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>One Subject </strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>16 hours</strong> - Study Sessions</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>8 hours</strong> - Lecture / Review Sessions </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>1 hour</strong> - 1 to 1 Tutoring</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Total Contact hours:</strong> 25</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Average cost per hour:</strong> $12 </p>
                                                            </li>

                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <p data-from="0" data-to="500" data-speed="1000" class="inner-number">1</p>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">Gold</h3>

                                                        <p class="pricing-subtitle">$500 per month </p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Two Subjects </strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>32 hours</strong> - Study Sessions</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>16 hours</strong> - Lecture / Review Sessions </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>2 hour</strong> - 1 to 1 Tutoring</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Total Contact hours:</strong> 50</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Average cost per hour:</strong> $10 </p>
                                                            </li>

                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-4">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <p data-from="0" data-to="600" data-speed="1000" class="inner-number">3</p>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">Platinum</h3>

                                                        <p class="pricing-subtitle">$600 per month </p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Three  Subjects </strong></p>
                                                            </li>
                                                            <li>
                                                                <p><strong>48 hours</strong> - Study Sessions</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>24 hours</strong> - Lecture / Review Sessions </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>3 hour</strong> - 1 to 1 Tutoring</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Total Contact hours:</strong> 75</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Average cost per hour:</strong> $8 </p>
                                                            </li>

                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="col-lg-12">
                                                <p>
                                                    <br />
                                                    All monthly plans offer Study sessions, Review sessions, and One-on-One tutoring at GetPrep's Learning Center in Bee Cave, TX.
                                                </p>
                                            </div>

                                        </div>
                                        <!--/row-->
                                    </div>
                                    <!--/item-->
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default" href="#myCarousel" data-slide="prev">
                                                        <i class="fa fa-chevron-left" aria-hidden="true"></i>Value Plans</button></span>
                                                <span class="col-lg-6 col-md-6 col-sm-6 text-center plan col-xs-12">
                                                    <h3>Lite Plans</h3>
                                                </span>
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default pull-right" href="#myCarousel" data-slide="next">
                                                        Flex Plans<i class="fa fa-chevron-right" aria-hidden="true"> </i>
                                                    </button>
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="text-new col-lg-12">
                                                    <p>
                                                        Lite plans are designed to meet the needs of students who are doing reasonably well in a subject
                    and require occasional help and review of material before quizzes and tests. Students enrolling in Lite plans are 
           generally understanding the material covered in school and are looking for periodic review and practice with advanced problems.

							
                                                    </p>
                                                    <p>
                                                        Lite plans offer weekly Concept Review sessions and introduce students to AP format questions such that they are 
           gradually prepared to do well by the time AP exam time comes. 
							 
                                                    </p>
                                                    <br />
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="pricing-widget">
                                                        <div class="pricing-header">
                                                            <div class="price-cost">
                                                                <div class="inner">
                                                                    <p data-from="0" data-to="200" data-speed="1000" class="inner-number">1</p>
                                                                </div>
                                                                <br />
                                                            </div>
                                                        </div>
                                                        <div class="pricing-content">
                                                            <h3 class="pricing-title">Silver Lite</h3>

                                                            <p class="pricing-subtitle">$200 per month </p>
                                                            <ul class="pricing-list">
                                                                <li>
                                                                    <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>One Subject </strong></p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>8 hours</strong> - Lecture / Review Sessions </p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>1 hour</strong> - 1 to 1 Tutoring</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Total Contact hours:</strong> 9</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Average cost per hour:</strong> $22 </p>
                                                                </li>

                                                            </ul>
                                                            <div class="pricing-button"><a href="#">choose plan</a></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="pricing-widget">
                                                        <div class="pricing-header">
                                                            <div class="price-cost">
                                                                <div class="inner">
                                                                    <p data-from="0" data-to="300" data-speed="1000" class="inner-number">1</p>
                                                                </div>
                                                                <br />
                                                            </div>
                                                        </div>
                                                        <div class="pricing-content">
                                                            <h3 class="pricing-title">Gold Lite</h3>

                                                            <p class="pricing-subtitle">$300 per month </p>
                                                            <ul class="pricing-list">
                                                                <li>
                                                                    <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Two Subjects </strong></p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>16 hours</strong> - Lecture / Review Sessions </p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>2 hour</strong> - 1 to 1 Tutoring</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Total Contact hours:</strong> 18</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Average cost per hour:</strong> $16 </p>
                                                                </li>

                                                            </ul>
                                                            <div class="pricing-button"><a href="#">choose plan</a></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="pricing-widget">
                                                        <div class="pricing-header">
                                                            <div class="price-cost">
                                                                <div class="inner">
                                                                    <p data-from="0" data-to="400" data-speed="1000" class="inner-number">1</p>
                                                                </div>
                                                                <br />
                                                            </div>
                                                        </div>
                                                        <div class="pricing-content">
                                                            <h3 class="pricing-title">Platinum Lite</h3>

                                                            <p class="pricing-subtitle">$400 per month </p>
                                                            <ul class="pricing-list">
                                                                <li>
                                                                    <p><strong>~ ~</strong>  includes  <strong>~ ~</strong></p>
                                                                </li>

                                                                <li>
                                                                    <p><strong>Three Subjects </strong></p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>24 hours</strong> - Lecture / Review Sessions </p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>3 hours</strong> - 1 to 1 Tutoring</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Total Contact hours:</strong> 27</p>
                                                                </li>
                                                                <li>
                                                                    <p><strong>Average cost per hour:</strong> $15 </p>
                                                                </li>

                                                            </ul>
                                                            <div class="pricing-button"><a href="#">choose plan</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <div class="col-lg-12">
                                                    <br />
                                                    <p>
                                                        All Monthly Lite plans offer Review sessions, and One-on-One 
                tutoring at GetPrep's Learning Center in Bee Cave, TX. Additional homework help or study sessions can be added to the plan
                as needed at an hourly rate of $25.
                                                    </p>
                                                </div>

                                            </div>
                                            <!--/row-->
                                        </div>
                                    </div>
                                    <!--/item-->
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default" href="#myCarousel" data-slide="prev">
                                                        <i class="fa fa-chevron-left" aria-hidden="true"></i> Lite Plans</button></span>
                                                <span class="col-lg-6 col-md-6 col-sm-6 text-center plan col-xs-12">
                                                    <h3>Flex Plans</h3>
                                                </span>
                                                <span class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                    <button class="btn btn-default pull-right" href="#myCarousel" data-slide="next">
                                                        Value Plans <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                           
                                          
                                            <div class="text-new col-lg-12">
                                                 
                                                <p>
                                                    Flex Plans are designed to meet the needs of students who are doing reasonably well in a subject
                    and require occasional help and review of material before quizzes and tests. Students enrolling in Lite plans are 
           generally understanding the material covered in school and are looking for periodic review and practice with advanced problems.

							
                                                </p>
                                                <p>
                                                    Flex plans offer weekly Concept Review sessions and introduce students to AP format questions such that they are 
           gradually prepared to do well by the time AP exam time comes. 
							 
                                                </p>
                                                <br />
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <h3>Flex</h3>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">AP / IB Classes</h3>
                                                        <strong>~ </strong>

                                                        <p class="pricing-subtitle">Pay as You Go</p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  Hourly Rates  <strong>~ ~</strong></p>
                                                            </li>

                                                            <li>
                                                                <p><strong>One-on-One Tutoring</strong> - $70 per hour </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Homework / Study Session</strong> - $30 per hour</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Review Session:</strong> - $50 per hour</p>
                                                            </li>


                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <h3>Flex</h3>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">High School</h3>
                                                        <strong>(non-AP)</strong>


                                                        <p class="pricing-subtitle">Pay as You Go</p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  Hourly Rates  <strong>~ ~</strong></p>
                                                            </li>

                                                            <li>
                                                                <p><strong>One-on-One Tutoring</strong> - $65 per hour </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Homework / Study Session</strong> - $28 per hour</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Review Session:</strong> - $45 per hour</p>
                                                            </li>


                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="pricing-widget">
                                                    <div class="pricing-header">
                                                        <div class="price-cost">
                                                            <div class="inner">
                                                                <h3>Flex</h3>
                                                            </div>
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="pricing-content">
                                                        <h3 class="pricing-title">Middle School</h3>
                                                        <strong>~ </strong>


                                                        <p class="pricing-subtitle">Pay as You Go</p>
                                                        <ul class="pricing-list">
                                                            <li>
                                                                <p><strong>~ ~</strong>  Hourly Rates  <strong>~ ~</strong></p>
                                                            </li>

                                                            <li>
                                                                <p><strong>One-on-One Tutoring</strong> - $55 per hour </p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Homework / Study Session</strong> - $25 per hour</p>
                                                            </li>
                                                            <li>
                                                                <p><strong>Review Session:</strong> - $40 per hour</p>
                                                            </li>


                                                        </ul>
                                                        <div class="pricing-button"><a href="#">choose plan</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="col-lg-12">
                                                <br />
                                                <p>
                                                    All Flex plans offer Review sessions, Study Sessions, and One-on-One 
                tutoring at GetPrep's Learning Center in Bee Cave, TX. Students are required to pay a one -time registration fee of $49 
                and keep a deposit of $200 in order to schedule classes in advance.
                                                </p>
                                            </div>

                                        </div>
                                        <!--/row-->
                                    </div>
                                    <!--/item-->                                 
                                </div>
                                  <!--/carousel-inner-->
                                    <div class="bu">
                                        <a class="slid-left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>

                                        <a class="slid-right right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                <!--/myCarousel-->
                            </div>
                            <!--/well-->
                        </div>

                    </div>

                </div>


            </div>

            <!-- SLIDER LOGO-->
            <div class="section slider-logo">
                <div class="container">
                    <div class="slider-logo-wrapper">
                        <div class="slider-logo-content">
                            <div class="carousel-logos owl-carousel">
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-1.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-2.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-3.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-4.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-5.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-6.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-7.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-8.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-9.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-10.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-11.png" alt="" class="img-responsive" /></a>
                                </div>
                                <div class="logo-iteam item">
                                    <a href="#">
                                        <img src="assets/images/logo/logo-carousel-12.png" alt="" class="img-responsive" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

