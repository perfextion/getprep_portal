﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsrMenuBar.ascx.cs" Inherits="UserControl_UsrMenuBar" %>
<div class="header-main homepage-01">
    <div class="container">
        <div class="header-main-wrapper">
            <div class="navbar-heade">
                <div class="logo pull-left">
                    <a href="Home.aspx" class="header-logo">
                        <img src="assets/images/logo-color-1.png" alt="" /></a>
                </div>
                <button type="button" data-toggle="collapse" data-target=".navigation" class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <nav class="navigation collapse navbar-collapse pull-right">
                <ul class="nav-links nav navbar-nav">
                    <li><a href="Home.aspx" class="main-menu">Home</a>

                    </li>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle main-menu" data-toggle="dropdown">Programs<span class="fa fa-angle-down icons-dropdown"></span></a>

                        <ul class="dropdown-menu edugate-dropdown-menu-1">

                            <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle link-page" data-toggle="dropdown">Subject Prep</a>
                                <ul class="dropdown-menu ">
                                    <li><a href="#" class="link-page">Math</a></li>
                                    <li><a href="#" class="link-page">Sciences</a></li>
                                    <li><a href="#" class="link-page">Statistics</a></li>
                                </ul>
                            </li>
                            <li><a href="courses-detail.html" class="link-page">SAT / ACT Prep</a></li>
                            <li><a href="events.html" class="link-page">PSAT</a></li>


                        </ul>

                    </li>

                    <li class="dropdown"><a href="javascript:void(0)" class="main-menu">About<span class="fa fa-angle-down icons-dropdown"></span></a>
                        <ul class="dropdown-menu edugate-dropdown-menu-1">
                            <li><a href="about-us.aspx" class="link-page">Why GetPrep</a></li>
                            <li><a href="gp_method.aspx" class="link-page">GP Method</a></li>
                            <li><a href="gp_services.aspx" class="link-page">Services</a></li>
                            <li><a href="faq.aspx" class="link-page">FAQs</a></li>
                        </ul>
                    </li>
                    <li><a href="gp_contact.aspx" class="main-menu">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
