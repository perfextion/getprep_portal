﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class about_us : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DateTime dtNow_begin = DateTime.Now;

        DateTime dtNow_end = DateTime.Now;
        TimeSpan ts = dtNow_end - dtNow_begin;
        int ts_ms = ts.Milliseconds;
        int ts_s = ts.Seconds;

        int ts_total = ts_s * 1000 + ts_ms;

        string dummy = gen_utils.page_stats(ts_total.ToString());
    }
}