﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="Server">
    <script>

        function signup() {

            if (VerifyAndSave()) {

                $.ajax({
                    type: "Post",
                    url: '/Portal/WebServices/GetPrep.asmx/SignUp',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        EmailAddress: $('#ContentBody_txtemail').val(),
                        CreatePassword: $("#ContentBody_txtpassword").val(),
                        FirstName: $("#ContentBody_txtFirstName").val(),
                        LastName: $("#ContentBody_txtLastName").val(),
                        ZipCode: $("#ContentBody_txtZipcode").val(),
                        ddlabout: $("#ContentBody_ddlAboutus").val(),
                        ddldescribes: $("#ContentBody_ddldescribes").val(),
                        ddlmaingoal: $("#ContentBody_ddlmaingoal").val()

                    }),
                    success: function (data) {
                        if (data.d != "") {
                            if (data.d == "0") {
                                alert("User already exists.")
                            } else {
                                $("#studentId").val(data.d);
                                $("#registration").hide();
                                $("#summary").show();
                                $("#modalTitle").html("Sign Up Confirmation");
                            }
                        }
                        else {
                            alert("There is some error while processing.Please try again!")
                        }
                    }


                })
            }

            return false;

        }

        function VerifyAndSave() {

            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate("SignUpValidationGroup");
            }
            return Page_IsValid;

        }



        function Register(button) {

            $("#ContentBody_studentId").val("");
            $("#ContentBody_txtemail").val("");
            $("#ContentBody_txtpassword").val("");
            $("#ContentBody_txtFirstName").val("");
            $("#ContentBody_txtLastName").val("");
            $("#ContentBody_txtZipcode").val("");


            $("#registration").show();
            $("#summary").hide();

            CaptureRegisterButtonEvent($(button).attr('id'), location.pathname);
            return false;
        }
    </script>
    <style>
        .center {
            text-align: center;
            margin-left: 20%;
            margin-right: 20%;
            width: 900px;
        }
    </style>

    <div class="container">
        <div class="login-table rlp-table">

            <div id="modalTitle" class="registration-title rlp-title">Create a Getprep Account!</div>
            <div class="registration-form bg-w-form rlp-form center">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel panel-default " style="background-color: #eee">
                            <div class="panel-body ">

                                <div id="registration">
                                   
                                    <div class=" row">
                                        <div class="col-sm-12">
                                            <label for="Email" style="color: black"><b>Email Address:</b></label>
                                            <asp:TextBox ID="txtemail" TextMode="Email" placeholder="Enter Your EmailAddress" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtemail" ValidationGroup="SignUpValidationGroup"
                                                ErrorMessage=" Email is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="password" style="color: black"><b>Choose a Password:</b></label>

                                            <asp:TextBox ID="txtpassword" TextMode="Password" placeholder="Enter Your Password" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpassword" ValidationGroup="SignUpValidationGroup" ErrorMessage=" Password is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-sm-6">
                                            <label for="FirstName" style="color: black"><b>Student's First Name:</b></label>

                                            <asp:TextBox ID="txtFirstName" placeholder="Enter Your FirstName" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="SignUpValidationGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>


                                        <div class="col-sm-6">
                                            <label for="LastName" style="color: black"><b>Student's LastName:</b></label>
                                            <asp:TextBox ID="txtLastName" placeholder="Enter Your LastName" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="SignUpValidationGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="Zipcode" style="color: black"><b>Zip code:</b></label>

                                            <asp:TextBox ID="txtZipcode" placeholder="Enter Your Zipcode" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="SignUpValidationGroup" ControlToValidate="txtZipcode" ErrorMessage=" Zipcode is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlAboutus" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="" Text="How did you hear about us?">  </asp:ListItem>
                                                <asp:ListItem>Google </asp:ListItem>
                                                <asp:ListItem>Post Card</asp:ListItem>
                                                <asp:ListItem>Offline</asp:ListItem>
                                                <asp:ListItem>Friend</asp:ListItem>
                                                <asp:ListItem>Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SignUpValidationGroup" runat="server" ControlToValidate="ddlAboutus" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddldescribes" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="" Text="Currently enrolled in:">  </asp:ListItem>
                                                <asp:ListItem>Elementary School </asp:ListItem>
                                                <asp:ListItem>Middle School</asp:ListItem>

                                                <asp:ListItem>High School</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="SignUpValidationGroup" ControlToValidate="ddldescribes" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                            <div class="help-block"></div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlmaingoal" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="" Text="What your main goal?">  </asp:ListItem>
                                                <asp:ListItem>Improve Grades</asp:ListItem>
                                                <asp:ListItem>Homework Help</asp:ListItem>
                                                <asp:ListItem>Learn Advanced Topics</asp:ListItem>
                                                <asp:ListItem>Prepare for AP Exam</asp:ListItem>
                                                <asp:ListItem>Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SignUpValidationGroup" runat="server" ControlToValidate="ddlmaingoal" ErrorMessage="Required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>
                                    <div>

                                        <asp:Button ID="btnSubmit" ValidationGroup="SignUpValidationGroup" Text="SignUp" CssClass="col-sm-12 btn btn-green" OnClientClick="return signup();" runat="server" />
                                    </div>

                                </div>
                                <div id="summary" style="display: none">
                                    <h4 style="color: blue; text-align: center">Thank you for registering with us!
                                            <br />
                                        <br />
                                        You have qualified for an one hour of free tutoring session.</h4>
                                    <div class="panel panel-info" style="cursor: pointer">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <h5 style="text-align: center">Please schedule your free tutoring session by calling us at (512) 501-3690</h5>

                                                    <h5 style="text-align: center">or </h5>
                                                    <h5 style="text-align: center">visiting us at our center located at:  </h5>

                                                    <div class="detail">
                                                        <h5 style="text-align: center">11612 Bee Cave Rd., Building 1, Suite 100
                                                                <br />
                                                            <br />
                                                            Austin, TX 78738
                                                        </h5>

                                                        <br />
                                                        <br />

                                                        <h5>** This offer expires if session is not scheduled in the next 10 business days.
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearBoth"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
                </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

