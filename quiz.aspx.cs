﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_Default : System.Web.UI.Page
{


    public o_type2_q q_item;


    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_title.Text = Session["cqz_quiz_title"].ToString();

        
        Page.Title = "GetPrep| Quiz";
      //  Session["userid"] = "1";
        if (!IsPostBack)
        {
            insert_quiz_summary();
            GetQuery();
            GetQuestion();

        }
    }

    private void GetQuery()
    {

        //string catid = Request.QueryString["courseid"].ToString();
        //string str_query = "select type2_id from  type2_qs where cc0 = " + catid + " and len(correct_ans) > 0 "
        //            + " and type2_id not in (select q_seq_id from st_answer where student_id = " + Session["userid"].ToString() + " ) "
        //            + " order by newid() ";

        //if (Session["quiz_start"] != null)
        //{
        //    str_query = Session["quiz_start"].ToString();
        //} 


        string str_qs = Session["cqz_quiz_q_string"].ToString();
        JObject parameters = JObject.FromObject(new
        {
            quiz_qs = str_qs,
        });
        Session["type2_parameters"] = parameters;
        Session["cqz_token"] = Session["cqz_quiz_code"].ToString();

    }

    public void GetQuestion()
    {
        JObject json = (JObject)Session["type2_parameters"];
        string str_qs = json["quiz_qs"].ToString();
        ArrayList ar_qs = fc_gen_utils.string2arraylist(str_qs);
        string i_type2_id = ar_qs[0].ToString();
        q_item = new o_type2_q(i_type2_id);

        lblQuestionType.Text = q_item.q_type; // It store q_type 
        lblQuestionType.Attributes.Add("qtype", q_item.q_type);
        lblQuestionType.Attributes.Add("selectedans", "");

        if (lblQuestionCount.Text == "")
        {

            lblQuestionCount.Text = "1/" + ar_qs.Count.ToString();
            lblTotalQuestions.Text = lblQuestionCount.Text.Split('/')[1].ToString();
        }
        pnlSolution.Visible = false;
        pnlFIB.Style.Add("display", "block");
        pnlMC.Style.Add("display", "block");
        
        CreateQuestion(q_item);
    }

    public void CreateQuestion(o_type2_q q_item)
    {
        try
        {
            lbl_debug.Text = "";
            if (Session["debug_on"].ToString() == "1")
            {
                lbl_debug.Text += "Creating question " + q_item.type2_id + "<br>";
                lbl_debug.Text += "Correct Ans " + q_item.correct_ans + " | " + q_item.fib_ans + "<br>";
            }
            lblSolution.Text = q_item.solution;

            lblMainQuestion.Text = q_item.q_text;
            hdnQuestionID.Value = q_item.type2_id.ToString();
            
            pnlFIB.Style.Add("display","none");

            pnlMC.Style.Add("display", "none");
            if (q_item.q_type == "MC")
            {
                pnlFIB.Style.Add("display", "none");
                pnlMC.Style.Add("display", "block");
                hdnCorrectAnswer.Value = q_item.correct_ans.ToString();
                lblAnswer.Text = "Correct Answer is " + q_item.correct_ans.ToString();
                string strAnswerType = q_item.answer_choices;

                if (strAnswerType == "")
                {
                    strAnswerType = "ABCD";
                }
                  
				
		     	rdans1.Visible = false;
				rdans2.Visible = false;
				rdans3.Visible = false;
				rdans4.Visible = false;
				rdans5.Visible = false;
				lblAnsOpt1.Visible = false;
				lblAnsOpt2.Visible = false;
				lblAnsOpt3.Visible = false;
				lblAnsOpt4.Visible = false;
				lblAnsOpt5.Visible = false;
				

              
                if (q_item.ans_1 != "" && q_item.ans_1 != "-")
                {
                    rdans1.Visible = true;
                    rdans1.Text = "&nbsp;&nbsp;" + q_item.ans_1;
                    lblAnsOpt1.Text = strAnswerType[0].ToString();
                }
                if (q_item.ans_2 != "" && q_item.ans_2 != "-")
                {
                    rdans2.Visible = true;
                    rdans2.Text = "&nbsp;&nbsp;" + q_item.ans_2;
                    lblAnsOpt2.Text = strAnswerType[1].ToString();
                }
                if (q_item.ans_3 != "" && q_item.ans_3 != "-")
                {
                    rdans3.Visible = true;
                    rdans3.Text = "&nbsp;&nbsp;" + q_item.ans_3;
                    lblAnsOpt3.Text = strAnswerType[2].ToString();
                }
                if (q_item.ans_4 != "" && q_item.ans_4 != "-")
                {
                    rdans4.Visible = true;
                    rdans4.Text = "&nbsp;&nbsp;" + q_item.ans_4;
                    lblAnsOpt4.Text = strAnswerType[3].ToString();
                }
                if (q_item.ans_5 != "" && q_item.ans_5 != "-")
                {
                    rdans5.Visible = true;
                    rdans5.Text = "&nbsp;&nbsp;" + q_item.ans_5;
                    lblAnsOpt5.Text = strAnswerType[4].ToString();
                }
            }
            else if (q_item.q_type == "FIB")
            {
                hdnCorrectAnswer.Value = q_item.fib_ans.ToString();
                lblAnswer.Text = "Correct Answer is " + q_item.fib_pre + " " + q_item.fib_ans + q_item.fib_post;
                pnlFIB.Style.Add("display", "block");
                pnlMC.Style.Add("display", "none");
            }
            else if (lblQuestionType.Text == "MFIB" || lblQuestionType.Text == "DD"|| lblQuestionType.Text == "MCM")
            {
                if (lblQuestionType.Text == "MFIB")
                {
                    hdnCorrectAnswer.Value = q_item.fib_ans.ToString();
                }
                else {
                    hdnCorrectAnswer.Value = q_item.correct_ans.ToString();
                
                }
                lblAnswer.Text = "Correct Answer is " + hdnCorrectAnswer.Value.ToString().Replace("|", ",");
                if (lblQuestionType.Text == "MCM")
                {
                    lblMainQuestion.Text = lblMainQuestion.Text + "<br>" + q_item.rendered_type2_q();
                }
                else {
                    lblMainQuestion.Text = q_item.rendered_type2_q();
                }

                pnlFIB.Style.Add("display", "none");
                pnlMC.Style.Add("display", "none");
            }
        }
        catch (Exception ex)
        {
            lblStatus.Text = ex.ToString();
        }
    }
     

    protected string get_selected_answer()
    {
        string str_return = "-";
        if (lblQuestionType.Text == "FIB")
        {
            str_return = txtAnswer.Text;
            if (Session["debug_on"].ToString() == "1") lbl_debug.Text += "str_return = " + str_return + "<br>";

           
        }
        else if (lblQuestionType.Text == "MC")
        {
            if (rdans1.Checked) str_return = lblAnsOpt1.Text;
            if (rdans2.Checked) str_return = lblAnsOpt2.Text;
            if (rdans3.Checked) str_return = lblAnsOpt3.Text;
            if (rdans4.Checked) str_return = lblAnsOpt4.Text;
            if (rdans5.Checked) str_return = lblAnsOpt5.Text;
        }
        else if (lblQuestionType.Text == "MFIB" || lblQuestionType.Text == "DD" ||lblQuestionType.Text == "MCM")
        {
            str_return = txtAnswer.Text;
        }
       
        return str_return;
    }

    protected string is_ans_correct()
    {
        string str_return = "-";

        string correct_ans = hdnCorrectAnswer.Value.ToString();

        if (lblQuestionType.Text == "MC")
        {
            string sel_ans = get_selected_answer();

            if (sel_ans.ToLower() == correct_ans.ToLower())
            {
                str_return = "1";
            }
            else
            {
                str_return = "0";
            }
        }
        else if (lblQuestionType.Text == "MCM")
        {
            string sel_ans = get_selected_answer();

            lbl_debug.Text += sel_ans + "|| " + correct_ans;

            if (sel_ans.ToLower() == correct_ans.ToLower())
            {
                str_return = "1";
            }
            else
            {
                str_return = "0";
            }
        }
        else if (lblQuestionType.Text == "FIB")
        {
            string your_ans = txtAnswer.Text.Trim();
            if (Session["debug_on"].ToString() == "1")
            {
                lbl_debug.Text += "Before Comparison Entered --- FIB Correct:" + correct_ans + " yours: " + your_ans;
                lbl_debug.Text += "Before Comparison toLower --- FIB Correct:" + correct_ans.ToLower() + " yours: " + your_ans.ToLower();
            }

            if (your_ans.ToLower() == correct_ans.ToLower())
            {
                str_return = "1";
            }
            else
            {
                str_return = "0";
            }


        }
        else if (lblQuestionType.Text == "MFIB")
        {
            string your_ans = txtAnswer.Text.Trim();

            if (Session["debug_on"].ToString() == "1") lbl_debug.Text += "MFIB Correct:" + hdnCorrectAnswer.Value + " yours: " + your_ans;

            if (your_ans.ToLower() == correct_ans.ToLower())
            {
                str_return = "1";
            }
            else
            {
                str_return = "0";
            }


        }


        return str_return;
    }

    protected void chkReviewer_CheckedChanged(object sender, EventArgs e)
    {
        if (chkReviewer.Checked)
        {
            btnConfirm.Visible = true;
            btnNext.Visible = false;
            this.Panel1.DefaultButton = this.btnConfirm.UniqueID;
        }
        else
        {
            btnConfirm.Visible = false;
            btnNext.Visible = true;
            this.Panel1.DefaultButton = this.btnNext.UniqueID;
        }
    }

    private void resetanswer()
    {
        rdans1.Style.Remove("color");
        lblAnsOpt1.Style.Remove("color");

        rdans2.Style.Remove("color");
        lblAnsOpt2.Style.Remove("color");

        rdans3.Style.Remove("color");
        lblAnsOpt3.Style.Remove("color");

        rdans4.Style.Remove("color");
        lblAnsOpt4.Style.Remove("color");

        rdans5.Style.Remove("color");
        lblAnsOpt5.Style.Remove("color");
    }
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
         
        
        resetanswer();

        try
        {
            // string correctanswer = gen_db_utils.gp_sql_scalar("select correct_ans from type2_qs where type2_id=" + Convert.ToInt32(hdnQuestionID.Value));
            btnConfirm.Visible = false;
            btnNext.Visible = true;
            this.Panel1.DefaultButton = this.btnNext.UniqueID;
            pnlSolution.Visible = true;
            pnlFIB.Style.Add("display", "none");
            string correctanswer = hdnCorrectAnswer.Value.ToString();
            string selected_answer = get_selected_answer();

            //if(lblQuestionType.Text == "MCM")
            //{
            //    pnlMC.Visible = false;
            //}
            //else {
            //    pnlMC.Visible = true;
            //}
           
            if (lblQuestionType.Text == "MFIB" || lblQuestionType.Text == "DD"|| lblQuestionType.Text == "MCM") {
                
                lblQuestionType.Attributes.Add("selectedans", selected_answer);
            }
            string is_correct = is_ans_correct();


            SaveAnswers();
            

             
            lblStatus.Text = "Answer " + hdnQuestionID.Value + " Saved in Database";
            lblSolution.Visible = true;
            if (is_correct == "1")
            {
                if (lblQuestionType.Text == "MFIB"|| lblQuestionType.Text == "MCM")
                {
                    selected_answer = selected_answer.Replace("|",",");
                    
                }
                lblCorrectAns.Text = " Your Answer " + selected_answer + " is correct.";
                lblCorrectAns.Style.Add("color", "green");
                lblAnswer.Visible = false;
            }
            else
            {
                if (lblQuestionType.Text == "MFIB" || lblQuestionType.Text == "MCM")
                {
                    selected_answer = selected_answer.Replace("|", ",");

                }
                lblCorrectAns.Text = " Your Answer " + selected_answer + " is wrong.";
                lblCorrectAns.Style.Add("color", "red");
                lblAnswer.Style.Add("color", "green");
                lblAnswer.Visible = true;
            }

            if (rdans1.Checked)
            {
                if (correctanswer == "A")
                {
                    //lblans1.Text = "<i class='glyphicon glyphicon-ok'></i>";
                    //lblans1.Style.Add("color", "green");
                    rdans1.Style.Add("color", "green");
                    lblAnsOpt1.Style.Add("color", "green");

                }
                else
                {
                    rdans1.Style.Add("color", "Red");
                    lblAnsOpt1.Style.Add("color", "Red");
                    //lblans1.Text = "X";
                    //lblans1.Style.Add("color", "red");
                    //rdans1.Style.Add("color", "Red");
                }
            }
            if (rdans2.Checked)
            {
                if (correctanswer == "B")
                {
                    //lblans2.Text = "<i class='glyphicon glyphicon-ok'></i>";
                    //lblans2.Style.Add("color", "green");
                    rdans2.Style.Add("color", "green");
                    lblAnsOpt2.Style.Add("color", "green");
                }
                else
                {
                    rdans2.Style.Add("color", "Red");
                    lblAnsOpt2.Style.Add("color", "Red");
                    //lblans2.Text = "X";
                    //lblans2.Style.Add("color", "red");
                }
            }
            if (rdans3.Checked)
            {
                if (correctanswer == "C")
                {
                    rdans3.Style.Add("color", "green");
                    lblAnsOpt3.Style.Add("color", "green");
                    //lblans3.Text = "<i class='glyphicon glyphicon-ok'></i>";
                    //lblans3.Style.Add("color", "green");
                }
                else
                {
                    rdans3.Style.Add("color", "Red");
                    lblAnsOpt3.Style.Add("color", "Red");
                    //lblans3.Text = "X";
                    //lblans3.Style.Add("color", "red");
                }
            }
            if (rdans4.Checked)
            {
                if (correctanswer == "D")
                {
                    rdans4.Style.Add("color", "green");
                    lblAnsOpt4.Style.Add("color", "green");
                    //lblans4.Text = "<i class='glyphicon glyphicon-ok'></i>";
                    //lblans4.Style.Add("color", "green");
                }
                else
                {
                    rdans4.Style.Add("color", "Red");
                    lblAnsOpt4.Style.Add("color", "Red");
                    //lblans4.Text = "X";
                    //lblans4.Style.Add("color", "red");
                }
            }
            if (rdans5.Checked)
            {

                if (correctanswer == "E")
                {
                    rdans5.Style.Add("color", "green");
                    lblAnsOpt5.Style.Add("color", "green");
                    //lblans5.Text = "<i class='glyphicon glyphicon-ok'></i>";
                    //lblans5.Style.Add("color", "green");
                }
                else
                {
                    rdans5.Style.Add("color", "Red");
                    lblAnsOpt5.Style.Add("color", "Red");
                    //lblans5.Text = "X";
                    //lblans5.Style.Add("color", "red");
                }
            }
        }
        catch (Exception ex)
        {
            lblStatus.Text = ex.ToString();
        }
         ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "funrefreshmathqst('c')", true);
     
    }

    protected void btnDone_Click(object sender, EventArgs e)
    {
        Response.Redirect("type2_summary.aspx");
    }

    private void SaveAnswers()
    {
        string correctanswer = hdnCorrectAnswer.Value.ToString();
        string selected_answer = get_selected_answer();
        string is_correct = is_ans_correct();
        if (is_correct == "1")
        {
            lblCorrectAnswers.Text = (Convert.ToInt16(lblCorrectAnswers.Text) + 1).ToString();
        }
        
        string str_sql = "insert into st_answer (student_id,q_src, q_seq_id, date_answered, correct_answer,response,q_type,correct,time_taken,comments,quiz_token) Values("
             + Session["userid"] + "," // student_id
             + "'type_2'," // q_src
             + Convert.ToInt32(hdnQuestionID.Value) + "," // q_seq_id
             + "GETDATE()," // date_answered
             + "'" + correctanswer + "'," // correct_ans
             + "'" + selected_answer + "'," // response
             + "'" + "1" + "'," // q_type
             + "'" + is_correct + "'," // correct
             + "'" + hdnTime.Value + "'," // time_taken
             + "'" + "-" + "'," // comments
             + "'" + Session["cqz_token"].ToString() + "'" // quiz_token
             + ")";
        gen_db_utils.gp_sql_execute(str_sql);
    }

    protected void NextQuestion()
    {
        try
        {
            if (!chkReviewer.Checked)
            {

                SaveAnswers();
           

            }
                
            JObject json = (JObject)Session["type2_parameters"];
            string str_qs = json["quiz_qs"].ToString();
            ArrayList ar_qs = fc_gen_utils.string2arraylist(str_qs);
            ar_qs.RemoveAt(0);
            string str_qs_new = fc_gen_utils.arraylist2string(ar_qs);
            if (str_qs_new != "")
            {
                var quiz_qs = json.Property("quiz_qs");
                var value = (string)str_qs_new;
                quiz_qs.Value = value.ToLower();
                Session["type2_parameters"] = json;
                GetQuestion();

                rdans1.Checked = false;
                rdans2.Checked = false;
                rdans3.Checked = false;
                rdans4.Checked = false;
                rdans5.Checked = false;
                resetanswer();
                if (!chkReviewer.Checked) {
                }
            }
            else
            {
                Response.Redirect("type2_summary.aspx");
            }
        }
        catch (Exception ex)
        {
            lblStatus.Text = ex.ToString();
        }
    }
    protected void btnSkip_Click(object sender, EventArgs e)
    {
        btnConfirm.Visible = false;
        btnNext.Visible = true;
        chkReviewer.Checked = true;
        this.Panel1.DefaultButton = this.btnNext.UniqueID;
        lblStatus.Text = "Answer " + hdnQuestionID.Value + " Saved in Database";
        NextQuestion();
        lblQuestionCount.Text = (Convert.ToInt16(lblQuestionCount.Text.Split('/')[0].ToString()) + 1).ToString() + "/" + lblQuestionCount.Text.Split('/')[1].ToString();
        lblTotalQuestions.Text = lblQuestionCount.Text.Split('/')[1].ToString();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {


        if (chkReviewer.Checked)
        {
            lblStatus.Text = "&nbsp";
            btnConfirm.Visible = true;
            btnNext.Visible = false;
            this.Panel1.DefaultButton = this.btnConfirm.UniqueID;
            NextQuestion();

        }
        else
        {
            btnConfirm.Visible = false;
            btnNext.Visible = true;
            this.Panel1.DefaultButton = this.btnNext.UniqueID;
            lblStatus.Text = "Answer " + hdnQuestionID.Value + " Saved in Database";
            NextQuestion();

        }
        lblQuestionCount.Text = (Convert.ToInt16(lblQuestionCount.Text.Split('/')[0].ToString()) + 1).ToString() + "/" + lblQuestionCount.Text.Split('/')[1].ToString();
        lblTotalQuestions.Text = lblQuestionCount.Text.Split('/')[1].ToString();
        txtAnswer.Text = "";
        //
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "funrefreshmathqst()", true);

    }


    private void insert_quiz_summary()
    {

        string sql_existing = "select count(*) from quiz_summary where st_id = " + Session["userid"].ToString()
            + " and test_key = '" + Session["cqz_quiz_code"].ToString() + "'";

        string cnt_existing = gen_db_utils.gp_sql_scalar(sql_existing);

        if (Int32.Parse(cnt_existing) <= 0)
        {

            string str_sql = "insert into quiz_summary (st_id, quiz_type, quiz_title, subject, test_key, start_time, quiz_query) Values("
                 + Session["userid"].ToString() + "," // st_id
                 + "'type_2'," // quiz_type
                 + "'" + Session["cqz_quiz_title"].ToString() + "'," // q_seq_id
                 + Session["cqz_quiz_subject"].ToString() + "," // subject
                 + "'" + Session["cqz_quiz_code"].ToString() + "'," // test_key
                 + "'" + DateTime.Now.ToString() + "'," // date_answered
                 + "'" + Session["cqz_query"].ToString() + "' " // date_answered
                 + ")";

            Session["dbg_sql_insert_summary"] = str_sql;
            gen_db_utils.gp_sql_execute(str_sql);
        }
    }
}
