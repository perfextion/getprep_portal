﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="gp_contact.aspx.cs" Inherits="gp_contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div id="wrapper-content">
        <!-- PAGE WRAPPER-->
        <div id="page-wrapper">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <!-- CONTENT-->
                <div class="content">
                    <div class="section background-opacity page-title set-height-top">
                        <div class="container">
                            <div class="page-title-wrapper">
                                <!--.page-title-content-->
                                <h2 class="captions">Contact Us</h2>
                               
                            </div>
                        </div>
                    </div>

                    <div class="section intro-edu">
                        <div class="container">


                            <hr>

                                   <div class="contact-main-wrapper">
                            <div class="row contact-method">
                                <div class="col-md-4">
                                    <div class="method-item"><i class="fa fa-map-marker"></i>

                                        <p class="sub">COME TO</p>

                                        <div class="detail">
                                            
                                            <p><b> GetPrep Academy </b></p>
                                            <p>11612 Bee Cave Rd., Building 1, Suite 1 - 100</p>
                                            <p>Austin, TX 78738 <br></p>
											 <p> (Located behind Bee Cave Urgent Care) </p>
										</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="method-item"><i class="fa fa-phone"></i>

                                        <p class="sub">CALL TO</p>

                                        <div class="detail"><p>Local: 1-512-501-3690</p>

                                            <p>Toll Free: 1-833-K12-PREP</p></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="method-item"><i class="fa fa-envelope"></i>

                                        <p class="sub">CONNECT TO</p>

                                        <div class="detail"><p>info@getprep.com</p>

                                            <p>www.getprep.com</p></div>
                                    </div>
                                </div>
                            </div>
							   <hr>
							   
			   <div class="contact-main-wrapper">
                            <div class="row contact-method">
                                <div class="col-md-4">
                                   
                                </div>
                                <div class="col-md-4">
                                    <div class="method-item">

                                        <p class="sub"><b>Working Hours</b></p>

                                        <div class="detail">
										    <p>Mon - Thu: 5 PM - 9PM </p>
											<p>Sat: 9 AM - 6 PM </p>

                                            
									    </div>
                                </div>
                                </div>
                                <div class="col-md-4">
                                   
                                </div>
                            </div>	   
							   
							   
							       <hr>
                            <form class="bg-w-form contact-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><label class="control-label form-label">NAME <span class="highlight">*</span></label><input type="text" placeholder="" class="form-control form-input"/><!--label.control-label.form-label.warning-label(for="") Warning for the above !--></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"><label class="control-label form-label">EMAIL <span class="highlight">*</span></label><input type="text" placeholder="" class="form-control form-input"/><!--label.control-label.form-label.warning-label(for="")--></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"><label class="control-label form-label">PURPOSE</label><select class="form-control form-input selectbox">
                                            <option value="">Please Select</option>
                                            <option value="">Program Information</option>
                                            <option value="">Enrollments</option>
                                            <option value="">Billing</option>
                                            <option value="">Other</option>
                                        </select><!--label.control-label.form-label.warning-label(for="", hidden)--></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"><label class="control-label form-label">SUBJECT</label><input type="text" placeholder="" class="form-control form-input"/><!--label.control-label.form-label.warning-label(for="", hidden)--></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="contact-question form-group"><label class="control-label form-label">HOW CAN WE HELP? <span class="highlight">*</span></label><textarea class="form-control form-input"></textarea></div>
                                    </div>
                                </div>
                                <div class="contact-submit">
                                    <button type="submit" class="btn btn-contact btn-green"><span>SUBMIT</span></button>
									
                                </div>
								<br>
									<br>
                            </form>
                        </div>
                            
                         
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>


                    