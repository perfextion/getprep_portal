﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Center_Checkin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["administrator"] == null)
        {
            Response.Redirect("~/Center_administrator_login.aspx");
        }
        else if (Session["administrator"].ToString() != "1")
        {
            Response.Redirect("~/Center_administrator_login.aspx");
        }
        
    }
    protected void btnCheckinOption_Click(object sender, EventArgs e)
    {
        divLogin.Visible = true;
        divOptions.Visible = false;
        divGoal.Visible = true;
        lblAction.Visible = false;
        lblAction.Text = "checkin";
      //  divCode.Visible = true;
        RequiredFieldValidator2.Enabled = true;
        login.Text = "Check In";
        lblErrorMessage.Text = "";
    }
    protected void btnCheckOutOption_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        divLogin.Visible = true;
        divOptions.Visible = false;
        divGoal.Visible = false;
        divCode.Visible = false;
        RequiredFieldValidator2.Enabled = false;
        lblAction.Visible = false;
        lblAction.Text = "checkout"; 
        login.Text = "Check Out";
    }
    public void StudentCheckIn(string userid)
    {
        string str_sql = "select top 1 * , datediff(minute,chkin_time,getdate()) timediffinmin from st_attendance where st_id=" + userid + "order by chkin_time desc";
        DataTable dt = new DataTable();
        dt = gen_db_utils.gp_sql_get_datatable(str_sql);
        if (dt.Rows.Count > 0)
        {
            if ((Convert.ToBoolean(dt.Rows[0]["is_active"]) == true) && dt.Rows[0]["chkout_time"].ToString() == "")
            {
                lblErrorMessage.Text = "You are already Checkin to the center";
            }
            else
            { 
                string sql_ins = "insert into st_attendance (st_id,chkin_time,is_active) values" + "(" + "'" + userid + "'," + "getdate()," + "'" + "1" + "'" + ")";
                gen_db_utils.gp_sql_execute(sql_ins);
            }
        } 
    }

    protected void btnCheckOut_Click(object sender, EventArgs e)
    {
        string comm = txtComment.InnerText;
        string sql_ins = "update st_attendance set  comments='" + comm + "' ,chkout_time = getdate(),  is_active=0 where st_id = " + lblStudentId.Text + "and chkout_time is null and is_active=1 ";
        gen_db_utils.gp_sql_execute(sql_ins);
        
    }

    public void StudentCheckout(string userid)
    {
        lblStudentId.Text = userid;
        string str_sql = "select top 1 * , datediff(minute,chkin_time,getdate()) timediffinmin from st_attendance where is_active=1 and chkout_time is null and st_id=" + userid + "order by chkin_time desc";
        DataTable dt = new DataTable();
        dt = gen_db_utils.gp_sql_get_datatable(str_sql);
        if (dt.Rows.Count == 0)
        {
            lblErrorMessage.Text = "You did not Check In to the Center";
        }
        else
        {
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }
    }
    protected void login_Click(object sender, EventArgs e)
    {         
            string login = "select st_id,plan_join_dt,username,password from students where username = '" + txtLoginemail.Text + "' and password = '" + txtLoginPassword.Text + "'";
            DataTable dt = gen_db_utils.gp_sql_get_datatable(login);
            if (dt.Rows.Count > 0)
            {
                string id = dt.Rows[0]["st_id"].ToString();  
                // Check if user has click on checkout or checkin button
                if (lblAction.Text == "checkin")
                {
                    StudentCheckIn(id);
                }
                else
                {
                    StudentCheckout(id);
                }
                divLogin.Visible = false;
                divOptions.Visible = true;
                divGoal.Visible = true;
                divCode.Visible = true;
                RequiredFieldValidator2.Enabled = true;
                lblAction.Visible = true;
                lblAction.Text = "";
                 
            }
            else
            {
                lblLoginStatus.Text = "Invalid Email or Password.";
            }
            //Response.Redirect("/Center_Checkin.aspx");
       
       
    }
}