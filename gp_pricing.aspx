﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="gp_pricing.aspx.cs" Inherits="gp_pricing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
 <div id="wrapper-content">
        <!-- PAGE WRAPPER-->
        <div id="page-wrapper">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <!-- CONTENT-->
                <div class="content">
                    <div class="section background-opacity page-title set-height-top">
                        <div class="container">
                            <div class="page-title-wrapper">
                                <!--.page-title-content-->
                                <h2 class="captions">Pricing & Packages</h2>
                                <ol class="breadcrumb">
                                    <li><a href="index.html">Home</a></li>
                                    <li class="active"><a href="#">Pricing & Packages</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>            
                   
                 <div class="container">
                 	<div class="col-lg-12">
                    	<div class="text-head intro-content">
                        	<h2>Pricing & Packages</h2>
                            <p>The packages listed below are based on 90 minute sessions. The frequency and length of sessions are
                            flexible, though most students find one or two 90 minute sessions per week to be ideal. We require a 
                            first-time minimum purchase of three 90 minute sessions (the First Time Package) due to the significant
                            amount of preparation that tutors perform in advance of the first few sessions. Although we have found the
                            options listed below to be well-aligned with our students' needs, we are happy to develop customized 
                            packages.</p>
                            <div class="table-responsive">
                            	<table class="table table-hover tab">
                                	<thead>
                                    	<tr>
                                        	<th>Service</th>
                                            <th>Standard Tier</th>
                                            <th>Senior Tier</th>
                                            <th>Guru Tier</th>
                                            <th>Master Tier</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<tr>
                                        	<td>
                                            	<strong>Hourly Rate</strong>
                                            </td>
                                            <td>$110/hr</td>
                                            <td>$150/hr</td>
                                            <td>$220/hr</td>
                                            <td>$270/hr</td>
                                        </tr>
                                        <tr>
                                        	<td>
                                            	<strong>First Time Package</strong><br>
                                                <small>3 90-minute sessions, 4.5 hours</small>
                                            </td>
                                                <td>$495</td>
                                                <td>$675</td>
                                                <td>$990</td>
                                                <td>$1215</td>
                                        </tr>
                                         <tr data-toggle="collapse" data-target="#tab-1">
                                        	<td>
                                            	<strong>Comprehensive Package, 5% off</strong><br>
                                                <small>16 90-minute sessions, 24 hours</small>
                                            </td>
                                                <td>$2508</td>
                                                <td>$3420</td>
                                                <td>$5016</td>
                                                <td>$6156</td>
                                        </tr>
                                         <tr class="zero"><td colspan="5"> <div id="tab-1" class="collapse">
                                       

<p>Most students use the three session package to address finite areas of the exam or specific problem types. Because we believe that tutoring is an iterative process, three sessions is the minimum number that we offer. The three session package is a good way to get your feet wet, evaluate the amount of tutoring you’ll ultimately need, and see if you feel comfortable with a tutor. We are always happy to apply your first payment to one of our longer discounted packages.</p>


                                        </div></td></tr>
                                        <tr data-toggle="collapse" data-target="#tab-2">
                                        	<td>
                                            	<strong>Planning Ahead Package, 10% off</strong><br>
                                                <small>20 90-minute sessions, 30 hours</small>
                                            </td>
                                                <td>$2970</td>
                                                <td>$4050</td>
                                                <td>$5940</td>
                                                <td>$7290</td>
                                               
                                        </tr>
                                        <tr><td colspan="5"> <div id="tab-2" class="collapse">
                                       

<p>Because the SAT heavily rewards repetition and coverage, we offer this package to students who would prefer to space their preparation out over a longer duration and complete additional practice exams. Some students find this package useful if they need to spend more time preparing for one section of the test. As in the Comprehensive Package, this package offers complete coverage of the SAT. We cover each aspect of the reading, writing, and math sections in detail. We review general test strategy and time management extensively. Students typically complete between eight and ten (untimed and timed) practice exams.</p>

<p>We dedicate six sessions to critical reading. We begin with a review of passage-based reading, addressing typical passage types and the major and minor question types. Reading techniques focusing on efficiency and content retention are central. Next, we dedicate several sessions to sentence completion, the other major question type in the critical reading section. We focus on expanding vocabulary, but also address roots, prefixes, suffixes, and common SAT phrases.</p>

<p>We devote six sessions to writing, with one or two sessions devoted to sentence improvement, paragraph improvement, sentence error identification, and the essay, respectively. Before proceeding to a comprehensive review of the major and minor question types, many students require a refresher of the main grammatical topics that frequently appear on the exam. Because the essay section of the SAT is so unusual (and differs markedly from what most high school students learn with respect to composition), sessions often begin or end with time spent analyzing the structure of successful essays, outlining essays, or writing essays based on prompts that have appeared on recent tests.</p>

<p>We allocate 8 sessions to mathematics. We cover both multiple choice and student-produced response question types and all of the content areas that occur most frequently on the exam: arithmetic word problems (percents, ratios, and proportions), integers (even/odd numbers, primes, and divisibility), rational numbers, sets, counting techniques, sequences and series, and basic number theory.</p>

                                        </div></td></tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                 </div>
                 
                   

                    </div>
                    </div>
                  
                </div>
            </div>
  

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

