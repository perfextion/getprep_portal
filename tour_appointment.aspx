﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="tour_appointment.aspx.cs" Inherits="tour_appointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_header" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBody" runat="Server">
    <div class="page-login">
        <div class="container">
            <div class="login-wrapper rlp-wrapper">

                <div class="login-table rlp-table" >
                    <div id="summary"  runat="server" visible="false">

                        <h3>Thank you for submitting time to visit our center. Looking forward to see you in center.</h3>
                        <div class="clearBoth"></div>

                    </div>
                    <div id="divEntry" runat="server">
                        <h3>Your profile is not verified by the administator. Please call or visit our center for more information.</h3>
                        <div class="login-title rlp-title">Enter Availability </div>
                        <div class="login-form bg-w-form rlp-form">
                            
                            <div class="row">
                                <div class="col-sm-6" style="margin-left: 25%;">

                                    <div class="panel panel-default" style="background-color: #eee">
                                        <div class="panel-body">

                                            <div class="row">

                                                <div class="col-sm-8">
                                                    <label for="txtDate" style="color: black"><b>Date</b></label>
                                                    <asp:TextBox ID="txtDate" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtDate" ErrorMessage=" Date is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <label for="txtTime" style="color: black"><b>Time</b></label>
                                                    <asp:TextBox ID="txtTime" CssClass=" form-control" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtTime" ErrorMessage=" Time is a required field." ForeColor="Red"></asp:RequiredFieldValidator>


                                                </div>

                                            </div>

                                            <div class="row">


                                                <div class="col-sm-12">
                                                    <div class="login-submit">
                                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="btn btn-login btn-green " Style="float: right" Text="Save" />

                                                    </div>

                                                </div>

                                            </div>
                                        </div>


                                    </div>

                                </div>


                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4"
    ContentPlaceHolderID="ContentBodyScripts" runat="Server">
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
        $("#ContentBody_txtDate").datepicker({ minDate: 0 });

        $('#ContentBody_txtTime').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '10',
            maxTime: '9:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    </script>
</asp:Content>

