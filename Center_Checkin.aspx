﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Center_Checkin.aspx.cs" Inherits="Center_Checkin" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Center Checkin</title>
    <link type="text/css" rel="stylesheet" href="assets/css/color-1.css" />
    <link type="text/css" rel="stylesheet" href="assets/libs/bootstrap-3.3.5/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900,300" />
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-awesome-4.4.0/css/font-awesome.css" />
    <link type="text/css" rel="stylesheet" href="assets/font/font-icon/font-svg/css/Glyphter.css" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <!-- HEADER-->
    <form id="form1" runat="server">

        <!-- WRAPPER-->
        <div id="wrapper-content">
            <!-- PAGE WRAPPER-->
            <div id="page-wrapper">
                <!-- MAIN CONTENT-->
                <div class="main-content">
                    <!-- CONTENT-->
                    <div class="content">
                        <div class="page-login ">
                            <div class="container">
                                <div class="login-wrapper rlp-wrapper">
                                    <div class="login-table rlp-table">
                                        <a href="index.html">
                                            <img src="assets/images/logo-color-1.png" alt="" class="login"></a>

                                        <asp:Label ID="lblAction" runat="server"></asp:Label>
                                        <div class="login-title rlp-title">login to your center!</div>
                                        <asp:Label ID="lblErrorMessage" runat="server" Style="color: red"></asp:Label>

                                        <div id="divOptions" runat="server">
                                            <asp:Label ID="lblOptionStatus" runat="server" Style="color: red"></asp:Label><br />
                                            <asp:Button ID="btnCheckOutOption" runat="server" OnClick="btnCheckOutOption_Click" CssClass="btn btn-login btn-green" Text="Check Out" />
                                            <asp:Button ID="btnCheckinOption" runat="server" OnClick="btnCheckinOption_Click" CssClass="btn btn-login btn-green" Text="Check In" />
                                            <br />
                                            <br />
                                        </div>
                                        <div id="divLogin" runat="server" visible="false">
                                            <div class="login-form bg-w-form rlp-form" style="margin-left: 30%">
                                                <div class="row">
                                                    <asp:Label ID="lblLoginStatus" runat="server" Style="color: red;"></asp:Label>
                                                    <div class="col-md-6">
                                                        <label for="regemail" class="control-label form-label">Student UserName <span class="highlight">*</span></label>
                                                        <asp:TextBox ID="txtLoginemail" placeholder="Enter Your EmailAddress" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rqLoginemail" ValidationGroup="loginvalidation" runat="server"
                                                            ControlToValidate="txtLoginemail" ErrorMessage=" Email is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <div class="help-block"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="regpassword" class="control-label form-label">student password <span class="highlight">*</span></label>

                                                        <asp:TextBox ID="txtLoginPassword" runat="server" TextMode="Password" placeholder="Enter Password " CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfloginpassword" ValidationGroup="loginvalidation" runat="server" ControlToValidate="txtLoginPassword" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password field is required."></asp:RequiredFieldValidator>
                                                        <div class="help-block"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6" id="divCode" runat="server" style="display: none">
                                                        <label for="txtCode" class="control-label form-label">code <span class="highlight">*</span></label>

                                                        <asp:TextBox ID="txtCode" runat="server" placeholder="Enter Code " CssClass="form-control"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="loginvalidation" runat="server" ControlToValidate="txtCode" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password field is required."></asp:RequiredFieldValidator>
                                                        <div class="help-block"></div>--%>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6" id="divGoal" runat="server">
                                                        <label for="ddlmaingoal" class="control-label form-label">Goal <span class="highlight">*</span></label>
                                                        <asp:DropDownList ID="ddlmaingoal" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="" Text="What your main goal?">  </asp:ListItem>
                                                            <asp:ListItem>Prepare for exam </asp:ListItem>
                                                            <asp:ListItem>Helping child</asp:ListItem>
                                                            <asp:ListItem>Other</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="loginvalidation" runat="server" ControlToValidate="ddlmaingoal" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Choose any Goal."></asp:RequiredFieldValidator>
                                                        <div class="help-block"></div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="login-submit">
                                                <asp:Button ID="login" runat="server" OnClick="login_Click" ValidationGroup="loginvalidation" CssClass="btn btn-login btn-green" Text="Check In" />

                                            </div>
                                        </div>

                                        <div id="divSummary" runat="server" visible="false">
                                            <h3 id="summarytext" runat="server">Welcome to the Center!</h3>
                                            <a href="Center_Checkin.aspx">Back to Login</a>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Activity</h3>
                    </div>

                    <div class="modal-body">

                        <asp:Label ID="lblStudentId" Style="font-size: 15px; display: none" runat="server">
                         
                        </asp:Label>
                        <div class="form-group">
                            <label>What did you work on today?</label>
                            <textarea class="form-control" id="txtComment" runat="server"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button OnClick="btnCheckOut_Click" CssClass="btn btn-login btn-green" ID="btnchkOut" runat="server" Text="CheckOut" />

                    </div>
                </div>
            </div>

        </div>
        <script src="/Portal/portalassets/js/jquery-1.12.4.js"></script>
        <script src="/Portal/portalassets/js/jquery-ui-1.12.1.js"></script>
        <script src="/Portal/portalassets/js/bootstrap.min.js"></script>
        <script src="assets/libs/bootstrap-3.3.5/js/bootstrap.min.js"></script>
        <script src="assets/libs/smooth-scroll/jquery-smoothscroll.js"></script>
        <script src="assets/libs/owl-carousel-2.0/owl.carousel.min.js"></script>
        <script src="assets/libs/appear/jquery.appear.js"></script>
        <script src="assets/libs/count-to/jquery.countTo.js"></script>
        <script src="assets/libs/wow-js/wow.min.js"></script>
        <script src="assets/libs/selectbox/js/jquery.selectbox-0.2.min.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox.js"></script>
        <script src="assets/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
        <!-- MAIN JS-->
        <script src="assets/js/main.js"></script>
        <!-- LOADING SCRIPTS FOR PAGE-->
    </form>
    <script src="Plugin/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="Plugin/tinymce/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: '#txtComment',
            plugins: '',
            toolbar: false,
            menubar: false
        });

    </script>
</body>
</html>
