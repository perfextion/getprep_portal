using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Text;

public class gp_integrity_checks
    {


  public static  string unused_OMR_forms()
    {

        string str_return = "None";


        string str_sql = "select distinct(form_id) from  OMR_form where form_id not in "
                 + " (select distinct(form_id) from omr_answers) ";

        ArrayList ar_mismatch = gen_db_utils.gp_sql_get_arraylist(str_sql);

        if (ar_mismatch.Count > 0)
        {
            str_return = ar_mismatch.Count.ToString() + " unused OMR_forms" + "<br>"
                
                + "Form_ids: " + fc_gen_utils.arraylist2string(ar_mismatch);



        }

        return str_return;



    }


  public static string orphan_form_ids()
  {

      string str_return = "<br> Dangling form_ids in OMR_answers not in ONR_form + None";


      string str_sql = "select distinct(form_id) from  omr_answers where form_id not in "
               + " (select distinct(form_id) from omr_form) ";

      ArrayList ar_mismatch = gen_db_utils.gp_sql_get_arraylist(str_sql);

      if (ar_mismatch.Count > 0)
      {
          str_return = "<br> " + ar_mismatch.Count.ToString() + " Dangling form_ids in OMR_answers not in ONR_form" + "<br>"

              + "Form_ids: " + fc_gen_utils.arraylist2string(ar_mismatch);



      }

      return str_return;



  }


  public static string set_core_cat_ccs()
  {
      string str_return = "";

      int tree_level = 1;

      while (tree_level < 5)
      {

          str_return += "Processing Tree Level " + tree_level.ToString() + "<br>";

          string str_sql = "select * from core_cat where tree_level = " +tree_level.ToString() ;

          DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

          str_return += dt1.Rows.Count.ToString() + "Records" + "<br>";

          foreach (DataRow dtr in dt1.Rows)
          {
              o_core_cat item_cc = new o_core_cat(dtr["id"].ToString());

              item_cc.set_ccs();

              item_cc.update_cc();

              str_return += dtr["id"].ToString() + " updating" + DateTime.Now.ToString() + "<br>";

          }

          tree_level++;
      }


      

      return str_return;



  }


  public static string sync_core_cat_ccs()
  {
      string str_return = "";

      string str_sql = "update core_cat set cc0=id,cc1=-1,cc2=-1,cc3=-1,cc4=-1 where tree_level=0";
            str_sql += "update core_cat set cc1=id,cc0=-5,cc2=-1,cc3=-1,cc4=-1 where tree_level=1";
            str_sql += "update core_cat set cc2=id,cc0=-5,cc1=-5,cc3=-1,cc4=-1 where tree_level=2";
            str_sql += "update core_cat set cc3=id,cc0=-5,cc1=-5,cc2=-5,cc4=-1 where tree_level=3";
            str_sql += "update core_cat set cc4=id,cc0=-5,cc1=-5,cc2=-5,cc3=-5 where tree_level=4";

       str_sql += " update t1 set cc3 = t2.parent_cat from core_cat t1 join core_cat t2 on t1.cc4 = t2.id where t1.cc4 > 0 ";
       str_sql += " update t1 set cc2 = t2.parent_cat from core_cat t1 join core_cat t2 on t1.cc3 = t2.id where t1.cc3 > 0 ";
       str_sql += "update t1 set cc1 = t2.parent_cat from core_cat t1 join core_cat t2 on t1.cc2 = t2.id where t1.cc2 > 0";
       str_sql += "update t1 set cc0 = t2.parent_cat from core_cat t1 join core_cat t2 on t1.cc1 = t2.id where t1.cc1 > 0";

       gen_db_utils.gp_sql_execute(str_sql);

      return str_return;



  }

  public static string sync_type2_qs_ccs()
  {
      string str_return = "";

      string str_sql = " update type2_qs set cc0=t2.cc0, cc1=t2.cc1, cc2=t2.cc2,cc3 = t2.cc3,cc4 = t2.cc4, cc_level=t2.tree_level from type2_qs t1 join core_cat t2 "
                    + " on t1.core_cat_id = t2.id  "
                    + " where t1.core_cat_id > 0 ";
     
      gen_db_utils.gp_sql_execute(str_sql);

      return str_return;



  }

  public static string sync_concepts_ccs()
  {
      string str_return = "";

      string str_sql = " update concepts set cc0=t2.cc0, cc1=t2.cc1, cc2=t2.cc2,cc3 = t2.cc3,cc4 = t2.cc4, cc_level=t2.tree_level from concepts t1 join core_cat t2 "
                    + " on t1.anchor_id = t2.id  "
                    + " where t1.anchor_to = 'core_cat' and t1.anchor_id > 0 ";

      gen_db_utils.gp_sql_execute(str_sql);

      return str_return;



  }

  public static string sync_single_type2_q_ccs(string type2_qid)
  {
      string str_return = "";

      string str_sql = " update type2_qs set cc0=t2.cc0, cc1=t2.cc1, cc2=t2.cc2,cc3 = t2.cc3,cc4 = t2.cc4 from type2_qs t1 join core_cat t2 "
                    + " on t1.core_cat_id = t2.id  "
                    + " where t1.type2_qid = " + type2_qid;

      gen_db_utils.gp_sql_execute(str_sql);

      return str_return;



  }

}