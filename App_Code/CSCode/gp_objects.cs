﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;


public class o_scan_q
{
    public string prob_id;
    public int seq_id;
    public string q_header_img;
    public string q_header_alt;
    public string q_header_f;
    public string q_main_img;
    public string q_main_alt;
    public string q_main_f;
    public string q_choices_img;
    public string ans_a;
    public string ans_b;
    public string ans_c;
    public string ans_d;
    public string ans_e;
    public string ans;
    public string soln_img;
    public string soln_alt;
    public string soln_f;
    public string approach_img;
    public string approach_alt;
    public string approach_f;
    public string gotchas_img;
    public string gotchas_alt;
    public string gotchas_f;
    public string group_id;
    public string order_id;
    public string book_id;
    public string chap_id;
    public string set_id;
    public string set_code;
    public string q_type;
    public int optionCount;


    public o_scan_q(string i_seq_id)
    {
        string str_sql = "Select * from scan_qs where seq_id = " + i_seq_id;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            set_constructor(dtr);
        }

    }


    public o_scan_q(DataRow dtr)
    {

        set_constructor(dtr);

    }

    public void set_constructor(DataRow dtr)
    {

        this.prob_id = dtr["prob_id"].ToString();
        this.seq_id = Int32.Parse(dtr["seq_id"].ToString());

        this.q_header_img = dtr["q_header_img"].ToString();
        this.q_header_alt = dtr["q_header_alt"].ToString();

        this.q_main_img = dtr["q_main_img"].ToString();
        this.q_main_alt = dtr["q_main_alt"].ToString();
        this.q_choices_img = dtr["q_choices_img"].ToString();
        this.ans_a = dtr["ans_a"].ToString();
        this.ans_b = dtr["ans_b"].ToString();
        this.ans_c = dtr["ans_c"].ToString();
        this.ans_d = dtr["ans_d"].ToString();
        this.ans_e = dtr["ans_e"].ToString();
        this.ans = dtr["ans"].ToString();
        this.soln_img = dtr["soln_img"].ToString();
        this.soln_alt = dtr["soln_alt"].ToString();

        this.group_id = dtr["group_id"].ToString();
        this.order_id = dtr["order_id"].ToString();
        this.book_id = dtr["book_id"].ToString();
        this.chap_id = dtr["chap_id"].ToString();
        this.set_id = dtr["set_id"].ToString();
        this.q_type = dtr["q_type"].ToString();
        this.set_code = dtr["set_code"].ToString();

        this.optionCount = 4;

        // /images/scan_img/b123/b123_ch10_s1_p10_h.PNG

        string image_prefix = "/images/scan_img/" + this.book_id + "/" + this.book_id + "_" + this.chap_id + "_" + this.set_id + "_" + this.prob_id;
        string img_file = "";

        if (this.q_header_alt.Length > 2)
        {
            q_header_f = q_header_alt;
        }
        else
        {
            if (this.q_header_img == ".")
            {
                q_header_f = "";
            }
            else
            {
                img_file = this.q_header_img;
                q_header_f = gp_utils.get_img_html(img_file);
            }

        }


        if (this.q_main_alt.Length > 1)
        {
            q_main_f = q_main_alt;
        }
        else
        {
            img_file = image_prefix + "_q.png";
            q_main_f = gp_utils.get_img_html(img_file);
        }

        if (this.soln_alt.Length > 1)
        {
            soln_f = soln_alt;
        }
        else
        {
            img_file = image_prefix + "_sol.png";
            soln_f = gp_utils.get_img_html(img_file);
        }





    }

    public string view_scan_q()
    {
        string str_return = "";

        if (q_header_f.Length <= 2) q_header_f = "";

        str_return = q_header_f + "<br><br>"
                     + q_main_f + "<br><br>"
                     + "Answer" + ans + "<br><br>"
                     + soln_f + "<br>";

        return str_return;

    }



}

public class o_book
{
    public int book_seq_id;
    public string book_id;
    public string book_title;
    public string subject;
    public string book_type;
    public string isbn_num;
    public string entry_by;
    public string entry_on;



    public o_book(int i_book_seq_id)
    {
        string str_sql = "Select * from books where book_seq_id = " + i_book_seq_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            this.book_seq_id = Int32.Parse(dtr["book_seq_id"].ToString());
            this.book_id = dtr["book_id"].ToString();
            this.book_title = dtr["book_title"].ToString();
            this.subject = dtr["subject"].ToString();
            this.book_type = dtr["book_type"].ToString();
            this.isbn_num = dtr["isbn_num"].ToString();
            this.entry_by = dtr["entry_by"].ToString();
            this.entry_on = dtr["entry_on"].ToString();


        }

    }

    public o_book(string i_book_id)
    {
        string str_sql = "Select * from books where book_id = " + i_book_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            this.book_seq_id = Int32.Parse(dtr["book_seq_id"].ToString());
            this.book_id = dtr["book_id"].ToString();
            this.book_title = dtr["book_title"].ToString();
            this.subject = dtr["subject"].ToString();
            this.book_type = dtr["book_type"].ToString();
            this.isbn_num = dtr["isbn_num"].ToString();
            this.entry_by = dtr["entry_by"].ToString();
            this.entry_on = dtr["entry_on"].ToString();


        }

    }






}


public class o_chapter
{
    public int chap_seq_id;
    public string chap_id;
    public string order_id;
    public string chap_title;
    public string book_id;
    public string p_book_seq_id;
    public string entry_by;
    public string entry_on;
    public string batch_id;



    public o_chapter(int i_chap_seq_id)
    {
        string str_sql = "Select * from chapters where chap_seq_id = " + i_chap_seq_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            this.chap_seq_id = Int32.Parse(dtr["chap_seq_id"].ToString());
            this.chap_id = dtr["chap_id"].ToString();
            this.order_id = dtr["order_id"].ToString();
            this.chap_title = dtr["chap_title"].ToString();
            this.book_id = dtr["book_id"].ToString();
            this.p_book_seq_id = dtr["p_book_seq_id"].ToString();
            this.entry_by = dtr["entry_by"].ToString();
            this.entry_on = dtr["entry_on"].ToString();
            this.batch_id = dtr["batch_id"].ToString();


        }

    }

    public o_chapter(string i_chap_id)
    {
        string str_sql = "Select * from chapters where chap_id = " + i_chap_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            this.chap_seq_id = Int32.Parse(dtr["chap_seq_id"].ToString());
            this.chap_id = dtr["chap_id"].ToString();
            this.order_id = dtr["order_id"].ToString();
            this.chap_title = dtr["chap_title"].ToString();
            this.book_id = dtr["book_id"].ToString();
            this.p_book_seq_id = dtr["p_book_seq_id"].ToString();
            this.entry_by = dtr["entry_by"].ToString();
            this.entry_on = dtr["entry_on"].ToString();
            this.batch_id = dtr["batch_id"].ToString();


        }

    }






}

public class o_book_server_dir
{
    public string full_path;
    public string total_file_count;
    public string cnt_chapters;
    public string cnt_sets;
    public string cnt_qs;
    public int cnt_header;
    public int cnt_q_images;
    public int cnt_sol_images;




    public o_book_server_dir(string i_book_id)
    {
        string path = HttpContext.Current.Server.MapPath("~/images/scan_img/" + i_book_id + "/");

        int cnt_header = 0;
        int cnt_q_images = 0;
        int cnt_sol_images = 0;

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);

        }

        string[] file_names = Directory.GetFiles(path);

        for (int ii = 0; ii < file_names.Length; ii++)
        {
            string f1 = file_names[ii].Replace(".PNG", "");
            if (f1.Contains("_sol")) cnt_sol_images++;
            if (f1.Contains("_h")) cnt_header++;
            if (f1.Contains("_q")) cnt_q_images++;

        }

        this.full_path = path;
        this.total_file_count = file_names.Length.ToString();
        this.cnt_header = cnt_header;
        this.cnt_sol_images = cnt_sol_images;
        this.cnt_q_images = cnt_q_images;


    }

}

public class o_book_details
{
    public string book_id;
    public string book_seq_id;
    public string cnt_chapters;
    public string cnt_sets;
    public string cnt_qs;
    public Hashtable h_q_cnt_by_chapter = new Hashtable();
    public Hashtable h_q_cnt_by_set = new Hashtable();
    public Hashtable h_ans_cnt_by_chapter = new Hashtable();





    public o_book_details(DataTable i_dt)
    {

        foreach (DataRow dtr in i_dt.Rows)
        {
            HttpContext.Current.Session["debug777"] = i_dt.Rows.Count;
            // Q count by chapter 

            if (h_q_cnt_by_chapter.Contains(dtr["chap_id"].ToString()))
            {
                int cnt = Int32.Parse((h_q_cnt_by_chapter[dtr["chap_id"].ToString()].ToString())) + 1;

                h_q_cnt_by_chapter[dtr["chap_id"].ToString()] = cnt;
            }
            else
            {
                h_q_cnt_by_chapter.Add(dtr["chap_id"].ToString(), "1");
            }

            // Q count by set 

            string set_id = dtr["chap_id"].ToString() + "_" + dtr["set_id"].ToString();

            if (h_q_cnt_by_set.Contains(dtr["set_id"].ToString()))
            {
                int cnt = Int32.Parse((h_q_cnt_by_set[dtr["set_id"].ToString()].ToString())) + 1;

                h_q_cnt_by_set[dtr["set_id"].ToString()] = cnt;
            }
            else
            {
                h_q_cnt_by_set.Add(dtr["set_id"].ToString(), "1");
            }

            // Ans count by set
            if (dtr["ans"].ToString().Length > 0)
            {
                if (h_ans_cnt_by_chapter.Contains(dtr["chap_id"].ToString()))
                {
                    int cnt = Int32.Parse((h_ans_cnt_by_chapter[dtr["chap_id"].ToString()].ToString())) + 1;

                    h_ans_cnt_by_chapter[dtr["chap_id"].ToString()] = cnt;
                }
                else
                {
                    h_ans_cnt_by_chapter.Add(dtr["chap_id"].ToString(), "1");
                }
            }





        }




    }

}
public class o_core_cat
{
    public int id;
    public string cat_name;
    public string order_id;
    public string tree_level;
    public string parent_cat;
    public string prereq;
    public string grade_min;
    public string grade_max;
    public string i_cnt;
    public string t_cnt;
    public string subject;
    public string keywords;
    public string cat_desc;
    public string cc0;
    public string cc1;
    public string cc2;
    public string cc3;
    public string cc4;


    public o_core_cat(string i_cat_id)
    {
        string str_sql = "Select * from core_cat where id = " + i_cat_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {

            set_constructor(dtr);

        }

    }

    public void set_constructor(DataRow dtr)
    {

        this.id = Int32.Parse(dtr["id"].ToString());

        this.cat_name = dtr["cat_name"].ToString();
        this.order_id = dtr["order_id"].ToString();

        this.tree_level = dtr["tree_level"].ToString();
        this.parent_cat = dtr["parent_cat"].ToString();
        this.prereq = dtr["prereq"].ToString();
        this.grade_min = dtr["grade_min"].ToString();
        this.grade_max = dtr["grade_max"].ToString();
        this.i_cnt = dtr["i_cnt"].ToString();
        this.t_cnt = dtr["t_cnt"].ToString();
        this.subject = dtr["subject"].ToString();
        this.keywords = dtr["keywords"].ToString();
        this.cat_desc = dtr["cat_desc"].ToString();
        this.cc0 = dtr["cc0"].ToString();
        this.cc1 = dtr["cc1"].ToString();
        this.cc2 = dtr["cc2"].ToString();
        this.cc3 = dtr["cc3"].ToString();
        this.cc4 = dtr["cc4"].ToString();

    }

    public void set_ccs()
    {

        if (tree_level == "4")
        {
            cc4 = id.ToString();
            cc3 = HttpContext.Current.Session["z_pc_" + cc4].ToString();
            cc2 = HttpContext.Current.Session["z_pc_" + cc3].ToString();
            cc1 = HttpContext.Current.Session["z_pc_" + cc2].ToString();
            cc0 = HttpContext.Current.Session["z_pc_" + cc1].ToString();

        }
        if (tree_level == "3")
        {
            cc4 = "-99";
            cc3 = id.ToString();
            cc2 = HttpContext.Current.Session["z_pc_" + cc3].ToString();
            cc1 = HttpContext.Current.Session["z_pc_" + cc2].ToString();
            cc0 = HttpContext.Current.Session["z_pc_" + cc1].ToString();

        }
        if (tree_level == "2")
        {
            cc4 = "-99";
            cc3 = "-99";
            cc2 = id.ToString();
            cc1 = HttpContext.Current.Session["z_pc_" + cc2].ToString();
            cc0 = HttpContext.Current.Session["z_pc_" + cc1].ToString();

        }
        if (tree_level == "1")
        {
            cc4 = "-99";
            cc3 = "-99";
            cc2 = "-99";
            cc1 = id.ToString();
            cc0 = HttpContext.Current.Session["z_pc_" + cc1].ToString();

        }
        if (tree_level == "0")
        {
            cc4 = "-99";
            cc3 = "-99";
            cc2 = "-99";
            cc1 = "-99";
            cc0 = id.ToString();

        }


    }

    public void update_cc()
    {

        string str_sql = "update core_cat  set  "
                + "cat_name = '" + fc_gen_utils.replace_quotes(cat_name) + "',"
                + "order_id = " + order_id + ","
                + "tree_level = " + tree_level + ","
                + "cat_desc = '" + fc_gen_utils.replace_quotes(cat_desc) + "',"
                + "keywords = '" + fc_gen_utils.replace_quotes(keywords) + "',"
                + "parent_cat = " + parent_cat + ","
                + "cc0 = " + cc0 + ","
                + "cc1 = " + cc1 + ","
                + "cc2 = " + cc2 + ","
                + "cc3 = " + cc3 + ","
                + "cc4 = " + cc4 + " "
                + " where id = " + id;

        gen_db_utils.gp_sql_execute(str_sql);

    }




}

public class o_match_set
{
    public int match_set_id;
    public string match_set_name;
    public string col_a;
    public string col_b;
    public ArrayList ar_a;
    public ArrayList ar_b;
    public int item_count;
    public string match_set_image;


    public o_match_set()
    {
        this.match_set_id = 0;

        this.match_set_name = "";
        this.col_a = "";
        this.col_b = "";
        this.match_set_image = "";


    }

    public o_match_set(string i_match_set_id)
    {
        string str_sql = "Select * from match_parts where match_set_id = " + i_match_set_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {

            set_constructor(dtr);

        }

    }

    public void set_constructor(DataRow dtr)
    {

        this.match_set_id = Int32.Parse(dtr["match_set_id"].ToString());

        this.match_set_name = dtr["match_set_name"].ToString();
        this.col_a = dtr["col_a"].ToString();
        this.col_b = dtr["col_b"].ToString();
        this.match_set_image = dtr["match_set_image"].ToString();
        set_derived();

    }

    public void set_derived()
    {
        this.ar_a = fc_gen_utils.string_split_by_delimter(this.col_a, "|");
        this.ar_b = fc_gen_utils.string_split_by_delimter(this.col_b, "|");
        this.item_count = ar_a.Count;

    }


    public void update_match_set()
    {
        string str_sql = "update  match_parts set "
           + " match_set_name = '" + fc_gen_utils.replace_quotes(match_set_name.Trim()) + "',"
           + " col_a = '" + fc_gen_utils.replace_quotes(col_a.Trim()) + "',"
           + " col_b = '" + fc_gen_utils.replace_quotes(col_b.Trim()) + "',"
           + " match_set_image = '" + fc_gen_utils.replace_quotes(match_set_image.Trim()) + "' "
           + "where match_set_id = " + match_set_id;

        gen_db_utils.gp_sql_execute(str_sql);

    }

    public string view_match_set()
    {
        string str_return = "";

        string details = "<b>Set ID:</b> (" + match_set_id.ToString() + "): <b>Match Set Name:</b> " + match_set_name + "<br><br>"
                           + "<b>Item Count:</b> " + item_count + "<br><br>"
                           + "<b>Image Location</b> " + match_set_image + "<br><br>"
                           + "<b>col_a</b> " + col_a + "<br><br>"
                           + "<b>col_b</b> " + col_b + "<br><br>";


        for (int ii = 0; ii < item_count; ii++)
        {
            details += ar_a[ii].ToString() + " " + ar_b[ii].ToString() + "<br><br>";

        }

        str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'> "
                         + "      <thead> "
                         + "     <tr> "
                         + "       <th >Images</th> "
                                + "       <th >Details</th> "
                                  + "   </tr> "
                                + "      </thead> "
                                + "        <tbody> ";
        str_return += "<tr>"
                          + "<td>" + "<img src=\"" + match_set_image + "\" height=\"200\" >" + "<br><br>"
                          + "<img src=\"" + match_set_image + "\" height=\"200\" >" + "<br><br>"
                          + "</td>"
                          + "<td>" + details + "</td>"
                          + "</tr>";

        str_return += "</tbody></table>";





        return str_return;

    }




}

public class o_omr_form
{
    public string form_id;
    public string total_paragraphs;
    public string total_questions;
    public string num_choices;
    public string para_starts;
    public string test_type;
    public string form_no;
    public string total_duration;
    public string is_even_odd;
    public string odd_letters;
    public string even_letters;
    public string answers;

    public o_omr_form(string i_form_id)
    {
        string str_sql = "Select * from omr_form where form_id = " + i_form_id;

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            this.form_id = dtr["form_id"].ToString();
            this.total_paragraphs = dtr["total_paragraphs"].ToString();
            this.total_questions = dtr["total_questions"].ToString();
            this.num_choices = dtr["num_choices"].ToString();
            this.para_starts = dtr["para_starts"].ToString();
            this.test_type = dtr["test_type"].ToString();
            this.form_no = dtr["form_no"].ToString();
            this.total_duration = dtr["total_duration"].ToString();
            this.is_even_odd = dtr["is_even_odd"].ToString();
            this.odd_letters = dtr["odd_letters"].ToString();
            this.even_letters = dtr["even_letters"].ToString();
            this.answers = dtr["answers"].ToString();

        }

    }



}


public class o_fc_set
{
    public int set_id;
    public string set_name;
    public string set_desc;
    public string set_keywords;
    public string owner_id;
    public string entry_by;
    public string entry_at;
    public string lastmod_by;
    public string lastmod_at;
    public string status;
    public string src;
    public string src_id;
    public string batch_id;





    public o_fc_set()
    {


    }


    public List<o_fc_set> GetAllSets(string query)
    {
        string str_sql = query;

        List<o_fc_set> objo_fc_set = new List<global::o_fc_set>();

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            o_fc_set ofc = new o_fc_set();
            ofc.set_id = Convert.ToInt32(dtr["set_id"]);
            ofc.set_name = (dtr["set_name"].ToString());
            ofc.set_desc = dtr["set_desc"].ToString();
            ofc.set_keywords = dtr["set_keywords"].ToString();
            ofc.owner_id = dtr["owner_id"].ToString();
            ofc.entry_by = dtr["entry_by"].ToString();
            ofc.entry_at = dtr["entry_at"].ToString();
            ofc.lastmod_by = dtr["lastmod_by"].ToString();
            ofc.lastmod_at = dtr["lastmod_at"].ToString();
            ofc.status = dtr["status"].ToString();
            ofc.src = dtr["src"].ToString();
            ofc.src_id = dtr["src_id"].ToString();
            ofc.batch_id = dtr["batch_id"].ToString();
            objo_fc_set.Add(ofc);
        }
        return objo_fc_set;

    }
}


public class o_fc_content
{

    public int fc_id;
    public string fc_term;
    public string fc_desc;
    public string fc_explanation;
    public string order_id;
    public string entry_by;
    public string entry_at;
    public string lastmod_by;
    public string lastmod_at;
    public string status;
    public string src_url;
    public string src_id;
    public string batch_id;
    public string set_id;

    public string fc_type;



    public List<o_fc_content> GetAllContent(string query)
    {
        string str_sql = query;

        List<o_fc_content> objo_fc_content = new List<o_fc_content>();

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {
            o_fc_content ofc = new o_fc_content();
            ofc.fc_id = Convert.ToInt32(dtr["fc_id"]);
            ofc.fc_term = (dtr["fc_term"].ToString());
            ofc.fc_explanation = dtr["fc_explanation"].ToString();
            ofc.fc_desc = dtr["fc_desc"].ToString();
            ofc.order_id = dtr["order_id"].ToString();
            ofc.entry_by = dtr["entry_by"].ToString();
            ofc.entry_at = dtr["entry_at"].ToString();
            ofc.lastmod_by = dtr["lastmod_by"].ToString();
            ofc.lastmod_at = dtr["lastmod_at"].ToString();
            ofc.status = dtr["status"].ToString();
            ofc.src_id = dtr["src_id"].ToString();
            ofc.set_id = dtr["set_id"].ToString();
            ofc.batch_id = dtr["batch_id"].ToString();
            objo_fc_content.Add(ofc);
        }
        return objo_fc_content;

    }
}

public class gp_objects
{





}


public class strat_data
{
    public string title;
    public string description;
    public string[] example;
    public string notes;

}

public class notes_data
{
    public string hint;
    public string note;


}

public class o_type2_q
{

    public int type2_id;
    public string version;
    public string obj_type;
    public string ver_date;
    public int order_id;
    public string q_type;
    public string q_text;
    public string ans_1;
    public string ans_2;
    public string ans_3;
    public string ans_4;
    public string ans_5;
    public string fib_pre;
    public string fib_ans;
    public string fib_post;
    public string answer_choices;
    public string correct_ans;
    public string solution;
    public string explanation;
    public string entry_by;
    public string entry_at;
    public string status;
    public string src_url;
    public string src_main;
    public string batch_id;
    public string core_cat_id;

    public string custom_str1;
    public string custom_int1;
    public string diff_level;
    public string has_image;
    public string has_eqn;
    public string subject;
    public string wp_status;
    public string mark_delete;
    public string mark_reason;
    public string cc_level;
    public string cc0;
    public string cc1;
    public string cc2;
    public string cc3;
    public string cc4;




    public o_type2_q(string i_type2_id)
    {
        string str_sql = "Select * from type2_qs where type2_id = " + i_type2_id;
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
        foreach (DataRow dtr in dt1.Rows)
        {
            set_constructor(dtr);
        }
    }

    public o_type2_q(DataRow dtr)
    {

        set_constructor(dtr);

    }

    public o_type2_q()
    {
        // Blank type2_q
        this.version = "2.0";
        this.obj_type = "type2_q";
        this.ver_date = "01/06/18";
        this.type2_id = 0;
        this.order_id = 0;
        this.q_type = "-";
        this.q_text = "-";
        this.ans_1 = "-";
        this.ans_2 = "-";
        this.ans_3 = "-";
        this.ans_4 = "-";
        this.ans_5 = "-";
        this.fib_pre = "-";
        this.fib_ans = "-";
        this.fib_post = "-";
        this.answer_choices = "ABCDE";
        this.correct_ans = "-";
        this.solution = "-";
        this.explanation = "-";
        this.entry_by = "-";
        this.entry_at = "-";
        this.status = "-";
        this.src_url = "-";
        this.src_main = "-";
        this.batch_id = "-";
        this.core_cat_id = "-";
        this.custom_str1 = "-";
        this.custom_int1 = "-";
        this.diff_level = "-";
        this.has_image = "-";
        this.has_eqn = "-";
        this.subject = "-";
        this.wp_status = "-";
        this.mark_delete = "-";
        this.mark_reason = "-";
        this.cc_level = "-";
        this.cc0 = "-";
        this.cc1 = "-";
        this.cc2 = "-";
        this.cc3 = "-";
        this.cc4 = "-";

    }

    public void set_constructor(DataRow dtr)
    {
        this.version = "2.0";
        this.obj_type = "type2_q";
        this.ver_date = "01/06/18";
        this.type2_id = Int32.Parse(dtr["type2_id"].ToString());
        this.order_id = Int32.Parse(dtr["order_id"].ToString());
        this.q_type = dtr["q_type"].ToString();
        this.q_text = dtr["q_text"].ToString();
        this.ans_1 = dtr["ans_1"].ToString();
        this.ans_2 = dtr["ans_2"].ToString();
        this.ans_3 = dtr["ans_3"].ToString();
        this.ans_4 = dtr["ans_4"].ToString();
        this.ans_5 = dtr["ans_5"].ToString();
        this.fib_pre = dtr["fib_pre"].ToString();
        this.fib_ans = dtr["fib_ans"].ToString();
        this.fib_post = dtr["fib_post"].ToString();
        this.answer_choices = dtr["answer_choices"].ToString();
        this.correct_ans = dtr["correct_ans"].ToString();
        this.solution = dtr["solution"].ToString();
        this.explanation = dtr["explanation"].ToString();
        this.entry_by = dtr["entry_by"].ToString();
        this.entry_at = dtr["entry_at"].ToString();
        this.status = dtr["status"].ToString();
        this.src_url = dtr["src_url"].ToString();
        this.src_main = dtr["src_main"].ToString();
        this.batch_id = dtr["batch_id"].ToString();
        this.core_cat_id = dtr["core_cat_id"].ToString();
        this.custom_str1 = dtr["custom_str1"].ToString();
        this.custom_int1 = dtr["custom_int1"].ToString();
        this.diff_level = dtr["diff_level"].ToString();
        this.has_image = dtr["has_image"].ToString();
        this.has_eqn = dtr["has_eqn"].ToString();
        this.subject = dtr["subject"].ToString();
        this.wp_status = dtr["wp_status"].ToString();
        this.mark_delete = dtr["mark_delete"].ToString();
        this.mark_reason = dtr["mark_reason"].ToString();
        this.cc_level = dtr["cc_level"].ToString();
        this.cc0 = dtr["cc0"].ToString();
        this.cc1 = dtr["cc1"].ToString();
        this.cc2 = dtr["cc2"].ToString();
        this.cc3 = dtr["cc3"].ToString();
        this.cc4 = dtr["cc4"].ToString();





    }

    public string insert_type2_q_sql(string new_id)
    {
        string str_return = "";



            str_return = "update type2_qs "
                             + "set q_text = '" + fc_gen_utils.replace_quotes(q_text) + "',"
                             + " ans_1 = '" + fc_gen_utils.replace_quotes(ans_1) + "',"
                             + " ans_2 = '" + fc_gen_utils.replace_quotes(ans_2) + "',"
                             + " ans_3 = '" + fc_gen_utils.replace_quotes(ans_3) + "',"
                             + " ans_4 = '" + fc_gen_utils.replace_quotes(ans_4) + "',"
                             + " ans_5 = '" + fc_gen_utils.replace_quotes(ans_5) + "',"
                             + " order_id = " + order_id + ","
                             + "q_type = '" + q_type + "',"
                             + " entry_by = 1,"
                             + "status = -999,"
                             + " src_url = '" + fc_gen_utils.replace_quotes(src_url) + "',"
                             + "src_main = '" + fc_gen_utils.replace_quotes(src_main) + "',"
                             + "batch_id = '" + batch_id + "',"
                             + "answer_choices = '" + answer_choices + "',"
                             + "solution = '" + fc_gen_utils.replace_quotes(solution) + "',"
                              + "explanation = '" + fc_gen_utils.replace_quotes(explanation) + "',"
                             + "correct_ans = '" + correct_ans + "',"
                             + "fib_ans = '" + fib_ans + "',"
                             + "custom_str1 = '" + custom_str1 + "',"
                             + "custom_int1 = " + custom_int1 + ","
                             + "diff_level = " + diff_level + ","
                             + "core_cat_id = " + core_cat_id + ","
                             + "entry_at = '" + DateTime.Now + "'"
                             + " where type2_id = " + new_id;
   
     
                     

        return str_return;

    }

    public string view_type2_q()
    {
        string str_return = "";

        if (q_type == "MC")
        {

            string ans1_d = ""; //display ans_1
            string ans2_d = "";
            string ans3_d = "";
            string ans4_d = "";
            string ans5_d = "";

            q_text = q_text.Replace("\\n", "<br>");

            str_return += "<b>Q:</b> " + q_text + "<br><br>";

            int ans_location = answer_choices.IndexOf(correct_ans);

            ans1_d = (ans_location == 0) ? " <b> " + ans_1 + " </b> " : ans_1;
            ans2_d = (ans_location == 1) ? " <b>" + ans_2 + " </b> " : ans_2;
            ans3_d = (ans_location == 2) ? " <b>" + ans_3 + " </b> " : ans_3;
            ans4_d = (ans_location == 3) ? " <b>" + ans_4 + " </b> " : ans_4;
            ans5_d = (ans_location == 4) ? " <b>" + ans_5 + " </b> " : ans_5;


            if (ans_1.Trim().Length > 0 && ans_1.Trim() != "-") str_return += " " + answer_choices.Substring(0, 1) + " :: " + ans1_d + "<br><br>";
            if (ans_2.Trim().Length > 0 && ans_2.Trim() != "-") str_return += " " + answer_choices.Substring(1, 1) + " :: " + ans2_d + "<br><br>";
            if (ans_3.Trim().Length > 0 && ans_3.Trim() != "-") str_return += " " + answer_choices.Substring(2, 1) + " :: " + ans3_d + "<br><br>";
            if (ans_4.Trim().Length > 0 && ans_4.Trim() != "-") str_return += " " + answer_choices.Substring(3, 1) + " :: " + ans4_d + "<br><br>";
            if (ans_5.Trim().Length > 0 && ans_5.Trim() != "-") str_return += " " + answer_choices.Substring(4, 1) + " :: " + ans5_d + "<br><br>";

            str_return += "<b>Answer: </b> " + correct_ans + "<br><br>";
            str_return += "<b>Solution: </b> " + solution + "<br><br>";
        }
        else if (q_type == "FIB")
        {



            q_text = q_text.Replace("\\n", "<br>");

            str_return += "<b>Q:</b> " + q_text + "<br><br>";


            str_return += "<b>Answer: </b> " + fib_pre + fib_ans + fib_post + "<br><br>";
            str_return += "<b>Solution: </b> " + solution + "<br><br>";
        }

        else if (q_type == "MFIB")
        {
            string temp1 = q_text;
            int i = 0;
            while ((i = temp1.IndexOf("{FIB", i)) != -1)
            {

                var startString = temp1.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                string inputtxt = "<input type='text' style='width:" + s.Split(':')[1] + "px' class='mfib' />";
                temp1 = temp1.Replace("{" + s + "}", inputtxt);
                i++;
            }

            str_return = temp1 + "<br> Correct answer = " + fib_ans ;

        }

        return str_return;

    }

    public string type2_q_errors()
    {
        string str_return = "";

        if (correct_ans.Length < 1) str_return += "missing Correct Ans <br>";

        if (answer_choices.Length < 1) str_return += "missing Answer Choices  <br>";

        if (!answer_choices.Contains(correct_ans)) str_return += "Ans Choice Diff than answer choices  <br>";



        return str_return;

    }

    public string view_type2_q_full()
    {
        string str_return = "";

        str_return += view_type2_q();

        str_return += "<b>Batch ID: </b> " + batch_id + " .. " + "<b>Src Main </b> " + src_main + " .. <br><br>";

        str_return += src_url + " <br><br>";
        return str_return;

    }

    public string rendered_type2_q_withanswer(string include_ans)
    {

        // include_ans = 1 shows Q with answers filled on
        // include_ans = 0 shows Q with answers as blanks
        string str_return = "";
        
        if (q_type == "MFIB")
        {
            str_return = q_text;
            int i = 0;
            ArrayList ansls = fc_gen_utils.string_split_by_delimter(fib_ans, "|");
            int k = 0;

            while ((i = str_return.IndexOf("{FIB", i)) != -1)
            {
                string inputtxt = "";
                var startString = str_return.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                if (include_ans == "1")
                {
                    inputtxt = "<b style='background: green;color: white;'>" + ansls[k].ToString() + "</b> ";
                }
                else
                {
                     inputtxt = "<input type='text' style='width:" + s.Split(':')[1] + "px' class='mfib' />";
                }
                str_return = str_return.Replace("{" + s + "}", inputtxt);
                i++;
                k++;
            }

        }
        else if (q_type == "FIB")
        {
            try
            {
                str_return =  render_FIB(include_ans);
            }
            catch
            {
                str_return = "Rendering error in (FIB) " + type2_id + ": " + q_text;
            }
           

          
        }

        else if (q_type == "DD")
        {

            int i = 0;
            int k = 0;
            str_return = q_text;
            ArrayList ansls = fc_gen_utils.string_split_by_delimter(correct_ans, "|");
            while ((i = str_return.IndexOf('{', i)) != -1)
            {
                string inputtxt = "";
                var startString = str_return.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                if (include_ans == "1")
                {
                    inputtxt = "<b style='background: green;color: white;'>" + ansls[k].ToString() + "</b> ";
                }
                else
                {
                    inputtxt = "<select id=" + k + " answer='' onchange='return funchangeddanswer(this);' class='ddl '><option>Select</option>";
                    String[] ddldata = (s.Split(':')[1]).Split(',');
                    for (int j = 0; j < ddldata.Length; j++)
                    {
                        inputtxt = inputtxt + "<option>" + ddldata[j] + "</option>";

                    }
                    inputtxt = inputtxt + "</select>";

                }
                str_return = str_return.Replace("{" + s + "}", inputtxt);
                i++;
                k++;
            }
        }
        else if (q_type == "MCM")
        {
            try
            {
                str_return = render_MCM(include_ans);
            }
            catch
            {
                str_return = "Rendering error in (MCM) " + type2_id + ": " + q_text;
            }

        }

        else if (q_type == "MC")
        {
            try
            {
                str_return = render_MC(include_ans);
            }
            catch
            {
                str_return = "Rendering error in (MC) " + type2_id + ": " + q_text;
            }
        }
        return str_return;

    }

    public string render_FIB(string i_ans)
    {
        string str_return = "";

        str_return =  q_text + "<br><br>";
        //str_return += "<b>Answer: </b> " + fib_ans + "<br><br>";
     
        return str_return;
    }

    public string render_MC(string i_ans)
    {
        string str_return = "";

        str_return = q_text.Replace("\\n", "<br>");

        str_return += "<br>";

        if (i_ans == "1")
        {

            int ans_location = answer_choices.IndexOf(correct_ans);
            string cor_ans = "";
            switch (ans_location)
            {
                case 0:
                    cor_ans = " <b> " + ans_1 + " </b> ";
                    break;
                case 1:
                    cor_ans = " <b> " + ans_2 + " </b> ";
                    break;
                case 2:
                    cor_ans = " <b> " + ans_3 + " </b> ";
                    break;
                case 3:
                    cor_ans = " <b> " + ans_4 + " </b> ";
                    break;
                case 4:
                    cor_ans = " <b> " + ans_5 + " </b> ";
                    break;

            }

            str_return += "<b style='background: green;color: white;'>Answer: " + cor_ans + "</b> <br><br>";
        }
        else
        {
            

            int ans_location = answer_choices.IndexOf(correct_ans);

            string ans1_d = (ans_location == 0) ? " <b> " + ans_1 + " </b> " : ans_1;
            string ans2_d = (ans_location == 1) ? " <b>" + ans_2 + " </b> " : ans_2;
            string ans3_d = (ans_location == 2) ? " <b>" + ans_3 + " </b> " : ans_3;
            string ans4_d = (ans_location == 3) ? " <b>" + ans_4 + " </b> " : ans_4;
            string ans5_d = (ans_location == 4) ? " <b>" + ans_5 + " </b> " : ans_5;


            if (ans_1.Trim().Length > 0 && ans_1.Trim() != "-") str_return += " " + answer_choices.Substring(0, 1) + " :: " + ans1_d + "<br><br>";
            if (ans_2.Trim().Length > 0 && ans_2.Trim() != "-") str_return += " " + answer_choices.Substring(1, 1) + " :: " + ans2_d + "<br><br>";
            if (ans_3.Trim().Length > 0 && ans_3.Trim() != "-") str_return += " " + answer_choices.Substring(2, 1) + " :: " + ans3_d + "<br><br>";
            if (ans_4.Trim().Length > 0 && ans_4.Trim() != "-") str_return += " " + answer_choices.Substring(3, 1) + " :: " + ans4_d + "<br><br>";
            if (ans_5.Trim().Length > 0 && ans_5.Trim() != "-") str_return += " " + answer_choices.Substring(4, 1) + " :: " + ans5_d + "<br><br>";

            //str_return += "<b>Answer: </b> " + correct_ans + "<br><br>";
            //str_return += "<b>Solution: </b> " + solution + "<br><br>";

        }

        return str_return;
    }

    public string render_MCM(string i_ans)
    {
        string str_return = "";

        string strAnswerType = answer_choices;
        str_return += q_text;
        str_return += "<br>";
        //    str_return += "<b style='background: green;color: white;'>Answer: </b> <br>";
        ArrayList ansls = fc_gen_utils.string_split_by_delimter(correct_ans, "|");
        for (int i = 0; i < ansls.Count; i++)
        {
            string cans = ansls[i].ToString();
            if (i_ans == "1")
            {
                int ans_location = answer_choices.IndexOf(cans);
                string cor_ans = "";
                switch (ans_location)
                {
                    case 0:
                        cor_ans = " <b> " + ans_1 + " </b> ";
                        break;
                    case 1:
                        cor_ans = " <b> " + ans_2 + " </b> ";
                        break;
                    case 2:
                        cor_ans = " <b> " + ans_3 + " </b> ";
                        break;
                    case 3:
                        cor_ans = " <b> " + ans_4 + " </b> ";
                        break;
                    case 4:
                        cor_ans = " <b> " + ans_5 + " </b> ";
                        break;

                }
                str_return += "<b style='background: green;color: white;'>" + cor_ans + "</b> <br>";
            }
            else
            {

                string chkoption = "";
                if (ans_1 != "" && ans_1 != "-")
                {
                    chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[0].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[0].ToString() + "' >" + ans_1 + "</label></div> ";

                }
                if (ans_2 != "" && ans_2 != "-")
                {
                    chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[1].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[1].ToString() + "' >" + ans_2 + "</label></div> ";


                }
                if (ans_3 != "" && ans_3 != "-")
                {
                    chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[2].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[2].ToString() + "' >" + ans_3 + "</label></div> ";
                }
                if (ans_4 != "" && ans_4 != "-")
                {
                    chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[3].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[3].ToString() + "' >" + ans_4 + "</label></div> ";
                }
                if (ans_5 != "" && ans_5 != "-")
                {

                    chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[4].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[4].ToString() + "' >" + ans_5 + "</label></div> ";
                }
                str_return += chkoption;


            }

        }
       
        return str_return;
    }


    public string rendered_type2_q()
    {

        // deprecated 2/2/18 - functionality included in  rendered_type2_q_withanswer(include_ans=0)
        string str_return = "";

        string ans1_d = ""; //display ans_1
        string ans2_d = "";
        string ans3_d = "";
        string ans4_d = "";
        string ans5_d = "";

        if (q_type != "MCM")
        {
            str_return = q_text;
        }


        if (q_type == "MFIB")
        {
            int i = 0;
            while ((i = str_return.IndexOf("{FIB", i)) != -1)
            {

                var startString = str_return.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                string inputtxt = "<input type='text' style='width:" + s.Split(':')[1] + "px' class='mfib' />";
                str_return = str_return.Replace("{" + s + "}", inputtxt);
                i++;
            }

        }
        else if (q_type == "DD")
        {
            int i = 0;
            int k = 0;
            while ((i = str_return.IndexOf('{', i)) != -1)
            {

                var startString = str_return.Substring(i + 1);
                string s = (startString).Substring(0, startString.IndexOf("}"));
                string inputtxt = "<select id=" + k + " answer='' onchange='return funchangeddanswer(this);' class='ddl '><option>Select</option>";
                String[] ddldata = (s.Split(':')[1]).Split(',');
                for (int j = 0; j < ddldata.Length; j++)
                {
                    inputtxt = inputtxt + "<option>" + ddldata[j] + "</option>";

                }
                inputtxt = inputtxt + "</select>";
                str_return = str_return.Replace("{" + s + "}", inputtxt);
                i++;
                k++;
            }
        }
        else if (q_type == "MCM")
        {
            string strAnswerType = answer_choices;
            string chkoption = "";
            if (ans_1 != "" && ans_1 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[0].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[0].ToString() + "' >" + ans_1 + "</label></div> ";

            }
            if (ans_2 != "" && ans_2 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[1].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[1].ToString() + "' >" + ans_2 + "</label></div> ";


            }
            if (ans_3 != "" && ans_3 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[2].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[2].ToString() + "' >" + ans_3 + "</label></div> ";
            }
            if (ans_4 != "" && ans_4 != "-")
            {
                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[3].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[3].ToString() + "' >" + ans_4 + "</label></div> ";
            }
            if (ans_5 != "" && ans_5 != "-")
            {

                chkoption = chkoption + "<div class='checkbox'><span> " + strAnswerType[4].ToString() + " </span><label><input type = 'checkbox' value='" + strAnswerType[4].ToString() + "' >" + ans_5 + "</label></div> ";
            }
            str_return = chkoption;

        }



        else if (q_type == "MC")
        {

            q_text = q_text.Replace("\\n", "<br>");

            str_return += "<b>Q:</b> " + q_text + "<br><br>";

            int ans_location = answer_choices.IndexOf(correct_ans);

            ans1_d = (ans_location == 0) ? " <b> " + ans_1 + " </b> " : ans_1;
            ans2_d = (ans_location == 1) ? " <b>" + ans_2 + " </b> " : ans_2;
            ans3_d = (ans_location == 2) ? " <b>" + ans_3 + " </b> " : ans_3;
            ans4_d = (ans_location == 3) ? " <b>" + ans_4 + " </b> " : ans_4;
            ans5_d = (ans_location == 4) ? " <b>" + ans_5 + " </b> " : ans_5;


            if (ans_1.Trim().Length > 1) str_return += " " + answer_choices.Substring(0, 1) + " :: " + ans1_d + "<br><br>";
            if (ans_2.Trim().Length > 1) str_return += " " + answer_choices.Substring(1, 1) + " :: " + ans2_d + "<br><br>";
            if (ans_3.Trim().Length > 1) str_return += " " + answer_choices.Substring(2, 1) + " :: " + ans3_d + "<br><br>";
            if (ans_4.Trim().Length > 1) str_return += " " + answer_choices.Substring(3, 1) + " :: " + ans4_d + "<br><br>";
            if (ans_5.Trim().Length > 1) str_return += " " + answer_choices.Substring(4, 1) + " :: " + ans5_d + "<br><br>";

            str_return += "<b>Answer: </b> " + correct_ans + "<br><br>";
            str_return += "<b>Solution: </b> " + solution + "<br><br>";


        }
        return str_return;

    }

}


public class o_concept
{

    public int concept_id;
    public string version;
    public string obj_type;
    public string ver_date;
    public string title;
    public string body;
    public string keywords;
    public string anchor_to;
    public string anchor_id;
    public string entry_at;
    public string entry_by;
    public string lastmod_at;
    public string lastmod_by;
    public string status;
    public string subject;
    public string batch_id;
    public string cc_level;
    public string cc0;
    public string cc1;
    public string cc2;
    public string cc3;
    public string cc4;


    public o_concept(string i_concept_id)
    {
        this.version = "1.1";
        this.obj_type = "concept";
        this.ver_date = "2/12/2017";


        string str_sql = "Select * from concepts where concept_id = " + i_concept_id;



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
        foreach (DataRow dtr in dt1.Rows)
        {
            set_constructor(dtr);

        }
    }

    public o_concept(DataRow dtr)
    {

        set_constructor(dtr);

    }

    public void set_constructor(DataRow dtr)
    {
        // Concept base constructor

        this.concept_id = Int32.Parse(dtr["concept_id"].ToString());
        this.title = dtr["title"].ToString();
        this.body = dtr["body"].ToString();
        this.keywords = dtr["keywords"].ToString();
        this.anchor_to = dtr["anchor_to"].ToString();
        this.anchor_id = dtr["anchor_id"].ToString();
        this.entry_at = dtr["entry_at"].ToString();
        this.entry_by = dtr["entry_by"].ToString();
        this.lastmod_at = dtr["lastmod_at"].ToString();
        this.lastmod_by = dtr["lastmod_by"].ToString();
        this.status = dtr["status"].ToString();
        this.subject = dtr["subject"].ToString();
        this.batch_id = dtr["batch_id"].ToString();
        this.cc_level = dtr["cc_level"].ToString();
        this.cc0 = dtr["cc0"].ToString();
        this.cc1 = dtr["cc1"].ToString();
        this.cc2 = dtr["cc2"].ToString();
        this.cc3 = dtr["cc3"].ToString();
        this.cc4 = dtr["cc4"].ToString();




    }

    public string view_concept()
    {
        string str_return = "";




        str_return += title + "<br><br>"
                    + body + "<br>";



        return str_return;

    }
}


public class o_fc
{

    public string fc_id;
    public string fc_term;
    public string fc_desc;
    public string fc_explanation;
    public string entry_by;
    public string entry_at;
    public string lastmod_by;
    public string lastmod_at;
    public string status;
    public string order_id;
    public string src_id;
    public string src_type;
    public string src_url;
    public string batch_id;
    public string anchor_id;
    public string anchor_to;

    public o_fc()
    {


    }

    public o_fc(DataRow dtr1)
    {

        set_constructor(dtr1);




    }

    public void set_constructor(DataRow dtr)
    {

        this.fc_id = dtr["fc_id"].ToString();
        this.fc_term = dtr["fc_term"].ToString();
        this.fc_desc = dtr["fc_desc"].ToString();
        

    }

    public string view_fc()
    {
        string str_return = "";



        str_return += "<b>" + fc_term + "</b>: "
                     + fc_desc + "<br>"
                   ;

        return str_return;

    }


}

public class o_word
{

    public int word_id;
    public string word_text;
    public string root_word;
    public string correct_spell;
    public int freq;
    public string is_root;
    public string word_type;


    public o_word(string i_word_id)
    {
        string str_sql = "Select * from words where word_id = " + i_word_id;
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
        foreach (DataRow dtr in dt1.Rows)
        {
            set_constructor(dtr);
        }
    }

    public o_word(DataRow dtr)
    {

        set_constructor(dtr);

    }


    public void set_constructor(DataRow dtr)
    {


        this.word_id = Int32.Parse(dtr["word_id"].ToString());
        this.freq = Int32.Parse(dtr["freq"].ToString());
        this.word_text = dtr["word_text"].ToString();
        this.root_word = dtr["root_word"].ToString();
        this.correct_spell = dtr["correct_spell"].ToString();
        this.is_root = dtr["is_root"].ToString();
        this.word_type = dtr["word_type"].ToString();

    }



    public string view_word()
    {

        string str_return = "";

        return str_return;

    }

}

public class o_quiz_summary
{
    public int id;
    public string token;
    public string cnt_correct;
    public string cnt_wrong;
    public string cnt_total;


    public o_quiz_summary(string i_token)
    {
        string str_sql = "Select * from st_answer  where quiz_token = '" + i_token + "'";



        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        int cnt_correct = 0;
        int cnt_wrong = 0;
        int cnt_total = 0;


        foreach (DataRow dtr in dt1.Rows)
        {

            if (dtr["correct"].ToString() == "1")
            {
                cnt_correct++;
            }
            else
            {
                cnt_wrong++;
            }
            cnt_total++;

            this.cnt_correct = cnt_correct.ToString();
            this.cnt_wrong = cnt_wrong.ToString();
            this.cnt_total = cnt_total.ToString();
        }

    }


}


public class o_quiz_param
{
    public string query;
    public string quiz_start_type;
    public string quiz_start_location;
    public string start_time;
    public string last_activity_time;
    public string quiz_code;
    public string quiz_q_string;
    public string quiz_title;
    public int quiz_subject;
    public bool NeedReview{ get; set; }



    public o_quiz_param()
    {
     this.query = "";
     this.quiz_start_type = "";
     this.quiz_start_location = "";
     this.start_time = "";
     this.last_activity_time = "";
     this.quiz_code = "";
     this.quiz_q_string = "";
     this.quiz_title = "";
     this.quiz_subject = 0;
     this.NeedReview = false;
    }

    public void set2session()
    {
        HttpContext.Current.Session["cqz_query"] = this.query;
        HttpContext.Current.Session["cqz_quiz_start_type"] = this.quiz_start_type;
        HttpContext.Current.Session["cqz_quiz_start_location"] = this.quiz_start_location;
        HttpContext.Current.Session["cqz_start_time"] = this.start_time;
        HttpContext.Current.Session["cqz_last_activity_time"] = this.last_activity_time;
        HttpContext.Current.Session["cqz_quiz_code"] = this.quiz_code;
        HttpContext.Current.Session["cqz_quiz_q_string"] = this.quiz_q_string;
        HttpContext.Current.Session["cqz_quiz_title"] = this.quiz_title;
        HttpContext.Current.Session["cqz_quiz_subject"] = this.quiz_subject.ToString();
        HttpContext.Current.Session["NeedReview"] = this.NeedReview.ToString();
        
    }

}



public class o_fcr_param
{

    // fcr FC Review
    public string fcr_query;
    public string fcr_start_type;
    public string fcr_start_location;
    public string fcr_start_time;
    public string fcr_last_activity_time;
    public string fcr_fc_id_string;
    public string fcr_code;
    public string fcr_title;
    public int fcr_subject;

    public o_fcr_param()
    {
        this.fcr_query = "";
        this.fcr_start_type = "";
        this.fcr_start_location = "";
        this.fcr_start_time = "";
        this.fcr_last_activity_time = "";
        this.fcr_fc_id_string = "";
        this.fcr_code = "";
        this.fcr_title = "";
        this.fcr_subject = 0;
       

    }

    public void set2session()
    {
        HttpContext.Current.Session["fcr_query"] = this.fcr_query;
        HttpContext.Current.Session["fcr_start_type"] = this.fcr_start_type;
        HttpContext.Current.Session["fcr_start_location"] = this.fcr_start_location;
        HttpContext.Current.Session["fcr_start_time"] = this.fcr_start_time;
        HttpContext.Current.Session["fcr_last_activity_time"] = this.fcr_last_activity_time;
        HttpContext.Current.Session["fcr_fc_id_string"] = this.fcr_fc_id_string;
        HttpContext.Current.Session["fcr_code"] = this.fcr_code;
        HttpContext.Current.Session["fcr_title"] = this.fcr_title;
        HttpContext.Current.Session["fcr_subject"] = this.fcr_subject.ToString();

    }

}





