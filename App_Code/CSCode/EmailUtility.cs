﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for EmailUtility
/// </summary>
public class EmailUtility
{
    public EmailUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void SendMail(string toemail, string ccEmail, string subject, string body)
    {
        try
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.tips4spelling.com");
            string from = "support@tips4spelling.com";
            mail.From = new MailAddress(from);
            mail.To.Add(toemail);
            mail.Subject = subject;
            mail.BodyEncoding = Encoding.UTF8;
            mail.SubjectEncoding = Encoding.UTF8;

            var htmlView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            var textView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Plain);

            mail.AlternateViews.Add(htmlView);
            mail.IsBodyHtml = true;
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(from, "Bruins$123");
            SmtpServer.EnableSsl = false;

            SmtpServer.Send(mail);
        }
        catch (Exception ex)
        {
            throw ex;
        }


    }

    
}