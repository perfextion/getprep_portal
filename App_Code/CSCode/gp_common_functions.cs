using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Text;

public static class gp_common_functions
    {


  public static  void update_quiz_summary()
    {

        string st_id = "";
        int cnt_total = 0;
        string duration = "";

        string str_sql = "select distinct(test_key) from omr_answers where test_key not in "
                 + " (select test_key from quiz_summary)";

        ArrayList ar_keys = gen_db_utils.gp_sql_get_arraylist(str_sql);

        foreach (string item_key in ar_keys)
        {
            string sql_prep = "  select count(*) as cnt_total, sum(duration) as duration , st_id  from omr_answers where test_key = '" + item_key + "'"
                              + " group by st_id ";

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(sql_prep);

            foreach (DataRow dtr1 in dt1.Rows)
            {
                st_id = dtr1["st_id"].ToString();
                cnt_total = Int32.Parse(dtr1["cnt_total"].ToString());
                duration = dtr1["duration"].ToString();

            }
            string str_ins = "insert into quiz_summary (test_key,st_id,cnt_total,duration) "
                + " values ( "
                + "'" + item_key + "',"
                + st_id + ","
                + cnt_total + ","
                + duration + ""
                + " ) ";

            gen_db_utils.gp_sql_execute(str_ins);


        }



       
        


    }


  public static string get_portal_menu(string role_id)
  {

      string str_return = "";
      if (role_id == "0")
      {
          str_return += "<ul class='nav-links nav navbar-nav'>" +
                  "  <li class='dropdown active'><a href='javascript:void(0)' class='main-menu'>Account<span class='fa fa-angle-down icons-dropdown'></span></a>" +
                     "   <ul class='dropdown-menu edugate-dropdown-menu-1'>" +
                     "       <li><a href='index.html' class='link-page'>Payment</a></li>" +
                      "      <li><a href='homepage-02.html' class='link-page'>Subjects</a></li>" +
                       "     <li><a href='homepage-03.html' class='link-page'>Availability</a></li>" +
                       " </ul>" +
                    "</li>" +
                    
               " </ul>";
          return str_return;
      }
      str_return += "<ul class='nav-links nav navbar-nav'>" +
                    "  <li class='dropdown active'><a href='javascript:void(0)' class='main-menu'>Home<span class='fa fa-angle-down icons-dropdown'></span></a>" +
                       "   <ul class='dropdown-menu edugate-dropdown-menu-1'>" +
                       "       <li><a href='index.html' class='link-page'>Home page 01</a></li>" +
                        "      <li><a href='homepage-02.html' class='link-page'>Home page 02</a></li>" +
                         "     <li><a href='homepage-03.html' class='link-page'>Home page 03</a></li>" +
                         " </ul>" +
                      "</li>" +
                      "<li class='dropdown'><a href='javascript:void(0)' class='main-menu'>Courses<span class='fa fa-angle-down icons-dropdown'></span></a>" +
                       "   <ul class='dropdown-menu edugate-dropdown-menu-1'>" +
                        "      <li><a href='courses.html' class='link-page'>courses</a></li>" +
                         "     <li><a href='courses-detail.html' class='link-page'>course detail</a></li>" +
                          "    <li><a href='events.html' class='link-page'>events</a></li>" +
                           "   <li><a href='event-detail.html' class='link-page'>event detail</a></li>" +
                          "</ul>" +
                      "</li>" +
                      "<li class='dropdown'><a href='javascript:void(0)' class='main-menu'>gallery<span class='fa fa-angle-down icons-dropdown'></span></a>" +
                       "   <ul class='dropdown-menu edugate-dropdown-menu-1'>" +
                        "      <li><a href='gallery-3column.html' class='link-page'>gallery 3 column</a></li>" +
                         "     <li><a href='gallery-4column.html' class='link-page'>gallery 4 column</a></li>" +
                          "    <li><a href='gallery-masonry.html' class='link-page'>gallery masonry</a></li>" +
                         " </ul>" +
                      "</li>" +
                      "<li class='dropdown'><a href='javascript:void(0)' class='main-menu'>pages<span class='fa fa-angle-down icons-dropdown'></span></a>" +
                       "   <ul class='dropdown-menu edugate-dropdown-menu-1'>" +
                        "      <li><a href='categories.html' class='link-page'>categories</a></li>" +
                         "     <li><a href='profile-teacher.html' class='link-page'>profile teacher</a></li>" +
                          "    <li><a href='about-us.html' class='link-page'>about us</a></li>" +
                           "   <li><a href='login.html' class='link-page'>login</a></li>" +
                            "  <li><a href='Register.aspx' class='link-page'>register</a></li>" +
                             " <li><a href='404.html' class='link-page'>404 page</a></li>" +
                              "<li><a href='faq.html' class='link-page'>FAQ page</a></li>" +
                          "</ul>" +
                      "</li>" +

                      "<li><a href='contact.html' class='main-menu'>Contact</a></li>" +
                      "<li class='button-search'>" +
                       "   <p class='main-menu'><i class='fa fa-search'></i></p>" +
                      "</li>" +
                      "<div class='nav-search hide'>" +

                       "   <input type='text' placeholder='Search' class='searchbox' />" +
                        "  <button type='submit' class='searchbutton fa fa-search'></button>" +

                      "</div>" +
                 " </ul>";

      return str_return;
  }

  public static void LogButtonEvent(string ButtonId, string PageName)
  {
      string sql = "insert into z_comments (message,date_inserted) values ('" + PageName + "_" + ButtonId + "',getdate()) ";
      gen_db_utils.gp_sql_execute(sql);
  }

}