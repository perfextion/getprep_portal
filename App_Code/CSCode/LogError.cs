﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LogError
/// </summary>
public static class LogError
{
	 
    public static void Log(string Filename ,string ErrorMessage ,Exception InnerException,string UserId, string StackTrace="", string SQLQuery="",bool MailSend=false)
    {
        try
        {
             string InnerEx = InnerException != null ? InnerException.ToString() : "";
            string sql = "INSERT dbo.ErrorLog(Filename ,ErrorMessage ,InnerExpception, StackTrace, SQLQuery,CreatedDate,UserId)" +
                            "Values( '" + Filename + "' ,'" + ErrorMessage + "' ,'" + InnerEx + "', '" + StackTrace + "','" + SQLQuery.Replace("'","`") + "',GetDate(),'" + UserId + "');select @@identity";

            string LogId = gen_db_utils.gp_sql_scalar(sql);

            if (MailSend)
            {
              //  SendHelpMail(LogId, Filename, ErrorMessage, InnerEx);
            }
        }
        catch (Exception Ex)
        {
            //SendHelpMail("", "LogError/Log", Ex.Message, Ex.InnerException != null ? Ex.InnerException.ToString() : "");
        }

    }

    private static void SendHelpMail(string LogId, string Filename,string ErrorMessage,string InnerException)
    
    {
        string ToEmail = "support@getprep.com";
        string Subject = "There is an error occured - Error log id :" + LogId;
        string Body = "There is an error occured at - FileName" + Filename + " . <br/>" + 
                        "Error Message : " + ErrorMessage + 
                        "Inner Exception : " + InnerException;

        EmailUtility.SendMail(ToEmail, "", Subject, Body);


    }
}