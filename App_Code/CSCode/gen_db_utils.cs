using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class gen_db_utils
    {



    // GP db    
    

 public static void gp_sql_execute(string strsql)
 {
     base_sql_execute(strsql, "gp_conn");
 }

   public static string gp_sql_scalar(string strsql)
    {

        string str_return = base_sql_scalar(strsql, "gp_conn");

       
        return str_return;
    }

   public static string get_database_name()
   {
       string connStr = ConfigurationManager.ConnectionStrings["gp_conn"].ConnectionString;
      // string str_return = base_sql_scalar(strsql, "gp_conn");
       string[] arr = new string[6];
       arr = connStr.Split(';');
       string name = arr[1].ToString();
       string[] arr1 = new string[6];
       arr1 = name.Split('=');
       string dbname = arr1[1].ToString();

       return dbname;
   }



   public static Hashtable gp_sql_get_hashtable(string strsql)
   {
       Hashtable x_hash = new Hashtable();

       x_hash = base_sql_get_hashtable(strsql, "gp_conn");
       return x_hash;



   }

 public static ArrayList gp_sql_get_arraylist(string strsql)
 {
     ArrayList x_arraylist = new ArrayList();

     x_arraylist = base_sql_get_arraylist(strsql, "gp_conn");
     return x_arraylist;



 }

 public static DataTable gp_sql_get_datatable(string strsql)
 {
     DataTable x_dt = new DataTable();

     x_dt = base_sql_get_datatable(strsql, "gp_conn");
     return x_dt;



 }
 


    // Base SQL classes 

public static string base_sql_scalar(string base_strsql, string dbconn)
        {
        SqlCommand cmdSelect;
        string str_return;

        string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
        SqlConnection sqlconDB = new SqlConnection(connStr);


        sqlconDB.Open();


        cmdSelect = new SqlCommand(base_strsql, sqlconDB);
        str_return = Convert.ToString(cmdSelect.ExecuteScalar());

        sqlconDB.Close();
        return str_return;
        }

public static void base_sql_execute(string base_strsql, string dbconn)
 {
     SqlCommand cmdInsert;

     string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
     SqlConnection sqlconDB = new SqlConnection(connStr);


     sqlconDB.Open();


     cmdInsert = new SqlCommand(base_strsql, sqlconDB);
     
     cmdInsert.ExecuteNonQuery();

     sqlconDB.Close();
 }


public static ArrayList base_sql_get_arraylist(string strsql, string dbconn)
{
    ArrayList temp_arraylist = new ArrayList();


    SqlCommand cmdSelect;
    SqlDataReader dt_rdr;
    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
    SqlConnection sqlconDB = new SqlConnection(connStr);


    sqlconDB.Open();



    cmdSelect = new SqlCommand(strsql, sqlconDB);
    dt_rdr = cmdSelect.ExecuteReader();

    while (dt_rdr.Read())
    {
        temp_arraylist.Add(dt_rdr[0].ToString());
    }

    dt_rdr.Close();
    sqlconDB.Close();
    return temp_arraylist;

}

public static Hashtable base_sql_get_hashtable(string strsql, string dbconn)
{
    Hashtable temp_hash = new Hashtable();


    SqlCommand cmdSelect;
    SqlDataReader dt_rdr;
    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
    SqlConnection sqlconDB = new SqlConnection(connStr);


    sqlconDB.Open();



    cmdSelect = new SqlCommand(strsql, sqlconDB);
    dt_rdr = cmdSelect.ExecuteReader();

    while (dt_rdr.Read())
    {
        temp_hash.Add(dt_rdr[0].ToString(), dt_rdr[1].ToString());
    }

    dt_rdr.Close();
    sqlconDB.Close();
    return temp_hash;



}

public static DataTable base_sql_get_datatable(string strsql, string dbconn)
{

    DataSet ds1 = new DataSet();
    DataTable dt1 = new DataTable();

    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;

    SqlConnection conDB = new SqlConnection(connStr);
    if (conDB.State == ConnectionState.Closed) { conDB.Open(); }
    SqlCommand cmdSelect = new SqlCommand(strsql, conDB);
    SqlDataAdapter adp = new SqlDataAdapter(cmdSelect);

    adp.Fill(ds1);
    if (conDB.State == ConnectionState.Open) { conDB.Close(); }
    dt1 = ds1.Tables[0];
    return dt1;
}


public static void base_sql_execute(string students)
{
    throw new NotImplementedException();
}

public static object base_sql_scalar()
{
    throw new NotImplementedException();
}
    }