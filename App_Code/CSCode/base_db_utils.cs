using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class base_db_utils
    {

  

  // Base MySQL classes 

public static void base_mysql_execute(string strsql, string dbconn )
{
    OdbcCommand cmdInsert;


    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
    OdbcConnection sqlconDB = new OdbcConnection(connStr);


    sqlconDB.Open();


    cmdInsert = new OdbcCommand(strsql, sqlconDB);
    cmdInsert.ExecuteNonQuery();

    sqlconDB.Close();
}

public static string base_mysql_scalar(string strsql, string dbconn)
{

    OdbcCommand cmdSelect;
    string str_return;

    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;
    OdbcConnection sqlconDB = new OdbcConnection(connStr);


    sqlconDB.Open();


    cmdSelect = new OdbcCommand(strsql, sqlconDB);
    str_return = Convert.ToString(cmdSelect.ExecuteScalar());

    sqlconDB.Close();
    return str_return;
}

public static DataTable base_mysql_get_datatable(string strsql, string dbconn)
{

    DataSet ds1 = new DataSet();
    DataTable dt1 = new DataTable();

    string connStr = ConfigurationManager.ConnectionStrings[dbconn].ConnectionString;


    OdbcConnection conDB = new OdbcConnection(connStr);
    if (conDB.State == ConnectionState.Closed) { conDB.Open(); }
    OdbcCommand cmdSelect = new OdbcCommand(strsql, conDB);
    OdbcDataAdapter adp = new OdbcDataAdapter(cmdSelect);



    adp.Fill(ds1);
    if (conDB.State == ConnectionState.Open) { conDB.Close(); }
    dt1 = ds1.Tables[0];
    return dt1;
}


public static string get_connstr()
{

    string str_return = "";

    if (HttpContext.Current.Application["current_db"].ToString() == "myed_stage")
    {
        str_return = " Data Source=flax.arvixe.com;Initial Catalog=myed_stage;User ID=;Password=; Max Pool Size = 200; Connection Lifetime=0";
    }
    else
    {
        str_return = "";
    }

    return str_return;
}

    }