using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Text;

public class gp_web_utils
    {


  public static string get_pf(string pf_key, string def_value)
    {

        string st_return = "";

        if (HttpContext.Current.Session[pf_key] == null)
        {
            st_return = def_value;
        }
        else
        {
            st_return = HttpContext.Current.Session[pf_key].ToString();
        }


        return st_return;

    }

  public static bool get_pf_bool(string pf_key, bool def_value)
  {

      bool st_return = false;

      if (HttpContext.Current.Session[pf_key] == null)
      {
          st_return = def_value;
      }
      else
      {
          if (HttpContext.Current.Session[pf_key].ToString() == "1")
          {
              st_return = true;
          }
          else
          {
              st_return = false;
          }
      }


      return st_return;

  }


  public static void load_correct_words2session()
  {


      string str_sql = "select word_text from words where is_correct = 1";

      ArrayList ar_words = gen_db_utils.gp_sql_get_arraylist(str_sql);

      HttpContext.Current.Session["correct_word_list"] = fc_gen_utils.arraylist2string(ar_words);

 

  }

  public static void load_common_words2session()
  {

      

      string str_sql = "select word_text from words where word_type = 1";

      ArrayList ar_words = gen_db_utils.gp_sql_get_arraylist(str_sql);

      HttpContext.Current.Session["common_word_list"] = fc_gen_utils.arraylist2string(ar_words);

   

  }

  public static ArrayList get_common_words()
  {

      ArrayList ar_temp = new ArrayList();

      if (HttpContext.Current.Session["common_word_list"] == null) load_common_words2session();

      string temp_list = HttpContext.Current.Session["common_word_list"].ToString();

      ar_temp = fc_gen_utils.string2arraylist(temp_list);

      return ar_temp;

  }

  public static ArrayList get_correct_words()
  {

      ArrayList ar_temp = new ArrayList();

      if (HttpContext.Current.Session["correct_word_list"] == null) load_correct_words2session();

      string temp_list = HttpContext.Current.Session["correct_word_list"].ToString();

      ar_temp = fc_gen_utils.string2arraylist(temp_list);

      return ar_temp;

  }



  public static string clean_text(string inp_string)
  {
      string str_return;

      str_return = Regex.Replace(inp_string, @"\[.*?\]", " "); // replace all occurences inside square brackets with space

      str_return = StripTagsCharArray(str_return); //  Replace all html tags <***>

      str_return = str_return.Replace("\\n", " "); // replace all occurences of \n  with space

      str_return = Regex.Replace(str_return, @"[^a-zA-Z\d-]", " "); // replace all non alphabet occurences  with space

      str_return = Regex.Replace(str_return, @"\s+", " "); // replace more than one space together  with single space


      return str_return;

  }


  public static string remove_common_words(string str_inp, ArrayList ar_common)
  {

      string str_return = str_inp;

      foreach (string item in ar_common)
      {
          str_return = str_return.Replace(" " + item + " " , " ");
      }

      str_return = Regex.Replace(str_return, @"\s+", " ");

      return str_return;
  }


  public static ArrayList get_misspelled_words(string str_inp, ArrayList ar_correct)
  {
      ArrayList ar_temp = new ArrayList();

      ArrayList ar_distinct_words = new ArrayList();

      ArrayList ar_sample = fc_gen_utils.string_split_by_delimter(str_inp, " ");

      foreach (string item in ar_sample)
      {
          if (!ar_distinct_words.Contains(item) && item.Length > 1) ar_distinct_words.Add(item);
      }


      foreach (string item1 in ar_distinct_words)
      {
          if (!ar_correct.Contains(item1.ToLower())) ar_temp.Add(item1);
      }

      

      return ar_temp;
  }


  public static string mark_text(string str_inp, ArrayList ar_mark_words)
  {

      string str_return = str_inp;

      foreach (string item in ar_mark_words)
      {
          str_return = str_return.Replace(" " + item + " ", " <mark>" + item + "</mark> ");
      }

      

      return str_return;
  }

  public static string overall_mark_text(string str_inp)
  {

      string str_return = str_inp;

      ArrayList ar_correct = gp_web_utils.get_correct_words();

      ArrayList ar_common = gp_web_utils.get_common_words();


      string clean_text = gp_web_utils.clean_text(str_return.ToLower());

      clean_text = gp_web_utils.remove_common_words(clean_text, ar_common);

      ArrayList ar_wrong = gp_web_utils.get_misspelled_words(clean_text, ar_correct);

      string marked_text = gp_web_utils.mark_text(str_inp, ar_wrong);



      return marked_text;
  }

  public static string StripTagsCharArray(string source)
  {
      //  Replace all html tags <***>

      char[] array = new char[source.Length];
      int arrayIndex = 0;
      bool inside = false;

      for (int i = 0; i < source.Length; i++)
      {
          char let = source[i];
          if (let == '<')
          {
              inside = true;
              continue;
          }
          if (let == '>')
          {
              inside = false;
              continue;
          }
          if (!inside)
          {
              array[arrayIndex] = let;
              arrayIndex++;
          }
      }
      return new string(array, 0, arrayIndex);
  }


}