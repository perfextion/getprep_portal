using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Text;
using Newtonsoft.Json;
 
public class gp_utils
    {

   

  public static string get_image_file(string i_book_id, string i_chap_id, string i_set_id)
    {
        string str_return = "";

        if (i_book_id.Substring(0, 1).ToLower() == "b") i_book_id = i_book_id.Substring(1);
        if (i_chap_id.Substring(0, 1).ToLower() == "c") i_chap_id = i_chap_id.Substring(1);
        if (i_set_id.Substring(0, 1).ToLower() == "s") i_set_id = i_set_id.Substring(1);

        str_return = "b" + i_book_id + "_c" + i_chap_id + "_s" + i_set_id;

        return str_return;


    }

  public static string get_iot(string inp_txt)
    {
        string str_return = "";

        if (inp_txt.Length > 4)
        {

            int str_length = inp_txt.Length;

            if (inp_txt.Substring(str_length - 4, 4) == ".png")
            {

                str_return = "<img src=\"" + inp_txt + "\">" + "<br>";
            }
            else
            {
                str_return = inp_txt;
            }
        }
        else if(inp_txt == null)
        {
            str_return = "";
        }
        else  
        {
            str_return = inp_txt;
        }
        return str_return;


    }

  public static string get_img_html(string i_file_name)
  {
      string str_return = "";

      str_return = " <img src =\"" + i_file_name + "\">";

      return str_return;


  }
    public static string html_jax(string inp_text_full)
    {

        string str_return = "";

        string txt_full = inp_text_full.Trim();

        if (txt_full.Contains("||"))
        {

            ArrayList blocks = fc_gen_utils.string_split_by_delimter(txt_full, "<end>");

            int Counter = 0;

            foreach (string item_block in blocks)
            {
                Counter++;
                try
                {
                    if (item_block.Trim().Length > 0)
                    {
                        str_return += html_jax_sub(item_block.Trim());
                    }
                }
                catch 
                {
                    str_return += "Error in Block" + Counter;
                }
            }
        }
        else
        {
            str_return = txt_full;
        }
        return str_return;
    }
    public static string html_jax_sub(string inp_txt)
    {
        string str_return = "";


        try
        {
            ArrayList lines = fc_gen_utils.string_split_by_delimter(inp_txt, "||");

            if (lines[0].ToString() == "basic")
            {
                str_return = inp_txt.Replace("basic||", "");

            }
            else if (lines[0].ToString() == "5col")
            {
                str_return = html_jax_5col(inp_txt.Replace("5col||", ""));

            }
            else if (lines[0].ToString() == "3col")
            {
                str_return = html_jax_3col(inp_txt.Replace("3col||", ""));

            }
        }
        catch
        {
            str_return = "error in html_jax" + "<br>" + inp_txt;
        }

            
        return str_return;

    }

    public static string html_jax_5col(string inp_txt)
    {
        string str_return = "";


        try
        {
            ArrayList lines = fc_gen_utils.string_split_by_delimter(inp_txt, "||");

            str_return += "<table>";
            foreach (string item_line in lines)
            {
                ArrayList pieces = fc_gen_utils.string_split_by_delimter_empties(item_line, "|");
                str_return += "<tr>";

                str_return += "<td align=right>" + pieces[0] + "</td>"
                              + "<td align=center>" + pieces[1] + "</td>"
                              + "<td align=left>" + pieces[2] + "</td>"
                              + "<td>" + pieces[3] + "</td>"
                              + "<td> <font  color=\"blue\">" + pieces[4] + "</font></td>";

                str_return += "</tr>";
            }
            str_return += "</table>";
        }
        catch
        {
            str_return = "error" + "<br>" + inp_txt;
        }
        return str_return;

    }

    public static string html_jax_3col(string inp_txt)
    {
        string str_return = "";


        try
        {
            ArrayList lines = fc_gen_utils.string_split_by_delimter(inp_txt, "||");

            str_return += "<table>";
            foreach (string item_line in lines)
            {
                ArrayList pieces = fc_gen_utils.string_split_by_delimter_empties(item_line, "|");
                str_return += "<tr>";

                str_return += "<td align=right>" + pieces[0] + "</td>"
                              + "<td align=center>" + pieces[1] + "</td>"
                              + "<td align=left>" + pieces[2] + "</td>";


                str_return += "</tr>";
            }
            str_return += "</table>";
        }
        catch
        {
            str_return = "error" + "<br>" + inp_txt;
        }
        return str_return;

    }


    public static void archive_object(string json)
    {
        string sql_ins = "";

        string obj_version = "";
        string obj_type_id = "";

        dynamic dynObj = JsonConvert.DeserializeObject(json);

        string obj_type = dynObj.obj_type;

        if (obj_type == "concept")
        {
HttpContext.Current.Session["obj_type"] = "111";
            obj_version = dynObj.version;
            obj_type_id = dynObj.concept_id;

        }
        else if (obj_type == "type2_q")
        {
            HttpContext.Current.Session["obj_type"] = "111";
            obj_version = dynObj.version;
            obj_type_id = dynObj.type2_id;

        }


        

        sql_ins = "insert into archived_objects (obj_type,obj_version,obj_type_id, obj_body, changed_by, changed_at) values "
                  + " ( "
                + "'" + obj_type+  "',"
                + "'" + obj_version+  "',"
                + "'" + obj_type_id+  "',"
                + "'" +fc_gen_utils.replace_quotes( json) +  "',"
                + "'" + "1" +  "',"
                + "'" + DateTime.Now.ToString() +  "'"
                + ")";
               

        HttpContext.Current.Session["sql_execute_last"]= sql_ins;

        gen_db_utils.gp_sql_execute(sql_ins);



    }


    public static void load_session_parent_cat()
    {
        string str_sql = "select id , parent_cat from core_cat";

        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

        foreach (DataRow dtr in dt1.Rows)
        {

            HttpContext.Current.Session["z_pc_" + dtr["id"].ToString()] = dtr["parent_cat"].ToString();
        }

        

       




    }

    public static ArrayList get_type2_blank_ids(string inp_count)
    {
        ArrayList ar_return = new ArrayList();
        
        string str_sql = "select top " + inp_count + " type2_id from type2_qs where status = -9999 order by newid() ";

        ar_return  = gen_db_utils.gp_sql_get_arraylist(str_sql);

        return ar_return;



    }
    public static void add_course(string inp_cc_id, string inp_st_id)
    {
        ArrayList ar_return = new ArrayList();



        string str_sql_cc1 = "select cc1 from core_cat where cc1 != -1 and cc0 = " + inp_cc_id;
        string str_sql_cc2 = "select cc2 from core_cat where cc2 != -1 and cc0 = " + inp_cc_id;

        string str_sql_existing = "select item_id from st_syllabus where st_id = " + inp_st_id;

        ArrayList ar_cc1 = gen_db_utils.gp_sql_get_arraylist(str_sql_cc1);
        ArrayList ar_cc2 = gen_db_utils.gp_sql_get_arraylist(str_sql_cc2);

        ArrayList ar_existing = gen_db_utils.gp_sql_get_arraylist(str_sql_existing);

        string sql_ins = "";

        if (!ar_existing.Contains(inp_cc_id)) sql_ins += "insert into st_syllabus (st_id, status, item_type, item_id)"
                                                   + " values ( "
                                                   + inp_st_id + ","
                                                   + "80,"
                                                   + "1,"
                                                   + inp_cc_id + ");";

        foreach (string item_cc1 in ar_cc1)
        {
            if (!ar_existing.Contains(item_cc1)) sql_ins += "insert into st_syllabus (st_id, status, item_type, item_id)"
                                                   + " values ( "
                                                   + inp_st_id + ","
                                                   + "80,"
                                                   + "1,"
                                                   + item_cc1 + ");";

        }

        foreach (string item_cc2 in ar_cc2)
        {
            if (!ar_existing.Contains(item_cc2)) sql_ins += "insert into st_syllabus (st_id, status, item_type, item_id)"
                                                   + " values ( "
                                                   + inp_st_id + ","
                                                   + "80,"
                                                   + "1,"
                                                   + item_cc2 + ");";

        }

        if (sql_ins.Length > 5) gen_db_utils.gp_sql_execute(sql_ins);

    }


}