﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;

using System.Drawing;


public class GoogleConnect
{
    public string ClientId = ConfigurationManager.AppSettings["GoogleClientId"];
    public string ClientSecret = ConfigurationManager.AppSettings["GoogleSecret"];

    public string RedirectURL = ConfigurationManager.AppSettings["GoogleUrlRedir"];
    public string GenerateGoogleOAuthURL(string state)
    {
        string Url = "https://accounts.google.com/o/oauth2/auth?scope={0}&redirect_uri={1}&response_type={2}&client_id={3}&state={4}&access_type={5}&approval_prompt={6}";

        string scope = _urlEncodeForGoogle("https://www.googleapis.com/auth/userinfo.email").Replace("%20", "+");
        string redirect_uri_encode = _urlEncodeForGoogle(this.RedirectURL);
        string response_type = "code";
        //string state = "profile";
        state = _urlEncodeForGoogle(state);
        string access_type = "offline";
        string approval_prompt = "force";


        return string.Format(Url, scope, redirect_uri_encode, response_type, this.ClientId, state, access_type, approval_prompt);
    }
    private string _urlEncodeForGoogle(string url)
    {
        string UnReservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

        StringBuilder result = new StringBuilder();

        foreach (char symbol in url)
        {
            if (UnReservedChars.IndexOf(symbol) != -1)
            {
                result.Append(symbol);
            }
            else
            {
                result.Append('%' + String.Format("{0:X2}", Convert.ToInt32(symbol)));
            }
        }

        return result.ToString();
    }

    public string GetEmail(string Accesstoken1)
    {

        string accessToken = GetValidAccessToken(Accesstoken1);
        //var jsonSerializer = new JavaScriptSerializer();
        //#Region "get google calendarID ----------------------------"
        //Dim calendar = GetGoogleCalendarInfo(refresh_token, accessToken, calendarID)

        //#End Region

        StringBuilder urlBuilder = new System.Text.StringBuilder();

        urlBuilder.Append("https://");
        urlBuilder.Append("www.googleapis.com/oauth2/v1/userinfo?alt=json");

        HttpWebRequest httpWebRequest__1 = HttpWebRequest.Create(urlBuilder.ToString()) as HttpWebRequest;

        httpWebRequest__1.CookieContainer = new CookieContainer();
        httpWebRequest__1.Headers["Authorization"] = string.Format("Bearer {0}", accessToken);

        WebResponse response1 = httpWebRequest__1.GetResponse();
        Stream receiveStream = response1.GetResponseStream();
        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
        string results = readStream.ReadToEnd();
        receiveStream.Close();
        receiveStream.Dispose();
        readStream.Close();
        readStream.Dispose();

        JavaScriptSerializer js = new JavaScriptSerializer();
        UserProfile objProfile = js.Deserialize<UserProfile>(results);
        string emailID = objProfile.email;
        return emailID;
    }
  
    public string OAuth(string Code)
    {
        string Url = "https://accounts.google.com/o/oauth2/token";
        string grant_type = "authorization_code";
        string redirect_uri_encode = _urlEncodeForGoogle(this.RedirectURL);
        string data = "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type={4}";

        HttpWebRequest request = HttpWebRequest.Create(Url) as HttpWebRequest;
        string result = null;
        request.Method = "POST";
        request.KeepAlive = true;
        request.ContentType = "application/x-www-form-urlencoded";
        string param = string.Format(data, Code, this.ClientId, this.ClientSecret, redirect_uri_encode, grant_type);
        Byte[] bs = Encoding.UTF8.GetBytes(param);
        using (Stream reqStream = request.GetRequestStream())
        {
            reqStream.Write(bs, 0, bs.Length);
            reqStream.Close();
        }

        using (WebResponse response = request.GetResponse())
        {
            StreamReader sr = new StreamReader(response.GetResponseStream());
            result = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();
        }

        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        GoogleTokenData tokenData = jsonSerializer.Deserialize<GoogleTokenData>(result);
        addToCache(tokenData.Refresh_Token, tokenData);
        return tokenData.Refresh_Token;
    }
    public string GetValidAccessToken(string refresh_token)
    {
        string access_token = "";

        try
        {
            string Url = "https://accounts.google.com/o/oauth2/token";
            string grant_type = "refresh_token";
            //string redirect_uri_encode = UrlEncodeForGoogle(ReturnUrl);
            string data = "refresh_token={0}&client_id={1}&client_secret={2}&grant_type={3}";

            HttpWebRequest request = HttpWebRequest.Create(Url) as HttpWebRequest;
            string result = null;
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentType = "application/x-www-form-urlencoded";
            string param = string.Format(data, refresh_token, this.ClientId, this.ClientSecret, grant_type);
            byte[] bs = Encoding.UTF8.GetBytes(param);
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
                reqStream.Close();
            }

            using (WebResponse response = request.GetResponse())
            {
                StreamReader sr = new StreamReader(response.GetResponseStream());
                result = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
            }

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            GoogleTokenData tokenData = jsonSerializer.Deserialize<GoogleTokenData>(result);
            access_token = tokenData.Access_Token;
            // Write code to handle exception
        }
        catch (Exception ex)
        {
        }
        return access_token;
    }

    private void addToCache(string refresh_token, GoogleTokenData tokenData)
    {
        TimeSpan slidingExpiration = new TimeSpan(0, 0, Convert.ToInt32(tokenData.Expires_In) - 30);
        HttpContext.Current.Cache.Add(refresh_token, tokenData.Access_Token, null, System.Web.Caching.Cache.NoAbsoluteExpiration, slidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
    }



    private class UserProfile
    {
        public string email
        {
            get { return m_email; }
            set { m_email = value; }
        }
        private string m_email;
    }
    private class GoogleTokenData
    {
        public string Access_Token
        {
            get { return m_Access_Token; }
            set { m_Access_Token = value; }
        }
        private string m_Access_Token;
        public string Refresh_Token
        {
            get { return m_Refresh_Token; }
            set { m_Refresh_Token = value; }
        }
        private string m_Refresh_Token;
        public string Expires_In
        {
            get { return m_Expires_In; }
            set { m_Expires_In = value; }
        }
        private string m_Expires_In;
        public string Token_Type
        {
            get { return m_Token_Type; }
            set { m_Token_Type = value; }
        }
        private string m_Token_Type;
    }

  

}
