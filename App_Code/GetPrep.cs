﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for GetPrep
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class GetPrep : System.Web.Services.WebService
{

    public GetPrep()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetClassScheduleCalendar(string studentid)
    {
        string str_sql = "select a.scs_id,pk_cs_id , title,convert(varchar(5),start_date,108) starttime, " +
       " convert(varchar(5),end_date,108) endtime, convert(varchar,start_date,120) " +
       " as start," +
       " Case when scs_id is not null then c.status_color else '#57889c' end as  backgroundColor," +
       " Case when scs_id is not null then c.status else 'Not Enrolled' end as [Status]," +
       " convert(varchar,end_date,120) as [end] " +
       " from student_class_schedule as a " +
       " join calendar_status as c on a.scs_status=c.status_id and " +
       " c.calendar_type='Student_CS_Calendar'" +
       " right join class_schedule as b on a.cs_id=b.pk_cs_id and a.st_id= " + studentid; 
        try
        {

           
            DataTable dtFCSetContents = gen_db_utils.gp_sql_get_datatable(str_sql);
            string result = gen_utils.DataTableToJSONWithJavaScriptSerializer(dtFCSetContents);

            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.Write(result);
        }
        catch (Exception Ex)
        {
            LogError.Log("Cs_Calendar.aspx/GetClassScheduleCalendar", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
        }

    }
    [WebMethod]     
    public string AddClassScheduleCalendar(string cs_id, string studentid)
     {
         string SQl = "insert  into  Student_class_schedule(st_id,cs_id,scs_status,enroll_date,lastmod_date) " +
                 "Values('" + studentid + "','" + cs_id + "','1',getdate(),getdate())"; 
         try
             { 
                
                 gen_db_utils.gp_sql_execute(SQl);
                
                 return "success";  
             }
         catch (Exception Ex)
         {
             LogError.Log("Cs_Calendar.aspx/AddClassScheduleCalendar", Ex.Message, Ex.InnerException, "", Ex.StackTrace, SQl, true);
             
           
         }
                 return "fail";
    }

    [WebMethod]    
    public string SaveStudentWelcomeInformation(string FirstName, string LastName, string CardNumber, string ExpiryDate, string Securitycode, string AddressStreet, string AddressAppartment, string City, string State, string ZipCode,string studentId)
    {
        string SQl = "update students(Payment_firstname='" + FirstName + "',Payment_lastname='" + LastName + "',Payment_cardnumber='" + CardNumber + "',Payment_expiryDate='" + ExpiryDate + "',Payment_securitycode='" + Securitycode + "',Billing_address_street='" + AddressStreet + "',Billing_address_Appartment='" + AddressAppartment + "',Billing_address_city='" + City + "',Billing_address_StateId='" + State + "',Billing_address_zipcode='" + ZipCode + "' where st_id= " + studentId;
          
        try
        {
              
            gen_db_utils.gp_sql_execute(SQl);           
            return "success";
        }
        catch (Exception Ex)
        {
            LogError.Log("Welcome.aspx/SaveStudentWelcomeInformation", Ex.Message, Ex.InnerException, "", Ex.StackTrace, SQl, true);
            return "fail";
        }

    }
    [WebMethod]     
    public string ChangeClassScheduleCalendar(string scs_id, string status)
    {
        string str_sql = "update student_class_schedule set scs_status='" + status + "' where scs_id=" + scs_id;    
        try
        {
                    
            DataTable dtFCSetContents = gen_db_utils.gp_sql_get_datatable(str_sql);

            return "success";
             
        }
        catch (Exception Ex)
        {
            LogError.Log("Cs_Calendar.aspx/ChangeClassScheduleCalendar", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
        }
        return "fail";
    }
    // [WebMethod]
    // [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string SignUp(string EmailAddress, string CreatePassword, string FirstName, string LastName, string ZipCode, string ddlabout, string ddldescribes, string ddlmaingoal)
    //{
    //    try
    //    {
    //        string SQl = "insert  into  students(username,password,f_name,l_name,e_mail,Zip_Code,hear_about_us,describes_you,main_goal,signup_date) " +
    //        "Values('" + EmailAddress + "','" + CreatePassword + "','" + FirstName + "','" + LastName + "','" + EmailAddress + "','" + ZipCode + "','" + ddlabout + "','" + ddldescribes + "','" + ddlmaingoal + "',getdate())" ;
    //        gen_db_utils.gp_sql_execute(SQl);
    //        //Context.Session["User_id"] = 
    //        return "success";
           
          
    //    }
    //    catch (Exception ex)
    //    {
    //        return "fail";
    //    }
    
    // }

    [WebMethod]
    public string SignUp(string EmailAddress, string CreatePassword, string FirstName, string LastName, string ZipCode, string ddlabout, string ddldescribes, string ddlmaingoal)
    {
        string SQl = "insert  into  students(username,password,f_name,l_name,e_mail,Zip_Code,hear_about_us,describes_you,main_goal,signup_date,IsVerified) " +
               "Values('" + EmailAddress + "','" + CreatePassword + "','" + FirstName + "','" + LastName + "','" + EmailAddress + "','" + ZipCode + "','" + ddlabout + "','" + ddldescribes + "','" + ddlmaingoal + "',getdate(),0);select @@identity as id";
        string st_id = "";
        string validatesql = "select top 1 st_id from students where username = '" + EmailAddress + "'";
        try
        {
          
            st_id = gen_db_utils.gp_sql_scalar(validatesql);
        }
        catch (Exception Ex)
        {
            LogError.Log("Registration.aspx/SignUp", Ex.Message, Ex.InnerException, "", Ex.StackTrace, validatesql, true);
           
        }
        try
        {
           
            if (st_id != null || st_id == "")
            {

                string id = gen_db_utils.gp_sql_scalar(SQl);
                EmailUtility.SendMail(EmailAddress, "", GetSignUpMailSubject(), GetSignUpMailBody(FirstName, LastName));
                return id;
            }
            else
            {
                return "0";
            }

        }
        catch (Exception Ex)
        {
            LogError.Log("Registration.aspx/SignUp", Ex.Message, Ex.InnerException, "", Ex.StackTrace, SQl, true);
        }
        return "fail";

    }
    private string GetSignUpMailBody(string FirstName, string LastName)
    {
        string body = "Dear " + FirstName + ", <br><br><br>"
                      + " Thank you for registering with GetPrep Academy. "
                      + " We want to thank you for registering with us and welcome you to GetPrep Academy. "
                      + " As a valued member of our Academy, we would like to extend you a free hour of tutoring session "
                      + " so that you can experience the quality of our services. <br><br> "


                     + " We want you to get the most out of GetPrep and if you have any questions or concerns, "
                     + " please don't hesitate to contact us. <br><br> "

                     + " Thank you for being a valued GetPrep Member. <br><br>"

                       + " GetPrep Customer Care ";


        return body;
    }

     private string GetSignUpMailSubject()
     {
         string subject = "Thank for registering with us!";

         return subject;
     }

     [WebMethod]
     [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
     public void GetAttendanceCalender(string studentid, string flag, string subject)
     {
         string filter = "";
         string str_sql = "select  goal as title,convert(varchar(5),chkin_time,108) starttime, " +
            " convert(varchar(5),chkout_time,108) endtime, convert(varchar,chkin_time,120) " +
            " as start," +
            " convert(varchar,chkout_time,120) as [end] ,comments,duration" +
            " from st_attendance " +
            " where st_id= " + studentid + " " + filter;
          
         try
         {
             DataTable data = new DataTable();

            
             if (flag != "ALL" || subject != "ALL")
             {
                 filter = " and goal='" + flag + "' or subjects='" + subject + "'";
             }
            
             data = gen_db_utils.gp_sql_get_datatable(str_sql);
             DataTable dtFCSetContents = gen_db_utils.gp_sql_get_datatable(str_sql);
             string result = gen_utils.DataTableToJSONWithJavaScriptSerializer(dtFCSetContents);
             this.Context.Response.ContentType = "application/json; charset=utf-8";
             this.Context.Response.Write(result);
         }
         catch (Exception Ex)
         {
             LogError.Log("Attendance.aspx/GetAttendanceCalender", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);

         }
     }

    [WebMethod]
    public string CaptureRegisterButtonEvent(string ButtonId, string PageName)
    {
        try
        {
            gp_common_functions.LogButtonEvent(ButtonId, PageName);
            return "pass";
        }
        catch (Exception Ex)
        {
            LogError.Log("Registration.aspx/SignUp", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
        }
        {
            return "fail";
        }

    }


}
