﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="gp_services.aspx.cs" Inherits="gp_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div id="wrapper-content">
        <!-- PAGE WRAPPER-->
        <div id="page-wrapper">
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <!-- CONTENT-->
                <div class="content">
                    <div class="section background-opacity page-title set-height-top">
                        <div class="container">
                            <div class="page-title-wrapper">
                                <!--.page-title-content-->
                                <h2 class="captions">Services & Programs</h2>
                               
                            </div>
                        </div>
                    </div>
                    <div class="group-title-index">
                      <!--    <h4 class="top-title">SomeText</h4> -->

                        <h2 class="center-title"></h2>

                        <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                    </div>
                    <div class="section intro-edu">
                        <div class="container"> 
                            <div class="intro-edu-wrapper">
                                <div class="row">
                                    <div class="top-courses-item">
                                        <div class="about-edugate-layout-2">
                                            <div class="about-edugate-layout-2-wrapper">
                                                <div class="about-edugate-content"> 
                                                    <div class="oval">
                                                        <img src="assets/images/blended.jpg" alt="" class="img-responsive" />
                                                    </div>
                                                    <p>
                                                        GetPrep offers carefully designed and timely sequence of pre-test prep review, honework help, and core concept review			 
													  sessions to aid students in understading concepts, providing sufficent practice, and reviewing core material
														so that they can excel in academics.   Students can simply sign up and attend the necessary sessions - with no long term contracts or commitments.  </p>
														
												   	  <p>GetPrep sessions are maintained in sync with each individual course progress at local schools 
														(EANES and Lake Travis ISD) thus facilitating students to receive necessary personalized 
														help exactly when they need it. Sessions are offered at convenient times (after school and during weekends) 
														at our learning center located within 15 minutes of driving distance for most students.
														
                                                    </p>



                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="gp-about-intro-content">
                                         
                                            <p>
                                              <b> Homework Help and Study Sessions - </b> <br> 
											  Students attending homework help and study sessions receive more than just  tips or solutions to complete 
											  the homework. They are provided with a quick review of the concepts and material necessary to complete that 
											  particular homework.  After the review, students are encouraged to solve the homework problems by themseleves. 
											  Tutors are available to answer any questions students may have while doing the homework.  This approach 
											  allows students to acquire the skills needed to apply their knowledge to solve test type problems.
											  Timely guidance from our well trained tutors minimizes the frustration in completing the homework at the same time
											  builds confidence in approaching and solving complex problems.
											  
                                            </p>
											
											<p>
                                              <b> Subject Review and Practice Sessions - </b> <br> 
											  These sessions are offered weekly to review  recently completed material in school and to provide additional practice 
											  with typical problems seen in school exams as well as AP/IB, SAT subject tests (where applicable).  Typically 1.5 to 2 hours in duration, 
											   these sessions include a 30 minute review of core concepts in each particular topic followed by hands on practice to apply and 
											   reinforce concepts learned. Instruction for these sessions is provided by highly qualified experts in the subject and follow on 
											   pratice is guided by 2 or 3 well trained junior tutors.  
	  
                                            </p>

											<p>
                                              <b> Online Learning and Practice - </b> <br> 
											  GetPrep provides a unique blended learnign approach in which students can learn, practice and assess themseleves in each course
											  using our proprietary online adaptive learning platform.  This online platform provides well curated course material in the form 
											  of flash cards, byte size snippets for core concepts, short assessments to diagnose strengths and weaknesses, and practice worksheets.
											  Students can leverage this platform at their own convenience (24/7) and the learning progress is captured and integrated with the offline 
											  offline activities at the GetPrep learning center. The personalized and adaptive features of the online platform allow the students 
											  to focus on reinforcing and reviewing specific skill gaps while preparing for quizzes / tests at school.
											 
	  
                                            </p>											
											
                                            <p>
                                              <b> 1-on-1 Tutoring and Acaemic Coaching - </b> <br> 
											  Significant portion of Students' academic needs can be accomplished by the broad array of online and offline sessions offered 
											  by GetPrep.  If any further fine tuning or coaching is needed periodically, students can sign up for a 1-on-1 tutoring or academic coaching 
											  session.  Our expereinced instructors and couselors can review the student's peformance and prograss, and provide guidance 
											  in terms of better study stratgies, time management techniques, exam preparation tips, etc.  <br><br>
											  
											  Students can also use these sessions to get personalized tutoring for a few complex concepts that are particularly difficult to grasp. On an ongoing basis,
											  our experienced counselors can also guide students in choosing the right course work, extra curricular activities, and project work to prepare
											  them on the journey toward their dream college.
											 
	  
                                            </p>
                                            <p>
                                                For a FREE Consultation on any of our services and programs, please contact us at (512) 501-3690. 
                                        We are available to discuss how we can develop a customized plan to help your child achieve success.
                                            </p>
                                       
                                    </div>
                                </div>
                            </div>
                            
                             <img src="assets/images/gp_Services.jpg" alt="services" style="max-width:100% !important" />
                    </div>
                </div>
                 
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>


                    