﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="coursedetail.aspx.cs" Inherits="Portal_coursedetail" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .progress {
            width: 90%;
            background: #d3dce2;
            height: 18px;
            margin: 15px;
        }

        .html {
            width: 0;
            background: #50bcb6;
            color: #231f1f;
            height: 18px;
            line-height: 18px;
            padding: 0 1px;
        }
    </style>
    <div class="courses-detail-wrapper col-12">
        <div class="row">
            <div class="col-md-12 layout-left">
                <div class="course-syllabus">
                    <div class="course-syllabus-title underline">Courses syllabus</div>
                    <asp:Button ID="btnCircullim" runat="server" CssClass="btn btn-login btn-green" Text="Change Curriculum" OnClick="btnCircullim_Click" />
                    &nbsp;  &nbsp;&nbsp; &nbsp;  &nbsp;&nbsp; &nbsp;  &nbsp;&nbsp; &nbsp;  &nbsp;&nbsp; &nbsp;  &nbsp;&nbsp; &nbsp;  &nbsp;&nbsp;
                    <b>
                        <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
                    <div class="course-table" id="div" runat="server">
                        <div class="outer-container">
                            <div class="inner-container">
                                <div class="table-header">
                                    <table class="edu-table-responsive">
                                        <thead>
                                            <tr class="heading-table">
                                                <th class="col-6">title</th>
                                                <th class="col-4"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="table-body">
                                    <asp:Label ID="lb1" runat="server"></asp:Label>
                                </div>
                                <asp:Label ID="lbl" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Practice_cc1(id) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funPractice_cc1",
                data: '{id: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }

        function Practice(id, level) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funPractice",
                data: '{id: "' + id + '" ,level : "' + level + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function Review(id, level) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funReview",
                data: '{id: "' + id + '" ,level : "' + level + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnReviewSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function OnPracticeSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;

        }
        function OnReviewSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;
        }
        function animateProgressBar(el, width) {
            el.animate(
                { width: width },
                {
                    duration: 2000,
                    step: function (now, fx) {
                        if (fx.prop == 'width') {
                            el.html(Math.round(now * 100) / 100 + '%');
                        }
                    }
                }
            );
        }
        $('.progress').each(function () {
            if ($(this).data("width") != "0%") {
                animateProgressBar($(this).find("div"), $(this).data("width"))
            }
            else {
                $(this).find("div").html('0%');
            }
        });
    </script>
</asp:Content>
