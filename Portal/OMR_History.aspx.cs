﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Portal_OMR_History : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Forms";
        if (!IsPostBack)
        {
            get_Forms();
        }
    }

    public void get_Forms()
    {
        string strForms = string.Empty;
        lbl_top.Text = string.Empty;
        strForms = @"select  f.form_id,f.form_no,a.test_key, min(answered_at) as answeredat,
                 count(a.test_key) as total_qn_cnt, isnull(sum(a.correct), 0) as correct_qn_cnt
                   from omr_form as f left join omr_answers as a on a.form_id = f.form_id and a.st_id = " + Session["UserID"] + @" where test_key is not null
                      group by f.form_id, f.form_no, a.test_key order by max(a.answered_at) desc";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            if (dt != null && dt.Rows.Count > 0)
            {
                var distinctValues = dt.AsEnumerable().Select(row => new { grp1 = row.Field<string>("grp1") }).Distinct();
                lbl_top.Text += generate_Grid(dt);
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_History.aspx/get_Forms", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    public string generate_Grid(DataTable dt)
    {
        string str_return = "<div class='col-md-12 m-1'><a href='./Form_Listing_GroupByNew.aspx' class='btn btncolor'><< Back</a></div>" +
            "<table id='datatable_fixed_column' class='table-hover table table-striped table-bordered'> "
                                 + "<thead>"
                                    + "  <tr> "
                                         + " <th >Form</th> "
                                         + " <th >Test Taken at</th> "
                                         + " <th >Score</th> "
                                         + " <th >Action</th> "
                                         + " <th >Action</th> "

                                       + "</tr>"
                                + "  </thead> "
                                  + " <tbody>";
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow r in dt.Rows)
            {
                str_return += "<tr>" +
                                "<td>" + r["form_no"] + "</td>" +
                                "<td>" + r["answeredat"] + "</td>" +
                                "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? (r["correct_qn_cnt"] + "/" + r["total_qn_cnt"]) : "-") + "</td> " +
                                 "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? "<a href='/Portal/OMR_Summary.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>Summary</a>" : "-") + " </td>" +
                                "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? "<a href='/Portal/OMR_Results.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>OMR Result</a>" : "<a href='form_confirmation.aspx?form_no=" + r["form_id"] + "'>Select</a>") + " </td>" +
                             "</tr>";
            }
            str_return += " </tbody> "
         + " </table> ";

        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }
}