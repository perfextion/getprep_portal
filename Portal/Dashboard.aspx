﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Portal_Dashboard" ValidateRequest="false" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="  underline">
        <span>Welcome to Getprep Academy</span><br />
        <asp:Label ID="lblUserName" runat="server"></asp:Label><br />
        <asp:Label ID="lblDate" runat="server"></asp:Label>
        <span style="float: right">
            <asp:Label ID="lblCheckin" runat="server"></asp:Label></span>
    </div>
    <b>
        <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>

    <div class="row">
        <div class="col-sm-5">

            <div class="panel panel-default " style="cursor: pointer">
                <div class="panel-body" style="text-align: center;">
                    <div class="row">
                        <div class="col-md-6">
                            <button style="background: none; border: none" id="btnCheckIn" onserverclick="btnCheckIn_Click" runat="server">

                                <i class="fa fa-sign-in" style="font-size: 24px; color: black" onclick="return StudentCheckin()"></i>
                                <br />
                                <b>Center
                            <br />
                                    CHECK IN </b>
                            </button>
                            <div class="row" style="background: none; border: none">

                                <i class="fa fa-clock-o " style="font-size: 50px;" runat="server" id="iClock">
                                    <br />
                                    <asp:Label ID="lblTimeSpent" Style="font-size: 20px;" runat="server">
                          
                                    </asp:Label></i>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row" style="margin-top: 5px;">
                                <button type="button" style="background: none; border: none" id="btnActivity" runat="server" onclick="return btnActivity_Click();">
                                    <i class="fa fa-comment" style="font-size: 24px; color: green;">Activity</i>
                                </button>
                            </div>
                            <div class="row" style="margin-top: 40px;">
                                <button style="background: none; border: none" id="btnCheckOut" runat="server" onclick="return btnChkOut()">
                                    <i class="fa fa-sign-out" style="font-size: 24px; color: red;">Check Out</i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default" style="cursor: pointer">
                <div class="panel-body" style="text-align: center;">
                    <b><i class="fa fa-area-chart" style="font-size: 24px"></i>
                        <br />
                        STUDENT<br />
                        ACTIVITY   </b>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default" style="cursor: pointer">
                <div class="panel-body" style="text-align: center;">
                    <b><i class="fa fa-user-circle-o" style="font-size: 24px"></i>
                        <br />
                        STUDENT<br />
                        PROFILE  </b>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="DivModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Activity</h3>
                </div>

                <div class="modal-body">
                    <label>Time spent at the Center :</label>
                    <asp:Label ID="lblTime" Style="font-size: 15px;" runat="server">
                         2 Hours
                    </asp:Label>
                    <div class="form-group">
                        <label>What did you work on today?</label>
                        <textarea class="form-control" id="txtComment" runat="server"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button OnClick="btnCheckOut_Click" CssClass="btn btn-login btn-green" ID="btnchkOut" runat="server" Text="Save & CheckOut" />
                    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-login btn-green" runat="server" Text="Save" />
                </div>
            </div>
        </div>

    </div>



    <div class="row">

        <div class="col-sm-12">
            <div class="row">

                <div class="panel panel-default " style="background-color: white">
                    <div class="panel-heading">
                        <h3 class="panel-title">My Courses
                    <span class="glyphicon glyphicon-arrow-right pull-right"></span></h3>
                    </div>
                    <div class="panel-body ">

                        <asp:Label ID="lblCourses" runat="server">

                        </asp:Label>
                        <asp:TextBox ID="lblCatID" runat="server" CssClass="CatID" Style="display: none"></asp:TextBox>

                        <asp:ListView ID="lstCourses" runat="server">
                            <ItemTemplate>

                                <fieldset>
                                    <legend>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <a href="coursedetail.aspx?courseid=<%# Eval("id") %>">
                                                    <span style="font-size: large"><%# Eval("cat_name") %></span>
                                                </a>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label runat="server" ID="lblCat" Style="display: none"></asp:Label>
                                                <!--<a href='/portal/lessons.aspx?courseid=<%# Eval("id") %>'>Practice</a>-->
                                                <a href='javascript:void(0)' onclick="Practice(<%# Eval("id") %>, 0)">Practice</a>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label runat="server" ID="Label1" Style="display: none"></asp:Label>
                                                <a href='javascript:void(0)' onclick="ReviewPractice(<%# Eval("id") %>, 0)" title="Review Questions">
                                                    <i class="glyphicon glyphicon-repeat"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40"
                                                        aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                        40% Complete (success)
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </legend>


                                </fieldset>
                            </ItemTemplate>

                        </asp:ListView>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <script src="Plugin/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="Plugin/tinymce/js/tinymce/tinymce.min.js"></script>

    <script>
        tinymce.init({
            selector: '#ContentPlaceHolder1_txtComment',
            plugins: '',
            toolbar: false,
            menubar: false
        });
    </script>


    <script>
        function btnChkOut() {
            $("#DivModal").modal('show');
            $("#ContentPlaceHolder1_btnSave").hide();
            $("#ContentPlaceHolder1_btnchkOut").show();
            return false;
        }

        function btnActivity_Click() {
            $("#DivModal").modal('show');
            $("#ContentPlaceHolder1_btnchkOut").hide();
            $("#ContentPlaceHolder1_btnSave").show();
            return false;
        }

        function Practice(id, level) {
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/funPractice",
                data: '{id: "' + id + '" ,level : "' + level + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function ReviewPractice(id, level) {
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/funReviewPractice",
                data: '{id: "' + id + '" ,level : "' + level + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnPracticeSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;
        }

    </script>


</asp:Content>

