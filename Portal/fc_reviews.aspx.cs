﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using Newtonsoft.Json.Linq;


public partial class Portal_fc_reviews : System.Web.UI.Page
{
    static string token = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetAllQuestionsAndAnswer();
            token = fc_gen_utils.new_sticky_id();
        }
    }
    private void GetAllQuestionsAndAnswer()
    {
        //string set_id = Request.QueryString["SetID"].ToString();
        string set_id = "1";
        string str_sql = "Select top 50 * from fc_repository where set_id='" + set_id + "'";
        DataTable dtQuestions = gen_db_utils.gp_sql_get_datatable(str_sql);
        string QuestionsHTML = "";
        int i = 0;
        string firstquestion = "";
        string firstquestionID = "";
        string firstanswer = "";
        foreach (DataRow dtr in dtQuestions.Rows)
        {
            string question = dtr["fc_term"].ToString();
            string solution = gp_utils.html_jax(dtr["fc_desc"].ToString());
            if (i == 0)
            {
                firstquestion = question;
                firstquestionID = dtr["fc_id"].ToString();
                firstanswer = solution;
            }
            i++;
            QuestionsHTML += "<img id='img_" + dtr["fc_id"].ToString() + "' style='float:left;margin-left:15px' src=\"../images/omittedSmall.png\" answer='" + solution + "' question='" + question + "'/>";


        }
        lblQuestionsList.Text = QuestionsHTML;

        lblTotalQuestions.Text = dtQuestions.Rows.Count.ToString();
        lblQuestionsStats.Text = "0 / " + dtQuestions.Rows.Count.ToString();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "FirstQuestion", "FirstQuestion('img_" + firstquestionID + "');", true);
    }

    [WebMethod]
    public static void InsertReviews(string result, string questionid, string time)
    {      
        string QuestionID = questionid;
        string ftime = time;
        string fresult = result;
        if (fresult != "")
        {
            string strSQL = "insert into FlashCard_review values(" + QuestionID + "," + time + ",getdate()," + result + ",'" + token + "')";
            gen_db_utils.gp_sql_execute(strSQL);
        }
    }
    protected void btnDone_ServerClick(object sender, EventArgs e)
    {
        string quiz_token = token;
        Response.Redirect("./fc_end.aspx?token=" + quiz_token);
    }
    protected void btnComment_Click(object sender, EventArgs e)
    {

    }
}