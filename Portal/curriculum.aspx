﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="curriculum.aspx.cs" Inherits="Portal_curriculum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="courses-detail-wrapper col-12">


        <div class="row">
            <div class="col-md-12 layout-left">
                <div class="course-syllabus">
                    <div class="course-syllabus-title underline">Curriculum</div>
                    <div id="div1" runat="server">
                        <b><asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
                        <br />
                        <br />
                        <div class="row">
                            Shown below is the list of all topics for your grade level. Set the curriculum by selecting all of the topics you’ve already learned and need to practice. Practice sessions and quizzes will be based on the curriculum you set here.
                        </div>
                        <br />
                        <div class="row" id="divaction">
                            <div class="col-lg-3" style="color:red">
                                 You have made changes to Curriculum -
                                </div>
                            <div class="col-lg-3">
                            <button id="btnsave"  type='button' class='btn btn-primary' style="float: left;margin-right:1%" >save</button>
                                 <button id="btncancel"  type='button' class='btn btn-primary' style="float: left;margin-right:1%" >cancel</button>
                            <%--<input type="submit" value="save" id="btnsave" class="btn btn-login btn-green" style="float: left;margin-right:1%" />--%>
                                </div>
                        </div>
                    </div>
                    <br />

                    </div>

                <div class="course-table" id="div" runat="server">
                    <div class="outer-container">
                        <div class="inner-container">
                            <div class="table-header">
                                <table class="edu-table-responsive">
                                    <thead>
                                        <tr class="heading-table">
                                            <th style='width: 5%'>
                                                <input type='checkbox' id="checkAll" data-id='0' /></th>
                                            <th class="col-6">title</th>


                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="table-body">
                                <asp:Label ID="lb1" runat="server"></asp:Label>

                            </div>
                            <asp:Label ID="lbl" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                </div>
            </div>

        </div>


   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <style>
        .checkSingle:checked + td > a.label-for-check {
            color: gray;
        }
    </style>
    <script>

        //$('#checkAll').click(function () {

        //    if ($('#checkAll').is(':checked')) {

        //        $('input:checkbox').prop('checked', true);
        //    } else {

        //        $('input:checkbox').prop('checked', false);
        //    }
        //});


        $(document).ready(function () {
            $("#divaction").hide();
            $("#checkAll").change(function () {
                if (this.checked) {
                    $(".checkSingle").each(function () {
                        this.checked = true;
                        $("#" + $(this).attr('data-id')).css("color", "#000000");
                    })
                } else {
                    $(".checkSingle").each(function () {
                        this.checked = false;
                        $("#" + $(this).attr('data-id')).css("color", "#6f8190");
                        
                    })
                }
                checkchange();
            });

            $(".checkSingle").click(function () {
                if ($(this).is(":checked")) {
                    var isAllChecked = 0;
                    $(".checkSingle").each(function () {
                        if (!this.checked)
                            isAllChecked = 1;
                    })
                    if (isAllChecked == 0) {
                        $("#checkAll").prop("checked", true);
                    }
                    $("#" + $(this).attr('data-id')).css("color", "#000000");
                } else {
                    $("#checkAll").prop("checked", false);
                    $("#" + $(this).attr('data-id')).css("color", "#6f8190");
                }
                checkchange();
            });

            function checkchange() {
                $(".checkSingle").each(function () {
                    var ClientSelection = "False";
                    var result = false;
                    if (this.checked) {
                        ClientSelection = "True";
                    }
                    $("#divaction").hide();
                    if (ClientSelection != $(this).attr('iserver')) {
                        $("#divaction").show();
                        return false;
                    }
                    
                    

                });
            }

            function getids() {
                var strids = "";
                $(".checkSingle").each(function () {
                    if (this.checked) {
                        strids += $(this).attr('data-severid')+",";
                    }
                });
               return strids = strids.substr(0, strids.length - 1);
            }
            $("#btnsave").click(function () {
                var ids = getids();
                debugger;
                savedata(ids);
            });
            $("#btncancel").click(function () {
                window.location = window.location;
            });
            
            function savedata(ids) {
                $.ajax({
                    type: "POST",
                    url: "curriculum.aspx/savedata",
                    data: '{ids: "' + ids + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            function OnSuccess(response) {
                alert("data updated successfully");
                window.location = window.location;

            }
        });

    </script>
</asp:Content>

