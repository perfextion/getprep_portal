﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Web.Services;
public partial class Portal_Default : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep|Welcome";       
        
        if (!Page.IsPostBack)
        {
            CreateData();
        }



    }

    private void CreateData()
    {
        string SQLString = "select id,cat_name,study_type from core_cat where tree_level=0 and Study_type is not null order by Study_type";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(SQLString);
            string StudyType = "";
            string HTML = "";
            foreach (DataRow dr in dt.Rows)
            {
                if (StudyType != dr["study_type"].ToString())
                {
                    if (StudyType != "")
                    {
                        HTML += " </div> " +
                        " </div><div class='clearBoth'></div>";
                    }
                    // Create Main category Html
                    StudyType = dr["study_type"].ToString();
                    HTML += "<span class='chevron_toggleable glyphicon glyphicon-chevron-right ' style='cursor: pointer; color: darkturquoise;font-size:large' data-toggle='collapse' data-target='#" + StudyType + "Panel'><span style='font-size:large;color:black'>" + StudyType + "</span></span>";
                    HTML += "  <div class='panel panel-default collapse'  id='" + StudyType + "Panel' style='background-color: #eee;margin-top: 30px;'> " +
                          " <div class='panel-body'>";

                }

                HTML += "  <input type='checkbox' style='margin-left:10px' onchange='OnChangeSubject(this);' name='" + dr["cat_name"].ToString() + "' value='" + dr["id"] + "' />" + dr["cat_name"];



                // Create Checkbox HTML
            }
            HTML += " </div> " +
                       " </div>";
            lblSubject.Text = HTML;
        }
        catch (Exception Ex)
        {
            LogError.Log("Welcome.aspx/CreateData", Ex.Message, Ex.InnerException, "", Ex.StackTrace, SQLString, true);
            lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
    }
    

}