﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_knowledge_map : System.Web.UI.Page
{
    DataTable dt_full;
    Hashtable h_cc1 = new Hashtable();
    Hashtable h_cc2 = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep| knowledge map";

        string parent_cat = Request.QueryString.GetValues("parent_cat") == null ? "1" : (Request.QueryString.GetValues("parent_cat")[0].ToString());
        //lb1.Text = "Subjects";

        string str_sql = "select * from core_cat where parent_cat = " + parent_cat + "  order by order_id";

        string str_sql_lower = "select * from core_cat where cc0 = " + parent_cat;
        try
        {
            dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
        }
        catch (Exception Ex)
        {
            LogError.Log("knowledge_map.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_lower, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }

        string str_h_cc1 = "select cc1, count(*) from type2_qs where subject = 11 and cc1>0 group by cc1";
        try
        {
            h_cc1 = gen_db_utils.gp_sql_get_hashtable(str_h_cc1);
        }
        catch (Exception Ex)
        {
            LogError.Log("knowledge_map.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_h_cc1, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }

        string str_h_cc2 = "select cc2, count(*) from type2_qs where subject = 11 and cc2>0 group by cc2";
        try
        {
            h_cc2 = gen_db_utils.gp_sql_get_hashtable(str_h_cc2);
        }
        catch (Exception Ex)
        {
            LogError.Log("knowledge_map.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_h_cc2, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
        string s = "";
        try
        {
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            foreach (DataRow dtr1 in dt1.Rows)
            {
                //  string lower_text = get_lower_rows();

                string vocab_link = "<a href=\"" + "fc_sql_setup.aspx?parent_cat="
                            + dtr1["id"].ToString() + "\">" + "Vocab Practice" + "</a>";

                string cc1_count = fc_gen_utils.hash_lookup(h_cc1, dtr1["id"].ToString(), "-");


                string quiz_link = "<a href=\"" + "setup_type2_quizstart.aspx?quiz_type=adapt&core_level=level0&core_id="
                               + dtr1["id"].ToString() + "\">" + "Practice" + "</a>";

                lb1.Text += " <table class='edu-table-responsive table-hover'>"
                                           + "<tbody>"
                + "<tr class='heading-content'>"
                + "  <div class='row'>"
                    + "<td colspan='2' class='left heading-content'>" + dtr1["order_id"].ToString() + ". " + dtr1["cat_name"].ToString() + cc1_count + "<a  data-toggle='collapse' class='fa fa-chevron-down pull-right fa-2x' style='margin-right: 20px;    font-size: 1em;'   href=#" + dtr1["cat_name"].ToString() + " aria-expanded='true'></a>"
                                  + "</td>"
                           + "</tr>"
                           + "</div>"
                            + " <tr class='table-row collapse in' id=" + dtr1["cat_name"].ToString() + ">"
                     + " <td colspan='1'>"
               + "<table class='edu-table-responsive table-hover' style='margin-left:40px'>"
               + get_lower_rows(dtr1["id"].ToString()) + "</td>"


                                   + " </table> "
                     + "</td>"
                                   + "</tr>";
                s = s + "<div class='panel panel-default'><div class='panel-heading' role='tab' id='heading" + dtr1["order_id"].ToString() + "'><h5 class='mb-0'><a data-toggle='collapse' href='#" + dtr1["id"].ToString() + "' role ='button' aria-expanded='true' aria-controls='" + dtr1["id"].ToString() + "'>" + dtr1["order_id"].ToString() + ". " + dtr1["cat_name"].ToString() + cc1_count + "</a></h5></div><div id ='" + dtr1["id"].ToString() + "' class='collapse in' role='tabpanel' aria-labelledby='headingOne' data-parent='#accordion'><div class='panel-body'>" + get_lower_rows(dtr1["id"].ToString()) + "</div></div></div>";

            }

            lb1.Text += " </tbody> "
                                  + " </table> ";
            lb1.Text = s;
        }

        catch (Exception Ex)
        {
            LogError.Log("knowledge_map.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }

    }
    public string get_lower_rows(string cat_id)
    {
        try
        {
            string str_return = "";

            string filter_exp = "cc1=" + cat_id + " and " + "tree_level = 2";

            DataRow[] dtr_temp = dt_full.Select(filter_exp, "order_id");
            str_return += "<div class='row'>";
            int count = 0;
            foreach (DataRow dtr in dtr_temp)
            {
                if (count % 2 == 0)
                {
                    str_return += "<div class='col-md-2 col-xs-6 thumbnail tooltip-demo well cusclstootip' style='height:80px;margin:2px ;background-color: #85E0FF; border-radius:0px'> ";

                }
                else
                {
                    str_return += "<div class='col-md-2 col-xs-6 thumbnail tooltip-demo well cusclstootip' style='height:80px;margin:2px; background-color: #FFB354; border-radius:0px'> ";
                }

                str_return += "<span  > <a class='cusclstootip' title='<div><h3>comming soon...</h3></div>'data-html='true' rel='tooltip' href='#'>" + dtr["order_id"].ToString() + ". " + dtr["cat_name"].ToString() + "</a></span> </div>";
                count = count + 1;



            }
            str_return += "</div>";

            return str_return;
        }
        catch (Exception Ex)
        {
            LogError.Log("knowledge_map.aspx/get_lower_rows", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            return "";
        }

    }

}