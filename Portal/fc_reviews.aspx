﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="fc_reviews.aspx.cs" Inherits="Portal_fc_reviews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../css/gpFlashCard.css" rel="stylesheet" />

    <div class="row">


        <!-- col -->
        <%--      <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i>

                <span>Fc
                </span>

            </h1>
        </div>--%>
        <!-- end col -->
        <div class="col-xs-10">
        </div>
        <div class="col-xs-1">
            <input type="button" id="btnDone" class="btn btn-info" style="float: right" runat="server" onserverclick="btnDone_ServerClick" value="Done" />
        </div>
    </div>

    <section id="widget-grid" class="">
        <div class="row">

            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget" id="wid-id-0">
                    <header>
                        <%--<span class="widget-icon"><i class="fa fa-table"></i></span>--%>
                        <h2>View </h2>

                    </header>
                    <div class="widget-body ">
                        <%--use this script and these labels for the timer  to get the time in seconds for 1 slide in other function use counter. TotalTime will return total time on the page  --%>
                        <label id="lblTime" class="lblTime" style="color: green">Time : </label>
                        <br />
                        <label id="lblTotalTime" class="lblTotalTime" style="color: green">Total Time : </label>


                        <asp:Label ID="lblTotalQuestions" runat="server" CssClass="lblTotalQuestions" Style="display: none"></asp:Label>

                        <div class="col-md-12">
                            <div class="col-md-9">
                                <div id="myCard" onclick='$("#myCard").toggleClass("flip");return funFlipAnswer();' class="flip-container vertical  col-lg-12">
                                    <div class="flipper" style="font-size: large">
                                        <div class="front">
                                            <label id="lblf" class="pull-right">Side 1</label>
                                            <span class="Centerer"></span>
                                            <div id="lblMainQuestion" class="Centered" currentquestion=""></div>
                                            <%--<img id="imgMainQuestion"  class="Centered" currentQuestion=""   src="images/front.jpg" />--%>
                                        </div>
                                        <div class="back">
                                            <label id="lblb" class="pull-right">Side 2</label>
                                            <label id="lblAnswer" class="card__text">back</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="divComments">
                                        <%--<asp:Label class="col-md-2 control-label" ID="lblSubject" runat="server">  </asp:Label>--%>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <span>Comments</span>
                                                <select id="ddlComments" class="form-control" onchange="funchangeComment();">
                                                    <option>Select</option>
                                                    <option>Not in Syllabus</option>
                                                    <option>Not clear  </option>
                                                </select>
                                            </div>
                                            <input type="submit" id="btnSubmit" style="display: none" value="Submit" class="btn btn-primary" onclick="return funSubmitComment();" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="submit" id="btnSkip" style="display: none" value="Skip" class="btn btn-primary" onclick="return funSkip();" />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin: 1rem">
                            <div class="col-md-2">
                                <button class="btn btn-primary" id="btnPrev" onclick="return funPrevious();">Previous</button>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="lblQuestionsStats" runat="server" CssClass="QuestionsStats"></asp:Label>

                                <div id="progressbar"></div>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-primary" onclick="return funNext('gotit');" style="display: none" id="btnGotit">I GOT IT</button>
                                <button class="btn btn-primary" onclick="return funNext('didget');" style="display: none" id="btnDidntGet">I DIDNT GET IT</button>
                            </div>
                        </div>


                        <div class="col-md-12" style="margin: 1rem">
                            <asp:Label ID="lblQuestionsList" runat="server" Width="750px" CssClass="lblQuestionsList"> <br /><br /></asp:Label>
                        </div>

                    </div>
                </div>
            </article>
        </div>
    </section>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script type="text/javascript" src='<%=ResolveUrl("../js/jquery-ui-1.10.1/ui/jquery.ui.progressbar.js")%>'></script>
    <script src="../Plugin/Timer/timer.js"></script>
    <script>


        function funFlipAnswer() {

            if ($("#myCard").hasClass("flip")) {
                $("#btnGotit").show();
                $("#btnDidntGet").show();
            }
            else {
                $("#btnGotit").hide();
                $("#btnDidntGet").hide();
            }

        }
        function FirstQuestion(currentQuestionID) {

            $("#lblMainQuestion").attr("currentQuestion", currentQuestionID);

            var currentQuestion = $("#" + currentQuestionID).attr("question");
            var currentAnswer = $("#" + currentQuestionID).attr("answer");

            $("#lblMainQuestion").html(currentQuestion);

            $(".card__text").html(currentAnswer);

            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblAnswer']);

        }
        function funChangeQuestion(action) {

            if ($("#myCard").removeClass("flip"));
            funFlipAnswer();
            var selectedEffect = "slide";
            var options = {};
            // run the effect
            $("#myCard").effect(selectedEffect, options, 500, callback);
            $(".card__text").html("test");
            var CurrentQuestionID = $("#lblMainQuestion").attr("currentQuestion");
            var NextQuest = "";
            var NextQuestionID = "";
            var NextQuestion = "";
            var NextAnswer = "";
            if (CurrentQuestionID == "") {
                NextQuest = $("#ContentPlaceHolder1_lblQuestionsList :first-child");
            }
            else if (action == "gotit" || action == "didget") {
                NextQuest = $("#" + CurrentQuestionID).next();
            }
            else {
                NextQuest = $("#" + CurrentQuestionID).prev();
            }
            // changing result in the end of image  
            NextQuestionID = $(NextQuest).attr("id");
            NextQuestion = $(NextQuest).attr("question");
            NextAnswer = $(NextQuest).attr("answer");
            $("#lblMainQuestion").attr("currentQuestion", NextQuestionID);
            $("#lblMainQuestion").html(NextQuestion);
            $(".card__text").html(NextAnswer);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblAnswer']);

        };
        var totalQuestions = 40;
        var currentQuestion = 0;

        function funNext(action) {
            var CurrentQuestionID = $("#lblMainQuestion").attr("currentQuestion");
            totalQuestions = $(".lblTotalQuestions").text();
            if (currentQuestion < totalQuestions) {
                currentQuestion = currentQuestion + 1;
                var proportion = (parseInt(100) / parseInt(totalQuestions)) * parseInt(currentQuestion);
                $("#progressbar").progressbar({
                    value: proportion
                });

                $(".QuestionsStats").text(currentQuestion + " / " + totalQuestions);

                funChangeQuestion(action);


            }

            if (action == "gotit") {

                $("#" + CurrentQuestionID).attr("src", "../images/correctSmall.png");
                funSaveTime("1", CurrentQuestionID);
            }
            else if (action == "didget") {
                $("#" + CurrentQuestionID).attr("src", "../images/incorrectSmall.png");
                funSaveTime("0", CurrentQuestionID);
            }

            return false;
        }
        function funPrevious() {
            totalQuestions = $(".lblTotalQuestions").text();
            if (currentQuestion > 0) {
                currentQuestion = currentQuestion - 1;
                var proportion = (parseInt(100) / parseInt(totalQuestions)) * parseInt(currentQuestion);
                $("#progressbar").progressbar({
                    value: proportion
                });
                $(".QuestionsStats").text(currentQuestion + " / " + totalQuestions);
                funChangeQuestion('prev');
                //funSaveTime(''); 
            }
            return false;
        }
        function funSaveTime(result, CurrentQuestionID) {
            var QuestionID = CurrentQuestionID.replace("img_", "");

            var time = parseInt(counter);

            counter = 0;
            $.ajax({
                type: "POST",
                url: "fc_reviews.aspx/InsertReviews",
                data: JSON.stringify({ result: result, questionid: QuestionID, time: time }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                },
                failure: function (response) {

                }
            });

        }
        function funOnSuccess() { }
        function callback() {
        };
        // Timer
    </script>
    <script>
        $(function () {

            $("#progressbar").progressbar({
                value: 01
            });
        });

        function funchangeComment() {

            if ($("#ddlComments").text() != "Select") {
                $("#btnSubmit").show();
            }
            else {
                $("#btnSubmit").hide();
            }
            return false;
        }
        function funSubmitComment() {
            $("#btnSubmit").hide();
            $("#divComments").hide();
            $("#btnSkip").show();
            return false;
        }
        function funSkip() {
            $("#btnSubmit").show();
            $("#divComments").show();
            $("#btnSkip").hide();
            funChangeQuestion("gotit");
            return false;
        }
    </script>
</asp:Content>

