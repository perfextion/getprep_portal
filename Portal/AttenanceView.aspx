﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="AttenanceView.aspx.cs" Inherits="Portal_AttenanceView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:Label ID="lblStudentId" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="lblatt_id" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="Label1" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="Label2" runat="server" Style="display: none"></asp:Label>


    <div class="col-sm-10">
        <div class="course-syllabus-title underline">
            Enroll Classes
        </div>
         <b> <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>  
    </div>
      
    <div class="col-sm-12 PLR-0" style="padding-bottom: 2% ;" id="div" runat="server">
         <div class="col-sm-3">
                 <label for="sel1">Select Subject:</label>
                <asp:DropDownList ID="ddlchkinsubject" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        <div class="col-sm-3">
                <label for="sel1">Select Goal:</label>
                <asp:DropDownList ID="ddlchkingoals" runat="server" CssClass="form-control">
                </asp:DropDownList>

            </div>
          
        <div class="col-sm-5 checkin-btn">
            <asp:Button ID="btnchkin" OnClick="btnchkin_Click" CssClass="btn btn-login btn-green" runat="server" Text="check in" />
            <asp:Button ID="btnchkout" OnClick="btnchkout_Click" CssClass="btn btn-login btn-green" runat="server" Text="check out" />
           
        </div>
       </div>
    <br />
    <br />
   
    <div class="form-group" id="div1" runat="server">

        <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
            <div class="col-md-3">
                <label for="sel1">Filter Goal:</label>
                <asp:DropDownList ID="ddlgoals" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>

            <div class="col-md-3">
                <label for="sel1">Filter Subject:</label>
                <asp:DropDownList ID="ddlsubject" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            
            <label class="btn bg-color-blue">
                <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white"/>
                <i class="fa fa-check txt-color-white">Not Enrolled</i>
            </label>
         

        </div>
           
    </div>

    <div id='calendar'></div>

    <div class="modal fade" id="myModalcomment" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label>Comments</label>
                                <textarea class="form-control" id="txtcomment" runat="server"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnsubmit" OnClick="btnsubmit_Click" CssClass="btn btn-login btn-green" runat="server" Text="Submit" />

                            <button type="button" class="btn btn-basic" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                  </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <%--  <link href="/Portal/portalassets/css/MyEventHeader.css" rel="stylesheet" />--%>
    <link href="/Portal/Plugin/fullcalendar-2.4.0/fullcalendar.css" rel="stylesheet" />
    <%--  <script src="portalassets/js/bootstrap.min.js"></script> --%>

    <script src="/Portal/Plugin/fullcalendar-2.4.0/lib/moment.min.js"></script>
    <script src="/Portal/Plugin/fullcalendar-2.4.0/fullcalendar.js"></script>


    <script>
        function Closepopup() {
            $('#myModalcomment').modal('hide');
            RefreshCalendar();
        }
        var calendar;
        var pageflag = "ALL";
        var pagesubjectflag = "ALL";
       
        function RefreshCalendar() {
            $('#fullCalModal').modal('hide');
            $('#calendar').fullCalendar('refetchEvents');
        }
        $(document).ready(function () {

            calendar = $('#calendar').fullCalendar({
                events: {
                    url: '/Portal/WebServices/GetPrep.asmx/GetAttendanceCalender',
                    data: {
                        studentid: $("#ContentPlaceHolder1_lblStudentId").text(),
                        flag: $("#ContentPlaceHolder1_Label1").text(),
                        subject: $("#ContentPlaceHolder1_Label2").text()
                    },
                    type: "Post",
                    success: function (events) {
                        events = $.map(events, function (e) {
                            return e;
                        });
                        return events;
                    }
                },

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                eventMouseover: function (event, element) {
                    $(this).tooltip({ title: event.comments, html: true, container: "body" });
                    $(this).tooltip('show');
                },
                eventClick: function (event, jsEvent, view) {

                    return false;


                }
            });

        })



        $("#ContentPlaceHolder1_ddlgoals").change(function () {
            pageflag = $(this).val();
            var events = {
                url: '/Portal/WebServices/GetPrep.asmx/GetAttendanceCalender',
                type: 'POST',
                data: {
                    studentid: $("#ContentPlaceHolder1_lblStudentId").text(),
                    flag: $(this).val(),
                    subject: $("#ContentPlaceHolder1_Label2").text()
                }
            }


            $("#ContentPlaceHolder1_Label1").text($(this).val());
            $('#calendar').fullCalendar('removeEventSource', events);
            $('#calendar').fullCalendar('addEventSource', events);
            $('#calendar').fullCalendar('refetchEvents');
            // RefreshCalendar();
        }).change();

        $("#ContentPlaceHolder1_ddlsubject").change(function () {
            pagesubjectflag = $(this).val();
            var events = {
                url: '/Portal/WebServices/GetPrep.asmx/GetAttendanceCalender',
                type: 'POST',
                data: {
                    studentid: $("#ContentPlaceHolder1_lblStudentId").text(),
                    flag: $("#ContentPlaceHolder1_Label1").text(),
                    subject:  $(this).val()

                }
            }


            $("#ContentPlaceHolder1_Label2").text($(this).val());
            $('#calendar').fullCalendar('removeEventSource', events);
            $('#calendar').fullCalendar('addEventSource', events);
            $('#calendar').fullCalendar('refetchEvents');
            // RefreshCalendar();
        }).change();
    </script>
    <script>
    
    </script>
</asp:Content>
