﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="knowledge_map.aspx.cs" Inherits="Portal_knowledge_map" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="courses-detail-wrapper col-12">


        <div class="row">
            <div class="col-md-12 layout-left">
                <div class="course-syllabus">
                    <div class="course-syllabus-title underline">Knowledge Map</div>
                    <br />
                    <b><asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
                    <div class="course-table">
                        <div class="outer-container">
                            <div class="inner-container">

                                <div class="table-body">
                                    <div id="accordion" role="tablist">
                                        <asp:Label ID="lb1" runat="server"></asp:Label>

                                    </div>



                                </div>
                                <asp:Label ID="lbl" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <style>
        .cusclstootip {
        font: normal 12px Tahoma, Geneva, sans-serif;
    color: #333;
    height: 30px;
    /* padding-top: 10px; */
    margin-top: 10px;
    margin-right: 5px;
    vertical-align: middle;
    text-align: center;
        }
        .ttdiv {
             background-color: #fff !important;
  /*!important is not necessary if you place custom.css at the end of your css calls. For the purpose of this demo, it seems to be required in SO snippet*/
  color: #000000;
        }
.tooltip-inner {
             background-color: #fff !important;
  /*!important is not necessary if you place custom.css at the end of your css calls. For the purpose of this demo, it seems to be required in SO snippet*/
  color: #000000;
   border: solid 2px #ffc107;
}

.tooltip.top .tooltip-arrow {
  border-top-color: #00acd6;
}

.tooltip.right .tooltip-arrow {
  border-right-color: #00acd6;
}

.tooltip.bottom .tooltip-arrow {
  border-bottom-color: #00acd6;
}

.tooltip.left .tooltip-arrow {
  border-left-color: #00acd6;
}
        .divcss {
            height: 100px;
            width: 500px;
            background-color: powderblue;
        }
    </style>
    <script>
        $('.tooltip-demo.well').tooltip({
            selector: "a[rel=tooltip]"
        })
    </script>
</asp:Content>

