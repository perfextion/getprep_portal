﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;
using System.Web.Script.Serialization;

public partial class Portal_curriculum : System.Web.UI.Page
{
    int page_debug = 0;

    DataTable dt_full;
    Hashtable h_cc1 = new Hashtable();
    Hashtable h_cc2 = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                CreateGrid();
                //Session["userid"] = "1299";
                Session["courseid"] = Request.QueryString.GetValues("courseid")[0].ToString();
            }

            catch (Exception Ex)
            {
                LogError.Log("curriculum.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
                lblErrormsg.Style.Add("color", "red");
                lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
             
            }
        }
    }
    [System.Web.Services.WebMethod]
    public static object savedata(string ids)
    {
        string str_sql = "update st_syllabus set Status = -90 where item_id in (select id from core_cat where cc0 = " + HttpContext.Current.Session["courseid"].ToString() + " and st_id =" + HttpContext.Current.Session["userid"].ToString() + ")";
        str_sql = "update st_syllabus set Status = 80 where item_id in (" + ids + ")and st_id =" + HttpContext.Current.Session["userid"].ToString() + "";
        try
        {
           
            gp_utils.add_course(HttpContext.Current.Session["courseid"].ToString(), HttpContext.Current.Session["userid"].ToString());
            string json = "{id:" + ids + ",reviewed_on:sa}";


           
            gen_db_utils.gp_sql_execute(str_sql);

           
            gen_db_utils.gp_sql_execute(str_sql);
            string str = "{ 'Data': { 'id': '" + ids + "', 'reviewed_on': 's'} }";
            JavaScriptSerializer j = new JavaScriptSerializer();
            object a = j.Deserialize(str, typeof(object));
            return a;
            // 
            // 
            // Whatever items are checked mark 
            // update st_syllabus set Status=80 where item_id in (selectedids) and st_id=Session["userid"].ToString()
        }
        catch (Exception Ex)
        {
            LogError.Log("curriculum.aspx/savedata", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            return"";
        }
    }
    public string get_lower_rows(string cat_id)
    {
        try
        {
           
            string str_return = "";

            string cc2_count = "";
            string dbg_id = "";

            string filter_exp = "cc1=" + cat_id + " and " + "tree_level = 2";

            DataRow[] dtr_temp = dt_full.Select(filter_exp, "order_id");

            foreach (DataRow dtr in dtr_temp)
            {
                string chk = "";
                bool issever = false;
                string style = "";
                if (page_debug == 1)
                {

                    cc2_count = fc_gen_utils.hash_lookup(h_cc2, dtr["id"].ToString(), "-");
                    dbg_id = "ID: (" + dtr["id"].ToString() + ")";
                }
                if (dtr["status"].ToString() == "80")
                {
                    chk = "checked";
                    issever = true;
                    style = "style=' color: rgb(0, 0, 0);'";
                }

                str_return += "<tr class='table-row'>";

                str_return += "<td class=' green-color' style='width:5%'>&nbsp;&nbsp;"
                                        + dtr["order_id"].ToString() + ". "
                                              + "<input type='checkbox' data-severid=" + dtr["id"].ToString() + "  iserver='" + issever + "' class='checkSingle' " + chk + " data-id='chk" + dtr["id"].ToString() + "' /></td>" +
                                              "<td class=' col-sm-8 label-for-check'><a href='#' " + style + " id='chk" + dtr["id"].ToString() + "' ><i class='mr18 fa fa-file-text'></i>";

                str_return += dbg_id
                               + dtr["cat_name"].ToString() + cc2_count + "</td>";
                ;
                str_return += "</tr>";


            }


            return str_return;
        }
        catch (Exception Ex)
        {
            LogError.Log("curriculum.aspx/get_lower_rows", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
            return "";
        }
    }
   
    private void CreateGrid()
    {
        Page.Title = "GetPrep| Course detail";
        string parent_cat = Request.QueryString.GetValues("courseid") == null ? "1" : (Request.QueryString.GetValues("courseid")[0].ToString());
        string str_sql_lower = "select distinct a.*,status from core_cat as a left join st_syllabus b on a.id=b.item_id and st_id = 1299 where cc0 = " + parent_cat;
       
        string str_sql = "select distinct a.*,b.Status from core_cat as a left join st_syllabus b on a.id=b.item_id and st_id = 1299 where     parent_cat = " + parent_cat + " ";
        try
        {
            
            dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);

          
        }
         catch (Exception Ex)
        {
            LogError.Log("curriculum.aspx/CreateGrid", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_lower, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
        }
       
    try{

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            
            foreach (DataRow dtr1 in dt1.Rows)
            {
                //  string lower_text = get_lower_rows();

                string cc1_count = "";
                string dbg_id = "";

                //string vocab_link = "<a href=\"" + "fc_sql_setup.aspx?parent_cat="
                //            + dtr1["id"].ToString() + "\">" + "Vocab Practice" + "</a>";

                if (page_debug == 1)
                {

                    cc1_count = fc_gen_utils.hash_lookup(h_cc1, dtr1["id"].ToString(), "-");
                    dbg_id = "ID: (" + dtr1["id"].ToString() + ")";
                }


                //string quiz_link = "<a href=\"" + "setup_type2_quizstart.aspx?quiz_type=adapt&core_level=level0&core_id="
                //               + dtr1["id"].ToString() + "\">" + "Practice" + "</a>";

                lb1.Text += " <table class='edu-table-responsive table-hover'>"
                                           + "<tbody>"
                                           + "<tr class='heading-content'>"
                                           + "  <div class='row'>"
                                             + "<td class=' green-color' style='width:5%'>"
                                             + dtr1["order_id"].ToString() + ". </td>"
                    // + "<input type='checkbox' class='selectCat' data-id='chk" + dtr1["id"].ToString() + "' /></td>"
                                              + "<td   class='col-8 left heading-content'>"
                                              + dbg_id + dtr1["cat_name"].ToString() + cc1_count
                                             + "</td>"

                           + "</div></tr>"
    + get_lower_rows(dtr1["id"].ToString());


            }

            lb1.Text += " </tbody> "
                                  + " </table> ";
         }

        catch (Exception Ex)
        {
            LogError.Log("curriculum.aspx/CreateGrid", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
        }
    }
}