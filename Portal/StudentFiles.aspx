﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="StudentFiles.aspx.cs" Inherits="Portal_upload1" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../assets/js/moment.js" type="text/javascript"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css" />

</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <script>
        $(function () {
            //$('#datetimepicker1').datetimepicker();

            //$('#datetimepicker2').datetimepicker({
            //    useCurrent: false //Important! See issue #1075
            //});

            //$("#datetimepicker1").on("dp.change", function (e) {
            //    $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
            //});

            //$("#datetimepicker2").on("dp.change", function (e) {
            //    $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
            //});
            

        });

        $("#btnfinalsubmit1").click(function (event) {
            debugger;
            if (1 == 1) {
                $("#ContentPlaceHolder1_btnfinalsubmit").click();
            } else {
                alert();
            }
        });
        function funmsg(val) {
            var title = "";
            var msg = "";
            var typemsg = "";
            if (val == 1) {
                typemsg = 'warning';
                title = 'File Upload';
                msg = 'Please Upload file  .';
            } else if (val == 2) {
                typemsg = 'success';
                title = 'File Upload';
                msg = 'file Uploaded successfully .';

            }


            $.notify({
                title: '<strong>' + title + '</strong></br>',
                message: msg
            }, {
                type: typemsg
            });
        }

        function readURL(input) {
            var files = input.files;
            var reader;
            var file;
            var i;
            $("#ContentPlaceHolder1_btnpost").click();

            for (i = 0; i < files.length; i++) {
                file = files[i];
                reader = new FileReader();
                reader.onload = (function (file) {
                    return function (e) {
                        var span = document.createElement('span');
                        if (file.name.match(/\.(jpg|jpeg|png|gif)$/)) {
                            span.innerHTML = ['<img src="', e.target.result,
                          '" title="', escape(file.name), '">'
                            ].join('');

                        } else {
                            span.innerHTML = ['<span ="', '',
                         '" title="', escape(file.name), '">'
                            ].join('');
                        }
                        document.getElementById('ContentPlaceHolder1_list').insertBefore(span, null);
                        $("#ContentPlaceHolder1_lblFiles").text("sdf");
                    };
                })(file);
                reader.readAsDataURL(file);
            }
        }
        $("#multifileupload").change(function () {
            readURL(this);
        });
        function fireServerButtonEvent(id) {
            $("#" + id).remove();
            $("#ContentPlaceHolder1_HiddenField1").val(id)
            $("#ContentPlaceHolder1_test").click();

        }
        


        //$('.modal').on('shown.bs.modal', function () {      //correct here use 'shown.bs.modal' event which comes in bootstrap3
        //    $(this).find('iframe').attr('src', 'http://www.google.com')
        //})
        //$('#basicModal').modal('show')
        function funpreview(filepath){
            
            $('#iframe').attr("src", filepath);
          
        };
            
        


    </script>




    <div class="container1">
        

        <div id="uploadone" runat="server" visible="false">
            <div class="panel panel-default">
                <div class="panel-heading">Upload item</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="form-group">
                            <div class="col--lg-6">

                                <div class="col--lg-4">
                                    <label for="email">Course:</label>

                                </div>
                                <div class="col--lg-8">
                                  <%--  <asp:DropDownList class="form-control" ID="Ddlcourse" runat="server">
                                    </asp:DropDownList>--%>

                                </div>
                            </div>
                        </div>

                    </div>
                    <br />
                    <div class="row">
                        <div class="col--lg-6">

                            <div class="form-group">
                                <div class="col--lg-4">

                                    <label for="pwd">Type Of Item:</label>
                                </div>
                                <div class="col--lg-8">

                                    <asp:DropDownList ID="Ddltypeofitem" class="form-control" runat="server">
                                        <asp:ListItem Text="Homework" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Take Home Quiz" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col--lg-6">
                            <div class="form-group">
                                <div class="col--lg-4">
                                    <label for="email">Due Date:</label>

                                </div>
                                <div class="col--lg-8">
                                    <div class='input-group date' id='datetimepicker1'>

                                        <asp:TextBox ID="duedate" class="form-control" type="text" placeholder="Enter Due Date" runat="server"></asp:TextBox>
                                        <%--<input type='text' class="form-control" />--%>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>

                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="duedate" ValidationGroup="Insert" ErrorMessage="Select date" ForeColor="Red"></asp:RequiredFieldValidator>

                                    <%--  <asp:TextBox ID="duedate" class="form-control" type="date" placeholder="Enter Due Date" runat="server"></asp:TextBox>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col--lg-6">
                            <div class="form-group">
                                <div class="col--lg-4">
                                    <label for="email">Number of file to upload:</label>
                                </div>
                                <div class="col--lg-8">
                                    <asp:TextBox ID="numberoffileupload" class="form-control" placeholder="Enter Number of file to upload" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="numberoffileupload" ValidationGroup="Insert" ErrorMessage="Please Enter Number of file to upload" ForeColor="Red"></asp:RequiredFieldValidator>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col--lg-6">

                            <asp:Button ID="btnsubmit" runat="server" ValidationGroup="Insert" CausesValidation="true" OnClick="btnsubmit_Click" class="btn btn-info pull-right" Text="Next" />
                        </div>
                    </div>
                </div>
            </div>




        </div>


        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="hdfieldid" runat="server" />
        <div id="uploadtwo" runat="server">
            <div class="col-lg-6" style="height: 200px">
                <div class="form-group">
                    <label for="email">Upload:</label>

                    <asp:FileUpload ID="multifileupload" accept=".png,.jpg,.jpeg,.gif,.pdf" AllowMultiple="true" runat="server" onchange="readURL(this)"></asp:FileUpload>
                    <asp:RegularExpressionValidator ID="regexValidator" runat="server"
                        ControlToValidate="multifileupload"
                           ErrorMessage="Only images and pdf are allowed"
                        ValidationExpression="(.*\.([Gg][Ii][Ff])|.*\.([Pp][Dd][Ff])|.*\.([Pp][Nn][Gg])|.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])$)">
             
                       >
                    </asp:RegularExpressionValidator>
               
                </div>
            </div>


            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Uploaded files</div>
                    <div class="panel-body" style="height: 200px">

                        <asp:Label ID="lblFiles" CssClass="uploadedfile" runat="server"></asp:Label>
                    </div>

                </div>
            </div>
            <div class="col-lg-12">
                <asp:Button ID="btnfinalsubmit" OnClick="btnfinalsubmit_Click" runat="server" Style="float: right" class="btn btn-info" Text="Submit" />
            </div>

        </div>




    </div>
    <asp:Button ID="btnpost" runat="server" Style="display: none" OnClick="btnpost_Click" class="btn btn-default" Text="Next" />
    <asp:Button ID="test" runat="server" Style="display: none" OnClick="test_Click" class="btn btn-default" Text="Next" />

    <div id="Div1" runat="server">
        <div class="panel panel-default">
            <div class="panel-heading">Upload item</div>
            <div class="panel-body">
                <asp:Label ID="uploadworks" runat="server"></asp:Label>

            </div>
        </div>
        <div class="modal fade" id="basicModal"   role="dialog" tabindex="-1" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Preview</h4>
                    </div>
                    <div class="modal-body">
                        <iframe   id="iframe"  width="100%" height="380" ></iframe>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    </div>

</asp:Content>

