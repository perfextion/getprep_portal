﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;
using System.Web.Script.Serialization;

public partial class quiz_summary : System.Web.UI.Page
{
    int page_debug = 0;



    protected void Page_Load(object sender, EventArgs e)
    {
        string str_sql = "select count(*) as total_count, sum(correct) as total_correct, sum(time_taken) as total_time, "
                                + " min(date_answered) as start_time, quiz_token, quiz_title from st_answer A  "
                                 + " join quiz_summary qs on qs.test_key = A.quiz_token  "
                                + " where student_id = " + Session["userid"].ToString() + " group by quiz_token, quiz_title order by start_time desc";
        try
        {
            if (!Page.IsPostBack)
            {
                Page.Title = "GetPrep| Quiz Summary";
                DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
                if (dt1.Rows.Count == 0)
                {
                    lb1.Text += "You have not taken any tests yet! <br><br>";
                }
                else
                {
                    lb1.Text += " <table class='edu-table-responsive table-hover' > "
                                                 + " <thead> "
                                                  + "<tr class='heading-table'>"
                                                  + "  <th style=\"width:20%\">Quiz Type</th>"
                                                  + "  <th style=\"width:20%\">Date / Time</th>"
                                                  + "   <th style=\"width:10%\">Quiz Code</th>"
                                                  + "   <th style=\"width:20%\"> Qs Correct</th>"
                                                  + " <th   style=\"width:15%\"> Duration (min) </th>"
                                                  + "   <th style=\"width:15%\">Score</th>"
                                                  //		    + "   <th class='col-2'>Score</th>"
                                                  + "</tr>"
                                                  + "</thead>  <tbody> ";

                    foreach (DataRow dtr1 in dt1.Rows)
                    {

                        if (page_debug == 1)
                        {

                        }

                        Double percent_correct = 0.0;
                        string str_percent = "-";
                        //string str_grade = "";

                        //if (Convert.ToDouble(dtr1["total_count"].ToString()) == 0.0)
                        //{

                        //}
                        //else
                        //{
                        //    percent_correct = Convert.ToDouble(dtr1["total_correct"].ToString()) * 100.0 / Convert.ToDouble(dtr1["total_count"].ToString());
                        //    if (percent_correct >= 90.0)
                        //    {
                        //        str_grade = "A+";
                        //    }
                        //    else if (percent_correct < 90.0 && percent_correct >= 85.0)
                        //    {
                        //        str_grade = "A";
                        //    }
                        //    else if (percent_correct < 85.0 && percent_correct >= 80.0)
                        //    {
                        //        str_grade = "B";
                        //    }
                        //    else if (percent_correct < 80.0 && percent_correct >= 70.0)
                        //    {
                        //        str_grade = "C";
                        //    }
                        //    else if (percent_correct < 70.0 && percent_correct >= 50.0)
                        //    {
                        //        str_grade = "D";
                        //    }
                        //    else if (percent_correct < 50.0)
                        //    {
                        //        str_grade = "F";
                        //    }

                        //}


                        Double duration_min = Math.Round((Convert.ToDouble(dtr1["total_time"].ToString()) / 60.0), 1);

                        string link_quiz_code = "<a style='color: #2196F3;' href=\"" + "/portal/type2_summary.aspx?quiz_code="
                                + dtr1["quiz_token"].ToString() + " \">" + dtr1["quiz_token"].ToString() + "</a>";

                        string link_skill = "<a style='color: #2196F3;'  href=\"" + "/portal/type2_summary.aspx?quiz_code="
                                + dtr1["quiz_token"].ToString() + " \">" + dtr1["quiz_title"].ToString() + "</a>";


                        lb1.Text +=
                                                   // " <table class='edu-table-responsive table-hover'>"

                                                   "<tr class='heading-content'>"
                                                      + "<td  class='center'>" + link_skill + "</td>"
                                                      + "<td  class='center'>" + dtr1["start_time"].ToString() + "</td>"
                                                      + "<td  class='center'>" + link_quiz_code + "</td>"
                                                       + " <td   class='center'>" + dtr1["total_correct"].ToString() + " out of " + dtr1["total_count"].ToString() + "</td>"
                                                       + "<td   class='center' class='green-color'>" + duration_min.ToString() + " min </td> "
                                                       + "<td   class='center'> " + " ( " + Math.Round(percent_correct).ToString() + "% )" + "</td>"
                                   + "</tr>"

                                   ;

                    }

                    lb1.Text += " </tbody> "
                                          + " </table> ";



                }
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("quiz_summary.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }


}

