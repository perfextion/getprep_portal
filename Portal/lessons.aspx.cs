﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.UI;

public partial class Portal_lessons : System.Web.UI.Page
{
    int page_debug = 0;

    DataTable dt_full;
    Hashtable h_cc1 = new Hashtable();
    Hashtable h_cc2 = new Hashtable();

    protected void Page_Load(object sender, EventArgs e)
    {
        string str_sql_title = "select top 1 cat_name from core_cat where id = " + Request.QueryString.GetValues("courseid")[0].ToString();
        try
        {
           
            if (!Page.IsPostBack)
            {

                if (Request.QueryString.GetValues("courseid")[0] == null)
                {
                    Response.Redirect("/Portal/Dashboard.aspx");

                }
              
                string title = gen_db_utils.gp_sql_scalar(str_sql_title);
                Page.Title = "GetPrep | " + title;
                lblTitle.Text = title;
                CreateGrid();

            }
        }
        catch (Exception Ex)
        {
            {
                LogError.Log("lessons.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_title, true);
                lblErrormsg.Text = "There is some error occured while processing.Please contact to Administrator.";
            }
        }
    }

    public string get_lower_rows(string cat_id)
    {
        string str_return = "";

        string cc2_count = "";
        string dbg_id = "";
        try
        {
            string filter_exp = "cc1=" + cat_id + " and " + "tree_level = 2";

            DataRow[] dtr_temp = dt_full.Select(filter_exp, "order_id");

            foreach (DataRow dtr in dtr_temp)
            {

                if (page_debug == 1)
                {

                    cc2_count = fc_gen_utils.hash_lookup(h_cc2, dtr["id"].ToString(), "-");
                    dbg_id = "ID: (" + dtr["id"].ToString() + ")";
                }
                str_return += "<tr class='table-row'>";
                str_return += "<td>" + dtr["cat_name"].ToString() + "</td>";
                str_return += "<td><input type='checkbox' id='chk_" + dtr["id"].ToString() + "'/></td>";
                str_return += "<td><a href='#' onclick='Practice(" + dtr["id"].ToString() + ",2,\"" + dtr["cat_name"].ToString() + "\")'><i class='mr18 fa fa-file-text' style='font:size:24px'></i>Practice</a></td>";
                str_return += "</tr>";


            }
            return str_return;
        }
        catch (Exception Ex)
        {
            LogError.Log("lessons.aspx/get_lower_rows", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Text = "There is some error occured while processing.Please contact to Administrator.";
            return "";
        }
    }

    private void CreateGrid()
    { 
        
        string str_sql = "select distinct a.*,b.Status from core_cat as a join st_syllabus b on a.id=b.item_id and st_id = 1299 where     parent_cat = " +Request.QueryString.GetValues("courseid")[0].ToString() + " ";

        string str_sql_lower = "select distinct a.*,status from core_cat as a join st_syllabus b on a.id=b.item_id and st_id = 1299 where cc0 = " +  Request.QueryString.GetValues("courseid")[0].ToString();
        try
        {
           
            dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
        }
        catch (Exception Ex)
        {
            LogError.Log("lessons.aspx/CreateGrid", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_lower, true);
            lblErrormsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
        try
        {
           
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

            if (dt1.Rows.Count == 0)
            {
                lb1.Text = "You do not have any courses Enroll. Please contact administrator.";
            }

            foreach (DataRow dtr1 in dt1.Rows)
            {

                string cc1_count = "";
                string dbg_id = "";

                if (page_debug == 1)
                {

                    cc1_count = fc_gen_utils.hash_lookup(h_cc1, dtr1["id"].ToString(), "-");
                    dbg_id = "ID: (" + dtr1["id"].ToString() + ")";
                }

                lb1.Text += " <table class='edu-table-responsive table-hover'>"
                                           + "<tbody>"
                                           + "<tr class='heading-content'>"
                                           + "  <div class='row'>"
                                              + "<td   class='col-8 left heading-content' colspan='3'>"
                                              + dbg_id + dtr1["cat_name"].ToString() + cc1_count
                                             + "</td>"
                           + "</div></tr>"
                   + get_lower_rows(dtr1["id"].ToString());
            }
                        lb1.Text += " </tbody> "
                                  + " </table> ";
        }
        catch (Exception Ex)
        {
            LogError.Log("lessons.aspx/CreateGrid", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
    }

    [WebMethod]
    public static object funPractice(string id, int level,string title,bool NeedReview)
    {
        string str_sql = "select top 100 type2_id from type2_qs where cc" + level + " = " + id + " and status >= -950 order by newid() ";
        try
        {
            HttpContext.Current.Session["debug_999"] = str_sql;
            ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

            o_quiz_param item_q_param = new o_quiz_param();

            item_q_param.quiz_title = title;

            item_q_param.start_time = DateTime.Now.ToString();

            item_q_param.last_activity_time = DateTime.Now.ToString();
            item_q_param.query = str_sql;
            item_q_param.quiz_q_string = fc_gen_utils.arraylist2string(ar_qs);
            item_q_param.quiz_code = fc_gen_utils.new_batch_id();
            item_q_param.NeedReview = NeedReview;
            item_q_param.set2session();
            HttpContext.Current.Session["cqz_quiz_start"] = str_sql;

            return new
            {
                Data = new
                {
                    id,
                    url = "/Portal/quiz.aspx?courseid=0"
                }
            };
        }
        catch (Exception Ex)
        {
            LogError.Log("lessons.aspx/funPractice", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            return "";
        }
         
    }
}