﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Form_Listing_GroupBy.aspx.cs" Inherits="Portal_Form_Listing_GroupBy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .table-hover > tbody > tr:hover {
            background-color: #bbf3f0;
            cursor: pointer
        }

        .panel1 {
            margin-top: 5rem;
            cursor: pointer;
        }

        .table {
            margin-left: 2rem !important;
            width: 98% !important;
        }
    </style>
    <div class="course-syllabus-title underline" id="abc" tabindex="1">
        Select Forms:
    </div>
    <b>
        <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-1 control-label">Type</label>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddltype" CssClass="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddltype_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="panel1">
                <asp:Label ID="lbl_top" runat="server"></asp:Label>
                <asp:Label ID="lbl_table" runat="server"></asp:Label>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
        function toggleIcon(e) {
            debugger
            $(this).find(".more-less").toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-heading').on('click', toggleIcon);
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
</asp:Content>

