﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;
using System.Web.Script.Serialization;
using System.Diagnostics;

public partial class coursedetail_nums : System.Web.UI.Page
{
    int page_debug = 0;
    int full_cnt = 0;

    
    
    DataTable dt_full;
    Hashtable h_cc1 = new Hashtable();
    Hashtable h_cc2 = new Hashtable();

    Dictionary<string, int> d_cnt_total = new Dictionary<string, int>();
    Dictionary<string, int> d_cnt_correct = new Dictionary<string, int>();
    Dictionary<string, int> d_cnt_wrong = new Dictionary<string, int>();

    //Hashtable h_review_last30 = new Hashtable();
    //Hashtable h_review_last30 = new Hashtable();
    //Hashtable h_review_last30 = new Hashtable();
     
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep| Course detail";
        if (Session["debug_on"].ToString() == "1") page_debug = 1;

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Stopwatch st = new Stopwatch();

        st.Start();

        for (int ii = 0; ii < 100; ii++)
        {
            compute_nums();
        }

        st.Stop();

        string time_elpased = st.ElapsedMilliseconds.ToString();
        lbl.Text += "time elaspsed = " + time_elpased + "|" + full_cnt.ToString();

        string parent_cat = Request.QueryString.GetValues("courseid") == null ? "1" : (Request.QueryString.GetValues("courseid")[0].ToString());
        //lb1.Text = "Subjects";

        string str_sql = "select * from core_cat where parent_cat = " + parent_cat + "  order by order_id";

        string str_sql_lower = "select * from core_cat where cc0 = " + parent_cat;

        dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);

        if (page_debug == 1)
        {

            string str_h_cc1 = "select cc1, count(*) from type2_qs where  status = -950 and cc1>0 group by cc1";
            h_cc1 = gen_db_utils.gp_sql_get_hashtable(str_h_cc1);

            string str_h_cc2 = "select cc2, count(*) from type2_qs where  status = -950 and cc2>0 group by cc2";
            h_cc2 = gen_db_utils.gp_sql_get_hashtable(str_h_cc2);


        }


        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);


        foreach (DataRow dtr1 in dt1.Rows)
        {
            //  string lower_text = get_lower_rows();

            string cc1_count = "";
            string dbg_id = "";

            int dummy_out = 0;
            string s_count_total = "";
            string s_count_correct = "";
            string s_count_wrong = "";


            if (d_cnt_total.TryGetValue(dtr1["id"].ToString(), out dummy_out))
            {
              s_count_total = (dummy_out + 1).ToString();
            }
            else
            {
                  s_count_total = "-";
            }
            if (d_cnt_correct.TryGetValue(dtr1["id"].ToString(), out dummy_out))
            {
                s_count_correct = (dummy_out + 1).ToString();
            }
            else
            {
                s_count_correct = "-";
            }

            if (d_cnt_wrong.TryGetValue(dtr1["id"].ToString(), out dummy_out))
            {
                s_count_wrong = (dummy_out + 1).ToString();
            }
            else
            {
                s_count_wrong = "-";
            }


            double image_width = 0.0;

            if (s_count_total != "-")
            {

                image_width = Convert.ToDouble(s_count_total) * 3.0;


            }


            string image_label = "<img src=\"/images/green.gif\"  width=\"" + image_width.ToString() + "\" height=\"10\" />";


            if (page_debug == 1)
            {

                 cc1_count = fc_gen_utils.hash_lookup(h_cc1, dtr1["id"].ToString(), "-");
                 dbg_id = "ID: (" + dtr1["id"].ToString() + ")";
            }



            lb1.Text += " <table class='edu-table-responsive table-hover'>"
                                       + "<tbody>"
                                       + "<tr class='heading-content'>"
                                       + "  <div class='row'>"
                                          + "<td   class='col-8 left heading-content'>" + dtr1["order_id"].ToString() + ". "
                                          + dbg_id + dtr1["cat_name"].ToString() + " (" + cc1_count + ") " 
                                          + fc_gen_utils.put_space(30)  
                                           + s_count_correct + " out of " + s_count_total + image_label  
                                         + "</td>"
                              + "<td class='green-color'>"
                              + "<a href='#' onclick = 'Practice(" + dtr1["id"].ToString() + ",1)'> Practice</a> "
                              + " <a href='#' onclick = 'Review(" + dtr1["id"].ToString() + ",1)'>Review</a>"
                               + "<a  data-toggle='collapse' class='fa fa-chevron-down pull-right fa-2x' style='margin-right: 20px;    font-size: 1em;'   href=#"
                                          + dtr1["cat_name"].ToString() + " aria-expanded='true'></a></td>"
                       + "</div></tr>"

                        

            +" <tr class='table-row collapse in' id=" + dtr1["cat_name"].ToString() + ">"
     + " <td colspan='1'>"
+ "<table class='edu-table-responsive table-hover' style='margin-left:40px'>"
+ get_lower_rows(dtr1["id"].ToString()) + "</td>"

                   + " </table> "
     + "</td>"
                   + "</tr>";

        }

        lb1.Text += " </tbody> "
                              + " </table> ";



    }


    public void compute_nums()
    {
        int result = 0;
       
        string str_sql = " select C.*, A.*,q.* from st_answer A "
                         + " join type2_qs q on A.q_seq_id = q.type2_id "
                         + " join core_cat C on C.id = q.core_cat_id "
                         + " where A.student_id = " +  Session["userid"].ToString()  +  " and q.cc0 = 2677 ";
        DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

       

        foreach (DataRow dtr in dt1.Rows)
        {
            full_cnt++;

            // Total

            if (d_cnt_total.TryGetValue(dtr["cc0"].ToString(), out result))
            {
                d_cnt_total[dtr["cc0"].ToString()] = result + 1;
            }
            else
            {
                d_cnt_total.Add(dtr["cc0"].ToString(), 0);
                
            }

            if (d_cnt_total.TryGetValue(dtr["cc1"].ToString(), out result))
            {
               
                d_cnt_total[dtr["cc1"].ToString()] = result + 1;
            }
            else
            {
                 d_cnt_total.Add(dtr["cc1"].ToString(), 0);
            }

            if (d_cnt_total.TryGetValue(dtr["cc2"].ToString(), out result))
            {
                d_cnt_total[dtr["cc2"].ToString()] = result + 1;
            }
            else
            {
               
                d_cnt_total.Add(dtr["cc2"].ToString(), 0);
            }


            // Correct
            if (dtr["correct"].ToString() == "1")
            {
                if (d_cnt_correct.TryGetValue(dtr["cc0"].ToString(), out result))
                {
                    
                    d_cnt_correct[dtr["cc0"].ToString()] = result + 1;
                }
                else
                {
                    d_cnt_correct.Add(dtr["cc0"].ToString(), 0);
                }

                if (d_cnt_correct.TryGetValue(dtr["cc1"].ToString(), out result))
                {
                      d_cnt_correct[dtr["cc1"].ToString()] = result + 1;
                }
                else
                {
                  
                    d_cnt_correct.Add(dtr["cc1"].ToString(), 0);
                }

                if (d_cnt_correct.TryGetValue(dtr["cc2"].ToString(), out result))
                {
                   d_cnt_correct[dtr["cc2"].ToString()] = result + 1;
                }
                else
                {
                     d_cnt_correct.Add(dtr["cc2"].ToString(), 0);
                }
            }
            else
            {
                if (d_cnt_wrong.TryGetValue(dtr["cc0"].ToString(), out result))
                {
                   d_cnt_wrong[dtr["cc0"].ToString()] = result + 1;
                }
                else
                {
                     d_cnt_wrong.Add(dtr["cc0"].ToString(), 0);
                }

                if (d_cnt_wrong.TryGetValue(dtr["cc1"].ToString(), out result))
                {
                   d_cnt_wrong[dtr["cc1"].ToString()] = result + 1;
                }
                else
                {
                     d_cnt_wrong.Add(dtr["cc1"].ToString(), 0);
                }

                if (d_cnt_wrong.TryGetValue(dtr["cc2"].ToString(), out result))
                {
                      d_cnt_wrong[dtr["cc2"].ToString()] = result + 1;
                }
                else
                {
                  d_cnt_wrong.Add(dtr["cc2"].ToString(), 0);
                }


            }

           



        }
      

    }

    public string get_lower_rows(string cat_id)
    {
        string str_return = "";

        string cc2_count = "";
        string dbg_id = "";

        string filter_exp = "cc1=" + cat_id + " and " + "tree_level = 2";

        DataRow[] dtr_temp = dt_full.Select(filter_exp, "order_id");

        foreach (DataRow dtr in dtr_temp)
        {
            int dummy_out = 0;
            string s_count_total = "";
            string s_count_correct = "";
            string s_count_wrong = "";


            if (d_cnt_total.TryGetValue(dtr["id"].ToString(), out dummy_out))
            {
               s_count_total = (dummy_out + 1).ToString();
            }
            else
            {
                 s_count_total = "-";
            }
            if (d_cnt_correct.TryGetValue(dtr["id"].ToString(), out dummy_out))
            {
                s_count_correct = (dummy_out + 1).ToString();
            }
            else
            {
                s_count_correct = "-";
            }

            if (d_cnt_wrong.TryGetValue(dtr["id"].ToString(), out dummy_out))
            {
                s_count_wrong = (dummy_out + 1).ToString();
            }
            else
            {
                s_count_wrong = "-";
            }


            double image_width = 0.0;

            if (s_count_total != "-")
            {

                image_width = Convert.ToDouble(s_count_total) * 3.0;

                
            }
            

             string image_label = "<img src=\"/images/FF9900.gif\"  width=\"" + image_width.ToString() + "\" height=\"10\" />";


            if (page_debug == 1)
            {
               
                cc2_count = fc_gen_utils.hash_lookup(h_cc2, dtr["id"].ToString(), "-");
                dbg_id = "ID: (" + dtr["id"].ToString() + ")";
            }

            str_return += "<tr class='table-row'>";
            str_return += "<td class=' col-sm-8'><a href='#'><i class='mr18 fa fa-file-text'></i>"
                           +    dtr["order_id"].ToString() + ". "  + dbg_id
                           + dtr["cat_name"].ToString() + "( " + cc2_count + " )" 
                              + s_count_correct + " out of " + s_count_total  + image_label + "|" + "<br>"
                              +"<td class='col-sm-2 green-color'>"
                              +  "<a href='#' onclick = 'Practice(" + dtr["id"].ToString() + ",2)'> Practice</a></td> "
                              + " <td><a href='#' onclick = 'Review(" + dtr["id"].ToString() + ",2)'>Review</a></td>"
                              + "</tr>";
            
          
        }


        return str_return;

    }

     

    [System.Web.Services.WebMethod]
    public static object funPractice(string id,int level) {

        string str_sql_title = "select top 1 cat_name from core_cat where cc" + level + " = " + id;

        string title = gen_db_utils.gp_sql_scalar(str_sql_title);
        HttpContext.Current.Session["quiz_title"] = str_sql_title;

        string str_sql = "select top 100 type2_id from type2_qs where cc" + level + " = " + id + " and status = -950 order by newid() ";
        ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

        o_quiz_param item_q_param = new o_quiz_param();

        item_q_param.start_time = DateTime.Now.ToString();
        item_q_param.last_activity_time = DateTime.Now.ToString();
        item_q_param.query = str_sql;
        item_q_param.quiz_q_string = fc_gen_utils.arraylist2string(ar_qs);
        item_q_param.quiz_code = fc_gen_utils.new_batch_id();


        item_q_param.quiz_title = title;

        
        
        item_q_param.set2session();

        HttpContext.Current.Session["quiz_start"] = str_sql + DateTime.Now.ToString();
        //HttpContext.Current.Response.
       // HttpContext.Current.Response.Redirect("/Portal/quiz.aspx?courseid=0", false);
        string str = "{ 'Data': { 'id': '" + id + "', 'url': '/Portal/quiz.aspx?courseid=0'} }";
        JavaScriptSerializer j = new JavaScriptSerializer();
        object a = j.Deserialize(str, typeof(object));
        return a;
       // HttpContext.Current.Response.Redirect("~/Portal/quiz.aspx?courseid=0", false);
    }

    [System.Web.Services.WebMethod]
    public static object funReview(string id, int level)
    {
        HttpContext.Current.Session["subqs_id"] = id;
        string str = "{ 'Data': { 'id': '" + id + "', 'url': '/Portal/flashcards.aspx'} }";
        JavaScriptSerializer j = new JavaScriptSerializer();
        object a = j.Deserialize(str, typeof(object));
        return a;
        // HttpContext.Current.Response.Redirect("~/Portal/quiz.aspx?courseid=0", false);
    }


    protected void btnCircullim_Click(object sender, EventArgs e)
    {
        string parent_cat = Request.QueryString.GetValues("courseid") == null ? "1" : (Request.QueryString.GetValues("courseid")[0].ToString());
        Response.Redirect("~/Portal/curriculum.aspx?courseid=" + parent_cat);
    }
}

