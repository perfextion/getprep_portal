﻿<%@ Page Title="OMRSheet" Language="C#" AutoEventWireup="true" MasterPageFile="~/Portal/MasterPageOnlyBody.master" CodeFile="OMR_shet.aspx.cs" Inherits="OMR_shet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .smart-form fieldset {
            display: block;
            padding: 5px 0px 3px;
            border: none;
            background: rgba(255,255,255,.9);
            position: relative;
        }

            .smart-form fieldset + fieldset {
                border-top: 1px solid rgba(0,0,0,.1);
            }

        .smart-form .button, .smart-form .checkbox, .smart-form .input, .smart-form .radio, .smart-form .select, .smart-form .textarea, .smart-form .toggle {
            position: relative;
            display: block;
            font-weight: 400;
        }


        .smart-form .checkbox, .smart-form .radio {
            margin-bottom: 4px;
            padding-left: 25px;
            line-height: 25px;
            color: #404040;
            cursor: pointer;
            font-size: 15px;
        }



            .smart-form .checkbox input, .smart-form .radio input {
                position: absolute;
                left: -9999px;
            }

            .smart-form .checkbox i, .smart-form .radio i {
                position: absolute;
                top: 10px;
                left: 0;
                display: block;
                width: 17px;
                height: 17px;
                outline: 0;
                border-width: 1px;
                border-style: solid;
                background: #FFF;
            }

            .smart-form .radio i {
                border-radius: 50%;
            }

            .smart-form .checkbox input + i:after, .smart-form .radio input + i:after {
                position: absolute;
                opacity: 0;
                transition: opacity .1s;
                -o-transition: opacity .1s;
                -ms-transition: opacity .1s;
                -moz-transition: opacity .1s;
                -webkit-transition: opacity .1s;
            }

            .smart-form .radio input + i:after {
                content: '';
                top: 4px;
                left: 4px;
                width: 9px;
                height: 9px;
                border-radius: 50%;
            }

            .smart-form .checkbox input + i:after {
                content: '\f00c';
                top: -1px;
                left: 1px;
                width: 15px;
                height: 15px;
                font: 400 16px/19px FontAwesome;
                text-align: center;
            }

            .smart-form .checkbox input:checked:hover + i:after {
                content: '\f00d';
            }

            .smart-form .checkbox input:checked:disabled:hover + i:after {
                content: '\f00c';
            }

            .smart-form .checkbox input:checked + i:after, .smart-form .radio input:checked + i:after {
                opacity: 1;
            }

        .smart-form .inline-group {
            margin: 0 -15px -4px 0;
        }

            .smart-form .inline-group:after {
                content: '';
                display: table;
                clear: both;
            }

            .smart-form .inline-group .checkbox, .smart-form .inline-group .radio {
                float: left;
                margin-right: 30px;
            }

                .smart-form .inline-group .checkbox:last-child, .smart-form .inline-group .radio:last-child {
                    margin-bottom: 4px;
                }

        .smart-form .toggle {
            margin-bottom: 4px;
            padding-right: 61px;
            font-size: 15px;
            line-height: 25px;
            color: #404040;
            cursor: pointer;
        }

            .smart-form .toggle:last-child {
                margin-bottom: 0;
            }

            .smart-form .toggle input {
                position: absolute;
                left: -9999px;
            }

            .smart-form .toggle i {
                content: '';
                position: absolute;
                top: 4px;
                right: 0;
                display: block;
                width: 49px;
                height: 17px;
                border-width: 1px;
                border-style: solid;
                border-radius: 12px;
                background: #fff;
            }

                .smart-form .toggle i:after {
                    content: attr(data-swchoff-text);
                    position: absolute;
                    top: 2px;
                    right: 8px;
                    left: 8px;
                    font-style: normal;
                    font-size: 9px;
                    line-height: 13px;
                    font-weight: 700;
                    text-align: left;
                    color: #5f5f5f;
                }

                .smart-form .toggle i:before {
                    content: '';
                    position: absolute;
                    z-index: 1;
                    top: 4px;
                    right: 4px;
                    display: block;
                    width: 9px;
                    height: 9px;
                    border-radius: 50%;
                    opacity: 1;
                    transition: right .2s;
                    -o-transition: right .2s;
                    -ms-transition: right .2s;
                    -moz-transition: right .2s;
                    -webkit-transition: right .2s;
                }

            .smart-form .toggle input:checked + i:after {
                content: attr(data-swchon-text);
                text-align: right;
            }

            .smart-form .toggle input:checked + i:before {
                right: 36px;
            }

        .smart-form .col-1 {
            width: 8.33%;
        }

        .smart-form .col-2 {
            width: 16.66%;
        }

        .smart-form .col-3 {
            width: 25%;
        }

        .smart-form .col-4 {
            width: 33.33%;
        }

        .smart-form .col-5 {
            width: 41.66%;
        }

        .smart-form .col-6 {
            width: 50%;
        }

        .smart-form .col-8 {
            width: 66.67%;
        }

        .smart-form .col-9 {
            width: 75%;
        }

        .smart-form .col-10 {
            width: 83.33%;
        }

        .smart-form .tooltip-bottom-left {
            top: 100%;
            margin-top: 15px;
        }

            .smart-form .tooltip-bottom-left:after {
                bottom: 100%;
                left: 11px;
                border-right: 4px solid transparent;
                border-bottom: 4px solid rgba(0,0,0,.9);
                border-left: 4px solid transparent;
            }

        .smart-form .input input:focus + .tooltip-bottom-left, .smart-form .textarea textarea:focus + .tooltip-bottom-left {
            right: auto;
            left: 0;
            margin-top: 5px;
        }

        .smart-form .checkbox i, .smart-form .icon-append, .smart-form .icon-prepend, .smart-form .input input, .smart-form .radio i, .smart-form .select select, .smart-form .textarea textarea, .smart-form .toggle i {
            border-color: #BDBDBD;
            transition: border-color .3s;
            -o-transition: border-color .3s;
            -ms-transition: border-color .3s;
            -moz-transition: border-color .3s;
            -webkit-transition: border-color .3s;
        }

            .smart-form .toggle i:before {
                background-color: #3276B1;
            }

        .smart-form .radio input + i:after {
            background-color: #3276B1;
        }

        .smart-form .checkbox input + i:after {
            color: #3276B1;
        }

        .smart-form .checkbox input:checked + i, .smart-form .radio input:checked + i, .smart-form .toggle input:checked + i {
            border-color: #3276B1;
        }

        .smart-form .rating input:checked ~ label {
            color: #3276B1;
        }
    </style>
    <script src="/Portal/Plugin/Timer/timer.js"></script>
    <section id="divSheet" class="">

        <div class="col-sm-10">
            <div class="course-syllabus-title underline">
                <div style="width: 50%; float: left">
                    OMR Sheet 
                </div>
                <label style="width: 50%; float: left">

                    <label id="Label5" class="lblTotalTime" style="color: green">Total Time : 11 mm 16 ss</label>

                </label>
                <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
            </div>

        </div>
        <div id="movieForm" class="form-horizontal">
            <div class="form-group">
                <div class="col-lg-4">
                    <div class="smart-form" id="divpane1"></div>
                </div>
                <div class="col-lg-4">
                    <div class="div smart-form" id="divpane2"></div>
                </div>
                <div class="col-lg-4">
                    <div class="div smart-form" id="divpane3"></div>
                </div>
            </div>
            <div class="form-actions">

                <input id="btn_go" type="button" value="Submit" onclick="SubmitTest();" class="btn btn-login btn-green pull-right" />
            </div>
        </div>
    </section>

    <section id="divSummary" class="form-horizontal">

        <div class="col-sm-10">
            <div class="course-syllabus-title underline">
                OMR Summary
            </div>
        </div>
        <label id="answerquestion"></label>
    </section>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script>
        var answeredQuestion = 0;
        function SubmitTest() {
            clearInterval(interVal);
            funGotoResult();
            $("#answerquestion").text("You submitted test successfully. You have answered " + answeredQuestion + " out of " + questions + ". ");
            $("#divSummary").show();
            $("#divSheet").hide();
        }
        function CheckTestTime() {
            if (TotalTestTime == TotalTime) {
                funGotoResult()
                $("#answerquestion").text("Your time is over. You have answered " + answeredQuestion + " out of " + questions + ". ");
                $("#divSummary").show();
                $("#divSheet").hide();
            }
        }


        function funGotoResult() {
            jsonobj = [];
            item = {};
            item["TestToken"] = TestToken;

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ jdata: TestToken }),
                url: "OMR_shet.aspx/set_session",
                success: function (data) {

                    window.location.href = "OMR_results.aspx"
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        var question_in_pane = "";
        var formno = "";
        var TotalTestTime = "";
        var interVal = "";
        var questions = "";
        var TestToken = "";
        var answers = "";
        $(document).ready(function () {
            debugger
            $("#divSummary").hide();
            $("#divSheet").show();
            var jsonobj = <%=Session["OMRjson"]%>;

            //  TestToken=  ' <%= fc_gen_utils.new_sticky_id().ToString() %> ';
            var paragraphs = "";
            var choices = "";
            var isevenodd = "";
            var oddchoices = "";
            var evenchoices = "";
            var paras = "";
            var NotTime = "";
            $.each(jsonobj, function (idx, obj) {
                paragraphs = obj.total_paragraphs;
                questions = obj.total_questions;
                choices = obj.num_choices;
                paras = JSON.parse(obj.para_starts);
                formno = obj.form_no;
                TotalTestTime = obj.total_duration;
                isevenodd = obj.is_even_odd;
                oddchoices = obj.odd_letters;
                evenchoices = obj.even_letters;
                answers = obj.answers;
                TestToken = obj.test_key;
                NotTime = obj.NotTime
                counter = 1;

            });

            if (NotTime == false) {

                interVal = setInterval(function () {
                    CheckTestTime();
                }, 1000);
            }
            else {
                interVal = "";
            }


            question_in_pane = questions / 3;

            funGetQuestions(formno, questions, choices, question_in_pane, paras, isevenodd, oddchoices, evenchoices);

        });

        function funGetQuestions(formno, questions, choices, question_in_pane, paras, isevenodd, oddchoices, evenchoices) {
            debugger
            var quesCounter = 0;
            var row = "";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "OMR_shet.aspx/Get_All_Question",
                data: JSON.stringify({ formno: formno }),
                success: function (data) {
                    debugger
                    var datajs = JSON.parse(data.d);
                    for (var i = 0; i < datajs.length; i++) {

                        var fullrow = "";
                        var paragraphNumber = "";
                        var paragraphstartQuestion = "";
                        for (var parai = 0; parai < paras.length; parai++) {
                            var paranumber = parai;
                            var paravalue = paras[parai];
                            var nextparanumber = parai + 1;
                            var nextparavalue;
                            if (paras.length - 1 > parai) {
                                nextparavalue = paras[parai + 1];
                            }
                            else {
                                nextparavalue = questions;
                            }
                            if (datajs[i].q_num >= paravalue && datajs[i].q_num <= nextparavalue) {
                                paragraphNumber = paranumber + 1;
                                paragraphstartQuestion = paravalue;
                            }
                        }
                        var Pararow = "";
                        if (paragraphstartQuestion == datajs[i].q_num) {
                            Pararow = "<div class='row' style='margin-bottom:30px'><section class='col-2'><label class='toggle'><input type='checkbox' class='paragraphRadio' paranumber='" + paragraphNumber + "' name='checkbox-toggle" + paragraphstartQuestion + "' class='' ><i data-swchon-text='Read' data-swchoff-text='UnRead'></i></label></section></div>";
                        }
                        row = " <fieldset>" + Pararow + "<div class='row' ><section><div class='inline-group' id='div" + datajs[i].q_num + "'><label class='' style='color:red;float:left;margin-right:8px;margin-top: 7px;'>" + datajs[i].q_num + "</label>";
                        var alphabet = "";
                        if (datajs[i].q_type == 'MC') {
                            for (var r = 1; r <= choices; r++) {

                                if (isevenodd == '1') {
                                    var isqueseven = datajs[i].q_num % 2;
                                    if (isqueseven == 0) {

                                        var evenarray = evenchoices.split(',');
                                        alphabet = evenarray[r - 1];
                                    }
                                    else {
                                        var oddarray = oddchoices.split(',');
                                        alphabet = oddarray[r - 1];
                                    }

                                    row += "<label class='radio'><input  id='chk_" + datajs[i].q_num + "_" + r + "' class='answerRadio'  name=" + datajs[i].q_num + " value=" + alphabet + " type='radio' /><i></i>" + alphabet + "</label>";
                                }
                                else {
                                    alphabet = getAlphabet(r);

                                    row += "<label class='radio'><input  id='chk_" + datajs[i].q_num + "_" + r + "' class='answerRadio'  name=" + datajs[i].q_num + " value=" + alphabet + " type='radio' /><i></i>" + alphabet + "</label>";
                                }
                            }
                        }
                        else if (datajs[i].q_type == 'FIB') {
                            row += "<input  id='txt_" + datajs[i].q_num + "_" + r + "' class='answerRadio' paranumber=" + paragraphNumber + " name=" + datajs[i].q_num + " autocomplete='off' type='text' />";
                        }

                        row += '</div></section></div></fieldset>'
                        fullrow = row;
                        quesCounter += 1;
                        if (quesCounter <= question_in_pane) {
                            $("#divpane1").append(fullrow);
                        }

                        else if (quesCounter <= question_in_pane + question_in_pane) {
                            $("#divpane2").append(fullrow);
                        }
                        else if (quesCounter >= question_in_pane + question_in_pane) {
                            $("#divpane3").append(fullrow);
                        }



                    }

                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        $(document).on("change", "input[class='paragraphRadio']", function () {

            if (this.checked) {
                var test_id1 = TestToken;
                var time = counter;
                var paragraphnumber = $(this).attr("paranumber");
                debugger
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "OMR_shet.aspx/save_answer_paragraph",
                    data: JSON.stringify({ test_id: test_id1, timetaken: time, paranumber: paragraphnumber, formno: formno }),
                    success: function (data) {
                        counter = 0;
                    }, error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        });
        $(document).on("change", "input[class='answerRadio']", function () {



            var test_id = TestToken;
            var time = counter;
            var id = $(this).attr("id");
            var idarr = id.split('_');
            var question_id = idarr[1];
            var answer = $("#" + id).val();


            var paragraphnumber = $(this).attr("paranumber");


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "OMR_shet.aspx/save_answer",
                data: JSON.stringify({ test_id: test_id, question_id: question_id, answer: answer, timetaken: time, formno: formno }),

                success: function (data) {
                    $("#div" + question_id).css("background-color", "rgba(136, 160, 153, 0.28)")
                    counter = 0;
                    answeredQuestion++;
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });

        });


        function getAlphabet(r) {
            var alphabet = "";
            if (r == 1) {
                alphabet = "A";
            }
            else if (r == 2) {
                alphabet = "B";
            }
            else if (r == 3) {
                alphabet = "C";
            }
            else if (r == 4) {
                alphabet = "D";
            }
            else if (r == 5) {
                alphabet = "E";
            }

            else if (r == 6) {
                alphabet = "F";
            }

            else if (r == 7) {
                alphabet = "G";
            }

            else if (r == 8) {
                alphabet = "H";
            }

            else if (r == 9) {
                alphabet = "I";
            }
            return alphabet;
        }




    </script>

</asp:Content>
