﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using Newtonsoft.Json.Linq;

public partial class Portal_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep| FlashCards";
        if (!Page.IsPostBack)
        {
            setParameters();
            GetAllQuestionsAndAnswer();

        }
    }


    string token = "";
    private void setParameters()
    {
        string str_sql = "";
        str_sql = "Select top 50 * from fc_repository where len(fc_term) > 4 order by newId()";
        try
        {

            //   str_sql = "Select top 15 * from fc_repository where fc_id in (12,13,20106,88974,112471,20134)";

           
            string quiz_token = fc_gen_utils.new_sticky_id();


            JObject parameters = JObject.FromObject(new
            {
                quiz_q = str_sql,
                st_id = Session["userid"],
                quiz_token = quiz_token

            });
            Session["fcparameters"] = parameters;
        }
        catch(Exception Ex)
        {
            LogError.Log("flashcards.aspx/setParameters", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblError.Style.Add("color", "red");
            lblError.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
        }
    }
    private void GetAllQuestionsAndAnswer()
    {
        try
        {
          
            JObject json = (JObject)Session["fcparameters"];
            string query = json["quiz_q"].ToString();
            token = json["quiz_token"].ToString();
            // string SQL = "Select top 50 * from fc_repository ";
            DataTable dtQuestions = gen_db_utils.gp_sql_get_datatable(query);
            string QuestionsHTML = "";
            int i = 0;
            string firstquestion = "";
            string firstquestionID = "";
            string firstanswer = "";
            foreach (DataRow dtr in dtQuestions.Rows)
            {
                string question = dtr["fc_term"].ToString();
                string solution = gp_utils.html_jax(dtr["fc_desc"].ToString());
                if (i == 0)
                {
                    firstquestion = question;
                    firstquestionID = dtr["fc_id"].ToString();
                    firstanswer = solution;
                }
                i++;
                QuestionsHTML += "<img id='img_" + dtr["fc_id"].ToString() + "' style='float:left;margin-left:15px' src=\"images/omittedSmall.png\" answer='" + solution + "' question='" + question + "'/>";


            }
            lblQuestionsList.Text = QuestionsHTML;

            lblTotalQuestions.Text = dtQuestions.Rows.Count.ToString();
            lblQuestionsStats.Text = "1 / " + dtQuestions.Rows.Count.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "FirstQuestion", "FirstQuestion('img_" + firstquestionID + "');", true);
        }
        catch(Exception Ex)
        {
            LogError.Log("flashcards.aspx/GetAllQuestionsAndAnswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace,"", false);
            lblError.Style.Add("color", "red");
            lblError.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
        }
    }
    [WebMethod]
    public static void InsertReviews(string result, string questionid, string time)
    {
        try
        {
           
            JObject json = (JObject)HttpContext.Current.Session["fcparameters"];
            string token = json["quiz_token"].ToString();
            string QuestionID = questionid;
            string ftime = time;
            string fresult = result;
            if (fresult != "")
            {
                string strSQL = "insert into FlashCard_review values(" + QuestionID + "," + time + ",getdate()," + result + ",'" + token + "')";
                gen_db_utils.gp_sql_execute(strSQL);
            }
        }
        catch(Exception Ex)
        {
            LogError.Log("flashcards.aspx/InsertReviews", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "strSQL", true);
            
        }
    }

    protected void btnDone_ServerClick(object sender, EventArgs e)
    {
        try
        {
           
            JObject json = (JObject)HttpContext.Current.Session["fcparameters"];
            string token = json["quiz_token"].ToString();
            Response.Redirect("~/Portal/fc_end.aspx?token=" + token);
        }
        catch(Exception Ex)
        {
            LogError.Log("flashcards.aspx/btnDone_ServerClick", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblError.Style.Add("color", "red");
            lblError.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
            div1.Visible = false;
        }
    }

    protected void btnComment_Click(object sender, EventArgs e)
    {

    }
}