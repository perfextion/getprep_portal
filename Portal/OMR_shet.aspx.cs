﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class OMR_shet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static void save_answer(string test_id, string question_id, string answer, string timetaken, string formno)
    {

        string correct = corrctanswer(test_id, question_id, answer, formno);

        string strUpdate = "UPDATE  OMR_answers set answered_at=getdate(),answer='" + answer + "',correct='" + correct + "',duration = duration  + " + timetaken
              + " where st_id='" + HttpContext.Current.Session["UserID"] + "' "
              + " and test_key='" + test_id + "' and item_no='" + question_id + "'";
        try
        {
            gen_db_utils.gp_sql_execute(strUpdate);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_shet.aspx/save_answer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strUpdate, true);
            //lblErrormsg.Style.Add("color", "red");
            //lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    private static bool ValidateAnswer(string q_type, string CorrectAnswer, string UserAnswer)
    {
        bool is_valid = false;
        try
        {
            if (q_type == "MC")
            {
                is_valid = (CorrectAnswer == UserAnswer) ? true : false;
               
            }
            if (q_type == "FIB")
            {
                if(!CorrectAnswer.Contains("|"))
                    {
                    is_valid = (CorrectAnswer == UserAnswer) ? true : false;
                    }
                else
                {
                    string[] matches = CorrectAnswer.Split('|');
                    
                    for (int ii = 0; ii < matches.Length; ii++)
                    {
                        if(matches[ii].ToString() == UserAnswer)
                        {
                            is_valid = true;
                            return is_valid;
                        }
                    }
                }
            }
            return is_valid;
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/ValidateAnswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            return false;
        }
    }
    public static string corrctanswer(string testkey, string questionid, string useranswer, string formnumber)
    {
        string answer = "";
        string strGetTest = "  select f.form_q_id,f.q_num,f.q_type as q_type, f.answer as correct_ans from OMR_FormQuestions as f ," +
            "omr_answers as ans where f.form_id = '" + formnumber + "' and ans.test_key = '" + testkey + "' and " +
            "f.q_num = ans.item_no and ans.item_type = 'Q' and q_num = '" + questionid + "' order by q_num";
        try
        {
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strGetTest);

            foreach (DataRow dtr in dt1.Rows)
            {
                if (ValidateAnswer(dtr["q_type"].ToString().ToUpper(),dtr["correct_ans"].ToString().ToUpper(), useranswer))
                {
                    answer = "1";
                }
                else
                {
                    answer = "0";
                }
            }

        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/view_summary", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strGetTest, true);
        }
        return answer;
    }

    [WebMethod]
    public static void save_answer_paragraph(string test_id, string timetaken, string paranumber, string formno)
    {
        string strResult = "insert into   OMR_Answers (answered_at,st_id,test_key,item_no,item_type,duration,paragraph,form_id) VALUES(getdate(),'" + HttpContext.Current.Session["UserID"] + "','" + test_id + "','" + paranumber + "','P','" + timetaken + "','" + paranumber + "','" + Convert.ToInt32(formno) + "')";
        try
        {
            gen_db_utils.gp_sql_execute(strResult);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_shet.aspx/save_answer_paragraph", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strResult, true);
            //lblErrormsg.Style.Add("color", "red");
            //lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    [WebMethod]
    public static void set_session(string jdata)
    {
        HttpContext.Current.Session["SessionSummary"] = jdata;
    }

    [WebMethod]
    public static string Get_All_Question(string formno)
    {
        string strResult = "Select * from OMR_FormQuestions where Form_ID=" + Convert.ToInt32(formno) + "";
        try
        {

            DataTable dt = gen_db_utils.gp_sql_get_datatable(strResult);
            return JsonConvert.SerializeObject(dt);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_shet.aspx/Get_All_Question", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strResult, true);
            return "";
        }
    }

}