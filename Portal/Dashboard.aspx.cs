﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Web.Services;

public partial class Portal_Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep | DashBoard";

        if (!Page.IsPostBack)
        {
           
            if (Session["userid"] != null)
            {
                CheckInCheckOut(Session["userid"].ToString());
                lblUserName.Text = Session["StudentName"].ToString();
                lblDate.Text = Session["SignUp_date"].ToString();
                BindAllCourses();
            }
            else
            {
               // Response.Redirect("/Login.aspx");
            }
        }
        
    }

    public void CheckIn()
    {
        string sql_ins = "insert into st_attendance (st_id,chkin_time,is_active) values" + "(" + "'" + Session["userid"].ToString() + "'," + "getdate()," + "'" + "1" + "'" + ")";
        try
        {
           
            gen_db_utils.gp_sql_execute(sql_ins);
            CheckInCheckOut(Session["userid"].ToString());
        }
        catch (Exception Ex)
        {
            LogError.Log("Dashboard.aspx/CheckIn", Ex.Message, Ex.InnerException, "", Ex.StackTrace, sql_ins, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    public void CheckInCheckOut(string userid)
    {
        lblCheckin.Text = "";
            string str_sql = "select top 1 * , datediff(minute,chkin_time,getdate()) timediffinmin from st_attendance where st_id=" + Session["userid"].ToString() + "order by chkin_time desc";
        try
        {
         
            DataTable dt = new DataTable();
            dt = gen_db_utils.gp_sql_get_datatable(str_sql);
            if (dt.Rows.Count > 0)
            {
                if ((Convert.ToBoolean(dt.Rows[0]["is_active"]) == true) && dt.Rows[0]["chkout_time"].ToString() == "")
                {
                    btnCheckOut.Visible = true;
                    btnActivity.Visible = true;
                    lblTimeSpent.Visible = true;
                    iClock.Visible = true;
                    btnCheckIn.Visible = false;
                    lblCheckin.Visible = true;
                    lblCheckin.Text = "Last Login " + Convert.ToDateTime(dt.Rows[0]["chkin_time"]).ToString("dd/MM/yyyy HH:mm");
                    txtComment.InnerText = dt.Rows[0]["comments"].ToString();
                    if (dt.Rows[0]["timediffinmin"] != null)
                    {
                        int totaltime = Convert.ToInt32(dt.Rows[0]["timediffinmin"]);
                        int timespentinhour = (totaltime / 60);
                        int minute = (totaltime % 60);
                        lblTimeSpent.Text = timespentinhour.ToString() + " Hrs " + minute.ToString() + " Min ";
                        lblTime.Text = timespentinhour.ToString() + " Hrs " + minute.ToString() + " Min ";
                    }
                }
                else
                {
                    btnCheckIn.Visible = true;
                    lblTimeSpent.Visible = false;
                    iClock.Visible = false;
                    btnActivity.Visible = false;
                    btnCheckOut.Visible = false;
                    lblCheckin.Visible = true;
                    lblCheckin.Text = "Last Login " + Convert.ToDateTime(dt.Rows[0]["chkin_time"]).ToString("dd/MM/yyyy HH:mm") + " to " + Convert.ToDateTime(dt.Rows[0]["chkout_time"]).ToString("dd/MM/yyyy HH:mm") + "";
                }
            }
            else
            {
                btnCheckOut.Visible = false;
                btnActivity.Visible = false;
                lblTimeSpent.Visible = false;
                iClock.Visible = false;
                btnCheckIn.Visible = true;
                lblCheckin.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Dashboard.aspx/CheckInCheckOut", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    public void BindAllCourses()
    {
        string str_sql = "select * from st_syllabus S "
              + " join core_cat C "
              + " on s.item_id = c.id "
             + " where c.tree_level = 0 and st_id = " + Session["userid"].ToString();
        try
        {
           
            DataTable dtCourses = gen_db_utils.gp_sql_get_datatable(str_sql);
            lstCourses.DataSource = dtCourses;
            lstCourses.DataBind();
        }
        catch (Exception Ex)
        {
            LogError.Log("Dashboard.aspx/BindAllCourses", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    [WebMethod]
    public static object funPractice(string id, int level)
    {
        string str_sql = "select top 100 type2_id from type2_qs where cc" + level + " = " + id + " and status = -950 ";

        if (level == 0 || level == 1)
        {
            str_sql += " and type2_id not in (select q_seq_id from st_answer where student_id = " + HttpContext.Current.Session["userid"].ToString() + "  ) ";
        }

        str_sql += " order by newid() ";

        HttpContext.Current.Session["debug_999"] = str_sql;
        ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

        string str_sql_title = "select top 1 cat_name from core_cat where cc" + level + " = " + id;

        string title = gen_db_utils.gp_sql_scalar(str_sql_title);

        o_quiz_param item_q_param = new o_quiz_param
        {
            quiz_title = title,

            start_time = DateTime.Now.ToString(),

            last_activity_time = DateTime.Now.ToString(),
            query = str_sql,
            quiz_q_string = fc_gen_utils.arraylist2string(ar_qs),
            quiz_code = fc_gen_utils.new_batch_id()
        };

        item_q_param.set2session();

        HttpContext.Current.Session["cqz_quiz_start"] = str_sql;

        return new
        {
            Data = new
            {
                id,
                url = "/Portal/quiz.aspx?courseid=0"
            }
        };
    }

    [WebMethod]
    public static object funReviewPractice(string id, int level)
    {
        string str_sql = "select top 50 sa.q_seq_id from st_answer sa inner join type2_qs tq on sa.q_seq_id = tq.type2_id " +
                        "where sa.student_id = " + HttpContext.Current.Session["userid"].ToString() + " and sa.correct = 0";

        if (level == 0 || level == 1)
        {
            str_sql += " and tq.cc" + level + " = " + id +  " ";
        }

        str_sql += " order by newid()";

        HttpContext.Current.Session["debug_999"] = str_sql;
        ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

        string str_sql_title = "select top 1 cat_name from core_cat where cc" + level + " = " + id;

        string title = gen_db_utils.gp_sql_scalar(str_sql_title);

        o_quiz_param item_q_param = new o_quiz_param
        {
            quiz_title = title,

            start_time = DateTime.Now.ToString(),

            last_activity_time = DateTime.Now.ToString(),
            query = str_sql,
            quiz_q_string = fc_gen_utils.arraylist2string(ar_qs),
            quiz_code = fc_gen_utils.new_batch_id()
        };

        item_q_param.set2session();

        HttpContext.Current.Session["cqz_quiz_start"] = str_sql;

        return new
        {
            Data = new
            {
                id,
                url = "/Portal/quiz.aspx?courseid=0"
            }
        };
    }

    protected void btnCheckOut_Click(object sender, EventArgs e)
    {
        string comm = txtComment.InnerText;
        string sql_ins = "update st_attendance set  comments='" + comm + "' ,chkout_time = getdate(),  is_active=0 where st_id = " + Session["userid"].ToString() + "and chkout_time is null and is_active=1 ";
        gen_db_utils.gp_sql_execute(sql_ins);
        CheckInCheckOut(Session["userid"].ToString());      
    }

    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        CheckIn();
    } 
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string comm = txtComment.InnerText;
          
        string sql_ins = "update st_attendance set  comments='" + comm + "' where st_id = " + Session["userid"].ToString() + "and chkout_time is null and is_active=1";
        try
        {
           
            gen_db_utils.gp_sql_execute(sql_ins);
        }
        catch (Exception Ex)
        {
            LogError.Log("Dashboard.aspx/CheckInCheckOut", Ex.Message, Ex.InnerException, "", Ex.StackTrace, sql_ins, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }    
}