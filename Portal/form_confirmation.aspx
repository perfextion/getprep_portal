﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="form_confirmation.aspx.cs" Inherits="form_confirmation" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="course-syllabus-title underline" id="abc" tabindex="1">
        Confirmation:
    </div>
    <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
    <asp:HiddenField ID="debugonsession" runat="server" />

    <div class="row">
        <table style="width: 40%!important; display: none" class="table" id="tableid">
            <tbody>
                <tr>
                    <td style="width: 30%">Do not show answers after test?:</td>
                    <td>
                        <input id="chkShowAnswers" type="checkbox" /></td>
                </tr>
                <tr>
                    <td style="width: 30%">Do not time the test?:</td>
                    <td>
                        <input id="chkTime" type="checkbox" /></td>
                </tr>
                <tr>
                    <td style="width: 30%">Form ID:</td>
                    <td><span id="st_form_id"></span></td>
                </tr>
                <tr>
                    <td style="width: 30%">Form No:</td>
                    <td><span id="st_form_no"></span></td>
                </tr>
                <tr>
                    <td>Type:</td>
                    <td><span id="type"></span></td>
                </tr>
                <tr>
                    <td>Total Paragraphs:</td>
                    <td><span id="total_paragraphs"></span></td>
                </tr>
                <tr>
                    <td>Total Questions:</td>
                    <td><span id="total_questions"></span></td>
                </tr>
                <tr>
                    <td>Number Of Choices:</td>
                    <td><span id="no_of_choices"></span></td>
                </tr>
                <tr>
                    <td>Paragraphs:</td>
                    <td><span id="paragraphs"></span></td>
                </tr>
                <tr>
                    <td>Total Time for Test:</td>
                    <td><span id="total_time"></span></td>
                </tr>
                <tr>
                    <td>Is even odd/choices:</td>
                    <td><span id="isevenodd"></span></td>
                </tr>
                <tr>
                    <td>Odd Choices:</td>
                    <td><span id="oddchoices"></span></td>
                </tr>
                <tr>
                    <td>Even Choices:</td>
                    <td><span id="evenchoices"></span></td>
                </tr>
                <tr id="trShowCorrectAns">
                    <td>
                        <input type="button" style="display: none" value="Hide Answers" class="btn-primary" id="btnHideAns" onclick="return funHideAns()" />
                        <input type="button" value="Show Answers" id="btnShowAns" class="btn-primary" onclick="    return funShowAns()" /></td>
                </tr>
                <tr id="trCorrectAnswer" style="display: none">
                    <td>Correct Answers:</td>
                    <td><span id="correctanswers"></span></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 50%!important; display: none;" class="table" id="tableready">
            <tbody>
                <tr>
                    <td style="width: 100%">You are ready to start <b><span id="st_form_name"></span></b>. <br /> 
                        There are <span id="total_qn"></span> questions. 
                        Total Duration for this test is <span id="total_qn_duration"></span> minutes. <br /> 
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="chkdisableTimeLimit">
                            <label class="form-check-label" for="exampleCheck1">Disable timeout limit.</label>
                        </div>
                        Please click to Start Test. </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <input id="btn_go" onclick='SaveData();' type="button" value="Start Test" class="btn btn-success" />
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script src="Plugin/gp_Common.js"></script>
    <script>
        function funShowAns() {
            $("#trCorrectAnswer").show();
            $("#btnHideAns").show();
            $("#btnShowAns").hide();
        }
        function funHideAns() {
            $("#trCorrectAnswer").hide();
            $("#btnHideAns").hide();
            $("#btnShowAns").show();
        }
        $(document).ready(function () {
            var form_id = GetQuerystringValue("form_no");
            GetForm(form_id);
        });
        function GetQuerystringValue(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
            results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return "";
            return decodeURIComponent(results[2].replace(/\+/g, ""));
        }
        function GetForm(form_id) {
            $.ajax({
                contentType: "application/json; charset=utf-8",
                url: "form_confirmation.aspx/Get_Form",
                data: JSON.stringify({ form_id: form_id }),
                type: "POST",
                dataType: "json",
                success: function (data) {
                    var datajs = JSON.parse(data.d);
                    $.each(datajs, function (i, datas) {
                        var paragraphs = datas.total_paragraphs;

                        $("#st_form_id").text(datas.form_id);
                        $("#st_form_no").text(datas.form_no);
                        $("#st_form_name").text(datas.form_no);
                        $("#type").text(datas.test_type);
                        $("#total_paragraphs").text(datas.total_paragraphs);
                        $("#total_questions").text(datas.total_questions);
                        $("#total_qn").text(datas.total_questions);
                        $("#no_of_choices").text(datas.num_choices);
                        $("#paragraphs").text(datas.para_starts);
                        $("#isevenodd").text(datas.is_even_odd);
                        $("#oddchoices").text(datas.odd_letters);
                        $("#evenchoices").text(datas.even_letters);
                        $("#correctanswers").text(datas.answers);
                        $("#total_time").text(datas.total_duration);
                        $("#total_qn_duration").text(Math.floor(datas.total_duration / 60));

                    })
                }, error: function (jqXHR, textStatus, errorThrown) {

                }
            })
        }
        function sessiondebugon() {
            var sessiondebugon = $("#ContentPlaceHolder1_debugonsession").val();
            if (sessiondebugon == 1) {
                $("#tableid").show();
                $("#tableready").hide();
            }
            else {
                $("#tableid").hide();
                $("#tableready").show();
            }
        }
        function SaveData() {
            var test_key = '<%= fc_gen_utils.new_sticky_id().ToString() %>';
            var form_no = $("#st_form_id").text();
            jsonobj = [];
            item = {};
            item["total_paragraphs"] = $("#total_paragraphs").text();
            item["total_questions"] = $("#total_questions").text();;
            item["test_type"] = $("#type").text();
            item["num_choices"] = $("#no_of_choices").text();
            item["total_duration"] = $("#total_time").text();
            item["para_starts"] = $("#paragraphs").text();
            item["form_no"] = form_no;
            item["is_even_odd"] = $("#isevenodd").text();
            item["odd_letters"] = $("#oddchoices").text();
            item["even_letters"] = $("#evenchoices").text();
            item["answers"] = $("#correctanswers").text();
            item["NotAnswers"] = $("#chkShowAnswers").prop("checked");
            item["NotTime"] = $("#chkTime").prop("checked") || $("#chkdisableTimeLimit").prop("checked");
            item["test_key"] = test_key;
            jsonobj.push(item);

            var jsonstring = JSON.stringify(jsonobj);

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ jdata: jsonstring, form_no: form_no, test_key: test_key }),
                url: "form_confirmation.aspx/set_session",
                success: function (data) {
                    window.location.href = "OMR_Shet.aspx"
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    </script>
</asp:Content>

