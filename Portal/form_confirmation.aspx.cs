﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class form_confirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["debug_on"] != null)
        {
            if (Session["debug_on"].ToString() == "1")
                debugonsession.Value = "1";
        }
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "sessiondebugon()", true);
    }

    [WebMethod]
    public static string Get_Form(string form_id)
    {
        string strResult = "";
        string strForms = "SELECT * from OMR_Form where form_id=" + Convert.ToInt32(form_id);
        try
        {

            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            return strResult = JsonConvert.SerializeObject(dt);
        }
        catch (Exception Ex)
        {
            LogError.Log("form_confirmation.aspx/Get_Form", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            //lblErrormsg.Style.Add("color", "red");
            //lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            return "";
        }

    }

    [WebMethod]
    public static void set_session(string jdata, string form_no, string test_key)
    {
        string strInsertResult = "insert into omr_answers(st_id,form_id,test_key,item_no,item_type,answer,duration,paragraph) select '"
                + HttpContext.Current.Session["UserID"]
                + "','" + Convert.ToInt32(form_no) + "','"
                + test_key + "',q_num,'Q','-','0','-'from OMR_FormQuestions where Form_ID=" + Convert.ToInt32(form_no);

        try
        {

            HttpContext.Current.Session["OMRjson"] = jdata;


            gen_db_utils.gp_sql_execute(strInsertResult);
        }
        catch (Exception Ex)
        {
            LogError.Log("form_confirmation.aspx/set_session", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strInsertResult, true);
            //lblErrormsg.Style.Add("color", "red");
            //lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }

    }

}