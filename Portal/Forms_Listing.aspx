﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Forms_Listing.aspx.cs" Inherits="Forms_Listing" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table-hover > tbody > tr:hover {
            background-color: #bbf3f0;
            cursor: pointer
        }

        .panel {
            border: 1px solid #d6e9c6;
        }
    </style>
    <div class="course-syllabus-title underline" id="abc" tabindex="1">
        Select Forms:
    </div>
    <b>
        <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-1 control-label">Type</label>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddltype" CssClass="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddltype_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <asp:Label ID="lbl_top" runat="server"></asp:Label>
            <asp:Label ID="lbl_table" runat="server"></asp:Label>
        </div>

    </div>
    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
</asp:Content>

