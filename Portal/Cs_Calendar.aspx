﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Cs_Calendar.aspx.cs" Inherits="Portal_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblStudentId" runat="server" Style="display: none"></asp:Label>

    
         
            <div class="col-sm-10">
                <div class="course-syllabus-title underline">
                    Enroll Classes
                </div>
            </div>

    <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
    <div class="form-group">

        <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
            <label class="btn bg-color-darken active">
                <input type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" checked="">
                <i class="fa fa-check txt-color-white">Cancelled</i>
            </label>
            <label class="btn bg-color-blue">
                <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white">
                <i class="fa fa-check txt-color-white">Not Enrolled</i>
            </label>
            <label class="btn bg-color-orange">
                <input type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white">
                <i class="fa fa-check txt-color-white">Waitlisted</i>
            </label>
            <label class="btn bg-color-greenLight">
                <input type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white">
                <i class="fa fa-check txt-color-white">Enrolled</i>
            </label>
            <label class="btn bg-color-blueLight">
                <input type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white">
                <i class="fa fa-check txt-color-white">Attended</i>
            </label>
            <label class="btn bg-color-red">
                <input type="radio" name="priority" id="option6" value="bg-color-red txt-color-white">
                <i class="fa fa-check txt-color-white">No Show</i>
            </label>
        </div>
    </div>


    <div id='calendar'></div>
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title" style="float: left">EnRoll For Class</h4>
                    <div class="col-sm-6">
                        <label id="lblenroll" class="col-sm-2"></label>
                    </div>


                </div>
                <div id="modalBody" class="modal-body">
                    <div class="row">
                        <div class="col-sm-1"></div>

                        <div class="col-sm-6">
                            <input type="hidden" id="cs_id" />
                            <input type="hidden" id="scs_id" />
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <label for="Title" class="col-sm-2"><b>Class:</b></label>
                        <div class="col-sm-6">
                            <label for="Start dat" id="lblclass" class="col-sm-6"></label>

                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <label for="Start dat" class="col-sm-2"><b>Start:</b></label>
                        <div class="col-sm-6">

                            <label for="Start dat" id="lblstartdate" class="col-sm-12"></label>

                        </div>

                        <div class="help-block"></div>


                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <label for="End Date" class="col-sm-2"><b>End:</b></label>
                        <div class="col-sm-6">
                            <label id="lblenddate" class="col-sm-6"></label>

                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <label for="End Date" class="col-sm-2"><b></b></label>
                        <div class="col-sm-6">
                            <label id="Label1" class="col-sm-6"></label>

                            <div class="help-block"></div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-1"></div>
                        <label for="End Date" class="col-sm-2"><b>Status:</b></label>
                        <div class="col-sm-6">
                            <label id="lblstatus" class="col-sm-6"></label>

                            <div class="help-block"></div>
                        </div>

                    </div>
                </div>


                <div class="modal-footer">

                    <input class="btn btn-primary" type="button" onclick="return Enroll();" value="Enroll" id="btnenroll" />
                    <button class="btn btn-info" type="submit" onclick="return ChangeStatus(2);" id="btnCancelled">Cancelled</button>

                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <%--  <link href="/Portal/portalassets/css/MyEventHeader.css" rel="stylesheet" />--%>
    <link href="/Portal/Plugin/fullcalendar-2.4.0/fullcalendar.css" rel="stylesheet" />
    <%--  <script src="portalassets/js/bootstrap.min.js"></script> --%>

    <script src="/Portal/Plugin/fullcalendar-2.4.0/lib/moment.min.js"></script>
    <script src="/Portal/Plugin/fullcalendar-2.4.0/fullcalendar.js"></script>


    <script>
        var calendar;

        function RefreshCalendar() {
            $('#fullCalModal').modal('hide');
            $('#calendar').fullCalendar('refetchEvents');
        }
        $(document).ready(function () {

            calendar = $('#calendar').fullCalendar({
                events: {
                    url: '/Portal/WebServices/GetPrep.asmx/GetClassScheduleCalendar',
                    data: {
                        studentid: $("#ContentPlaceHolder1_lblStudentId").text()

                    },
                    type: "Post",
                    success: function (events) {
                        events = $.map(events, function (e) {
                            return e;
                        });
                        return events;
                    }
                },

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                eventClick: function (event, jsEvent, view) {


                    $('#cs_id').val(event.pk_cs_id);
                    $("#scs_id").val(event.scs_id)
                    $('#lblclass').text(event.title);
                    $('#lblenroll').text(event.title);
                    $('#lblstatus').text(event.Status);

                    $("#lblstartdate").text(moment(event.start, "MMDDYYYY HH:mm").format('MM/DD/YYYY HH:mm'));
                    $("#lblenddate").text(moment(event.end, "MMDDYYYY").format('MM/DD/YYYY'));
                    $("#txtstartTime").text(event.starttime);
                    if (parseInt(event.scs_id) > 0) {
                        $("#btnCancelled").show();
                        $("#btnenroll").hide();

                    }
                    else {
                        $("#btnCancelled").hide();
                        $("#btnenroll").show();
                    }
                    $('#fullCalModal').modal();

                    return false;


                }
            });

        })


    </script>
    <script>
        function Enroll() {

            $.ajax({
                type: "Post",
                url: '/Portal/WebServices/GetPrep.asmx/AddClassScheduleCalendar',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    cs_id: $("#cs_id").val(),
                    studentid: $("#ContentPlaceHolder1_lblStudentId").text()

                }),
                error: function (jqXHR, textStatus, errorThrown) {

                },
                success: function () {
                    RefreshCalendar();
                }


            })
            return false;


        };
        function ChangeStatus(status) {

            $.ajax({
                type: "Post",
                url: "../Portal/webservices/GetPrep.asmx/ChangeClassScheduleCalendar",
                data: {
                    scs_id: $('#scs_id').val(),
                    status: status
                },
                success: function () {
                    RefreshCalendar();
                }
            });
            return false;

        }

    </script>
</asp:Content>

