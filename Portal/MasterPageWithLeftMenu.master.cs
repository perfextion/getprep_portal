﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_MasterPageWithLeftMenu : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("/Login.aspx");
        }
        GenerateMenu();
    }

    private void GenerateMenu()
    {
        string strMenu = "<ul class='list-group'>"
                                       +" <li class='icon-bg'>"
                                          +"   <div class='col-lg-3'>"
                                           +"      <div class='icon'>"
                                           +"          <i class='fa fa-user-circle-o fa-2x'></i>"
                                           +"      </div>"
                                          +"   </div>"
                                          +"   <div class='col-lg-9'>"
                                         +"        <h4 style='word-break:break-all'>" + Session["username"].ToString() + "</h4>"
                                          +"       <h5>Member since 2017</h5>"
                                           +"  </div>"
                                          +"   <div class='clearfix'></div>"
                                        +" </li>"
                                       +"  <li class='blue activeleftnav'>"
                                       +"      <a href='/portal/dashboard.aspx'><i class='fa fa-tachometer'>"
                                         +"        <h4>Dashboard</h4>"
                                       +"      </i></a>"
                                       +"  </li>                   "              
                                       +"  <li class='blue'>"
                                       +"      <a href='/portal/quiz_summary.aspx'><i class='fa fa-area-chart'>"
                                         +"        <h4>Recent Activity</h4>"
                                        +"     </i></a>"
                                        +" </li>"
                                        +" <li class='blue'>"
                                        +"     <a href='#'><i class='fa fa-indent '>"
                                         +"        <h4>My Courses</h4>"
                                         +"    </i></a>"
                                        +" </li>"
                                        +" <li class='blue'>"
                                         + "    <a href='/portal/Forms_Listing.aspx'><i class='fa fa-calendar'>"
                                        +"         <h4>OMR Listing</h4>"
                                         +"    </i></a>"
                                        +" </li>"
                                        +" <li class='blue'>"
                                        + "     <a href='/portal/AttenanceView.aspx'><i class='fa fa-file-text-o'>"
                                         +"        <h4>Attendance</h4>"
                                        +"     </i></a>"
                                        +" </li>"
                                       // +" <li class='blue'>"
                                       // +"     <a href='#'><i class='fa fa-play-circle-o'>"
                                       // +"         <h4>Custom Courses</h4>"
                                       //  +"    </i></a>"
                                       //+"  </li>"
                                       +"  <li class='blue'>"
                                         +"    <a href='#'><i class='fa fa-files-o'>"
                                         +"        <h4>Instant Answers</h4>"
                                         +"    </i></a>"
                                         +"</li>"
                                         +"<li class='white'>"
                                         +"    <h5>Accounts</h5>"
                                        +" </li>"
                                        +" <li class='blue'>"
                                         + "    <a href='/portal/Welcome.aspx'><i class='fa fa-id-card-o '>"
                                         +"        <h4>Membership</h4>"
                                           +"  </i></a>"
                                        + " </li>"
                                     +"</ul>";
        // removed from left menu temporarily

               //                          <li class='white'>
               //                             <h5>Study Tool</h5>
              //                          </li>

        lblMenu.Text = strMenu;
    }
}
