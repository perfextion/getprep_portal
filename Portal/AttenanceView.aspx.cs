﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_AttenanceView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        // Session["userid"] = "1299";
        
            //lblStudentId.Text = Session["userid"].ToString();
            //checkinstatut(Session["userid"].ToString());
            //Label1.Text = "ALL";
     
        if (!Page.IsPostBack)
        {
           
            if (Session["userid"] != null)
            {
                lblStudentId.Text = Session["userid"].ToString();
                checkinstatut(Session["userid"].ToString());
                Label1.Text = "ALL";
                GetGoals();
            }
            else
             {
                // Response.Redirect("/Login.aspx");
             }
            //GetGoals();
        }

    }
    public void checkin()
    {
        string sql_ins = "insert into st_attendance (st_id,chkin_time,is_active,goal,subjects) values "
                      + " ( "
                    + "'" + lblStudentId.Text + "',"
                    + "getdate(),"
                    + "'" + "1" + "',"
                    + "'" + ddlchkingoals.SelectedValue + "',"
                    + "'" + ddlchkinsubject.SelectedValue + "'"
                    + ")";

        try
        {
           
            gen_db_utils.gp_sql_execute(sql_ins);
            
            checkinstatut(lblStudentId.Text);
            ddlchkingoals.SelectedIndex = 0;
        }
        catch(Exception Ex)
        {
            LogError.Log("AttenanceView.aspx/checkin", Ex.Message, Ex.InnerException, "", Ex.StackTrace, sql_ins, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
           
            div1.Visible = false;
        }
    }
    public void checkinstatut(string userid)
    {
        string str_sql = "select * from st_attendance where st_id=" + userid + " and  CONVERT(date, chkin_time)=CONVERT(date, getdate()) and chkout_time is null";
        try
        {
           
            lblatt_id.Text = "";
          
            DataTable data = new DataTable();
            data = gen_db_utils.gp_sql_get_datatable(str_sql);
            if (data.Rows.Count > 0)
            {
                lblatt_id.Text = data.Rows[0]["att_id"].ToString();
                btnchkin.Enabled = false;
                btnchkout.Enabled = true;
            }
            else
            {
                btnchkin.Enabled = true;
                btnchkout.Enabled = false;
            }

        }
        catch(Exception Ex)
        {
            LogError.Log("AttenanceView.aspx/checkinstatut", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
          
            div1.Visible = false;
        }
    }


    public void GetGoals()
    {
       
        string str_sql = "select Distinct Goal from st_attendance";
        try
        {
            DataTable data = new DataTable();
           
            data = gen_db_utils.gp_sql_get_datatable(str_sql);
           
            ddlgoals.DataSource = data;
            ddlgoals.DataTextField = "Goal";
            ddlgoals.DataValueField = "Goal";
            ddlgoals.DataBind();
            ddlgoals.Items.Insert(0, new ListItem("ALL", "ALL"));

            ddlchkingoals.DataSource = data;
            ddlchkingoals.DataTextField = "Goal";
            ddlchkingoals.DataValueField = "Goal";
            ddlchkingoals.DataBind();
            ddlchkingoals.Items.Insert(0, new ListItem("Select goal", "Select goal"));

            DataTable datasub = new DataTable();
            str_sql = "select Distinct top 10 * from subjects";
            datasub = gen_db_utils.gp_sql_get_datatable(str_sql);

            ddlsubject.DataSource = datasub;
            ddlsubject.DataTextField = "subj_name";
            ddlsubject.DataValueField = "subj_name";
            ddlsubject.DataBind();
            ddlsubject.Items.Insert(0, new ListItem("ALL", "ALL"));


            ddlchkinsubject.DataSource = datasub;
            ddlchkinsubject.DataTextField = "subj_name";
            ddlchkinsubject.DataValueField = "subj_name";
            ddlchkinsubject.DataBind();
            ddlchkinsubject.Items.Insert(0, new ListItem("Select subject", "Select subject"));


        }

        catch(Exception Ex)
        {
            LogError.Log("AttenanceView.aspx/GetGoals", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
           
            div1.Visible = false;
        }
    }

    protected void btnchkin_Click(object sender, EventArgs e)
    {
        if (ddlchkingoals.SelectedIndex != 0 && ddlchkinsubject.SelectedIndex!=0)
        {
            checkin();
        }
        else
        {
            string script = "alert(\"please select goal or sbject.\");";
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "ServerControlScript", script, true);
        }

            }

    protected void btnchkout_Click(object sender, EventArgs e)
    {
        txtcomment.InnerText = "";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModalcomment').modal();", true);

    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        
        string s = txtcomment.InnerText;
        string ss = "";
        string sql_ins = "update st_attendance set chkout_time = getdate() ,comments='" + s + "' where st_id = " + lblStudentId.Text + "and att_id = " + lblatt_id.Text + "";
        try
        {
            
           
            gen_db_utils.gp_sql_execute(sql_ins);
           
            checkinstatut(lblStudentId.Text);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Close Modal Popup", "Closepopup();", true);
        }
        catch(Exception Ex)
        {
            LogError.Log("AttenanceView.aspx/btnsubmit_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, sql_ins, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
           
            div1.Visible = false;
        }
    }
}