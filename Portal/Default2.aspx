﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Portal_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <style>
        label {
            font-weight:normal;
        }
    </style>
     <link href="portalassets/css/gpFlashCard.css" rel="stylesheet" />

    <script src="portalassets/plugin/Timer/timer.js"></script>
    <script src="portalassets/js/jquery-ui-1.10.1/ui/jquery.ui.effect.js"></script>
    <script>
        function funFlipAnswer() {
            if ($("#myCard").hasClass("flip")) {
                $("#btnGotit").show();
                $("#btnDidntGet").show();
                $("#btnPrev").hide();
                $("#btnNext").hide();
                $("#btnrefresh").hide();
            }
            else {
                $("#btnGotit").hide();
                $("#btnDidntGet").hide();
                $("#btnPrev").show();
                $("btnrefresh").show();
                $("btnNext").show();
            }
            if (currentQuestion == 1) {
                $("#btnPrev").hide();
            }
            if (currentQuestion == totalQuestions) {

                $("#btnNext").hide();
            }
        }
        function FirstQuestion(currentQuestionID) {
            $("#lblMainQuestion").attr("currentQuestion", currentQuestionID);

            var currentQuestion = $("#" + currentQuestionID).attr("question");
            var currentAnswer = $("#" + currentQuestionID).attr("answer");
            
            $("#lblMainQuestion").html(currentQuestion);

            $(".card__text").html(currentAnswer);

            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblAnswer']);

        }
        function funChangeQuestion(action) {
            if ($("#myCard").removeClass("flip"));
            funFlipAnswer();
            var selectedEffect = "slide";
            var options = {};
           
            $("#myCard").effect(selectedEffect, options, 500, callback);
            $(".card__text").html("test");
            var CurrentQuestionID = $("#lblMainQuestion").attr("currentQuestion");
            var NextQuest = "";
            var NextQuestionID = "";
            var NextQuestion = "";
            var NextAnswer = "";
            if (CurrentQuestionID == "") {
                NextQuest = $("#ContentPlaceHolder1_lblQuestionsList :first-child");
            }
            else if (action == "gotit" || action == "didget") {
                NextQuest = $("#" + CurrentQuestionID).next();
            }
            else if (action == "next") {
                NextQuest = $("#" + CurrentQuestionID).next();
            }
            else {
                NextQuest = $("#" + CurrentQuestionID).prev();
            }
             
            NextQuestionID = $(NextQuest).attr("id");
            NextQuestion = $(NextQuest).attr("question");
            NextAnswer = $(NextQuest).attr("answer");
            $("#lblMainQuestion").attr("currentQuestion", NextQuestionID);
            $("#lblMainQuestion").html(NextQuestion);
            $(".card__text").html(NextAnswer);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblAnswer']);

        };

        var totalQuestions = 40;
        var currentQuestion = 1;
        var GetIt = 0;
        var MissedIt = 0;
        function funNext(action){
            var CurrentQuestionID=$("#lblMainQuestion").attr("currentQuestion");
            totalQuestions=$(".lblTotalQuestions").text();
            if (currentQuestion < totalQuestions) {
                currentQuestion = currentQuestion + 1;
                var proportion = (parseInt(100) / parseInt(totalQuestions)) * parseInt(currentQuestion);

                $(".QuestionsStats").text(currentQuestion + " / " + totalQuestions);

                funChangeQuestion(action);
                if (currentQuestion == totalQuestions) {
                    $("#btnNext").hide();

                }
            }

            if (action == "gotit") {
                GetIt++;
                $('#green').html(GetIt);


                $("#" + CurrentQuestionID).attr("src", "images/correctSmall.png");
                funSaveTime("1", CurrentQuestionID);
            }
            else if (action == "didget") {
                MissedIt++;
                $('#red').html(MissedIt);
                $("#" + CurrentQuestionID).attr("src", "images/incorrectSmall.png");
                funSaveTime("0", CurrentQuestionID);

            }


            return false;
        }

        function funPrevious() {

            totalQuestions = $(".lblTotalQuestions").text();

            if (currentQuestion > 1) {
                currentQuestion = currentQuestion - 1;

                $(".QuestionStats").text(currentQuestion + " / " + totalQuestions);
                funChangeQuestion('prev');

                if (currentQuestion == 1) {

                    $("#btnPrev").hide();

                }
                
            }

            return false;
        }

        function funSaveTime(result, CurrentQuestionID) {
            var QuestionID = CurrentQuestionID.replace("img_", "");

            var time = parseInt(counter);

            counter = 0;
            $.ajax({
                type: "POST",
                url: "flashcards.aspx/InsertReviews",
                data: JSON.stringify({ result: result, questionid: QuestionID, time: time }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                },
                failure: function (response) {

                }
            });

        }
        function funOnSuccess() { }
        function callback() {
        };
        // Timer

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" Runat="Server">
    <div class="course-syllabus-titile underline">
        Flashcard
        <input type="button" id="btnDone" class="btn btn-green" style="float:right" runat="server" onserverclick="btnDone_ServerClick" value="Done" />
    </div>

    <div class="row" id="div">
        <div class="col-sm-9">
            <asp:Label ID="lblTotalQuestions" runat="server" CssClass="lblTotalQuestions" style="display:none"></asp:Label>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-9">

                        <span>
                            <asp:Label ID="lblQuestionStats" runat="server" CssClass="QuestionStats"></asp:Label>
                        </span>

                    </div>
                    <div class="col-sm-3">
                        <div style="float:right"><span class="glyphicon glyphicon-remove icon-danger  pull-right" style="color:red"></span></div>
                        <div class="col-sm-1" style="float:right"><span id="red">0</span></div>
                        <div style="float:right"><span class="glyphicon glyphicon-ok icon-success pull-right" style="color: green"></span></div>
                        <div class="col-sm-1" style="float: right"><span id="green">0</span></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="myCard" onclick='$("#myCard").toggleClass("flip");return funFlipAnswer();' class="flip-container vertical">
                        <div class="flipper">
                            <div class="front" id="front">
                                <label id="lblf" class="pull-right"></label>
                                <span class="Centerer"></span>
                                <div id="lblMainQuestion" class="Centered" currentquestion=""></div>

                            </div>
                            <div class="back">
                                <label id="lblb" class="pull-right"></label>
                                <label id="lblAnswer" class="card__text"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button class="btn btn-gray" onclick="return funNext('gotit');" style="display:none; float:left; width:49%; height:50px; font-size:large; color: darkturquoise" id="btnGotit" >GOT IT</button>
                    <button class="btn btn-gray" onclick="return funNext('didget');" style="display:none; float:right; width:50%; height:50px; font-size:large;color: darkturquoise" id="btnDidntGet">MISSED IT</button>
                </div>

            </div>
            <div class="col-lg-12">
                <asp:Label ID="lblQuestionsList" Style="display:none" runat="server" CssClass="lblQuestionsList" Width="750px"></asp:Label>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="panel-info" style="background-color:white">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-3">
                            <label id="lblTime" class="lblTime" style="color:green">Time :</label>
                            <label id="lblTotalTime" class="lblTotalTime" style="color:green">Total Time :</label>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default" style="background-color:white">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-3">
                            <span>Problem With Questions</span>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="margin: 5px;">
                    <div class="row">
                         <div class="form-group" id="divComments">
                             <select id="ddlComments" onchange="funchangeComment();" class="form-control" style="float:left">
                                 <option>Select</option>
                                 <option>Not in Syllabus</option>
                                 <option>Not Clear</option>
                             </select>
                             </div>
                    </div>


                    <div class="row">
                          <div style="float: right; width: 170px;">
                            <div class="form-group">
                                <input type="submit" id="btnSkip" style="display: none; float: right; width: 75px;" value="Skip" class="btn btn-primary" onclick="return funSkip();" />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div style="float: right">
                            <input type="submit" id="btnSubmit" style="display: none" value="Submit" class="btn btn-green" onclick="return funSubmitComment();" />
                        </div>  
                    </div>
                </div>
            </div> 
        </div>
    </div>

     <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-3">
                <span class="glyphicon glyphicon-chevron-left  fa-2x disabled" style="display: none; cursor: pointer; color: darkturquoise" id="btnPrev" onclick="return funPrevious();"></span>
            </div>
            <div class="col-lg-2">
                <span class="glyphicon glyphicon-refresh fa-2x" style="cursor: pointer; color: darkturquoise" id="btnrefresh" onclick='$("#myCard").toggleClass("flip");return funFlipAnswer();'></span>
            </div>
            <div class="col-lg-2">
                <span class="glyphicon glyphicon-chevron-right  fa-2x pull-right" style="cursor: pointer; color: darkturquoise;" id="btnNext" onclick="return funNext('next');"></span>
            </div>
            <div class="col-lg-1">
            </div>
        </div>
    </div> 
</asp:Content>

