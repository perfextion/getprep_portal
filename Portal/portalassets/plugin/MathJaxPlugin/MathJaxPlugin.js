﻿ 
  
function funSladderOpen(textbox) {
    
    $(textbox).prev().addClass("activeMathJaxPlugin");
   //$("<div id='divPopup'  class=\"MathJaxPluginDiv\"></div>").appendTo(document.body);
    $('<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> '+
 '       <div class="modal-dialog"> '+
 '           <div class="modal-content"> '+
 '               <div class="modal-header"> '+
 '                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> '+
 '                       &times; '+
 '   </button> '+
 '   <h4 class="modal-title" id="myModalLabel">MathJax </h4>'+
'</div>'+
'<div class="modal-body "> ' +
   ' <div class="row">'+
   '     <div class="col-md-12 MathJaxPluginDiv" >' +
  '      </div>'+
 '   </div>'+ 
'</div>'+
'<div class="modal-footer">'+
    '<button type="button" class="btn btn-default" data-dismiss="modal">'+
    'Cancel'+
    '</button>'+
   ' <button type="button" onclick="funSubmit();" class="btn btn-primary">' +
  '  Submit'+
 '   </button>'+
'</div>'+
'</div>'+      
'</div>'+  
'</div>').appendTo(document.body);
   
    $(".MathJaxPluginDiv").load("MathJaxPlugin.html", function (response, status, xhr) {
        if (status == "success") {
           
            $("#txtArea").val($(".activeMathJaxPlugin").val());
            $("#divPopup").draggable();
           // $(".latex-editor").draggable();
        }
    });
    return false;
}
