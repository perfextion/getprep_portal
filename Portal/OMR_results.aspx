﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" Trace="true" CodeFile="OMR_results.aspx.cs" Inherits="OMR_results" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .hide {
            display: none;
        }

        .correct_ans {
            text-align: center;
        }
    </style>
    <div class="col-sm-12">
        <div class="course-syllabus-title underline">
            OMT Result
        </div>

        <b>
            <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b><br />

        <input id="btn_go" type="button" value="Show Answers" style="display: none;" onclick="ShowAnswers();" class="btn btn-primary pull-right" />
        <input id="btnShowTime" type="button" value="Show Time" style="display: none;" onclick="ShowTime();" class="btn btn-primary pull-right" />
    </div>

    <div class="col-sm-12">
        <fieldset>
            <div class="form-group">
                <div class="col-md-12">
                    <asp:Label ID="lblsummery" Style="float: right;"
                        runat="server"></asp:Label>
                    <ul class="list-unstyled ">
                        <li>
                            <p class="text-muted">
                                Total Questions:
                                                        <asp:Label ForeColor="Green" ID="lbltotalQuestion" runat="server"></asp:Label>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                Total Answered:&nbsp; 
                                                        <asp:Label ForeColor="Green" ID="lblanswered" runat="server"></asp:Label>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                Total Correct:&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" ID="lblCorrect" runat="server"></asp:Label>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                Total InCorrect:
                                                        <asp:Label ForeColor="Red" ID="lblIncorrect" runat="server"></asp:Label>
                            </p>
                        </li>
                        <li>
                            <p class="text-muted">
                                Total Time:&nbsp;&nbsp; 
                                                        <asp:Label ID="lblTotalTime" runat="server"></asp:Label>
                                <a href="#" id="showanswers" style="float: right;">Show All Answers</a>
                            </p>
                        </li>
                    </ul>

                </div>
                <div class="col-md-12">
                    <asp:Label ID="lbl_top" runat="server"></asp:Label>
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script>
     <%--      $(document).ready(function () {
            debugger
         var session = '<%=Session["OMRjson"] != null%>';
            if (session == "False") {
                ddlbind();
                return;
            }
            else {
                if (session == "True") {
                    var jsonobj = '<%=Session["OMRjson"]%>';
                    var NotTime = "";
                    $.each(jsonobj, function (idx, obj) {
                        NotAnswers = obj.NotAnswers;
                        NotTime = obj.NotTime;
                    });

                    if (NotTime == true) {
                        $(".time_taken").hide();
                        $("#btnShowTime").show();
                    }

                    if (NotAnswers == true) {
                        $(".correct_ans").hide();
                        $("#btn_go").show();
                    }
                    ddlbind();
                }
            }
            ddlbind();
        });
        function ddlbind() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "OMR_results.aspx/Get_Remarks",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    $.each(result, function (i, datas) {
                        $(".ddl").append("<option value=" + datas.cause_id + ">" + datas.error_cause_desc + "</option>")
                    });
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }--%>
        $(document).on("change", ".ddl", function () {
            debugger
            var remark = $(this).val();
            var id = $(this).closest('tr').attr("id");
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ remark: remark, id: id }),
                url: "OMR_results.aspx/Update_Remarks",
                success: function (data) {
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        });
        function ShowAnswers() {
            $(".correct_ans").show();
        }
        function ShowTime() {
            $(".time_taken").show();
        }
        $(function () {
            let number = 0;
            var obj = {
                Init: function () {
                    //$('.hideone').hide();
                    $("#showanswers").on('click', function () {
                        if (number == 1) {
                            number = 0
                            $('.hideone').css('display', 'none');
                            $("#showanswers").text("Show Answers")
                        }
                        else if (number == 0) {
                            number = 1
                            $('.hideone').css('display', '');
                            $("#showanswers").text("Hide Answers")
                        }
                    })
                },
            }
            obj.Init();
        });
        let number = 0;
        function ShowRecord(id) {
            debugger
            let qID = id;
            if (number == 1) {
                number = 0
                $('.hide' + qID + '').css('display', 'none');
                $('.hideshow' + qID + '').text("Show")
            }
            else if (number == 0) {
                number = 1
                $('.hide' + qID + '').css('display', '');
                $('.hideshow' + qID + '').text("Hide")
            }
        }
    </script>
</asp:Content>

