﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="OMR_Summary.aspx.cs" Inherits="OMR_Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
    </style>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0">
                <div class="course-syllabus-title underline" id="abc" tabindex="1">
                    Summary:
                </div>
                <b>
                    <asp:Label ID="lblErrormsg" runat="server"></asp:Label>

                </b>
                <asp:Label ID="lblsummery" Style="float: right;"
                    runat="server"></asp:Label>
                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div id="movieForm" class="form-horizontal">
                            <fieldset>
                                <div class="row">

                                    <div class="col-md-6">
                                        <h3>Test Details</h3>
                                        <ul class="list-unstyled" style="padding-left: 50px!important">
                                            <li>
                                                <p class="text-muted">
                                                    Test Date:
                                                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Questions:
                                                        <asp:Label ForeColor="Green" ID="lbltotalQuestion" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Answered:&nbsp; 
                                                        <asp:Label ForeColor="Green" ID="lblanswered" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Not Answered:&nbsp; 
                                                        <asp:Label ForeColor="#FDB45C" ID="lblnotanswered" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Correct:&nbsp;&nbsp; &nbsp;<asp:Label ForeColor="#46BFBD" ID="lblCorrect" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    InCorrect:
                                                        <asp:Label ForeColor="Red" ID="lblIncorrect" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Time(Seconds):&nbsp;&nbsp; &nbsp;  
                                                        <asp:Label ID="lblTotalTime" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    Average Time Taken (Seconds):&nbsp;&nbsp;&nbsp; 
                                                        <asp:Label ID="lblavgTime" runat="server"></asp:Label>
                                                    <asp:Label ID="lblTestaverage" CssClass="hidden" runat="server"></asp:Label>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <h3>Stats</h3>
                                        <div>
                                            <div id="pieChart"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="barChart"></div>
                                </div>

                            </fieldset>
                            <table id="tbldetails" class='table'>
                                <thead>
                                    <tr>
                                        <td>Question</td>
                                        <td>User Answer</td>
                                        <td>Correct Answer</td>
                                        <td>Time Taken</td>
                                    </tr>
                                </thead>
                            </table>
                            <div class="row">
                                <div id="container"></div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- row -->



    <!-- end row -->





    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>

        $(document).ready(function () {
            PieChart();
            BarChart();
            lineChart();
        });

        function PieChart() {
            $('#pieChart').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 300,
                    height: 300
                },
                title: {
                    text: 'Pie View',
                    floating: true,
                    align: 'right',
                    x: -30,
                    y: -30
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.point.name + '<b>:' + this.point.y;
                    }
                    // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Questions',
                    colorByPoint: true,
                    data: [{
                        name: 'InCorrect',
                        y: parseInt($("#ContentPlaceHolder1_lblIncorrect").text()),
                        color: '#F1948A',
                    }, {
                        name: 'Correct',
                        y: parseInt($("#ContentPlaceHolder1_lblCorrect").text()),
                        color: '#ABEBC6',
                    }, {
                        name: 'NotAnswered',
                        y: parseInt($("#ContentPlaceHolder1_lblnotanswered").text()),
                        color: '#F9E79F',
                    },]
                }]
            });
        }
        function secondsToHms(d) {
            d = Number(d);
            //var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);

            //var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? "" : "") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
            if (s <= 9) {
                s = '0' + s;
            }
            return m + '.' + s;
        }
        function BarChart() {
            var question = [];
            var timetaken = [];
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "OMR_Summary.aspx/GetBarChartData",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    var lblTestaverage = parseInt($("#ContentPlaceHolder1_lblTestaverage").text());
                    $.each(result, function (i, datas) {
                        question.push(datas.item_no);
                        //var avg = parseInt(datas.duration) / 60;
                        var avg = parseFloat(secondsToHms(datas.duration));
                        if (datas.correct == 1) {
                            timetaken.push({ y: avg, color: "#ABEBC6" });
                        }
                        else {
                            timetaken.push({ y: avg, color: "#F1948A" });
                        }
                    });
                    $('#barChart').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Average Time as per test duration'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: question,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Average(Minutes)'
                            },
                            tickInterval: 1,
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            //    '<td style="padding:0"><b>{point.y} Minutes</b></td></tr>',
                            formatter: function () {
                                debugger
                                if (this.y > 1) {
                                    return '<span style="font-size:10px">' + this.x + '-</span> <tr><td style="color:' + this.points["0"].series.color + ';padding:0">' + this.points["0"].series.name + ': </td>' +
                                        '<td style="padding:0"><b>' + this.y + ' Minutes</b></td></tr>';
                                } else {
                                    return '<span style="font-size:10px">' + this.x + '-</span><tr><td style="color:' + this.points["0"].series.color + ';padding:0">' + this.points["0"].series.name + ': </td>' +
                                        '<td style="padding:0"><b>' + this.y + ' Seconds</b></td></tr>';
                                }
                            },
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Questions',
                            data: timetaken,
                            point: { events: { click: function (ev) { GetQuestionDetails(ev.point.category) } } }
                        }
                        ]
                    });
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
        function lineChart() {
            var question = [];
            var timetaken = [];
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: "OMR_Summary.aspx/GetlineChartData",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    var lblTestaverage = parseInt($("#ContentPlaceHolder1_lblTestaverage").text());
                    $.each(result, function (i, datas) {
                        //var avg = parseFloat(secondsToHms(datas.duration));
                        timetaken = [];
                        timetaken.push(datas.item_no);
                        timetaken.push(parseFloat(secondsToHms(datas.duration)));
                        question.push(timetaken)
                    });
                   
                    //Highcharts.chart('container', {
                    //    series: [{
                    //        data: question
                    //    },],
                    //});
                    $('#container').highcharts({
                        title: {
                            text: 'Average Time as per test duration'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Average(Minutes)'
                            },
                            tickInterval: 1,
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            //    '<td style="padding:0"><b>{point.y} Minutes</b></td></tr>',
                            formatter: function () {
                                debugger
                                if (this.y > 1) {
                                    return '<span style="font-size:10px">' + this.x + '-</span> <tr><td style="color:' + this.points["0"].series.color + ';padding:0">' + this.points["0"].series.name + ': </td>' +
                                        '<td style="padding:0"><b>' + this.y + ' Minutes</b></td></tr>';
                                } else {
                                    return '<span style="font-size:10px">' + this.x + '-</span><tr><td style="color:' + this.points["0"].series.color + ';padding:0">' + this.points["0"].series.name + ': </td>' +
                                        '<td style="padding:0"><b>' + this.y + ' Seconds</b></td></tr>';
                                }
                            },
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Questions',
                            data: question,
                            point: { events: { click: function (ev) { GetQuestionDetails(ev.point.category) } } }
                        }
                        ]
                    });
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }

        function GetQuestionDetails(questionNumber) {
            debugger
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ questionNumber: questionNumber }),
                url: "OMR_Summary.aspx/GetQuestionDetails",
                success: function (data) {
                    var result = JSON.parse(data.d);
                    $('#tbldetails tbody > tr').remove();
                    $.each(result, function (i, datas) {
                        $("#tbldetails").append("<tbody><tr><td>" + datas.item_no + "</td><td>" + datas.answer + "</td><td>" + datas.correct_ans + "</td><td>" + datas.duration + "</td></tr><tbody>");
                    });
                }, error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    </script>
</asp:Content>

