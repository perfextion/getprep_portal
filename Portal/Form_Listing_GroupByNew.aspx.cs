﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Portal_Form_Listing_GroupByNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Forms";
        if (!IsPostBack)
        {
            getFormTypes();
            if (Session["ddltype"] != null)
            {
                ddltype.SelectedValue = Session["ddltype"].ToString();
                get_Forms(Session["ddltype"].ToString());
            }
        }
    }

    public void getFormTypes()
    {
        string strForms = " select distinct test_type from omr_form ";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            ddltype.DataSource = dt;
            ddltype.DataTextField = "test_type";
            ddltype.DataBind();
            ddltype.Items.Insert(0, new ListItem("-Select-", ""));
        }
        catch (Exception Ex)
        {
            LogError.Log("Form_Listing_GroupByNew.aspx/getFormTypes", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["ddltype"] = ddltype.SelectedItem.Text;
        get_Forms(ddltype.SelectedItem.Text);

    }

    public void get_Forms(string test_type)
    {
        string strForms = string.Empty;
        lbl_top.Text = string.Empty;
        // strForms = "select *, '' as answeredat from omr_form where  test_type='" + test_type + "' and  form_id not in (select distinct form_id  from omr_answers where st_id=" + HttpContext.Current.Session["UserID"] + ") order by [sort_order]";
        strForms = "select *, '' as answeredat from omr_form where  test_type='" + test_type + "'  order by [sort_order]";

        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            if (dt != null && dt.Rows.Count > 0)
            {
                var distinctValues = dt.AsEnumerable().Select(row => new { grp1 = row.Field<string>("grp1") }).Distinct();

                foreach (var subitems in distinctValues)
                {
                    if (!string.IsNullOrEmpty(subitems.grp1))
                    {
                        strForms = "select  f.form_id,f.form_no,a.test_key, min(answered_at) as answeredat, count(a.test_key) as total_qn_cnt, isnull(sum(a.correct), 0) as correct_qn_cnt  " +
                            "from omr_form as f " +
                            "left join omr_answers as a on  a.form_id = f.form_id and a.st_id = " + Session["UserID"] +
                            " where  f.test_type='" + test_type + "' and f.grp1='" + subitems.grp1 + "' " +
                            "group by f.form_id, f.form_no, a.test_key " +
                            "order by max(a.answered_at) desc";

                        var dt3 = gen_db_utils.gp_sql_get_datatable(strForms);
                        lbl_top.Text += generate_Grid(subitems.grp1.ToString(), dt3);
                    }
                }

                strForms = "select  f.form_id,f.form_no,a.test_key, min(answered_at) as answeredat, count(a.test_key) as total_qn_cnt, isnull(sum(a.correct), 0) as correct_qn_cnt  " +
                            "from omr_form as f " +
                            "left join omr_answers as a on  a.form_id = f.form_id and a.st_id = " + Session["UserID"] +
                            " where  f.test_type='" + test_type + "' and f.grp1 is null " +
                            "group by f.form_id, f.form_no, a.test_key " +
                            "order by max(a.answered_at) desc";

                DataTable dt2 = gen_db_utils.gp_sql_get_datatable(strForms);
                lbl_top.Text += generate_Grid("Miscellaneous", dt2);
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Form_Listing_GroupByNew.aspx/get_Forms", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    public string generate_Grid(string name, DataTable dt)
    {
        bool isCourseIncomplete = dt.AsEnumerable().Any(a => string.IsNullOrEmpty(a["test_key"].ToString()));
        bool isCourseNotStarted = dt.AsEnumerable().All(a => string.IsNullOrEmpty(a["test_key"].ToString()));

        string panel_class = string.Empty;

        if (isCourseNotStarted)
        {
            panel_class = "not-started";
        }
        else
        {
            if (isCourseIncomplete)
            {
                panel_class = "in-progress";
            }
            else
            {
                panel_class = "completed";
            }
        }

        string str_return = " <div class='panel panel-default'><div class='panel-heading accordion-toggle collapsed " + panel_class + "' data-toggle='collapse'" +
         " data-parent='#accordion' data-target='#" + name.Replace(' ', '_') + "'><i class='more-less glyphicon glyphicon-plus'></i>  " + name + "</div> ";
        str_return += " <div id='" + name.Replace(' ', '_') + "' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table-hover table table-striped table-bordered'> "
                                 + "<thead>"
                                    + "  <tr> "
                                         + " <th >Form</th> "
                                         + " <th >Test Taken at</th> "
                                         + " <th >Score</th> "
                                         + " <th >Action</th>"
                                         + " <th >Action</th>"
                                       + "</tr>"
                                + "  </thead> "
                                  + " <tbody>";
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow r in dt.Rows)
            {
                str_return += "<tr>" +
                                "<td>" + r["form_no"] + "</td>" +
                                "<td>" + r["answeredat"] + "</td>" +
                                "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? (r["correct_qn_cnt"] + "/" + r["total_qn_cnt"]) : "-") + "</td> " +
                                "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? "<a href='/Portal/OMR_Summary.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>Summary</a>" : "-") + " </td>" +
                                "<td>" + (!string.IsNullOrEmpty(r["test_key"].ToString()) ? "<a href='/Portal/OMR_Results.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>OMR Result</a>" : "<a href='form_confirmation.aspx?form_no=" + r["form_id"] + "'>Select</a>") + " </td>" +
                             "</tr>";
            }
            str_return += " </tbody> "
         + " </table> "
         + " </div></div></div > ";

        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }
    public string generate_completedGrid(string name, DataTable dt)
    {
        string str_return = "";
        str_return += " <div class='panel-success panel '><div class='panel-heading accordion-toggle collapsed' data-toggle='collapse'" +
         " data-parent='#accordion' data-target='#" + name.Replace(' ', '_') + "'><i class='more-less glyphicon glyphicon-plus'></i>  " + name + "</div> ";
        str_return += " <div id='" + name.Replace(' ', '_') + "' class='panel-collapse collapse'><div class='panel-body' style='overflow-y:auto'>  <table id='datatable_fixed_column' class='table-hover table table-striped table-bordered'> "
                                    + "<thead>"
                                      + "  <tr> "
                                         + " <th >Form</th> "
                                         + " <th >Last Answered at</th> "
                                         + " <th >Score</th> "
                                         + "<th >Action</th>"
                                         + "<th >Action</th>"
                                          + " </tr>"
                                        + "  </thead> "
                                         + " <tbody>";
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow r in dt.Rows)
            {
                str_return += "<tr>" +
                    "<td>" + r["form_no"] + "</td> " +
                    "<td>" + r["answeredat"] + "</td> " +
                    "<td>" + r["correct_qn_cnt"] + "/" + r["total_qn_cnt"] + "</td> " +
                    "<td><a href='/Portal/OMR_Summary.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>Summary</a></td>" +
                    "<td><a href='/Portal/OMR_Results.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>OMR Result</a></td>" +
                    "</tr>";
            }
            str_return += " </tbody> "
         + " </table> "
         + " </div></div></div > ";
        }
        else
        {
            str_return += " <table id='datatable_fixed_column' class='table table-striped table-bordered'>";
            str_return += " </tbody> "
                                   + " </table> "
                                     + "</div></div></div >";
        }
        return str_return;
    }

    public void get_CompletedForms(string test_type)
    {
        string strForms = "";
        lbl_table.Text = "";
        strForms = "select  f.form_id,f.form_no,a.test_key, min(answered_at) as answeredat, count(a.test_key) as total_qn_cnt, isnull(sum(a.correct), 0) as correct_qn_cnt  from omr_form as f inner join omr_answers as a " +
             "on  a.form_id=f.form_id where  f.test_type='" + test_type + "' and   " +
             "st_id=" + Session["UserID"] + "group by f.form_id,f.form_no,a.test_key order by max(a.answered_at) desc";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            if (dt != null && dt.Rows.Count > 0)
            {
                lbl_table.Text = generate_completedGrid("Completed", dt);
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Form_Listing_GroupByNew.aspx/get_CompletedForms", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
}