﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="OMR_History.aspx.cs" Inherits="Portal_OMR_History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .table-hover > tbody > tr:hover {
            background-color: #bbf3f0;
            cursor: pointer
        }

        .panel1 {
            margin-top: 5rem;
            cursor: pointer;
        }

        .table {
            margin-left: 2rem !important;
            width: 98% !important;
        }

        .panel-heading.completed {
            background-color: #31b231 !important;
            color: #fff !important;
        }

        .panel-heading.in-progress {
            background-color: #eb7e01 !important;
            color: #fff !important;
        }

        .panel-heading.not-started {
            background-color: #ddd !important;
        }

        .btncolor {
            background-color: #bbf3f0;
        }

        .m-1 {
            margin: 1rem;
        }
    </style>
    <div class="course-syllabus-title underline" id="abc" tabindex="1">
        Select Forms:
    </div>
    <b>
        <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
    <div class="row" style="display: none;">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-1 control-label">Type</label>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddltype" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="panel1">
        <asp:Label ID="lbl_top" runat="server"></asp:Label>
        <asp:Label ID="lbl_table" runat="server"></asp:Label>
    </div>

    <script>
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });
        });
        function toggleIcon(e) {
            $(this).find(".more-less").toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-heading').on('click', toggleIcon);
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
</asp:Content>

