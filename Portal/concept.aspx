﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="concept.aspx.cs" Inherits="Portal_concept" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script>
        function RefreshMathJax() {
           
           
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblConcept']);
           
            $('body,html').animate({
                scrollTop: 0                     
            }, 100);
            $(".course-syllabus-title").focus();
          
        }
       
       
    </script>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="portalassets/plugin/Timer/timer.js"></script>
    <div class="course-syllabus-title underline" id="abc" tabindex="1">
        Concept: 
        <asp:Label ID="lblConceptTitle" runat="server" Text=""></asp:Label>

    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="true">
        <ContentTemplate>
            <div class="row" id="div">
                <div class="col-sm-9">
                    <div class="panel panel-default" style="background-color: white">
                        <div class="panel-body">
                            <asp:Panel ID="Panel1" runat="server" Visible="true">
                                <div class="col-lg-12" style="min-height: 500px">
                                    <asp:Label ID="lblConceptId" runat="server" Visible="false" Text=""></asp:Label>
                                    <asp:Label ID="lblConcept" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>
                        </div>

                    </div>
                    <div class="col-lg-12" id="div1" runat="server" >
                        <asp:Button ID="btnPrevious" style="display:none" OnClick="btnPrevious_Click" runat="server" />
                          <asp:Button ID="btnNext" style="display:none" OnClick="btnNext_Click" runat="server" />
                        <span class="glyphicon glyphicon-chevron-left  fa-2x disabled" onclick="$('#ContentPlaceHolder1_btnPrevious').click()" style="cursor: pointer; color: darkturquoise" id="btnPrev" runat="server"></span>
                        <asp:Button CssClass="btn btn-grey" Style="width: 45%; height: 50px; font-size: large; color: darkturquoise" runat="server" ID="btnGotit" Text="GOT IT"></asp:Button>
                        <button class="btn btn-grey" style="width: 45%; height: 50px; font-size: large; color: darkturquoise;" id="btnDidntGet">MISSED IT</button>
           <span class="glyphicon glyphicon-chevron-right  fa-2x " id="scroll"  onclick="$('#ContentPlaceHolder1_btnNext').click();" style="cursor: pointer; color:darkturquoise;">
                          
                        </span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel panel-info" style="background-color: white">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md -3">
                                    <label id="Label5" class="lblTotalTime" style="color: green">Total Time :</label>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40"
                                            aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            40% Complete (success)
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel panel-default" style="background-color: white">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md -3">
                                    <span>Next Concepts</span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="margin: 5px;">
                            <div class="col-sm-12">
                                <asp:ListView ID="lstConcepts" runat="server">
                                    <ItemTemplate>
                                        <fieldset>
                                            <legend>
                                                <div class="row">
                                                    <a href="concept.aspx?conceptId=<%# Eval("concept_id") %>">
                                                        <span style="font-size: large"><%# Eval("title") %></span></a>
                                                </div>
                                            </legend>
                                        </fieldset>
                                    </ItemTemplate>

                                </asp:ListView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <%-- <Triggers>
            <asp:AsyncPostBackTrigger ControlID="frmConcepts" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>


