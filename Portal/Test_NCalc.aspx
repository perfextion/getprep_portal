﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Test_NCalc.aspx.cs" Inherits="Portal_Test_NCalc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:TextBox runat="server" ID="expressionTxt"></asp:TextBox>
    <asp:Button runat="server" ID="calcBtn" Text="EvaluateExpression" OnClick="calcBtn_Click" />

    <asp:Label runat="server" ID="resultLabel" ForeColor="Black"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" Runat="Server">
</asp:Content>

