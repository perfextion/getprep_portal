﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="type2_summary.aspx.cs" Inherits="Portal_type2_summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="course-syllabus-title underline">
        Daily Quiz Summary
    </div>
    <b>
        <asp:Label ID="lblErrorMsg" runat="server" Style="color: red"></asp:Label></b>
    <br />
    <div>
        <div>
            <span>
                <div>
                    <table>
                        <tbody>
                            <tr>
                                <td><b>Attempts</b></td>
                                <td><b>: </b></td>
                                <td><span id="tdattempts" runat="server"></span></td>
                            </tr>
                            <tr>
                                <td><b>Correct</b></td>
                                <td><b>: </b></td>
                                <td><span id="tdcorrect" runat="server"></span></td>
                            </tr>
                            <tr>
                                <td><b>Wrong</b></td>
                                <td><b>: </b></td>
                                <td><span id="tdwrong" runat="server"></span></td>
                            </tr>
                            <tr>
                                <td><b>Time Spent</b></td>
                                <td><b>: </b></td>
                                <td><span id="tdtimespent" runat="server"></span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </span>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" Visible="true">
        <ContentTemplate>
            <div>
                <asp:Button ID="btncorrectans" runat="server" class="btn" Text="Correct Answer" OnClick="btncorrectans_Click" />
                <asp:Button ID="btnwrongans" runat="server" class="btn" Text="Wrong Answer" OnClick="btnwrongans_Click" />

            </div>
            <asp:Label ID="lblMainQuestion" runat="server">
            </asp:Label>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">

    <script>
        function funrefreshmathqst() {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblMainQuestion']);
        }
        function ReviewAnswer(id) {
            $.ajax({
                type: "POST",
                url: "type2_summary.aspx/updateanswer",
                data: '{id: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function RemoveAnswer(id) {
            if (confirm("Are sure ready to delete?")) {
                $.ajax({
                    type: "POST",
                    url: "type2_summary.aspx/RemoveAnswer",
                    data: '{id: "' + id + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        alert("Question removed successful");
                        window.location.reload();
                    },
                    failure: function (response) {
                        alert("Question removed Failse");
                    }
                });
            }
        }
        function OnSuccess(response) {
            $("#" + response.d.Data.id).css("display", "none");
            $("#s_" + response.d.Data.id).css("display", "block");

            var s = getdateforamte(response.d.Data.reviewed_on)
            $("#s_" + response.d.Data.id).text(s);

        }
        function showhidesolution(id, data) {
            debugger;
            if ($(data).text().trim() == "Show Solution") {
                $(data).text("Hide Solution");
                $("#div_" + id).css("display", "block");
            } else {
                $(data).text("Show Solution");
                $("#div_" + id).css("display", "none");

            }
        }
    </script>

</asp:Content>

