﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="debug.aspx.cs" Inherits="Portal_debug" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid" style="margin-top: 3rem">
        <div class="col-md-12">
            <div class="form-inline">
                <div class="form-goup">
                    <%--<asp:CheckBox ID="checkbox" runat="server" CssClass="form-control" />--%>
                    <asp:Label ID="Label1" runat="server" Text="Password" />
                    <asp:TextBox runat="server" ID="txtpassword" CssClass="form-control" ToolTip="Password" TextMode="Password" AutoCompleteType="Disabled" />
                    <asp:Button Text="Submit" ID="btndebug" runat="server" OnClick="btn_click" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-inline">
                <div class="form-goup" style="margin: 3rem;">
                    <asp:Label ID="lblmessage" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
</asp:Content>

