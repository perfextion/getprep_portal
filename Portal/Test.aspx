﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Portal_Test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="bootstrap-wizard-1" class="col-sm-12">
        <div class="form-bootstrapWizard">
            <ul class="bootstrapWizard form-wizard">
                <li class="active" data-target="#step1">
                    <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Membership Plan</span> </a>
                </li>
                <li data-target="#step2">
                    <a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Choose Subject </span></a>
                </li>
                <li data-target="#step3">
                    <a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Availability </span></a>
                </li>
                <li data-target="#step4">
                    <a href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">Billing Information</span> </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">

                <div class="row">

                     
                </div>

            </div>
            <div class="tab-pane" id="tab2">
                <div class="row" style="margin-top: 20px">

                    <div class="col-sm-4">
                        <hr>
                        <ul class="reset" id="ulSelectedSubject">
                        </ul>
                        <hr>
                    </div>
                    <div class="col-sm-8" style="margin-top: 20px">
                        <asp:Label ID="lblSubject" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab3">
                <br>
                <h3><strong>Step 3</strong> - Domain Setup</h3>

            </div>
            <div class="tab-pane" id="tab4">
                <br />
                <h3><strong>Step 4</strong></h3>
                <div class="row">
                    <div class="course-syllabus-title  act active" onclick="SelectDivcard(this);" style="width: 48%; float: left; border: 1px solid grey">Pay by card <i class="fa fa-credit-card"></i></div>
                    <div class="course-syllabus-title  act" onclick="SelectDivCheque(this);" style="width: 48%; float: left; margin-left: 5px; border: 1px solid grey">Pay by cheque <i class="fa fa-book"></i></div>
                </div>
                <div id="divCard">




                    <div class="row">

                        <div class="col-sm-5">
                            <label for="FirstName" style="color: black"><b>FirstName</b></label>
                            <asp:TextBox ID="txtFirstName" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                        </div>


                        <div class="col-sm-5">
                            <label for="LastName" style="color: black"><b>LastName</b></label>
                            <asp:TextBox ID="txtLastName" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                            <div class="help-block"></div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-12">
                            <label for="Cardnumber" style="color: black"><b>Cardnumber</b></label>
                            <asp:TextBox ID="txtCardnumber" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtZipcode" ErrorMessage=" Zipcode is a required field." ForeColor="Red"></asp:RequiredFieldValidator>


                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-3">
                            <label for="ExpiryDate" style="color: black"><b>ExpiryDate</b></label>
                            <asp:TextBox ID="txtExpiryDate" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                        </div>


                        <div class="col-sm-5">
                            <label for="Securitycode" style="color: black"><b>Securitycode</b></label>
                            <asp:TextBox ID="txtsecuritycode" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                            <div class="help-block"></div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Billing Address</label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12">
                            <label for="Address" style="color: black"><b>Address</b></label>
                            <asp:TextBox ID="txtaddressStreet" placeholder="StreetNumber" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                            <asp:TextBox ID="txtAddressAppartment" placeholder="Appartment or suite" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                            <div class="help-block"></div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-5">
                            <label for="City" style="color: black"><b>City</b></label>
                            <asp:TextBox ID="txtCity" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                        </div>


                        <div class="col-sm-3">
                            <label for="State" style="color: black"><b>State</b></label>
                            <%--<asp:DropDownList runat="server" CssClass="form-control"></asp:DropDownList>--%>
                            <asp:TextBox ID="txtState" CssClass="form-control" runat="server"></asp:TextBox>
                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-sm-4">
                            <label for="Zipcode" style="color: black"><b>Zipcode</b></label>
                            <asp:TextBox ID="txtZipcode" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                            <div class="help-block"></div>
                        </div>

                    </div>

                </div>
                <div id="divPaybyCheque" style="display: none">
                    pay by check
                </div>
            </div>
            <div class="form-actions">
															<div class="row">
																<div class="col-sm-12">
																	<ul class="pager wizard no-margin">
																		<!--<li class="previous first disabled">
																		<a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
																		</li>-->
																		<li class="previous disabled">
																			<a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
																		</li>
																		<!--<li class="next last">
																		<a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
																		</li>-->
																		<li class="next">
																			<a href="javascript:void(0);" class="btn btn-lg txt-color-darken"> Next </a>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
        </div>
    </div>
     

    <script src="portalassets/js/jquery.bootstrap.wizard.min.js"></script>


    <link href="portalassets/css/Wizard.css" rel="stylesheet" />
    <script>
        $('#bootstrap-wizard-1').bootstrapWizard({
            'tabClass': 'form-wizard',
            'onNext': function (tab, navigation, index) {

                $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                  'complete');
                $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                .html('<i class="fa fa-check"></i>');

            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
</asp:Content>

