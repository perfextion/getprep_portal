﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="Portal_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="course-syllabus-title underline">Welcome</div>
   <b> <asp:Label ID="lblErrorMsg" runat="server"></asp:Label></b>
    <div class="row">
        <div class="col-sm-9">
            <div class="row">

                <div id="wizard-1">
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        <div class="form-bootstrapWizard">
                            <ul class="bootstrapWizard form-wizard">
                                <li class="active" data-target="#step1">
                                    <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Membership Plan</span> </a>
                                </li>
                                <li data-target="#step2">
                                    <a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Choose Subject </span></a>
                                </li>
                                <li data-target="#step3">
                                    <a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title">Availability </span></a>
                                </li>
                                <li data-target="#step4">
                                    <a href="#tab4" data-toggle="tab"><span class="step">4</span> <span class="title">Billing Information</span> </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">

                                <div class="row">

                                    <div class="section section-padding pricing" style="margin-top: 50px">

                                        <div class="group-title-index">
                                            <h4 class="top-title"></h4>

                                            <h2 class="center-title"></h2>

                                            <div class="bottom-title"><i class="bottom-icon icon-a-1-01-01"></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="pricing-wrapper">
                                                <!------------>
                                                <div class="col-md-12">
                                                    <div class="carousel slide" id="myCarousel">
                                                        <div class="carousel-inner">
                                                            <div class="item active" >
                                                                <div class="col-sm-4">
                                                                    <div class="pricing-widget" onclick="SelectMembershipPlan(this);">
                                                                        <div class="pricing-header">
                                                                            <div class="price-cost">
                                                                                <div class="inner">
                                                                                    <p data-from="0" data-to="0" data-speed="1000" class="inner-number">0</p>
                                                                                </div>
                                                                                <br>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pricing-content">
                                                                            <h3 class="pricing-title">trial</h3>

                                                                            <p class="pricing-subtitle">30 days free trial</p>
                                                                            <ul class="pricing-list">
                                                                                <li>
                                                                                    <p><strong>One day</strong> trial</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Limited</strong> courses</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Free</strong> 3 lessons</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>No</strong> supporter</p>
                                                                                </li>

                                                                            </ul>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="col-sm-4">
                                                                    <div class="pricing-widget" onclick="SelectMembershipPlan(this);">
                                                                        <div class="pricing-header">
                                                                            <div class="price-cost">
                                                                                <div class="inner">
                                                                                    <p data-from="0" data-to="35" data-speed="1000" class="inner-number">35</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pricing-content">
                                                                            <h3 class="pricing-title">Standard</h3>

                                                                            <p class="pricing-subtitle">per month when paid annually</p>
                                                                            <ul class="pricing-list">
                                                                                <li>
                                                                                    <p><strong>One year</strong> standard access</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Limited</strong> courses</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>300+</strong> lessons</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Random</strong> supporter</p>
                                                                                </li>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="item">
                                                                <div class="col-sm-4">
                                                                    <div class="pricing-widget active" onclick="SelectMembershipPlan(this);">
                                                                        <div class="pricing-header">
                                                                            <div class="price-cost">
                                                                                <div class="inner">
                                                                                    <p data-from="0" data-to="89" data-speed="1000" class="inner-number">89</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pricing-content">
                                                                            <h3 class="pricing-title">Premiere</h3>

                                                                            <p class="pricing-subtitle">per month when paid annually</p>
                                                                            <ul class="pricing-list">
                                                                                <li>
                                                                                    <p><strong>Life time</strong> access</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Unlimited</strong> All courses</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>3000+</strong> lessons &amp; growing</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Free</strong> supporter</p>
                                                                                </li>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="col-sm-4">
                                                                    <div class="pricing-widget active" onclick="SelectMembershipPlan(this);">
                                                                        <div class="pricing-header">
                                                                            <div class="price-cost">
                                                                                <div class="inner">
                                                                                    <p data-from="0" data-to="89" data-speed="1000" class="inner-number">89</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="pricing-content">
                                                                            <h3 class="pricing-title">Premiere</h3>

                                                                            <p class="pricing-subtitle">per month when paid annually</p>
                                                                            <ul class="pricing-list">
                                                                                <li>
                                                                                    <p><strong>Life time</strong> access</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Unlimited</strong> All courses</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>3000+</strong> lessons &amp; growing</p>
                                                                                </li>
                                                                                <li>
                                                                                    <p><strong>Free</strong> supporter</p>
                                                                                </li>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                                        <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                                    </div>
                                                </div>

                                                <!------------->

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="row" style="margin-top: 20px">

                                    <div class="col-sm-4">
                                        <hr>
                                        <ul class="reset" id="ulSelectedSubject">
                                        </ul>
                                        <hr>
                                    </div>
                                    <div class="col-sm-8" style="margin-top: 20px">
                                        <asp:Label ID="lblSubject" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <br/>
                                <h3><strong>Step 3</strong> - Domain Setup</h3>

                            </div>
                            <div class="tab-pane" id="tab4">
                                <br />
                                <h3><strong>Step 4</strong></h3>
                                <div class="row">
                                    <div class="course-syllabus-title  act active" onclick="SelectDivcard(this);" style="width: 48%; float: left; border: 1px solid grey">Pay by card <i class="fa fa-credit-card"></i></div>
                                    <div class="course-syllabus-title  act" onclick="SelectDivCheque(this);" style="width: 48%; float: left; margin-left: 5px; border: 1px solid grey">Pay by cheque <i class="fa fa-book"></i></div>
                                </div>
                                <div id="divCard">




                                    <div class="row">

                                        <div class="col-sm-5">
                                            <label for="FirstName" style="color: black"><b>FirstName</b></label>
                                            <asp:TextBox ID="txtFirstName" CssClass="form-control" runat="server"></asp:TextBox>
                                            
                                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                                        </div>


                                        <div class="col-sm-5">
                                            <label for="LastName" style="color: black"><b>LastName</b></label>
                                            <asp:TextBox ID="txtLastName" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <label for="Cardnumber" style="color: black"><b>Cardnumber</b></label>
                                            <asp:TextBox ID="txtCardnumber" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtZipcode" ErrorMessage=" Zipcode is a required field." ForeColor="Red"></asp:RequiredFieldValidator>


                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <label for="ExpiryDate" style="color: black"><b>ExpiryDate</b></label>
                                            <asp:TextBox ID="txtExpiryDate" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                                        </div>


                                        <div class="col-sm-5">
                                            <label for="Securitycode" style="color: black"><b>Securitycode</b></label>
                                            <asp:TextBox ID="txtsecuritycode" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Billing Address</label>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <label for="Address" style="color: black"><b>Address</b></label>
                                            <asp:TextBox ID="txtaddressStreet" placeholder="StreetNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">

                                            <asp:TextBox ID="txtAddressAppartment" placeholder="Appartment or suite" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-5">
                                            <label for="City" style="color: black"><b>City</b></label>
                                            <asp:TextBox ID="txtCity" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtFirstName" ErrorMessage="FirstName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                            <%--         
                                <input type="text" id="firstname" placeholder="First name" class="form-control" required="required" />--%>
                                        </div>


                                        <div class="col-sm-3">
                                            <label for="State" style="color: black"><b>State</b></label>
                                            <%--<asp:DropDownList runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                            <asp:TextBox ID="txtState" CssClass="form-control" runat="server"></asp:TextBox>
                                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="Zipcode" style="color: black"><b>Zipcode</b></label>
                                            <asp:TextBox ID="txtZipcode" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="testValidationgGroup" ControlToValidate="txtLastName" ErrorMessage=" LastName is a required field." ForeColor="Red"></asp:RequiredFieldValidator>

                                            <%--   <input type="text" id="LastName" placeholder="Last Name" class="form-control" />--%>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>

                                </div>
                                <div id="divPaybyCheque" style="display: none">
                                    pay by check
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="pager wizard no-margin">

                                            <li class="previous disabled">
                                                <a href="javascript:void(0);" class="btn btn-lg txt-color-darken next-step">Previous </a>
                                            </li>

                                            <li class="next">
                                                <a href="javascript:void(0);" class="btn btn-lg txt-color-darken prev-step">Next </a>

                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <script src="portalassets/js/jquery.bootstrap.wizard.min.js"></script>


    <link href="portalassets/css/Wizard.css" rel="stylesheet" />
    <style>
        .reset li {
            background: #50bcb6;
            list-style: outside none none;
            margin: 2px;
            padding: 5px;
            color: #fff;
            font-family: lato;
            font-weight: bold;
        }

        .act {
            font-size: 30px;
            font-weight: bold;
            margin-bottom: 50px;
            padding-left: 10px;
            color: #666;
        }
       

            .act.active {
                border: 2px solid #50bcb6 !important;
                color: #50bcb6;
            }
    </style>
    <script type="text/javascript">


        $(document).ready(function () {
            $('.act').click(function () {
                $('.act.active').removeClass('active');
                $(this).addClass('active');
            });
            $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                
                'onNext': function (tab, navigation, index) {
                    
                    var IsValid = true;
                    var ErrorMessage = "Please select membership plan.";
                    if (index == 1) {
                        IsValid = ValidateMemberShipTab();
                        ErrorMessage == "Please select membership plan.";
                    } else if (index == 4) {
                        SaveInformation();
                    }

                    if (!IsValid) {
                        alert(ErrorMessage)
                        return false;
                    } else {

                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                          'complete');
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                        .html('<i class="fa fa-check"></i>');
                    }
                }
            });

        })

        function SelectMembershipPlan(id) {

            $(".pricing-widget").removeClass("active");
            $(id).addClass("active");
        }



        function ValidateMemberShipTab() {

            var ActiveCounter = 0;
            $(".pricing-widget").each(function ValidateMemberShipTab() {

                if ($(this).hasClass("active")) {
                    ActiveCounter++;
                }

            })

            if (ActiveCounter == 0) {
                return false;
            }
            else {
                return true;
            }
        }



        function SelectDivcard() {
            $("#divCard").show();
            $("#divPaybyCheque").hide();
        }

        function SelectDivCheque() {
            $("#divPaybyCheque").show();
            $("#divCard").hide();
        }

        function SaveInformation() {

            $.ajax({
                type: "Post",
                url: '/Portal/WebServices/GetPrep.asmx/SaveStudentWelcomeInformation',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    FirstName: $("#ContentPlaceHolder1_txtFirstName").val(),
                    LastName: $("#ContentPlaceHolder1_txtLastName").val(),
                    CardNumber: $("#ContentPlaceHolder1_txtCardnumber").val(),
                    ExpiryDate: $("#ContentPlaceHolder1_txtExpiryDate").val(),
                    Securitycode: $("#ContentPlaceHolder1_txtsecuritycode").val(),
                    AddressStreet: $("#ContentPlaceHolder1_txtaddressStreet").val(),
                    AddressAppartment: $("#ContentPlaceHolder1_txtAddressAppartment").val(),
                    City: $("#ContentPlaceHolder1_txtCity").val(),
                    State: $("#ContentPlaceHolder1_txtState").val(),
                    ZipCode: $("#ContentPlaceHolder1_txtZipcode").val()
                })
            })

        };

    </script>
    <script>

        $('.chevron_toggleable').on('click', function () {
            $(this).toggleClass('glyphicon-chevron-right glyphicon-chevron-down');
        });
    </script>
    <script>
        function OnChangeSubject(checkbox) {
            var subjectId = $(checkbox).attr("value");
            var subjectName = $(checkbox).attr("name");

            if ($(checkbox).is(':checked')) {

                $("#ulSelectedSubject").append("<li id='" + subjectId + "'>" + subjectName + "<a href='#' onclick='$(# + subjectId).remove();' </li>");
            } else {
                $("#" + subjectId).remove();
            }
        }
        $('#myCarousel').carousel({
            interval: false
        })

        $('.carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo($(this));
            }
            else {
                $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
        });


    </script>


</asp:Content>
