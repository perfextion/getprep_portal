﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class Forms_Listing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getFormTypes();
        }
    }


    public void get_Forms(string test_type)
    {
        string strForms = "SELECT * from OMR_Form where   test_type='" + test_type + "' ";
        strForms = "select * from omr_form where  test_type='" + test_type + "' and  form_id not in (select distinct form_id  from omr_answers where st_id=" + HttpContext.Current.Session["UserID"] + ") order by [sort_order]";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            lbl_top.Text = "<table id='datatable_fixed_column' class='table-hover table table-striped table-bordered'>"
                                  + "<thead>"
                                    + "  <tr> "
                                       + " <th >Form</th> "
                                       + "<th >Action</th>"
                                        + " </tr>"
                                      + "  </thead> "
                                         + " <tbody>";
            foreach (DataRow r in dt.Rows)
            {
                lbl_top.Text += "<tr class='clickable-row' data-href='form_confirmation.aspx?form_no=" + r["form_id"] + "'><td>" + r["form_no"]
                    + "</td><td><a href='form_confirmation.aspx?form_no=" + r["form_id"] + "'>Select</a></td></tr>";
            }
            lbl_top.Text += "</tbody>";
            lbl_top.Text += "</table>";

        }

        catch (Exception Ex)
        {
            LogError.Log("Forms_Listing.aspx/get_Forms", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    public void get_CompletedForms(string test_type)
    {
        string strForms = "";
        strForms = "select  f.form_id,f.form_no,a.test_key,max(answered_at) as answeredat  from omr_form as f inner join omr_answers as a " +
            "on  a.form_id=f.form_id where  f.test_type='" + test_type + "' and   " +
            "st_id=" + Session["UserID"] + "group by f.form_id,f.form_no,a.test_key order by max(a.answered_at) desc";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            lbl_table.Text = "  <div class='panel panel-success'><div class='panel-heading'>Completed</div><div class='panel-body'><table id='tblTakenTest' class='table table-hover table-striped  table-bordered'style='width: 98%;margin-left: 1rem;' >"
                                      + "<thead>"
                                      + "  <tr> "
                                         + " <th >Form</th> "
                                         + "<th >Action</th>"
                                         + "<th >Action</th>"
                                          + " </tr>"
                                        + "  </thead> "
                                           + " <tbody>";
            foreach (DataRow r in dt.Rows)
            {
                lbl_table.Text += "<tr><td>" + r["form_no"] + "</td> " +
                    "<td><a href='/Portal/OMR_Summary.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>Summary</a></td>" +
                    "<td><a href='/Portal/OMR_Results.aspx?form_no=" + r["form_id"] + "&test_key=" + r["test_key"] + "'>OMR Result</a></td>" +
                    "</tr>";
            }
            lbl_table.Text += "</tbody>";
            lbl_table.Text += "</table> </div></div>";
        }
        catch (Exception Ex)
        {
            LogError.Log("Forms_Listing.aspx/get_CompletedForms", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    public void getFormTypes()
    {

        string strForms = " select distinct test_type from omr_form ";
        try
        {

            DataTable dt = gen_db_utils.gp_sql_get_datatable(strForms);
            ddltype.DataSource = dt;
            ddltype.DataTextField = "test_type";
            ddltype.DataTextField = "test_type";
            ddltype.DataBind();
            ddltype.Items.Insert(0, new ListItem("-Select-", ""));
        }
        catch (Exception Ex)
        {
            LogError.Log("Forms_Listing.aspx/getFormTypes", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strForms, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }


    protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
    {
        get_Forms(ddltype.SelectedItem.Text);
        get_CompletedForms(ddltype.SelectedItem.Text);
    }
}