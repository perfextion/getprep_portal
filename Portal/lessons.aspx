﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="lessons.aspx.cs" Inherits="Portal_lessons" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        tr.table-row td:first-child {
            width: 85%;
            padding-left: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="course-syllabus-title underline">
        <asp:Label ID="lblTitle" CssClass="lbltitle" runat="server"></asp:Label></div>
     <b><asp:Label ID="lblErrormsg" runat="server" style="color:red"></asp:Label></b>
     <div class="row">
           <asp:Label ID="lb1" runat="server"></asp:Label>
         </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <script>

        function Practice(id, level, title) {           
            var checkboxid = "chk_" + id;
            var needreview = false;            

            if ($('input[id*=' + checkboxid + ']').is(":checked")) {                 
                needreview = true;
            }
            
            $.ajax({
                type: "POST",
                url: "/portal/lessons.aspx/funPractice",
                data: '{id: "' + id + '" ,level : "' + level + '", title:"' + title + '", NeedReview: ' + needreview + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }

        function OnPracticeSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;

        }
    </script>
</asp:Content>

