﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;

public partial class OMR_results : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dynamic obj = "";
        if (!IsPostBack)
        {
            if (Session["OMRjson"] != null)
            {
                obj = JsonConvert.DeserializeObject(HttpContext.Current.Session["OMRjson"].ToString());
            }
            //if (Request.QueryString["aspxerrorpath"] != null)
            //{
            //    string testkey = 
            //    string form_no = ;
            //}

            if (string.IsNullOrEmpty(Request.QueryString["test_key"]) && string.IsNullOrEmpty(Request.QueryString["form_no"]))
            {
                foreach (var data in obj)
                {
                    view_summary(data.test_key.ToString(), data.form_no.ToString());
                    lblsummery.Text = "<a href='/Portal/OMR_Summary.aspx?form_no=" + data.form_no.ToString() + "&test_key=" + data.test_key.ToString() + "'>Summary</a>";
                }
            }
            else
            {
                view_summary(Request.QueryString["test_key"].ToString(), Request.QueryString["form_no"].ToString());
                lblsummery.Text = "<a href='/Portal/OMR_Summary.aspx?form_no=" + Request.QueryString["form_no"].ToString() + "&test_key=" + Request.QueryString["test_key"].ToString() + "'>Summary</a>";
            }
        }
    }
    [WebMethod]
    public static DataTable Get_Remarks()
    {
        DataTable dt = new DataTable();
        string strRemarks = "select cause_id,error_cause_desc,error_cause_code from error_causes order by error_cause_code";
        try
        {
            dt = gen_db_utils.gp_sql_get_datatable(strRemarks);
            //return JsonConvert.SerializeObject(dt);
            return dt;
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/Get_Remarks", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strRemarks, true);
            throw;
        }
    }
    [WebMethod]
    public static void Update_Remarks(string id, string remark)
    {
        string strUpdate = "Update OMR_answers set error_code='" + remark + "' where ans_id=" + id;
        try
        {
            gen_db_utils.gp_sql_execute(strUpdate);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/Update_Remarks", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strUpdate, true);
        }
    }
    private bool ValidateAnswer(string CorrectAnswer, string UserAnswer)
    {
        try
        {
            if (CorrectAnswer == UserAnswer)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/ValidateAnswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            return false;
        }
    }
    public void view_summary(string testtoken, string formno)
    {
        DataTable dataddls;

        string strGetTest = "select f.form_q_id,f.q_num,f.answer as correct_ans,ans.correct,ans.answer as user_ans,ans.duration,answered_at,ans.ans_id,ans.error_code from  "
                  + " OMR_FormQuestions as f ,omr_answers as ans where f.form_id="
                  + Convert.ToInt32(formno) + " and ans.test_key='" + testtoken
                  + "' and f.q_num=ans.item_no and ans.item_type='Q' order by q_num ";

        try
        {
            dataddls = Get_Remarks();

            string strReturn = "";
            string remarks = "";

            strReturn = "<table id='tblexample' class='table '>";
            strReturn += "<tr>"
                          + "<th>Question</i></th>"
                           + "<th style='display:none'>AnsId</th>"
                           + "<th style='text-align:center;'>Answer</th>"
                              + " <th class='correct_ans'>Correct Answer</th>"
                              + " <th>Action</th>"
                               + " <th>Result</th>"
                               + " <th class='time_taken'>Time Taken(In seconds)</th>"
                                  + " <th>Remarks</th>"
                         + "</tr>";
            Session["fdr"] = strGetTest;


            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strGetTest);
            string isAnsCorrect = "";
            lbltotalQuestion.Text = dt1.Rows.Count.ToString();
            int TotalCorrect = 0;
            int TotalInCorrect = 0;
            int TotalTime = 0;
            string ddlnames = "";
            string totalanswered = gen_db_utils.gp_sql_scalar("select count(*) from  "
                + " OMR_FormQuestions as f ,omr_answers as ans where f.form_id="
                + Convert.ToInt32(formno) + " and ans.test_key='" + testtoken
                + "' and f.q_num=ans.item_no and ans.item_type='Q' and ans.answer!='-'  ");
            lblanswered.Text = totalanswered;

            object maxDate = dt1.Compute("MAX(answered_at)", null);
            object minDate = dt1.Compute("MIN(answered_at)", null);

            DateTime startTime = Convert.ToDateTime(maxDate);
            DateTime endtime = Convert.ToDateTime(minDate);

            TimeSpan duration = startTime - endtime;

            foreach (DataRow dtr in dt1.Rows)
            {
                ddlnames = "";

                foreach (DataRow dtddlrow in dataddls.Rows)
                {
                    if (dtr["error_code"].ToString() == dtddlrow["cause_id"].ToString())
                    {
                        ddlnames += "<option value='" + dtddlrow["cause_id"].ToString() + "' selected>" + dtddlrow["error_cause_desc"].ToString() + "</option>";
                    }
                    else
                    {
                        ddlnames += "<option value='" + dtddlrow["cause_id"].ToString() + "'>" + dtddlrow["error_cause_desc"].ToString() + "</option>";
                    }
                }
                if (dtr["correct"].ToString() == "1")
                {
                    strReturn += "<tr id='" + dtr["ans_id"] + "' class='success'>";
                    isAnsCorrect = "<i class='glyphicon glyphicon-ok'></i>";
                    remarks = "";
                    TotalCorrect += 1;
                }
                else
                {
                    strReturn += "<tr id='" + dtr["ans_id"] + "' class='danger'>";
                    isAnsCorrect = "X";
                    remarks = "<select id='" + dtr["q_num"] + "' class='ddl form-control'/><option>Select</option>" + ddlnames + "</select>";
                    TotalInCorrect += 1;
                }
                strReturn += "<td>" + dtr["q_num"] + "</td>";
                //strReturn += "<td id='" + dtr["ans_id"] + "' style='display:none'>" + dtr["ans_id"] + "</td>";
                strReturn += "<td style=' text-align: center;'>" + dtr["user_ans"] + "</td>";
                strReturn += "<td class='correct_ans'><span class='hideone hide" + dtr["ans_id"] + "' style='display:none'> " + dtr["correct_ans"] + "</span></td>"
                                   + "<td><a class='hideshow" + dtr["ans_id"] + "' href ='javascript:ShowRecord(" + dtr["ans_id"] + ")' > Show</a></td>"
                                   + "<td>" + isAnsCorrect + "</td>"
                                   + "<td>" + dtr["duration"] + "</td>"
                                   + "<td>" + remarks + "<td>"
                                   + "</tr>";
                //TotalTime += Convert.ToInt32(dtr["duration"]);
            }
            strReturn += "</table>";
            lbl_top.Text = strReturn;
            lblCorrect.Text = TotalCorrect.ToString();
            lblIncorrect.Text = TotalInCorrect.ToString();

            //var timespan = TimeSpan.FromSeconds(TotalTime);
            lblTotalTime.Text = duration.ToString(@"mm\:ss");

            //lblTotalTime.Text = TotalTime.ToString();
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_result.aspx/view_summary", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strGetTest, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }

    }
}