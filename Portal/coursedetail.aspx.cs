﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.Script.Serialization;

public partial class Portal_coursedetail : System.Web.UI.Page
{
    int page_debug = 0;
    DataTable dt_full;
    Hashtable h_cc1 = new Hashtable();
    Hashtable h_cc2 = new Hashtable();
    Hashtable h_score = new Hashtable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep| Course detail";
        string parent_cat = Request.QueryString.GetValues("courseid") == null ? "1" : (Request.QueryString.GetValues("courseid")[0].ToString());
        //lb1.Text = "Subjects";
        debugOn();
        get_hashtable();
        get_datatable(parent_cat);
        get_Page_Headers(parent_cat);
    }
    public void get_datatable(string parentcat)
    {
        string str_sql_lower = "select * from core_cat where cc0 = " + parentcat;
        try
        {
            dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/Course detail", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_lower, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    public void get_hashtable()
    {
        string str_h_cc1 = "select cc1, count(*) from type2_qs where  status = -950 and cc1>0 group by cc1";
        try
        {
            h_cc1 = gen_db_utils.gp_sql_get_hashtable(str_h_cc1);
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/Course detail", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_h_cc1, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
        }
    }
    public void debugOn()
    {
        //string str_sql = "select * from core_cat where parent_cat = " + parent_cat + "  order by order_id";
        try
        {
            if (Session["debug_on"] != null)
            {
                if (Session["debug_on"].ToString() == "1") page_debug = 1;
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/Course detail", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "debugOn", true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;
        }
    }
    public void get_Page_Headers(string parent_cat)
    {
        string str_sql = "select * from core_cat where parent_cat = " + parent_cat + "  order by order_id";
        string str_h_cc2 = "select cc2, count(*) from type2_qs where  status = -950 and cc2>0 group by cc2";
        try
        {
            h_cc2 = gen_db_utils.gp_sql_get_hashtable(str_h_cc2);
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            string str_sql1 = " select item_id,max(score) from st_syllabus where st_id ='" + Session["userid"] + "'  group by item_id";
            h_score = gen_db_utils.gp_sql_get_hashtable(str_sql1);
            foreach (DataRow dtr1 in dt1.Rows)
            {
                //  string lower_text = get_lower_rows();
                int cc1_count = 0;
                string dbg_id = "";
                //string vocab_link = "<a href=\"" + "fc_sql_setup.aspx?parent_cat="
                //            + dtr1["id"].ToString() + "\">" + "Vocab Practice" + "</a>";
                cc1_count = Convert.ToInt32(fc_gen_utils.hash_lookup(h_cc1, dtr1["id"].ToString(), "0"));
                if (page_debug == 1)
                {
                    dbg_id = "ID: (" + dtr1["id"].ToString() + ")";
                }
                //string quiz_link = "<a href=\"" + "setup_type2_quizstart.aspx?quiz_type=adapt&core_level=level0&core_id="
                //               + dtr1["id"].ToString() + "\">" + "Practice" + "</a>";
                string practiceLink = "";
                if (cc1_count > 0)
                {
                    practiceLink = "<a href='./fc_reviews.aspx?SetID=1' style='color:blue;'> FlashCards</a> &nbsp;&nbsp;<a href='#' style='color:blue; padding-right: 10px;' onclick = 'Practice(" + dtr1["id"].ToString() + ",1)'> Practice</a> ";
                }
                lb1.Text += " <table class='edu-table-responsive table-hover'>"
                                           + "<tbody>"
                                           + "<tr class='heading-content'>"
                                           + "  <div class='row'>"
                                                  + "<td style='width: 60%;' class='left heading-content'>" + dtr1["order_id"].ToString() + ". "
                                                    + dbg_id + dtr1["cat_name"].ToString() + cc1_count
                                                  + "</td>"
                                                  + "<td class='green-color'>"
                                                  + practiceLink
                                                  + " <a href='javascript:void(0)' style='padding-left: 10px;' onclick = 'Review(" + dtr1["id"].ToString() + ",1)'>Review</a>"
                                                       + "<a  data-toggle='collapse' class='fa fa-chevron-down pull-right fa-2x' style='margin-right: 20px;    font-size: 1em;'   href=#"
                                                                  + dtr1["cat_name"].ToString() + " aria-expanded='true'>" +
                                                        "</a>" +
                                                    "</td>"
                                            + "</div>" +
                                            "</tr>"

                + " <tr class='table-row collapse in' id=" + dtr1["cat_name"].ToString() + ">"
                + " <td colspan='2'>"
                    + "<table class='edu-table-responsive table-hover' style='margin-left:40px; width: calc(100% - 40px);'>"
                        + get_lower_rows(dtr1["id"].ToString()) + "</td>"
                    + " </table> "
                + "</td>"
            + "</tr>";
            }
            lb1.Text += " </tbody> "
                                  + " </table> ";
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/Course detail", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_h_cc2, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div.Visible = false;

        }
    }
    public string get_lower_rows(string cat_id)
    {
        try
        {
            StringBuilder str_return = new StringBuilder();
            int cc2_count = 0;
            string cc2_countdebug = "";
            string dbg_id = "";
            string filter_exp = "cc1=" + cat_id + " and " + "tree_level = 2";
            DataRow[] dtr_temp = dt_full.Select(filter_exp, "order_id");
            foreach (DataRow dtr in dtr_temp)
            {
                cc2_count = Convert.ToInt16(fc_gen_utils.hash_lookup(h_cc2, dtr["id"].ToString(), "0"));
                string practiceLink = "";
                string Progressbar = "";
                double valueis = 0;
                valueis = hashvalue(dtr["id"].ToString());
                if (cc2_count > 0)
                {
                    practiceLink = "<a href='#' style='color:blue' onclick = 'Practice(" + dtr["id"].ToString() + ",2)'> Practice</a>";
                }
                if (page_debug == 1)
                {
                    dbg_id = "ID: (" + dtr["id"].ToString() + ")";
                    cc2_countdebug = cc2_count.ToString();
                }
                str_return.Append(@"<tr class='table-row'>
                  <td class=' col-sm-7'><a href='javascript:void(0)'><i class='mr18 fa fa-file-text'></i>");
                Progressbar = "<div class='progress' data-width='" + valueis + "%'><div class='html'></div></div>";

                str_return.Append(dtr["order_id"].ToString() + ". " + dbg_id
                               + "<span>" + dtr["cat_name"].ToString() + " " + cc2_countdebug + "</span><br>");
                str_return.Append("<td class='col-sm-2'>" + Progressbar + "</td> ");
                str_return.Append("<td class='col-sm-1 green-color'>"
                                  + practiceLink + "</td> "
                                  + " <td class='col-sm-1 green-color'><a href='#' onclick = 'Review(" + dtr["id"].ToString() + ",2)'>Review</a></td>");
                str_return.Append("</tr>");
            }
            return str_return.ToString();
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/get_lower_rows", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            div.Visible = false;
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            return "";
        }
    }

    public double hashvalue(string number)
    {
        double valueis = 0;
        number = fc_gen_utils.hash_lookup(h_score, number, "0");
        if (!string.IsNullOrEmpty(number))
        {
            valueis = Math.Round(Convert.ToDouble(number), 2);
        }
        return valueis;
    }
    [System.Web.Services.WebMethod]
    public static object funPractice(string id, int level)
    {
        string title = "";
        string str_sql_title = "select top 1 cat_name from core_cat where cc" + level + " = " + id;
        try
        {
            title = gen_db_utils.gp_sql_scalar(str_sql_title);
            HttpContext.Current.Session["quiz_title"] = str_sql_title;
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/funPractice", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_title, true);
            return new { };
        }

        string str_sql = "select top 100 type2_id from type2_qs where cc" + level + " = " + id + " and status = -950 ";

        if (level == 0 || level == 1)
        {
            str_sql += " and type2_id not in (select q_seq_id from st_answer where student_id = " + HttpContext.Current.Session["userid"].ToString() + "  ) ";
        }

        str_sql += " order by newid() ";

        try
        {
            ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

            o_quiz_param item_q_param = new o_quiz_param
            {
                start_time = DateTime.Now.ToString(),
                last_activity_time = DateTime.Now.ToString(),
                query = str_sql,
                quiz_title = title,
                quiz_q_string = fc_gen_utils.arraylist2string(ar_qs),
                quiz_code = fc_gen_utils.new_batch_id()
            };
            item_q_param.set2session();

            HttpContext.Current.Session["quiz_start"] = str_sql;

            return new
            {
                Data = new
                {
                    id,
                    url = "/Portal/quiz.aspx?courseid=0"
                }
            };
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/funPractice", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            return new { };
        }
    }

    [System.Web.Services.WebMethod]
    public static object funReview(string id, int level)
    {
        HttpContext.Current.Session["fcr_123"] = (id + ' ' + level);
        string str_sql_title = "select top 1 cat_name from core_cat where cc" + level + " = " + id;

        string title = gen_db_utils.gp_sql_scalar(str_sql_title);

        string str_sql = "select top 100 * from fc_repository where cc" + level + " = " + id + " and status > -950 order by newid() ";

        HttpContext.Current.Session["fcr_123"] += str_sql;
        ArrayList ar_fc_ids = gen_db_utils.gp_sql_get_arraylist(str_sql);

        o_fcr_param item_fcr_param = new o_fcr_param();

        HttpContext.Current.Session["fcr_123"] += "step 2";

        item_fcr_param.fcr_start_time = DateTime.Now.ToString();
        item_fcr_param.fcr_last_activity_time = DateTime.Now.ToString();
        item_fcr_param.fcr_query = str_sql;
        item_fcr_param.fcr_fc_id_string = fc_gen_utils.arraylist2string(ar_fc_ids);
        item_fcr_param.fcr_code = fc_gen_utils.new_batch_id();

        item_fcr_param.fcr_title = title;
        HttpContext.Current.Session["fcr_123"] += "step 3";

        item_fcr_param.set2session();

        HttpContext.Current.Session["fcr_start"] = str_sql;

        return new
        {
            Data = new
            {
                id,
                url = "/Portal/fc_quiz.aspx"
            }
        };
    }

    protected void btnCircullim_Click(object sender, EventArgs e)
    {
        try
        {

            string parent_cat = Request.QueryString.GetValues("courseid") == null ? "1" : (Request.QueryString.GetValues("courseid")[0].ToString());
            Response.Redirect("~/Portal/curriculum.aspx?courseid=" + parent_cat);
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/get_lower_rows", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            btnCircullim.Visible = false;
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";

        }
    }
}

