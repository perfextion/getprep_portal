﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="quiz.aspx.cs" Inherits="Portal_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentbodyscripts" runat="Server">
    <style>
        label {
            font-weight: normal;
        }

        .mfib {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .ddl {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .donebtn {
            float: right;
            margin-right: 18px;
            width: 130px;
            border-radius: 5px;
            padding: 0px;
        }
    </style>
    <script src="portalassets/plugin/Timer/timer.js"></script>
    <script>
        function MyFunction() {
            let cqz_query;
            cqz_query = $('#ContentPlaceHolder1_hdncqz_query').val();
            localStorage.setItem('cqz_query', cqz_query);
        }
        $(document).ready(function () {

            var totalval = $('#ContentPlaceHolder1_lblQuestionCount').text().split('/');

            if (totalval == "") {
                $('#ContentPlaceHolder1_lblTotalQuestions').text('0');
            }
            else {

                $('#ContentPlaceHolder1_lblTotalQuestions').text(totalval[0] - 1);
            }
            //$('#ContentPlaceHolder1_lblTotalQuestions').text('0');


        });
        function funresetoldselectedvalue() {
            if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MFIB") {
                var resval = $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans").split('|');
                var corrans = $('#ContentPlaceHolder1_hdnCorrectAnswer').val().split('|');
                var counter = 0;
                $("#ContentPlaceHolder1_lblMainQuestion :input").each(function (e) {
                    this.value = resval[counter];

                    if (resval[counter] == corrans[counter]) {
                        this.style = "width:" + this.style.width + "; border-color:green";
                    } else {
                        this.style = "width:" + this.style.width + "; border-color:red";
                    }
                    counter++
                });
            } else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "DD") {
                var resval = $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans").split('|');
                var corrans = $('#ContentPlaceHolder1_hdnCorrectAnswer').val().split('|');
                var counter = 0;
                $.each($('.ddl'), function (index, value) {
                    $(this).val(resval[counter]);
                    if (resval[counter] == corrans[counter]) {
                        this.style = "width:" + this.style.width + "; border-color:green";
                    } else {
                        this.style = "width:" + this.style.width + "; border-color:red";
                    }
                    counter++;
                });
            } else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MCM") {
                var resval = $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans").split('|');
                var counter = 0;
                $("#ContentPlaceHolder1_lblMainQuestion :input").each(function (e) {

                    for (var i = 0; i < resval.length; i++) {
                        if (resval[i].toLocaleLowerCase() == this.value.toLocaleLowerCase()) {
                            this.checked = true;

                        }
                    }
                });
            }
        }
        function funvalidatecheckopt(val, inlist) {
            var res = false;
        }
        function funConfirmAnswer() {
            $("#ContentPlaceHolder1_hdnTime").val(counter);
            counter = 0;
            if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MFIB") {
                var fibans = "";
                var resval = true;
                $("#ContentPlaceHolder1_lblMainQuestion :input").each(function (e) {
                    if ((this.value) == "") {
                        resval = false;
                    } else {
                        fibans += this.value + "|";
                    }
                });
                if (!resval) {
                    alert("Please enter an answer");
                    return false;
                } else {

                    $('#ContentPlaceHolder1_txtAnswer').val(fibans.slice(0, -1));
                }
            }
            else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "DD") {
                var ddans = "";
                var resval = true;
                $.each($('.ddl'), function (index, value) {
                    if ($(value).attr("answer") != "") {
                        ddans += $.trim($(value).attr("answer")) + "|";
                    } else {
                        resval = false;
                    }
                });

                if (!resval) {
                    alert("Please select an answer");
                    return false;
                } else {

                    $('#ContentPlaceHolder1_txtAnswer').val(ddans.slice(0, -1));
                }
            }
            else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MC") {

                if ($('input[id*=ContentPlaceHolder1_rdans1]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans2]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans3]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans4]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans5]').is(":checked")) {
                    return true;
                } else {
                    alert("Please select an answer");
                    return false;
                }
            }
            else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MCM") {
                var fibans = "";
                var resval = true;
                $("#ContentPlaceHolder1_lblMainQuestion :input").each(function (e) {
                    if (this.checked) {
                        fibans += this.value.toLocaleLowerCase() + "|";
                    }
                });
                if (fibans == "") {
                    alert("Please enter an answer");
                    return false;
                } else {

                    $('#ContentPlaceHolder1_txtAnswer').val(fibans.slice(0, -1));
                }
            }
            else {
                if ($('#ContentPlaceHolder1_txtAnswer').val() != "") {
                    return true;
                } else {
                    alert("Please select an answer");
                    return false;
                }
            }
        }
        function funNextClick() {
            $("#ContentPlaceHolder1_hdnTime").val(counter);
            counter = 0;
            totalvalue();
            if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MFIB") {
                var fibans = "";
                var resval = true;
                $("#ContentPlaceHolder1_lblMainQuestion :input").each(function (e) {
                    if ($.trim(this.value) == "") {
                        resval = false;
                    } else {
                        fibans += this.value + "|";
                    }
                });
                if (!resval) {
                    $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans", "");
                    alert("Please enter an answer");
                    return false;
                } else {

                    $('#ContentPlaceHolder1_txtAnswer').val(fibans.slice(0, -1));
                    $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans", fibans.slice(0, -1));
                }
            } else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "DD") {
                var ddans = "";
                var resval = true;
                $.each($('.ddl'), function (index, value) {
                    if ($.trim($(value).attr("answer") != "")) {
                        ddans += $.trim($(value).attr("answer")) + "|";
                    } else {
                        resval = false;
                    }
                });

                if (!resval) {
                    $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans", "");
                    alert("Please select an answer");
                    return false;
                } else {
                    $('#ContentPlaceHolder1_txtAnswer').val(ddans.slice(0, -1));
                    $('#ContentPlaceHolder1_lblQuestionType').attr("selectedans", ddans.slice(0, -1));

                }
            } else if ($('#ContentPlaceHolder1_lblQuestionType').attr("qtype") == "MC") {

                if ($('input[id*=ContentPlaceHolder1_rdans1]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans2]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans3]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans4]').is(":checked")
                    || $('input[id*=ContentPlaceHolder1_rdans5]').is(":checked")) {
                    totalvalue();
                    //   crctans()
                    return true;
                } else {
                    alert("Please select an answer");
                    return false;
                }
            } else {
                if ($('#ContentPlaceHolder1_txtAnswer').val() != "") {
                    totalvalue();
                    //  crctans()
                    return true;
                } else {
                    alert("Please select an answer");
                    return false;
                }
            }
            //  crctans();
        }
        function crctans() {

            var crctansw = $('#ContentPlaceHolder1_hdnans').val();
            if (crctansw != "") {
                $('#ContentPlaceHolder1_lblCorrectAnswers').text(crctansw);
            }
            else {
                $('#ContentPlaceHolder1_lblCorrectAnswers').text('0');
            }
        }
        function totalvalue() {
            var totalval = $('#ContentPlaceHolder1_lblQuestionCount').text().split('/');

            if (totalval == "") {
                $('#ContentPlaceHolder1_lblTotalQuestions').text('0');
            }
            else {

                var val = totalval[0];
                $('#ContentPlaceHolder1_lblTotalQuestions').text(parseFloat(val));
            }
        }
        function funrefreshmathqst(val) {
            if (val == 'c') {
                funresetoldselectedvalue();
            }
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, 'lblMainQuestion']);
            crctans();
        }
        function funchangeComment() {

            if ($("#ddlComments").text() != "Select") {
                $("#btnSubmit").show();
            }
            else {
                $("#btnSubmit").hide();
            }
            return false;
        }
        function funchangeddanswer(val) {

            if ($(val).val() != "Select") {

                $(val).attr("answer", $(val).val());

            }

        }
        function funSubmitComment() {
            $("#btnSubmit").hide();
            $("#divComments").hide();
            $("#ContentPlaceHolder1_btnSkip").show();
            return false;
        }
        function QuestionNotclear() {
            if (confirm("Are you sure ? Problem with question?")) {
                $("#ContentPlaceHolder1_btnQuestionNotclear").click();
            }
            return false;
        }
        function funSkip() {
            $("#btnSubmit").show();
            $("#divComments").show();
            $("#ContentPlaceHolder1_btnSkip").hide();
            totalvalue();
        }
        $("#ContentPlaceHolder1_txtAnswer").keyup(function (event) {
            if (event.keyCode === 13) {
                if ($('#ContentPlaceHolder1_btnConfirm').is(':visible')) {
                    // Code
                    $("#ContentPlaceHolder1_btnConfirm").click();
                } else {
                    $("#ContentPlaceHolder1_btnNext").click();
                }

            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnTime" Value="0" runat="server" />
    <asp:HiddenField ID="hdncqz_query" Value="0" runat="server" />
    <div class="course-syllabus-title underline">
        <div class="col-sm-7" style="line-height: .9;">
            <asp:Label ID="lbl_title" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="col-sm-5">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md- 3">
                            <div style="float: left;">
                                <label id="Label1" style="color: green;">Correct :</label>
                                <asp:Label ID="lblCorrectAnswers" runat="server" Style="color: green;" CssClass="lblTotalQuestions" Text="0"> </asp:Label><span> /</span>
                                <asp:Label ID="lblTotalQuestions" runat="server" Style="color: green;" Text="" CssClass="lblTotalQuestions"></asp:Label>
                            </div>
                            <div style="text-align: right;">
                                <label id="Label4" style="color: green;" class="lblTime">This Question Time:</label>
                                <label id="Label5" class="lblTotalTime" style="color: green">Total Time :</label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-">
        <asp:Button ID="btnDone" runat="server" CssClass="btn btn-login btn-green donebtn" Text="Done Practice" OnClick="btnDone_Click" />
    </div>
    <br />
    <br />
    <br />
    <b>
        <asp:Label ID="lblErrorMessage" runat="server" Style="color: red;"></asp:Label></b>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" Visible="true">

        <ContentTemplate>

            <div class="row" id="div">

                <div class="col-sm-12">



                    <div class="row">
                        <div class="col-sm-9">
                            <asp:Label ID="lblStatus" runat="server" Style="color: red; display: none" Text="&nbsp"></asp:Label>
                            <asp:Label ID="lblQuestionType" runat="server" Style="display: none"></asp:Label>
                            <asp:Label ID="lblQuestionCount" runat="server" Style="color: green;" Text=""></asp:Label>
                        </div>

                        <div class="col-sm-3" style="float: right; display: none">
                            <asp:CheckBox ID="chkReviewer" AutoPostBack="true" CssClass="" runat="server" Checked="true" OnCheckedChanged="chkReviewer_CheckedChanged" />
                            Review Answer 
                        </div>
                    </div>

                    <div class="panel panel-default" style="background-color: white">



                        <div class="panel-body">

                            <asp:Panel ID="Panel1" DefaultButton="btnConfirm" runat="server" Visible="true">

                                <div class="col-lg-12" style="min-height: 200px">

                                    <div class="col-sm-12">
                                        <asp:HiddenField ID="hdnQuestionID" runat="server" />
                                        <asp:HiddenField ID="hdnCorrectAnswer" runat="server" />
                                        <asp:HiddenField ID="hdnans" runat="server" />
                                        <asp:Label Style="display: none" ID="hdnMFIBDDAnswer" runat="server" />
                                        <asp:Label ID="lblMainQuestion" runat="server">
                                        </asp:Label>
                                    </div>

                                    <asp:Panel ID="pnlMC" runat="server">
                                        <div class="col-md-12">
                                            <%--<asp:Label ID="lblans1" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblAnsOpt1" runat="server"></asp:Label>
                                            <asp:RadioButton ID="rdans1" GroupName="G1" Style="margin-left: 10px" Visible="false" runat="server" />
                                        </div>
                                        <div class="col-md-12">
                                            <%--<asp:Label ID="lblans2" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblAnsOpt2" runat="server"></asp:Label>
                                            <asp:RadioButton ID="rdans2" Style="margin-left: 10px" GroupName="G1" Visible="false" runat="server" />
                                        </div>
                                        <div class="col-md-12">
                                            <%--<asp:Label ID="lblans3" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblAnsOpt3" runat="server"></asp:Label>
                                            <asp:RadioButton ID="rdans3" GroupName="G1" Style="margin-left: 10px" Visible="false" runat="server" />
                                        </div>
                                        <div class="col-md-12">
                                            <%--<asp:Label ID="lblans4" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblAnsOpt4" runat="server"></asp:Label>
                                            <asp:RadioButton ID="rdans4" GroupName="G1" Style="margin-left: 10px" Visible="false" runat="server" />
                                        </div>
                                        <div class="col-md-12">
                                            <%--<asp:Label ID="lblans5" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblAnsOpt5" runat="server"></asp:Label>
                                            <asp:RadioButton ID="rdans5" GroupName="G1" Style="margin-left: 10px" Visible="false" runat="server" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlFIB" runat="server">
                                        <div class="col-md-12">

                                            <asp:TextBox ID="txtAnswer" Width="200px" Style="margin-left: 10px" AutoCompleteType="Disabled" runat="server"></asp:TextBox>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSolution" runat="server">
                                        <div class="col-md-8 " style="text-align: left">
                                            <asp:Label ID="lblCorrectAns" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblAnswer" runat="server"></asp:Label><br />
                                            Solution:  
                                                    <asp:Label ID="lblSolution" runat="server"></asp:Label>
                                        </div>
                                    </asp:Panel>



                                </div>
                                <div class="col-sm-12" style="text-align: right">

                                    <asp:Button ID="btnConfirm" runat="server" class="btn btn-primary" Style="width: 150px; float: right" Text="Confirm" OnClientClick="return funConfirmAnswer();" OnClick="btnConfirm_Click" />
                                    <asp:Button ID="btnNext" runat="server" class="btn btn-warning" Style="width: 150px" Visible="false" OnClientClick="return funNextClick()" Text="Next" OnClick="btnNext_Click" />

                                </div>
                                <asp:Label ID="lbl_debug" runat="server" Text="Label"></asp:Label>
                            </asp:Panel>


                        </div>
                    </div>
                </div>
                <div class="col-sm-3" style="float: right">
                    <a href="#" onclick="QuestionNotclear()"><i class="glyphicon glyphicon-warning-sign"></i></a>
                    <div class="panel panel-danger ">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md -3">
                                    <span>Problem with question</span>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body" style="margin: 5px;">

                            <div class="row">

                                <div class="form-group" id="divComments">
                                    <%-- <asp:Label class="col-md-2 control-label" ID="lblSubject" runat="server"> Comments </asp:Label>--%>
                                    <%--    <div class="col-md-10">--%>

                                    <select id="ddlComments" onchange="return funchangeComment();" class="form-control">
                                        <option>Select</option>
                                        <option>Not in Syllabus</option>
                                        <option>Not clear  </option>
                                    </select>
                                    <%-- </div>--%>
                                </div>
                            </div>
                            <div class="row">
                                <div style="float: right;">
                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnSkip" CausesValidation="false" Style="display: none; float: right; width: 75px;" Text="Skip" CssClass="btn btn-primary" OnClientClick="return funSkip();" OnClick="btnSkip_Click" />
                                        <asp:Button runat="server" ID="btnQuestionNotclear" CausesValidation="false" Style="display: none; float: right; width: 75px;" Text="Skip" CssClass="btn btn-primary" OnClick="btnQuestionNotclear_Click" />

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div style="float: right">
                                    <input type="submit" id="btnSubmit" style="display: none" value="Submit" class="btn btn-success" onclick="return funSubmitComment();" />

                                </div>
                            </div>


                        </div>


                    </div>

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConfirm" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

