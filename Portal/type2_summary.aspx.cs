﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_type2_summary : System.Web.UI.Page
{
    DataTable data = new DataTable();
    public o_type2_q q_item;
    string quiz_code = "";
    int page_debug = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["quiz_code"] == null)
            {
                quiz_code = Session["cqz_token"].ToString();
            }
            else
            {
                quiz_code = Request.QueryString["quiz_code"].ToString();
            }
            debugOn();
            if (!IsPostBack)
            {
                getdata();
            }

        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
    }
    public void debugOn()
    {
        try
        {
            if (Session["debug_on"] != null)
            {
                if (Session["debug_on"].ToString() == "1") page_debug = 1;
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Coursedetail.aspx/Course detail", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "debugOn", true);
        }
    }
    public void getdata()
    {
        string str_sql = "select q.q_text,q.q_type,*from st_answer a join type2_qs q on q.type2_id = a.q_seq_id where a.quiz_token = '" + quiz_code + "'";
        try
        {
            data = gen_db_utils.gp_sql_get_datatable(str_sql);
            if (data.Rows.Count > 0)
            {
                tdattempts.InnerText = data.Rows.Count.ToString();
                int correccout = 0, wrongcount = 0;
                double totaltime = 0;

                for (int i = 0; i < data.Rows.Count; i++)
                {
                    if (data.Rows[i]["correct_answer"].ToString().ToUpper() == data.Rows[i]["response"].ToString().ToUpper())
                    {
                        correccout = correccout + 1;
                    }
                    else
                    {
                        wrongcount = wrongcount + 1;
                    }
                    totaltime = totaltime + (Convert.ToDouble(data.Rows[i]["time_taken"].ToString()));
                }
                tdwrong.InnerText = wrongcount.ToString();
                tdcorrect.InnerText = correccout.ToString();
                TimeSpan t = TimeSpan.FromSeconds(totaltime);

                string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                t.Hours,
                                t.Minutes,
                                t.Seconds,
                                t.Milliseconds);
                tdtimespent.InnerText = answer;

            }
            funcreatequestion("A");
        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/getdata", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
    }


    public void funcreatequestion(string display)
    {
        string str_sql = "";
        string sql_filter = "";
        string Tablecaption = "";
        if (display == "C")
        {
            sql_filter = " and a.correct = 1";
            Tablecaption = "Correct Questions";
            btncorrectans.CssClass = "btn btn-primary";
            btnwrongans.CssClass = "btn btn-info";
        }
        else if (display == "W")
        {
            sql_filter = " and a.correct = 0";
            Tablecaption = "Wrong Questions";
            btnwrongans.CssClass = "btn btn-primary";
            btncorrectans.CssClass = "btn btn-info";

        }
        else if (display == "A")
        {
            sql_filter = " and 1=1";
            Tablecaption = "All Questions";
            btncorrectans.CssClass = "btn btn-info";
            btnwrongans.CssClass = "btn btn-info";

        }
        // if (display == "A")

        str_sql = "select a.correct_answer,a.ans_id,a.reviewed_on,a.response,a.correct,q.*, a.time_taken from st_answer a join type2_qs q on q.type2_id = a.q_seq_id "
                     + " where a.quiz_token = '" + quiz_code + "'" + sql_filter
                     + " order by a.ans_id";
        try
        {
            data = gen_db_utils.gp_sql_get_datatable(str_sql);
            string trdatas = "";
            if (data.Rows.Count > 0)
            {
                int counter = 1;
                //int time = 0;
                foreach (DataRow dtr in data.Rows)
                {
                    int i_duration = Int32.Parse(dtr["time_taken"].ToString());
                    string s_dur_mins = "";
                    //int timetaken = 0;
                    //timetaken = i_duration - time;
                    //time = time + i_duration;
                    //i_duration = timetaken;
                    if (i_duration < 60)
                    {
                        s_dur_mins = i_duration.ToString() + " sec";
                    }
                    else
                    {
                        s_dur_mins = Math.Round(Convert.ToDouble(i_duration) / 60.0, 1).ToString() + " minutes";
                    }

                    q_item = new o_type2_q(dtr);
                    string str_return = q_item.rendered_type2_q_withanswer("0");
                    string str_return_sol = "";
                    if (dtr["q_type"].ToString() == "FIB")
                    {
                        str_return_sol = "<b>Answer: </b> " + dtr["fib_ans"].ToString() + "<br><br>";
                        str_return_sol += dtr["solution"].ToString();
                    }
                    else if (dtr["q_type"].ToString() == "MC")
                    {
                        str_return_sol = "<b>Answer: </b> " + dtr["correct_ans"].ToString() + "<br><br>";
                        str_return_sol += dtr["solution"].ToString();
                    }

                    string str_sol = "";
                    if (str_return_sol != "")
                    {
                        str_sol = "<span style='color: #337ab7; cursor: pointer' onclick = 'showhidesolution(" + dtr["ans_id"].ToString() + ",this)'>Show Solution</span><div id='div_" + dtr["ans_id"].ToString() + "' style='display:none'>" + str_return_sol + "</div>";
                    }

                    string rowstyle = "'border-left: 2px solid green;'";
                    if (dtr["correct"].ToString() == "0")
                    {
                        rowstyle = "'border-left: 2px solid red;' ";
                    }
                    trdatas = trdatas + "<tr><th scope='row'  style=" + rowstyle + ">" + counter + "</th><td><span id='dynquestion' runat='server'>" + str_return + "</span>" + str_sol + "</td><td>" + dtr["response"] + "</td><td>";
                    if (dtr["correct"].ToString() == "0")
                    {
                        trdatas = trdatas + "<span class='fa fa-times fa-2x'></span>";
                    }
                    else
                    {
                        trdatas = trdatas + "<span class='fa fa-check fa-2x'></span>";
                    }
                    string removebtn = "";
                    if (page_debug == 1)
                    {
                        removebtn = "<button type='button' class='btn btn-danger' onclick = 'RemoveAnswer(" + dtr["ans_id"].ToString() + ")'>Remove</button>";
                    }
                    if (dtr["reviewed_on"].ToString() != "")
                    {
                        DateTime myDate = DateTime.Parse(dtr["reviewed_on"].ToString());
                        trdatas = trdatas + "</td><td><span id='s_" + dtr["ans_id"].ToString() + "' style='display:block'>" + myDate.ToString("MM/dd/yyyy") + "</span>";
                        trdatas = trdatas + "<button style='display:none' id='" + dtr["ans_id"].ToString() + "' type='button' class='btn btn-primary' onclick = 'ReviewAnswer(" + dtr["ans_id"].ToString() + ")'>Review</button><br><br>" + " Time: " + s_dur_mins + "</td></tr>";
                    }
                    else
                    {
                        trdatas = trdatas + "</td><td><span id='s_" + dtr["ans_id"].ToString() + "' style='display:none'></span>";
                        trdatas = trdatas + "<button style='display:block' id='" + dtr["ans_id"].ToString() + "' type='button' class='btn btn-primary' onclick = 'ReviewAnswer(" + dtr["ans_id"].ToString() + ")'>Review</button><br>" + removebtn + "<br>" + " Time: " + s_dur_mins + "</td></tr>";
                    }
                    counter++;
                }
            }

            lblMainQuestion.Text = "";
            string tabledata = "<table class='table'><caption>Summary for " + Tablecaption + " </caption><thead><tr><th scope ='col'>#</th><th scope='col'>Question</th><th scope = 'col'>Your Answer</th><th scope ='col'> Status </th><th scope ='col'> Review </th></tr></thead><tbody>" + trdatas + "</tbody></table>";
            lblMainQuestion.Text = tabledata;
        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/funcreatequestion", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }

    }


    [System.Web.Services.WebMethod]
    public static object updateanswer(string id)
    {
        DataTable data = new DataTable();
        string str_sql = "update st_answer set reviewed_on=getdate(),reviewed_by=1 where ans_id='" + id + "'";
        try
        {
            gen_db_utils.gp_sql_execute(str_sql);
        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/updateanswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            //lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }

        str_sql = "select reviewed_on from st_answer where ans_id='" + id + "'";
        try
        {
            data = gen_db_utils.gp_sql_get_datatable(str_sql);
            string json = "{id:" + id + ",reviewed_on:" + data.Rows[0]["reviewed_on"] + "}";
            string str = "{ 'Data': { 'id': '" + id + "', 'reviewed_on': '" + data.Rows[0]["reviewed_on"] + "'} }";
            JavaScriptSerializer j = new JavaScriptSerializer();
            object a = j.Deserialize(str, typeof(object));
            return a;
        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/updateanswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            //lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
            return "";
        }
    }


    [System.Web.Services.WebMethod]
    public static void RemoveAnswer(string id)
    {
        string str_sql = "delete st_answer where ans_id='" + id + "'";
        try
        {
            gen_db_utils.gp_sql_execute(str_sql);
        }
        catch (Exception Ex)
        {
            LogError.Log("type2_summary.aspx/updateanswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            //lblErrorMsg.Text = "There is some error occured while processing.Please contact to Administrator.";
        }
    }
    protected void btnwrongans_Click(object sender, EventArgs e)
    {
        funcreatequestion("W");
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "funrefreshmathqst()", true);

    }

    protected void btncorrectans_Click(object sender, EventArgs e)
    {
        funcreatequestion("C");
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "funrefreshmathqst()", true);

    }
}