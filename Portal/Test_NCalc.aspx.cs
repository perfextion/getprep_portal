﻿using System;
using NCalc;

public partial class Portal_Test_NCalc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void calcBtn_Click(object sender, EventArgs e)
    {
        var exp = expressionTxt.Text;
        if(!string.IsNullOrEmpty(exp))
        {
            try
            {
                var expression = new Expression(exp);
                var res = expression.Evaluate();
                resultLabel.Text = res.ToString();
            }
            catch (Exception ex)
            {
                resultLabel.Text = ex.Message;
            }
        }
    }
}