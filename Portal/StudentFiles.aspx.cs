﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
public partial class Portal_upload1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            bind();
            getstudentuploaddata();
            //uploadtwo.Visible = false;
            uploadtwo.Visible = true;
        }
    }
    public void bind() {
        //DataTable dt_full;
        //string str_sql_lower = "select top 4 * from CMS_courses ";

        //dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);

        //Ddlcourse.DataTextField = "course_name";
        //Ddlcourse.DataValueField = "course_id";
        //Ddlcourse.DataSource = dt_full;

        //Ddlcourse.DataBind();

    }
    public void getstudentuploaddata() {
         string str_sql_lower = "select DocumentId, St_Id, DocumentName, DocumentFileName, CreatedTime from Student_Files where St_Id = " + Session["userid"] + " order by Student_Files.CreatedTime desc";
         try
         {

             DataTable dt = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
             uploadworks.Text = "<table class='table table-striped'>";

             for (int i = 0; i < dt.Rows.Count; i++)
             {

                 // filename,filesize,uploadeddatetime,removelink,preview


                 uploadworks.Text += "<tr><td  id='u" + dt.Rows[i]["DocumentId"].ToString() + "'>";
                 uploadworks.Text += dt.Rows[i]["DocumentName"].ToString() + "</td>";
                 uploadworks.Text += "<td>" + dt.Rows[i]["CreatedTime"].ToString() + "</td>";
                 string path = "~/studentfile/";
                 string sfullpath = Path.Combine(Server.MapPath(path), dt.Rows[i]["DocumentName"].ToString());
                 string fullpath = "../studentfile/" + dt.Rows[i]["St_Id"].ToString() + "_" + dt.Rows[i]["Documentid"].ToString() + "_" + dt.Rows[i]["DocumentName"].ToString();
                 uploadworks.Text += "<td><button  type='button' class='btn btn-info openBtn' onclick=\"funpreview('" + fullpath + "')\" data-toggle='modal' data-target='#basicModal'>Preview</button></td>";
                 uploadworks.Text += "</tr>";

             }
             uploadworks.Text += " </table>";
         }
         catch (Exception Ex)
         {
             LogError.Log("StudentFiles.aspx/getstudentuploaddata", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql_lower, true);
             
         }
    }


    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        //string s = "INSERT INTO cms_hw(hw_title,hw_desc,hw_prob_count,fk_hw_type,hw_due_date,hw_issue_date,hw_status,fk_course_id,fk_st_id)VALUES('', '', '" + numberoffileupload.Text + "', '"+Ddltypeofitem.SelectedValue +"', '"+duedate.Text +"', '','','"+Ddlcourse.SelectedValue+"','"+ Session["StudentId"].ToString() + "')";
        //gen_db_utils.gp_sql_execute(s);
        //string str_sql_lower = "select max(hw_id) from cms_hw";
        //DataTable dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
        //Session["hw_id"] = dt_full.Rows[0][0];
        //uploadone.Visible = false;
        //uploadtwo.Visible = true;
      

    }

    protected void btnfinalsubmit_Click(object sender, EventArgs e)
    {
        if (lblFiles.Text != "")
        {

            //string s = "update CMS_HW set hw_status=1 where hw_id=" + Session["hw_id"];
            //gen_db_utils.gp_sql_execute(s);
            //Session["hw_id"] = null;
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "funmsg(2)", true);
            //numberoffileupload.Text = "";
            //duedate.Text = "";
            //bind();
            //getstudentuploaddata();
            //uploadtwo.Visible = false;
            //uploadone.Visible = true;
            //  Response.Redirect("~/Students/upload1.aspx");
            getstudentuploaddata();
            lblFiles.Text = "";
        }
        else {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "funmsg(1)", true);

        }

    }

    protected void btnpost_Click(object sender, EventArgs e)
    {
       
        try
        {
            string path = "~/studentfile";
            bool folderExists = Directory.Exists(Server.MapPath(path));
            if (!folderExists)
                Directory.CreateDirectory(Server.MapPath(path));

            int i = 1;
            lblFiles.Text = "<div>";
            foreach (HttpPostedFile thefile in multifileupload.PostedFiles)
            {
                //Naming Convention st_id_file name_document_id
                string s = "insert into Student_Files(St_Id, DocumentName, DocumentFileName, CreatedTime)values(" + Session["userid"] + ",'" + thefile.FileName + "', '" + thefile.FileName + "',getdate());select @@identity as id";
                string id = gen_db_utils.gp_sql_scalar(s);

                string sfile = Session["userid"].ToString() + "_" + id + "_" + thefile.FileName;
                string sfullpath = Path.Combine(Server.MapPath(path), sfile);
                thefile.SaveAs(sfullpath);


                //string str_sql_lower = "select max(hw_id) from cms_hw";
                //DataTable dt_full = gen_db_utils.gp_sql_get_datatable(str_sql_lower);
                i++;
                int b = (int)thefile.ContentLength;
                float kb = b / 1024;
                float mb = kb / 1024;
                lblFiles.Text += "<div id='" + id + "' class='row inneruploadedfile'><div class='col-lg-8'><a href='#'>" + thefile.FileName + "   (" + mb + " kb)</a> </div><div class='col-lg-4' ><div style='color: red;cursor: pointer' onclick='fireServerButtonEvent(" + id + ")'>X</div> </div></br></div>";
            }
            lblFiles.Text += "</div>";
        }
        catch (Exception Ex)
        {
            LogError.Log("StudentFiles.aspx/btnpost_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", true);
        }
           
    }

    protected void test_Click(object sender, EventArgs e)
    {
        string s = "delete from CMS_HW_Attachments where attachment_id='"+HiddenField1.Value.Replace("u","")+"'";
        try
        {
            gen_db_utils.gp_sql_execute(s);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "$('#" + HiddenField1.Value + "').remove()", true);
        }
        catch (Exception Ex)
        {
            LogError.Log("StudentFiles.aspx/test_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, s, true);
            
        }
    }
    protected void test_Click1(object sender, EventArgs e)
    {
        string s = "delete from CMS_HW_Attachments where attachment_id='" + HiddenField1.Value + "'";
        try
        {
            gen_db_utils.gp_sql_execute(s);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "$('#u" + HiddenField1.Value + "').remove()", true);
        }
        catch (Exception Ex)
        {
            LogError.Log("StudentFiles.aspx/test_Click1", Ex.Message, Ex.InnerException, "", Ex.StackTrace, s, true);
           
        }
    }
}