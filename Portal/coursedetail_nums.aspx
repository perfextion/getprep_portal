﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="coursedetail_nums.aspx.cs" Inherits="coursedetail_nums" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="courses-detail-wrapper col-12">


        <div class="row">
            <div class="col-md-12 layout-left">
                <div class="course-syllabus">
                    <div class="course-syllabus-title underline">Courses syllabus</div>
                     <asp:Button ID="btnCircullim" runat="server" CssClass="btn btn-login btn-green" Text="Change Cirriculum" OnClick="btnCircullim_Click" />
                    
                    <div class="course-table">
                        <div class="outer-container">
                            <div class="inner-container">
                                <div class="table-header">
                                    <table class="edu-table-responsive" >
                                        <thead>
                                            <tr class="heading-table">
                                                <th class="col-6">title</th>
                                                <th class="col-4"></th>
                                              
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="table-body">
                                    <asp:Label ID="lb1" runat="server"></asp:Label>

 

                                </div>
                                 <asp:Label ID="lbl" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <script>
        function Practice_cc1(id) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funPractice_cc1",
                data: '{id: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function Review_cc1(id) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funReview_cc1",
                data: '{id: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnReviewSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function Practice(id, level) {
           // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funPractice",
                data: '{id: "' + id + '" ,level : "' + level + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPracticeSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function Review(id, level) {
            // alert(id);
            $.ajax({
                type: "POST",
                url: "coursedetail.aspx/funReview",
                data: '{id: "' + id + '" ,level : "' + level + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnReviewSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        function OnPracticeSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;

        }
        function OnReviewSuccess(response) {
            var s = response.d.Data.url;
            window.location.href = s;
        }
    </script>
</asp:Content>
