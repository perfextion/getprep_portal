﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OMR_Summary : System.Web.UI.Page
{
    public static string test_key { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                test_key = Request.QueryString["test_key"].ToString();
                view_summary(Request.QueryString["test_key"].ToString(), Request.QueryString["form_no"].ToString());
                lblsummery.Text = "<a href='/Portal/OMR_results.aspx?form_no=" + Request.QueryString["form_no"].ToString() + "&test_key=" + Request.QueryString["test_key"].ToString() + "'>OMR Results</a>";
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/Page_Load", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";

        }
    }

    public void view_summary(string testtoken, string formno)
    {
        string strGetTest = "  ";
        strGetTest = "select f.form_q_id,f.q_num,f.answer as correct_ans,ans.answer as user_ans,ans.duration,answered_at,ans.ans_id,ans.correct from "
               + " OMR_FormQuestions as f inner join omr_answers as ans on f.q_num=ans.item_no and  f.form_id=ans.form_id "
               + "where   ans.test_key='" + testtoken + "' and st_id=" + Session["UserID"] + " "
              + " and    ans.item_type='Q' order by q_num  ";
        try
        {

            lblDate.Text = gen_db_utils.gp_sql_scalar("select MIN(answered_at) as TestDate from OMR_answers where test_key='" + testtoken + "'");


            string TotalTimeForTest = gen_db_utils.gp_sql_scalar("Select total_duration from omr_form where form_id=" + formno).ToString();

            Session["fdr"] = strGetTest;
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(strGetTest);


            lbltotalQuestion.Text = dt1.Rows.Count.ToString();

            int totalQuestions = dt1.Rows.Count;
            int TotalCorrect = 0;
            int TotalIncorrect = 0;
            int TotalAnswered = 0;
            int TotalTime = 0;
            object maxDate = dt1.Compute("MAX(answered_at)", null);
            object minDate = dt1.Compute("MIN(answered_at)", null);

            DateTime startTime = Convert.ToDateTime(maxDate);
            DateTime endtime = Convert.ToDateTime(minDate);

            TimeSpan duration = startTime - endtime;
            foreach (DataRow dtr in dt1.Rows)
            {
                if (dtr["user_ans"].ToString() != "-" && dtr["user_ans"].ToString() != "")
                {
                    TotalAnswered++;
                }
                if (dtr["correct"].ToString() == "1")
                {
                    TotalCorrect += 1;
                }
                else
                {
                    TotalIncorrect++;
                }

                TotalTime += Convert.ToInt32(dtr["duration"]);
            }
            lblanswered.Text = TotalAnswered.ToString();
            lblTestaverage.Text = (Convert.ToDouble(TotalTimeForTest) / dt1.Rows.Count).ToString();
            lblCorrect.Text = TotalCorrect.ToString();
            lblIncorrect.Text = TotalIncorrect.ToString();
            int seconds = Convert.ToInt32(TotalTime);
            var timespan = TimeSpan.FromSeconds(seconds);
            lblTotalTime.Text = duration.ToString(@"mm\:ss");
            lblavgTime.Text = Math.Round((Convert.ToDouble(TotalTime) / Convert.ToDouble(TotalAnswered)), 1).ToString();
            lblnotanswered.Text = (totalQuestions - Convert.ToInt32(TotalAnswered)).ToString();
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/view_summary", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strGetTest, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    [WebMethod]
    public static string GetBarChartData()
    {
        string strRemarks = "select item_no,duration,correct from OMR_answers where  item_type='Q' and test_key='" + test_key + "'";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strRemarks);
            string val = GetlineChartData();
            return JsonConvert.SerializeObject(dt);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/GetBarChartData", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strRemarks, true);
            return "";
        }
    }
    [WebMethod]
    public static string GetQuestionDetails(string questionNumber)
    {
        string strDetails = "";
        strDetails = "select f.form_q_id,f.q_num,f.answer as correct_ans,ans.answer, "
 + " ans.item_no,ans.answer as user_ans,ans.duration,ans.ans_id from   "
  + " OMR_FormQuestions as f inner join  omr_answers as ans on f.q_num=ans.item_no and f.form_id=ans.form_id "
   + " where ans.test_key='" + test_key + "' "
  + " and ans.item_type='Q' and f.q_num='" + questionNumber + "'";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strDetails);

            return JsonConvert.SerializeObject(dt);
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/GetQuestionDetails", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strDetails, true);
            return "";
        }
    }
    [WebMethod]
    public static string GetlineChartData()
    {
        string strRemarks = "select item_no,duration,correct from OMR_answers where  item_type='Q' and test_key='" + test_key + "'";
        try
        {
            DataTable dt = gen_db_utils.gp_sql_get_datatable(strRemarks);
            return JsonConvert.SerializeObject(GetlineChartDatatable(dt));
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/GetlineChartData", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strRemarks, true);
            return "";
        }
    }
    public static DataTable GetlineChartDatatable(DataTable dt1)
    {
        DataTable dt = new DataTable();
        try
        {
            string qno = "";
            double duration = 0;
            string correct = "";
            dt.Columns.Add("item_no", typeof(int));
            dt.Columns.Add("duration", typeof(string));
            dt.Columns.Add("correct", typeof(string));
            foreach (DataRow dtr in dt1.Rows)
            {
                qno = dtr["item_no"].ToString();
                duration += Convert.ToDouble(dtr["duration"]);
                correct = dtr["correct"].ToString();
                dt.Rows.Add(qno, duration, correct);
            }
            return dt;
        }
        catch (Exception Ex)
        {
            LogError.Log("OMR_Summary.aspx/GetlineChartDatatable", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "GetlineChartDatatable", true);
            return dt;
        }
    }
}