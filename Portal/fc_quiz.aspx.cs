﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using Newtonsoft.Json.Linq;

public partial class fc_quiz : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "GetPrep| FlashCards";
        if (!Page.IsPostBack)
        {
            setParameters();
            GetAllQuestionsAndAnswer();

        }
    }


    string token = "";
    private void setParameters()
    {
         
        string str_sql = "";

     //   str_sql = "Select top 15 * from fc_repository where fc_id in (12,13,20106,88974,112471,20134)";
        if (Session["fcr_query"] != null)
        {
            str_sql = Session["fcr_query"].ToString();
        }
            string quiz_token = Session["fcr_code"].ToString();
       

       
    }
    private void GetAllQuestionsAndAnswer()
    {

        string query = "";
        if (Session["fcr_query"] != null)
        {
            query = Session["fcr_query"].ToString();
        }

        token = Session["fcr_code"].ToString();
           
        try
        {
            // string SQL = "Select top 50 * from fc_repository ";
            DataTable dtQuestions = gen_db_utils.gp_sql_get_datatable(query);
            string QuestionsHTML = "";
            int i = 0;
            string firstquestion = "";
            string firstquestionID = "";
            string firstanswer = "";
            foreach (DataRow dtr in dtQuestions.Rows)
            {
                string question = dtr["fc_term"].ToString();
                string solution = gp_utils.html_jax(dtr["fc_desc"].ToString());
                if (i == 0)
                {
                    firstquestion = question;
                    firstquestionID = dtr["fc_id"].ToString();
                    firstanswer = solution;
                }
                i++;
                QuestionsHTML += "<img id='img_" + dtr["fc_id"].ToString() + "' style='float:left;margin-left:15px' src=\"images/omittedSmall.png\" answer='" + solution + "' question='" + question + "'/>";

            }
            lblQuestionsList.Text = QuestionsHTML;
            lblTotalQuestions.Text = dtQuestions.Rows.Count.ToString();
            lblQuestionsStats.Text = "1 / " + dtQuestions.Rows.Count.ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "FirstQuestion", "FirstQuestion('img_" + firstquestionID + "');", true);
        }
        catch (Exception Ex)
        {
            LogError.Log("fc_quiz.aspx/GetAllQuestionsAndAnswer", Ex.Message, Ex.InnerException, "", Ex.StackTrace, "", false);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
    [WebMethod]
    public static void InsertReviews(string result, string questionid, string time)
    {
        string token = "";
        string QuestionID = "";
        string strSQL = "insert into FlashCard_review values(" + QuestionID + "," + time + ",getdate()," + result + ",'" + token + "')";
        try
        {
           
             token = HttpContext.Current.Session["fcr_code"].ToString();
             QuestionID = questionid;
            string ftime = time;
            string fresult = result;
            if (fresult != "")
            {
              
                gen_db_utils.gp_sql_execute(strSQL);
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("fc_quiz.aspx/InsertReviews", Ex.Message, Ex.InnerException, "", Ex.StackTrace, strSQL, true);
            //lblErrormsg.Style.Add("color", "red");
            //lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }

    protected void btnDone_ServerClick(object sender, EventArgs e)
    {


        string token = HttpContext.Current.Session["fcr_code"].ToString();
        Response.Redirect("/Portal/fc_end.aspx?token=" + token);
    }


    protected void btnComment_Click(object sender, EventArgs e)
    {

    }
}