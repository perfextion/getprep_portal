﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_UsrHeaderBar_UsrHeaderBar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] == null || Session["roleid"] == null)
        {
            //Response.Redirect("/login.aspx");
        }
        if (!Page.IsPostBack)
        {
           // lblTopMenu.Text = gp_common_functions.get_portal_menu(Session["roleid"].ToString());
            bindlist();
        }
    }

    private void bindlist() {

        // menu dropdown from top right

        listmenu.InnerHtml = "<li class='dropdown'><a class='dropdown-toggle' role='button' data-toggle='dropdown' href='#'><i class='fa fa-user-circle-o fa-2x'></i><span class='caret'></span></a><ul id = 'g-account-menu' class='dropdown-menu' role='menu'>"
                          + "<li><a href = '/portal/Dashboard.aspx'><i class='fa fa-tachometer'><h4>DashBoard</h4></i></a> </li>"
                          + "<li><a href = '#' ><i class='fa fa-indent'><h4>My Courses</h4></i></a></li>"
                          + " <li><a href ='coursedetail.aspx'><i class='fa fa-calendar'><h4>Course Details</h4></i></a></li>"
                          + " <li><a href = 'flashcards.aspx'><i class='fa fa-file-text-o'><h4>FlashCards</h4></i></a></li>"
                          + "<li><a href ='#'><i class='fa fa-play-circle-o'><h4>Custom Courses</h4></i></a></li>"
                         +  "<li><a href ='concept.aspx'><i class='fa fa-play-circle-o'><h4>Concepts</h4></i></a></li>"
                         +   "<li><a href ='#'><i class='fa fa-user-circle-o'><h4>Profile</h4></i></a></li>"
                         +   "<li><a href ='Cs_Calendar.aspx'><i class='fa fa-calendar'><h4>Calendar</h4></i></a></li>"
                         + "<hr><li><a href = '../../Login.aspx'><i class='fa fa-sign-out'><h4>Signout</h4></i></a></li></ul></li>";
    }
}