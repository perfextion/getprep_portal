﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsrFooterBar.ascx.cs" Inherits="Portal_UserControl_UsrFooterBar" %>
<footer>

            <div class="footer-main">
                <div class="container">
                    
                    <div class="hyperlink">
                        <div class="pull-left hyper-left">
                            <ul class="list-inline">
                                <li><a href="index.html">HOME</a></li>
                                <li><a href="courses.html">COURSES</a></li>
                                <li><a href="about-us.html">ABOUT</a></li>
                                <li><a href="categories.html">PAGES</a></li>
                                <li><a href="news.html">NEWS</a></li>
                                <li><a href="contact.html">CONTACT</a></li>
                            </ul>
                        </div>
                        <div class="pull-right hyper-right">@ SWLABS</div>
                    </div>
                </div>
            </div>
        </footer>