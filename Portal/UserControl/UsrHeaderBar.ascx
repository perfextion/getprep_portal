﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsrHeaderBar.ascx.cs" Inherits="Portal_UsrHeaderBar_UsrHeaderBar" %>

<header>
    <%--<div id="top-nav" class="navbar navbar-inverse navbar-static-top">--%>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-toggle"></span>
        </button>

    </div>
    <div class="navbar-collapse collapse">
    </div>
    <%-- </div>--%>
    <div class="support">
        <img src="/Portal/portalassets/images/support.png" style="cursor: pointer" data-toggle="modal" data-target="#myModal" 
            class="img-responsive" alt="support">
    </div>
    <div class="header-main homepage-01">
        <div class="container">
            <div class="header-main-wrapper">
                <div class="navbar-header">
                    <div class="logo pull-left">
                        <a href="/Portal/Dashboard.aspx" class="header-logo">
                            <img src="/assets/images/logo-color-1.png" alt="" />
                        </a>
                    </div>
                    <button type="button" data-toggle="collapse" data-target=".navigation" class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <nav class="navigation collapse navbar-collapse pull-right">
                     <asp:Label ID="lblTopMenu" runat="server"></asp:Label>

                    <ul id="listmenu" runat="server" class="nav navbar-nav pull-right">

                     

                    </ul>


                </nav>
            </div>
        </div>
    </div>
</header>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Support</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control"></select>
                </div>

                <div class="form-group">
                    <label>Comments</label>
                    <textarea class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal">Submit</button>
                <button type="button" class="btn btn-basic" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
