﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;
using System.Web.Script.Serialization;
using System.Text;


public partial class Portal_debug : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["debug_on"] != null)
            {
                if (Session["debug_on"].ToString() == "1")
                {
                    lblmessage.Text = "Debug is On";
                    lblmessage.ForeColor = System.Drawing.Color.Green;
                    btndebug.Text = "Off";
                    btndebug.CssClass = "btn btn-danger";
                }
                else
                {
                    lblmessage.Text = "Debug is Off";
                    lblmessage.ForeColor = System.Drawing.Color.Red;
                    btndebug.Text = "On";
                    btndebug.CssClass = "btn btn-success";
                }
            }
            else
            {
                lblmessage.Text = "Debug is Off";
                lblmessage.ForeColor = System.Drawing.Color.Red;
                btndebug.Text = "On";
                btndebug.CssClass = "btn btn-success";
            }
        }
    }

    protected void btn_click(object sender, EventArgs e)
    {
        if (txtpassword.Text == "Sigma$123")
        {

            if (btndebug.Text == "On")
            {
                Session["debug_on"] = 1;
                lblmessage.Text = "Debug is On";
                lblmessage.ForeColor = System.Drawing.Color.Green;
                btndebug.Text = "Off";
                btndebug.CssClass = "btn btn-danger";
            }
            else if (btndebug.Text == "Off")
            {
                Session["debug_on"] = null;
                lblmessage.Text = "Debug is Off";
                lblmessage.ForeColor = System.Drawing.Color.Red;
                btndebug.Text = "On";
                btndebug.CssClass = "btn btn-success";
            }
        }
        else
        {
            lblmessage.Text = "Invalid Password";
            lblmessage.ForeColor = System.Drawing.Color.Red;
        }
    }
}