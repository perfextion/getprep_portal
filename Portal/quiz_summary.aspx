﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="quiz_summary.aspx.cs" Inherits="quiz_summary" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="courses-detail-wrapper col-12">


        <div class="row">
            <div class="col-md-12 layout-left">
                <div class="course-syllabus">
                    <div class="course-syllabus-title underline">Quiz Summary</div>
                    <asp:Label ID="lblErrormsg" runat="server"></asp:Label>
                    <div class="course-table">
                        <div class="outer-container">
                            <div class="inner-container">
                                <div class="table-header">
                                </div>
                                <div class="table-body">
                                    <asp:Label ID="lb1" runat="server"></asp:Label>

                                </div>
                                <asp:Label ID="lbl" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>


</asp:Content>
