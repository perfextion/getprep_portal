﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_concept : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            GetConcept("0");
        }
    }
    string courseid = "";
    string conceptId = "";
    protected void GetConcept(string PreviousConceptId)
    {
        string conceptId = "";
        string str_sql = "Select top 1 * from concepts where title is not null and title!='' order by concept_id";
        str_sql = "Select top 1 * from concepts   where title is not null and title!='' and  concept_id = " + conceptId + " order by concept_id";
        try
        {

            if (Request.QueryString["conceptId"] != null)
            {
                conceptId = Request.QueryString["conceptId"].ToString();
              
            }
            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            lblConcept.Text = dt1.Rows[0]["body"].ToString();
            lblConceptTitle.Text = dt1.Rows[0]["title"].ToString();
            courseid = dt1.Rows[0]["concept_id"].ToString();
            lblConceptId.Text = courseid;
            BindNextConcepts(dt1.Rows[0]["concept_id"].ToString());
        }
        catch (Exception Ex)
        {
            LogError.Log("concept.aspx/GetConcept", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
            div1.Visible = false;
        }

        //str_sql = "Select top 1 * from concepts  where title is not null and title!='' and  concept_id = " + conceptId + " order by concept_id ";
        //try
        //{
        //    if (Request.QueryString["conceptId"] != null)
        //    {
        //        conceptId = Request.QueryString["conceptId"].ToString();
        //    }

        //    lblConcept.Text = dt1.Rows[0]["body"].ToString();
        //    lblConceptTitle.Text = dt1.Rows[0]["title"].ToString();
        //    courseid = dt1.Rows[0]["concept_id"].ToString();
        //    lblConceptId.Text = courseid;
        //    BindNextConcepts(dt1.Rows[0]["concept_id"].ToString());
        //}
        //catch (Exception Ex)
        //{
        //    LogError.Log("concept.aspx/GetConcept", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
        //    lblErrormsg.Style.Add("color", "red");
        //    lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        //    div1.Visible = false;
        //}
    }
    protected void BindNextConcepts(string CurrentConceptId)
       {
        string str_sql = "Select top 5 * from concepts where title is not null and title!='' and concept_id >= " + CurrentConceptId + " order by concept_id";
        try
        {

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            lstConcepts.DataSource = dt1;
            lstConcepts.DataBind();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "FirstQuestion", "RefreshMathJax();", true);
            ScriptManager.RegisterStartupScript(Page, GetType(), "disp_confirm", "<script>RefreshMathJax();</script>", false);
        }
        catch (Exception Ex)
        {
            LogError.Log("concept.aspx/BindNextConcepts", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
        string str_sql = "Select top 1 * from concepts  where title is not null and title!='' and  concept_id > " + courseid + " order by concept_id";
      
            courseid = lblConceptId.Text;

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            lblConcept.Text = dt1.Rows[0]["body"].ToString();
            lblConceptTitle.Text = dt1.Rows[0]["title"].ToString();
            courseid = dt1.Rows[0]["concept_id"].ToString();
            lblConceptId.Text = courseid;
            BindNextConcepts(dt1.Rows[0]["concept_id"].ToString());
        }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        string str_sql = "Select top 1 * from concepts   where title is not null and title!='' and  concept_id < " + courseid + " order by concept_id desc";
        try
        {
            courseid = lblConceptId.Text;

            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);
            lblConcept.Text = dt1.Rows[0]["body"].ToString();
            lblConceptTitle.Text = dt1.Rows[0]["title"].ToString();
            courseid = dt1.Rows[0]["concept_id"].ToString();
            lblConceptId.Text = courseid;
            BindNextConcepts(dt1.Rows[0]["concept_id"].ToString());
        }
        catch
        {
        }
    }
}
