﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class fc_end : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillData();
        }
    }

    private void fillData()
    {
        try
        {


            string str_sql = "select * from FlashCard_review  as a join fc_repository as b on a.ques_seq_id =b.fc_id  where tokenId ='" + Request.QueryString["token"] + "'";

            lbl_top1.Text = "<table id=\"example - datatable\" class=\"table \">"
                                     + "<tr>"

                            + "<th>Sequence</i></th>"
                              + "<th>Question</th>"
                             + "<th>Time Taken</th>"
                                + " <th>Result</th>"

                           + "</tr>";


            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

            foreach (DataRow dtr in dt1.Rows)
            {

                string isAnsCorrect = "";

                if (dtr["Result"].ToString() == "True")
                {
                    lbl_top1.Text += "<tr class='success'>";
                    isAnsCorrect = "<i class='glyphicon glyphicon-ok'></i>";
                }
                else
                {
                    lbl_top1.Text += "<tr class='danger'>";
                    isAnsCorrect = "X";
                }

                lbl_top1.Text += "<td>" + dtr["ques_seq_id"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + dtr["fc_term"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + dtr["timeinterval"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + isAnsCorrect + "</td>" + "</tr>";
            }
            lbl_top1.Text += "</table>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ApplyMathJax", "ApplyMathJax();", true);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void fillData1()
    {
        string str_sql = "select * from FlashCard_review  as a join fc_repository as b on a.ques_seq_id =b.fc_id  where tokenId ='" + Request.QueryString["token"] + "'";
        try
        {
            lbl_top1.Text = "<table id=\"example - datatable\" class=\"table \">"
                         + "<tr>"

                + "<th>Sequence</i></th>"
                  + "<th>Question</th>"
                 + "<th>Time Taken</th>"
                    + " <th>Result</th>"

               + "</tr>";


            DataTable dt1 = gen_db_utils.gp_sql_get_datatable(str_sql);

            foreach (DataRow dtr in dt1.Rows)
            {

                string isAnsCorrect = "";

                if (dtr["Result"].ToString() == "True")
                {
                    lbl_top1.Text += "<tr class='success'>";
                    isAnsCorrect = "<i class='glyphicon glyphicon-ok'></i>";
                }
                else
                {
                    lbl_top1.Text += "<tr class='danger'>";
                    isAnsCorrect = "X";
                }

                lbl_top1.Text += "<td>" + dtr["ques_seq_id"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + dtr["fc_term"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + dtr["timeinterval"].ToString() + "</td>";
                lbl_top1.Text += "<td>" + isAnsCorrect + "</td>" + "</tr>";
            }

            lbl_top1.Text += "</table>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ApplyMathJax", "ApplyMathJax();", true);
        }
        catch (Exception Ex)
        {
            LogError.Log("fc_end.aspx/fillData", Ex.Message, Ex.InnerException, "", Ex.StackTrace, str_sql, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    }
}