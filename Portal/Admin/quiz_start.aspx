﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Portal/PortalMasterPage.master" AutoEventWireup="true" CodeFile="quiz_start.aspx.cs" Inherits="Portal_Admin_quiz_start" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="course-syllabus-title underline ">
        Quiz
    </div>
    <div class="row ">
        <div class="form-group">
            <div class="col-sm-9">
                <asp:TextBox ID="txtquizstart" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="4"></asp:TextBox>
            </div>

            <div class="col-sm-3" style="float: right">
                <asp:Button ID="btnDone" runat="server" CssClass="btn btn-login btn-green" Style="float: right" Text="Start" OnClick="btnDone_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentbodyscripts" runat="Server">
   
  
</asp:Content>

