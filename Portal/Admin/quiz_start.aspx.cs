﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class Portal_Admin_quiz_start : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtquizstart.Text = "select top 100 type2_id from type2_qs where q_type = 'MC'";
        }
    }

    protected void btnDone_Click(object sender, EventArgs e)
    {

        if (txtquizstart.Text != "") {

            //set quiz parameters

            string str_sql = txtquizstart.Text;
            ArrayList ar_qs = gen_db_utils.gp_sql_get_arraylist(str_sql);

            o_quiz_param item_q_param = new o_quiz_param();

            item_q_param.start_time = DateTime.Now.ToString();
            item_q_param.last_activity_time = DateTime.Now.ToString();
            item_q_param.query = txtquizstart.Text;
            item_q_param.quiz_q_string = fc_gen_utils.arraylist2string(ar_qs);
            item_q_param.quiz_code = fc_gen_utils.new_batch_id();
            

            item_q_param.set2session();
           
                              
                                
            Session["quiz_start"] = txtquizstart.Text;
            Response.Redirect("/Portal/quiz.aspx?courseid=0");
        }
    }
}