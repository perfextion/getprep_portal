﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ValidateToken();
        }
    }

    private void ValidateToken()
    {
        string chngpwd = "select * from students where PasswordToken ='" + Request.QueryString["Token"] + "' and PasswordExpirationDate is not null and PasswordExpirationDate>=getdate()";
        try
        {
           
            DataTable dt = new DataTable();
            dt = gen_db_utils.gp_sql_get_datatable(chngpwd);


            if (dt.Rows.Count == 0)
            {
                divBody.Visible = false;
                // divtit.Visible = false;
                lblMsg.Visible = true;
                lblMsg.Text = "<h3>" + "Your Token has been expired" + "</h3>";
                lblMsg.Style.Add("color", "red");

            }
        }
            catch (Exception Ex)
        {
            LogError.Log("Changepassword.aspx/ValidateToken", Ex.Message, Ex.InnerException, "", Ex.StackTrace, chngpwd, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        }
    
        }

    
    protected void ChngPassword_Click(object sender, EventArgs e)
    {
        string chngpwd = "select * from students where PasswordToken ='" + Request.QueryString["Token"] + "'";
        try
        {
           
            DataTable dt = new DataTable();
            dt = gen_db_utils.gp_sql_get_datatable(chngpwd);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["PasswordExpirationDate"] != null && Convert.ToDateTime(dt.Rows[0]["PasswordExpirationDate"]) >= DateTime.Now)
                {
                    string newPwd = "update students set password='" + txtPassword.Text + "',PasswordToken=null where PasswordToken='" + Request.QueryString["Token"] + "'";
                    gen_db_utils.gp_sql_execute(newPwd);
                    divBody.Visible = false;
                    //divtit.Visible = false;
                    lblMsg.Visible = true;
                    lblMsg.Text = "Your Password has been Changed Successfully. Please procees to " + " <a href='/Login.aspx'>" + " <h4>Login</h4>" + " </a>";
                    lblMsg.Style.Add("color", "green");

                }
                else
                {
                    divBody.Visible = false;
                    //divtit.Visible = false;
                    lblMsg.Visible = true;
                    lblMsg.Text = "<h3>" + "Your Token has been expired" + "<h3>";
                    lblMsg.Style.Add("color", "red");
                }
            }
            else
            {
                divBody.Visible = false;
                // divtit.Visible = false;
                lblMsg.Visible = true;
                lblMsg.Text = "<h3>" + "Your Token has been expired" + "</h3>";
                lblMsg.Style.Add("color", "red");
            }
        }
        catch (Exception Ex)
        {
            LogError.Log("Changepassword.aspx/ChngPassword_Click", Ex.Message, Ex.InnerException, "", Ex.StackTrace, chngpwd, true);
            lblErrormsg.Style.Add("color", "red");
            lblErrormsg.Text = "Unfortunately, there is an error occurred. Getprep Team will get back to you soon";
        } 
    }
}