﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LandingPageMasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
      <style>
          .left {
              text-align: left;
              
          }
        .margin {
           margin-left: 30%;
            
           
            min-height: 400px;
        }
          
    </style>
         <div class="container">
             <div class="login-table rlp-table">
                  <div id="modalTitle" class="registration-title rlp-title">Login to Getprep Academy!</div>
            <div class="login-form bg-w-form rlp-form">
                            <div class="row margin">
                            <div class="col-sm-7">
                                       <div class="panel panel-default" style="background-color: #eee">
                                        <div class="panel-body">
                                            <b>
                                                <asp:Label ID="lblErrormsg" runat="server"></asp:Label></b>
                                            <div class="row">
                                                    <div class="col-sm-12">
                                                        <label for="Email">Email</label>
                                                        <asp:TextBox ID="txtemail" placeholder="Enter Your EmailAddress" ValidationGroup="LoginValidation" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="LoginValidation" ControlToValidate="txtemail" ErrorMessage=" Email is a required field." ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <div class="help-block"></div>
                                                    </div>
                                              
                                            </div>
                                            <div class="row">
                                              
                                                    <div class="col-sm-12">
                                                        <label for="Password">Password</label>
                                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" ValidationGroup="LoginValidation" placeholder="Enter Password " CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="LoginValidation" runat="server" ControlToValidate="txtPassword" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Password field is required."></asp:RequiredFieldValidator>

                                                        <div class="help-block"></div>
                                                    </div>
                                               
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="login-submit">
                                                        <asp:Button ID="login" runat="server" OnClick="login_Click" ValidationGroup="LoginValidation" CssClass="btn btn-login btn-green btn-lg btn-block" Text="Log in" />
                                                                   </div>
                                                    <div class="help-block"></div>
                                                    <br />
                                                    <a href="#" id="btnForgot" onclick="return Forgot(this);"><font color="black"><span>Forget Your Password?</span></font></a>
                                                </div>

                                                <br />

                                            </div>
                                        </div>
                                    </div>
                                     
                                        <span class="pull-right">Not a GetPrep Member?
                                             <asp:Button ID="register" runat="server" OnClick="Register_Click" Text="Register" CssClass="btn-grey" /></span>
                                   
                                    <br />
                                </div>
                            </div>
                            
        </div>
                 </div>

    <div class="modal fade" id="myDivModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <asp:UpdatePanel ID="updpnl1" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                              <asp:Label ID="lblError" runat="server"></asp:Label>                              
                            <h3>Forgot Password</h3>
                           </div>
                                <div class="modal-body">
                                       <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                   <div id="divUsername" class="form-group left" runat="server">
                                  <label for="UserName">UserName :</label>
                                <asp:TextBox ID="txtUserName" placeholder="Enter Your UserName" CssClass="form-control" ValidationGroup="FrogotValidation" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="FrogotValidation" runat="server" ControlToValidate="txtUserName" CssClass="field-validation-error" ForeColor="Red" ErrorMessage="Username is required."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                            
               
                        <div class="modal-footer">

                            <asp:Button ID="btnChangePassword" CssClass="btn btn-login btn-green" runat="server" ValidationGroup="FrogotValidation" Text="Change Password" OnClick="ChangePassword_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
           </div>
    </div>
           
         
    <script>
        function Forgot() {

            $("#myDivModal").modal('show');

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentBodyScripts" runat="Server">
</asp:Content>

